package com.mirth.utility.general;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class RandomLib {

    public static String getRandomDate(int startYear, int endYear) {

        GregorianCalendar gc = new GregorianCalendar();
        int year = randBetween(startYear, endYear);
        gc.set(Calendar.YEAR, year);
        int dayOfYear = randBetween(1, gc.getActualMaximum(Calendar.DAY_OF_YEAR));
        gc.set(Calendar.DAY_OF_YEAR, dayOfYear);
        SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMdd");
        String dateFormatted = fmt.format(gc.getTime());
        return dateFormatted;
        
    }
    
    public static String getRandomDateInFormat(int startYear, int endYear,String dateFormat) {

        GregorianCalendar gc = new GregorianCalendar();
        int year = randBetween(startYear, endYear);
        gc.set(Calendar.YEAR, year);
        int dayOfYear = randBetween(1, gc.getActualMaximum(Calendar.DAY_OF_YEAR));
        gc.set(Calendar.DAY_OF_YEAR, dayOfYear);
        SimpleDateFormat fmt = new SimpleDateFormat(dateFormat);
        String dateFormatted = fmt.format(gc.getTime());
        return dateFormatted;
        
    }

    public static int randBetween(int start, int end) {
        return start + (int)Math.round(Math.random() * (end - start));
    }
    
    public static double randBetween(double start, double end) {
    		
    		start = Math.round( start * 100.0 ) / 100.0;
    		end = Math.round( end * 100.0 ) / 100.0;
   
    		double randValue= start + (double)Math.round(Math.random() * (end - start));
    		
        return (randValue*100.0)/100.0;
    }
    
    public static String getRandomSex() {
       
    		int a = 1 + (int)Math.round(Math.random());
        return (a==1)?"M":"F"; 
        
    }
    
    public static String getRandomSex(String... allValues) {
        
    	int numberOfValues = allValues.length;
    	int valueInt = RandomLib.randBetween(0, numberOfValues-1);
    	
    	return allValues[valueInt];  
    }
    
    public static String getRandomNumString(int numOfDigits) {
    	
    	String num = "";
    	
    	for (int i=0; i< numOfDigits; i++) {
    		
    		num += randBetween(0,9);
    		
    	}
    	
    	return num;    	
    }
    
/* 
    public static void main(String[] args) {
    	System.out.println(getRandomSex("male","female"));
    }
*/
}
