package com.mirth.utility.general;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.mirth.dataObjects.Browser;
import com.mirth.exceptions.UnknownBrowserException;
import com.mirth.properties.ControllerConfig;

public class StaticLib {	
	
	@SuppressWarnings("unchecked")
	public static <T> List<T> cloneList(List<T> orig) throws InstantiationException, IllegalAccessException, ClassNotFoundException{
		
		List<T> newList = (List<T>) Class.forName(orig.getClass().getName()).newInstance();
		
		for (T data: orig){
			newList.add(data);
		}
		
		return newList;
		
	}
	
	public static String appendFolderSeparatorIfNotPresent (String filePath) {
		String fileName ="";
		String separator = "";
		
		if (System.getProperty("os.name").contains("Windows")) {
			
			if (filePath.toCharArray()[filePath.length()-1] != '\\') {
				separator = "\\";
			}
		}
		
		else {
			if (filePath.toCharArray()[filePath.length()-1] != '/') {
				separator = "/";
			}
		}
		
		fileName = filePath + separator;
		return fileName;
	}
	
	
	public static WebElement getObjectLinkText(WebDriver driver, String text) {
		WebElement element = null;
		
		try{
			element = driver.findElement(By.linkText(text));
		} catch (NoSuchElementException nse) {
			System.out.println("Exception in finding the element.");
			throw nse;
		}
		
		return element;
	}

	/*
	public static WebElement getObject(WebDriver driver,String xpKey) {
		WebElement element = null;
		
		try{
			element = driver.findElement(By.xpath(objects.getProperty(xpKey)));
		} catch (NoSuchElementException nse) {
			System.out.println("Exception in finding the element.");
			nse.printStackTrace();
			throw nse;
		}
		
		return element;
	}
*/
	public static WebElement getObjectDirectly(WebDriver driver,String xpKey) {
		
		WebElement element = null;
		
		try{
			element = driver.findElement(By.xpath(xpKey));
		} catch (NoSuchElementException nse) {
			System.out.println("Exception in finding the element for xpath: " + xpKey);
			nse.printStackTrace();
			throw nse;
		}
		
		return element;
	}	
	
	public static String getDateTimeNameInFormat(String prefix,String format) {
		String name = prefix;
		name += TimeLib.getDateTimeInFormat(format); 
		
		return name;
	}
	
	public static String getCustomDateTimeNameInFormat(Calendar cal, String prefix,String format) {
		String name = prefix;
		name += TimeLib.getCustomrDateTimeInFormat(cal,format); 
		
		return name;
	}
	
	public static String getDateTimeName(String prefix) {
		String name = prefix;
		name += TimeLib.getSimpleDateTime(); 
		
		return name;
	}
	
	public static String getTimeName(String prefix) {
		String name = prefix;
		name += TimeLib.getSimpleTime();
	
		return name;
	}
	
	public static Browser getBrowserFromString(String browserString) throws UnknownBrowserException, IOException {
		if (browserString.equalsIgnoreCase(ControllerConfig.Firefox)) {
			return Browser.Firefox;
		}
		
		else if (browserString.equalsIgnoreCase(ControllerConfig.Chrome)) {
			return Browser.Chrome;
		}
			
		else if (browserString.equalsIgnoreCase(ControllerConfig.IE)){
			return Browser.IE;
		}
		
		else if (browserString.equalsIgnoreCase(ControllerConfig.Safari)){
			return Browser.Safari;
		}
		
		else {
			throw new UnknownBrowserException(browserString);
		}
	}//public static Browser getBrowserFromString(String browserString) throws UnknownBrowserException
	
	public static void printMemoryStatistics() {
		Runtime runtime = Runtime.getRuntime();

		NumberFormat format = NumberFormat.getInstance();

		StringBuilder sb = new StringBuilder();
		long maxMemory = runtime.maxMemory();
		long allocatedMemory = runtime.totalMemory();
		long freeMemory = runtime.freeMemory();

		sb.append("free memory: " + format.format(freeMemory / 1024)+ "\n");
		sb.append("allocated memory: " + format.format(allocatedMemory / 1024) + "\n");
		sb.append("max memory: " + format.format(maxMemory / 1024) + "\n");
		sb.append("total free memory: " + format.format((freeMemory + (maxMemory - allocatedMemory)) / 1024) + "\n");
		
		System.out.println(sb);
	}
	
	
	public static String getFrameWrorkFolderWithSeparator() {
		
		String path = null;
		if (System.getProperty("os.name").contains("Windows")) {
			path = StaticLib.appendFolderSeparatorIfNotPresent(ControllerConfig.MirthAutomationSuitePathWin);
		}		
		else {
			path = StaticLib.appendFolderSeparatorIfNotPresent(ControllerConfig.MirthAutomationSuitePathMac);
		}
		
		return path;
	}
	
	public static String getFrameWorkPackageWithSeparator() throws IOException {
		
		String path = null;		
		path = StaticLib.appendFolderSeparatorIfNotPresent(new File(ControllerConfig.FrameWorkPackage).getAbsolutePath());				
		
		
		return path;
	
	}
	
	public static boolean isThisMachineWindows() {
		
		if (System.getProperty("os.name").contains("Windows")) {
			return true;
		}
		
		return false;
	}
	
	public static boolean isThisMachineMac() {
		
		if (System.getProperty("os.name").contains("Mac")) {
			return true;
		}
		
		return false;
	}
	
	
	public static String getStringListValuesAsString(List<String> listData,String separator)  {
		
		String listDataString="";
		
		if (listData==null) {
			return "";
		}
		
		if (separator == null) {
			separator = "";
		}
		
		for (String entry: listData) {
			
			if (!listDataString.equalsIgnoreCase("")) {
				listDataString+=separator;
			}
			
			listDataString += entry;
		}
		
		return listDataString;
		
	}
	
	public static synchronized String getGloballyUniqueStringValue(boolean isUpperCase) {
		
		String guidInRadix36 = new BigInteger(UUID.randomUUID().toString().replace("-", ""),16).toString(36);
		return isUpperCase?guidInRadix36.toUpperCase():guidInRadix36.toLowerCase(); 
		
	}
	
	public static synchronized String getGloballyUniqueEmailValue(String domain) {		
		String guidInRadix36 = new BigInteger(UUID.randomUUID().toString().replace("-", ""),16).toString(36);
		return guidInRadix36+"@"+domain+".com";		
	}

	public static void appendToMap(Map<String,String> fromMap,Map<String,String> toMap) {
		
		if (toMap == null) {
			throw new IllegalArgumentException("Destination is Null. Cannot append!!");
		}
		
		if (fromMap == null) {
			return;
		}
		
		for(String item: fromMap.keySet()) {
			toMap.put(item, fromMap.get(item));
		}
		
	}
	
	public static void appendOnlyDataToMap(Map<String,String[]> fromMap,Map<String,String> toMap) {
		
		if (toMap == null) {
			throw new IllegalArgumentException("Destination is Null. Cannot append!!");
		}
		
		if (fromMap == null) {
			return;
		}
		
		for(String item: fromMap.keySet()) {
			
			
			toMap.put(item, fromMap.get(item)[1]);
		}		
	}
	
	/*Converters*/	
	@SuppressWarnings("unchecked")
	public static <Y,Z> Map<Y,Z> convertToMap(Object object, Class<Y> keyFormat ,Class<Z> valueFormat){
		
		if (object!=null) {
			if (object instanceof Map) {
				return (Map<Y,Z>) object; 
			}
		}
		
		return null;
				
	}
	
	@SuppressWarnings("unchecked")
	public static <Y> Y convertToType(Object currentObj, Class<Y> newObj) {	
			return ( (Y)currentObj );	
	}
	
	@SuppressWarnings("unchecked")
	public static <Y,Z> Map<Y,Z> getNewMapCopy(final Map<Y,Z> originalMap) throws InstantiationException, IllegalAccessException{
		
		final Map<Y,Z> newMap = (Map<Y,Z>)originalMap.getClass().newInstance();
		newMap.putAll(originalMap);
		
		return newMap;
		
	}

	//Converters end	
	public static String getValueAfterLastIndex(String sourceString,char character){
		return sourceString.substring(sourceString.lastIndexOf(character) + 1);
	}
	
	public static boolean isContainsSameValues(List<String> listA, List<String> listB) {
		
		if (listA == null || listB == null) {
			return false;
		}
		
		if(listA.size()!=listB.size()) {
			return false;
		}
		
		for (String stringA: listA) {
			if (!listB.contains(stringA)){
				return false;
			}
		}//for		
		
		return true;		
	}
	
 	public static String getPreviousCanonicalOrigin(int num) {
 		return Thread.currentThread().getStackTrace()[num+1].getClassName();
 	}
 	
 	public static String getUSPhoneFormat(String numberString) {
 		if (numberString == null || numberString.length()<3) {
 			return "(" +numberString +")";
 		}
 		
 		else if (numberString.length()<=6) {
 			return "("+ numberString.substring(0, 3) +")"+ numberString.substring(3);
 		}
 		
 		return "("+ numberString.substring(0, 3) +") "+ numberString.substring(3,6) + "-"+ numberString.substring(6);
 	}
 	
 	public static String getUSPostalFormat(String numberString) {
 		if (numberString == null || numberString.length()<=5) {
 			return numberString;
 		}
 		
 		return numberString.substring(0, 5) +"-"+ numberString.substring(5);
 	}
 	
 	public static String getExceptionAsString(Exception e){
 		StringWriter sw = new StringWriter();
 	 	e.printStackTrace(new PrintWriter(sw));
 	 	return sw.toString();	
 	}
 	
 	public static Certificate createCertificateObjectFromPem(String pemFilePath) throws CertificateException, FileNotFoundException{
 		CertificateFactory cf = CertificateFactory.getInstance("X.509");
 		return  cf.generateCertificate(new FileInputStream(pemFilePath));
 	}
 	
 	
 	public static String getPEMFingerprint(String pemFilename) throws CertificateException, FileNotFoundException{
 		
 		Certificate cert = createCertificateObjectFromPem(pemFilename);
 		return getFingerprint(cert, "MD5");
 	}
 
	public static String getPEMFingerprintWithOutSpaces(String pemFilename) throws CertificateException, FileNotFoundException{
 		
		return getPEMFingerprint(pemFilename).replace(" ", "");
 	}
 
 	
 	public static String getFingerprint(Certificate cert,String type){
 		  try {
 		    MessageDigest md=MessageDigest.getInstance(type);
 		    byte[] publicKey=md.digest(cert.getEncoded());
 		    StringBuffer hexString=new StringBuffer();
 		    for (int i=0; i < publicKey.length; i++) {
 		      String appendString=Integer.toHexString(0xFF & publicKey[i]);
 		      if (appendString.length() == 1)       hexString.append("0");
 		      hexString.append(appendString);
 		      hexString.append(' ');
 		    } 
 		    return hexString.toString();
 		  }
 		 catch (  Exception e1) {
 		    e1.printStackTrace();
 		    return null;
 		  }
 	} 	 	
 	
 	public static float parseFloatTo2Decimals(float val){       
 	     DecimalFormat twoDForm = new DecimalFormat("#.##");
 	     return Float.valueOf(twoDForm.format(val));
 	}
 	    
 	
}
