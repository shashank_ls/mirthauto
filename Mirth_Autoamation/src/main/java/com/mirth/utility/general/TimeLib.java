package com.mirth.utility.general;

import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class TimeLib {

	public static String getDateTimeInFormat(String format) {
		
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		return sdf.format(cal.getTime());
	}
	
	public static String getCustomrDateTimeInFormat(Calendar cal, String format) {
		
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		return sdf.format(cal.getTime());
	}
	
	
	public static String getSimpleDateTime() {
		
		return getDateTimeInFormat("yyMMddHHmmss");
		
	}
	
	
	public static String getSimpleDate() {
		
		Calendar cal = Calendar.getInstance();
		cal.getTime();
		SimpleDateFormat sdf = new SimpleDateFormat("MMddyy");
		return sdf.format(cal.getTime()) ;
		
	}
	
	
	
	public static String getSimpleTime() {
		
		Calendar cal = Calendar.getInstance();
		cal.getTime();
		SimpleDateFormat sdf = new SimpleDateFormat("HHmmss");
		return sdf.format(cal.getTime()) ;
		
	}
	
	public static String getDateTimeInHex() {
		
		return Long.toHexString(Calendar.getInstance().getTime().getTime());
		
	}
	
	//Radix36 Implementations
	
	public static String getTimeNameRadix36(String prefix) {
		String name = prefix;
		String radix36Time = new BigInteger(getSimpleTime(),10).toString(36);
		
		name += new StringBuffer(radix36Time).reverse(); 
		
		return name;
	}
	
	public static String getTimeInRadix36() {
		return getTimeNameRadix36("");
	}
	
	public static String getFullTimeNameRadix36(String prefix) {
		String name = prefix;
		String radix36Time = new BigInteger(getDateTimeInFormat("yyyymmddhhmmssSSS"),10).toString(36);
		
		name += new StringBuffer(radix36Time).reverse(); 
		
		return name;
	}
	
	public static String getFullTimeInRadix36() {
		return getFullTimeNameRadix36("");
	}
	
	public static String getDateTimeNameRadix36(String prefix) {
		String name = prefix;
		String radix36DateTime = new BigInteger(getSimpleDateTime(),10).toString(36);
		
		name += new StringBuffer(radix36DateTime).reverse().toString(); 
		
		return name;
	}
	
	public static String getDateTimeInRadix36() {
		return getDateTimeNameRadix36("");
	}
	
	public static String getFullDateTimeNameRadix36(String prefix) {
		String name = prefix;
		String radix36DateTime = new BigInteger(getDateTimeInFormat("yyyymmddhhmmssSSS"),10).toString(36);
		
		name += new StringBuffer(radix36DateTime).reverse().toString(); 
		
		return name;
	}
	
	public static String getFullDateTimeInRadix36(){
		return getFullDateTimeNameRadix36("");
	}
	
	//Radix36 Functions End
	
}
