package com.mirth.utility.general;

public class StringLib {

	public static String arrayToString(String[] strArray) {
		
		String str = "";
		for (int i=0;i<strArray.length;i++){
			if (i!=0){
				str+="^";
			}
			
			if (strArray != null) {
				str+=strArray[i];
			}
			
		}
		for (String s: strArray) {
			if (s==null){
				str+= "";
			}//for
		}
		
		return str;
		
	}
	
	public static String twoDimensionArrayToString(String[][] strArray) {
		
		String str ="";
		
		for (int i=0; i< strArray.length; i++)
		{
			for (int j=0; j<strArray[i].length;j++) {
				
				if (j!=0) {str+= "|";}
				if (strArray[i][j] != null) {
					str+= strArray[i][j];
				}
			}
			str+= "\n";
		}
		
		return str;
	}
	
	public static String getNotNullString(String str,String nullString) {
		
		if (str==null) {
			return nullString;
		}
		
		return str;
	}
	
}
