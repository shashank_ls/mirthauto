package com.mirth.utility.general;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

public class TextFile {
	
	public static String readFileAsString(String file) throws IOException {

		File f = new File(file);
        FileInputStream inp = new FileInputStream(f);
        byte[] bf = new byte[(int)f.length()];
        inp.read(bf);
        inp.close();
        
        return new String(bf, "UTF-8");
	}
	
	
}
