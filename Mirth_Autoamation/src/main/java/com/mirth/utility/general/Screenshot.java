package com.mirth.utility.general;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.commons.io.IOUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

public class Screenshot {
	
	/*
	public static void takeScreenshot (WebDriver driver, String destination) throws IOException {
		
		byte[] imageBytes = ((TakesScreenshot)driver).getScreenshotAs(OutputType.BYTES);
		IOUtils.write(imageBytes, new FileOutputStream(new File(destination).getAbsolutePath()));
	
	}	
	
	public static void takeScreenshot (WebDriver driver, File destination) throws IOException {
		
		byte[] imageBytes = ((TakesScreenshot)driver).getScreenshotAs(OutputType.BYTES);
		IOUtils.write(imageBytes, new FileOutputStream(destination.getAbsolutePath()));
	}	
	*/
	
	public synchronized static byte[] getScreenshotInBytes (WebDriver driver) throws IOException {

		bringWindowToFront(driver);
		return ((TakesScreenshot)driver).getScreenshotAs(OutputType.BYTES);
	}

	public synchronized static String getScreenshotInBase64 (WebDriver driver) throws IOException {	

		bringWindowToFront(driver);
		return ((TakesScreenshot)driver).getScreenshotAs(OutputType.BASE64);
	}
	
	public synchronized static File getScreenshotInFile (WebDriver driver) throws IOException {		

		bringWindowToFront(driver);
		return ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
	}
	
	public  static void createFileFromByteArray(byte[] imageBytes, File file) throws IOException {
		
		 IOUtils.write(imageBytes, new FileOutputStream(file.getAbsolutePath()));
		
	}
	
	public static void bringWindowToFront(WebDriver driver) {
		
		
	}

	
}//class
