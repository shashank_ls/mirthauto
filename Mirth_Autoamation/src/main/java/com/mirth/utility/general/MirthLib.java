package com.mirth.utility.general;

import java.io.IOException;

import com.mirth.controller.ThreadController;
import com.mirth.dataObjects.ControllerData;
import com.mirth.exceptions.AppNotPresentException;
import com.mirth.properties.ApplicationConfig;
import com.mirth.properties.ControllerConfig;

public class MirthLib {

 	public static String getAppNameForCanonical(String  canonicalOrigin) throws IOException, AppNotPresentException {
 		
 		//Stack trace will have previous calling class in index 2. [0 - Thread, 1 - current class]
 		String appName = "";
 		
 		if (canonicalOrigin.startsWith("com.mirth.application.version2.mso")) {
 			appName = ApplicationConfig.getValue("MSO");
 		}
 		else if (canonicalOrigin.startsWith("com.mirth.application.version2.results")) {
 			appName = ApplicationConfig.getValue("Results");
 		} 
 		else if (canonicalOrigin.startsWith("com.mirth.application.version2.match")) {
 			appName = ApplicationConfig.getValue("Match");
 		}
 		
 		else {
 			throw new AppNotPresentException(canonicalOrigin);
 		}
 		
 		return appName;
 	}  	
 	
 	public static String getServerNameOrIP() {
 		return ControllerData.getAppDataMap()
		 .get(ThreadController.getInventoryOfThread(Thread.currentThread().getId()).appName)
		 .getLocalApplicationAttributesMap().get(ControllerConfig.TestServerSH) ;
 	}
 	
 	
}
