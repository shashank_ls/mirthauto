package com.mirth.utility.general;

public class SleepLib {

	//-----------Sleep Functions-------------------//
	public static void sleepMilliSeconds(int milliSeconds) throws InterruptedException {
		Thread.sleep(milliSeconds);
	}
	
	public static void sleepSeconds(int seconds) throws InterruptedException {
		sleepMilliSeconds(seconds * 1000);
	}
	
	public static void sleep1Second() throws InterruptedException {
		sleepSeconds(1);
	}
	
	public static void sleep2Seconds() throws InterruptedException {
		sleepSeconds(2);
	}	
	
	public static void sleep3Seconds() throws InterruptedException {
		sleepSeconds(3);
	}	
	
	public static void sleep4Seconds() throws InterruptedException {
		sleepSeconds(4);
	}	
	
	public static void sleep5Seconds() throws InterruptedException {
		sleepSeconds(5);
	}	
	
	public static void sleep10Seconds() throws InterruptedException {
		sleepSeconds(10);
	}	
	
	public static void sleepMinutes(int minutes) throws InterruptedException {
		sleepSeconds(60 * minutes);
	}
	
	public static void sleep1Minute() throws InterruptedException {
		sleepMinutes(1);
	}
	
	public static void sleep2Minutes() throws InterruptedException {
		sleepMinutes(2);
	}
	
	public static void sleep3Minutes() throws InterruptedException {
		sleepMinutes(3);
	}
	
	public static void sleep4Minutes() throws InterruptedException {
		sleepMinutes(4);
	}
	
	public static void sleep5Minutes() throws InterruptedException {
		sleepMinutes(5);
	}
	
	public static void sleep10Minutes() throws InterruptedException {
		sleepMinutes(10);
	}
	//-----------Sleep Functions-------------------//	
	
}
