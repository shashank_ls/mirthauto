package com.mirth.utility.general;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;

public class TCPSender {

	public static synchronized void sendMessageTCP(String url, int port , String message) throws IOException, InterruptedException {
		Socket socket = null;
        PrintWriter out = null;
        
        //Check if TCP listner present in specified url and port.
        socket = new Socket(url, port);
        out = new PrintWriter(socket.getOutputStream(), true);

        out.print(message);
       // System.out.println("TCP message sent succesfully.");
        
        out.close();
        socket.close();

        Thread.sleep(500);

	}
	/*
	public static void main(String[] args) throws IOException {
		
		String hl7Message = UpdateHL7.update("/users/shashanks/Documents/000_Drive2/MRC Mapping/hl7.txt", "pe", "measureName", "value", "codeSet", "code");
		System.out.println(hl7Message);
		TCPSender.sendMessageTCP("caretest.dev.mirthcorp.com", 6661, hl7Message);
		
	}
	*/
}
