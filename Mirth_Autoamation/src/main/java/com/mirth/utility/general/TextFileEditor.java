package com.mirth.utility.general;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;


public class TextFileEditor {
	
	private File textFile = null;
	
	public TextFileEditor(String filePath){
		this.textFile = new File(filePath);
	}
	
	public TextFileEditor(File file){
		this.textFile = file;
	}
	
	public String getFilePathAsString(){
		return textFile.getAbsolutePath();
	}
	
	public boolean isExists(){
		return textFile.exists();
	}
	
	public boolean createFileIfNotExist(String content) throws IOException{
		
		if (!isExists()){
			BufferedWriter fileOut = new BufferedWriter(new FileWriter(textFile));
			fileOut.write(StringLib.getNotNullString(content, ""));
			fileOut.close();
			return true;
		}
		return false;				
	}
	
	public boolean appendString(String content) throws IOException{
		
		FileWriter fileOut = new FileWriter(textFile);
		
		if (!isExists()){
			fileOut.write(StringLib.getNotNullString(content, ""));
			fileOut.flush();
		}
		
		fileOut.append(content);
		fileOut.close();
		
		if (textFile.exists()){
			return true;
		}
				
		return false;	
		
	}
	
	public String getFileContentAsString() throws IOException{
		return new String(Files.readAllBytes(Paths.get(textFile.getAbsolutePath())), StandardCharsets.UTF_8);
	}
	
	public void replaceString(String regex,String replacement) throws IOException{		
		String fileContent = getFileContentAsString();
		fileContent = fileContent.replaceAll(regex, replacement);
		Files.write(Paths.get(textFile.getAbsolutePath()), fileContent.getBytes(StandardCharsets.UTF_8));		
	}
	
	public void copyFile(File newFile,boolean isReplaceIfExist) throws IOException{
		
		if (isReplaceIfExist){
			Files.copy(Paths.get(textFile.getAbsolutePath()), Paths.get(newFile.getAbsolutePath()),StandardCopyOption.REPLACE_EXISTING);
		}
		else {
			Files.copy(Paths.get(textFile.getAbsolutePath()), Paths.get(newFile.getAbsolutePath()));
		}		
	}
	
	
}
