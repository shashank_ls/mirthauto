package com.mirth.utility.seleniumLib;


public class TextBoxS extends SeleniumElement{
	
	public static final Setter setter = Setter.textBox;
		
	public TextBoxS(String data){		
		super(data);
	}
	
	@Override
	public Setter getSetter(){
		return setter;
	}
	

}
