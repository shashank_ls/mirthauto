package com.mirth.utility.seleniumLib;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;

public class MirthDriver {

	private WebDriver driver = null;
	private MirthWait waitTime = new MirthWait(1,TimeUnit.SECONDS);

	public MirthDriver(WebDriver driverVal){
		setDriver(driverVal);
	}	
	
	public WebDriver getDriver(){
		return driver;
	}
	
	public void setDriver(WebDriver driverVal){
		this.driver = driverVal;
	}
	
	public MirthWait getWaitTime(){
		return waitTime;
	}
	
	public void setWaitTime(long tOut, TimeUnit tUnit){
		waitTime.setTimeOutDetails(tOut, tUnit);
		driver.manage().timeouts().implicitlyWait(tOut, tUnit);
	}	
	
}
