package com.mirth.utility.seleniumLib;

import java.util.concurrent.TimeUnit;

public class MirthWait {
	private long timeOut = 0;
	private TimeUnit timeUnit = TimeUnit.SECONDS;
	
	public MirthWait(long tOut,TimeUnit tUnit){
		setTimeOutDetails(tOut, tUnit);
	}
	
	public long getTimeOut(){
		return timeOut;
	}
	
	public TimeUnit getTimeUnit(){
		return timeUnit;
	}
	
	public void setTimeOut(long tOut){
		timeOut = tOut;
	}
	
	public void setTimeUnit(TimeUnit tUnit){
		timeUnit = tUnit;
	}
	
	public void setTimeOutDetails(long tOut,TimeUnit tUnit){
		setTimeOut(tOut);
		setTimeUnit(tUnit);
	}
	
}
