package com.mirth.utility.seleniumLib;


public class ButtonS extends SeleniumElement{
	
	public static final Setter setter = Setter.button;
			
	public ButtonS(String data){
		super(data);
	}
	
	@Override
	public Setter getSetter(){
		return setter;
	}
	
}
