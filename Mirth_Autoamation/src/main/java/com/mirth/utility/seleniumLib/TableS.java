package com.mirth.utility.seleniumLib;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;

import com.mirth.application.master.General;
import com.mirth.controller.Interact;
import com.mirth.controller.objects.ORMaster;
import com.mirth.controller.objects.SeleniumLocator;
import com.mirth.exceptions.AppNotPresentException;
import com.mirth.exceptions.ClassPageMappingNotFoundException;
import com.mirth.exceptions.ColumnNotFoundInTableForHeading;
import com.mirth.exceptions.NoSuchCellInTableException;
import com.mirth.exceptions.NotATableElementException;

public class TableS extends SeleniumElement{
	
	
	public static Setter setter = Setter.htmlTable;

	private boolean isProcessed = false;
	private WebElement table = null;
	private Map<Integer,List<String>> tableData;
	
	@Override
	public Setter getSetter(){
		return setter;
	}
	
	public String getEntireTable() {
		if (tableData!=null) {
			return tableData.toString();
		}
		return null;
	}
	
	public TableS(String text){
		super(text);
	}
	
	public void processTable() throws IOException, ClassPageMappingNotFoundException, AppNotPresentException, Exception {
		if (!isProcessed){
			forceProcessTable();
			isProcessed=true;
		}		
	}
	
	public void forceProcessTable() throws IOException, ClassPageMappingNotFoundException, AppNotPresentException, Exception {
		
		this.table = this.interact().webElement();
		
		if (table == null) {
			throw new NotATableElementException(table);
		}	
		
		tableData = new HashMap<Integer,List<String>>();
		this.parseTable();
		isProcessed=true;
	}
	
	public Interact getHeadingCellTDElement(String heading) throws Exception {		
		return getHeadingCellTDElement(this.getColumnNumForHeader(heading));	
	}
	
	public Interact getHeadingCellTDElement(int colNum) throws Exception {
		
		if (colNum > getColumnsCount() || colNum<1){
			throw new NoSuchCellInTableException(0,colNum);
		}
		Interact interact = null; 
		
		try{	
		interact = new Interact(ORMaster.getObject(table, SeleniumLocator.XPath, "thead/tr/th[" + colNum + "]"));
		}catch(StaleElementReferenceException e){
			this.forceProcessTable();
			interact = new Interact(ORMaster.getObject(table, SeleniumLocator.XPath, "thead/tr/th[" + colNum + "]"));
		}
		return interact;		
		
	}
	
	public Interact getCellTDElement(int row, int col) throws Exception{
		
		if (row > getDataRowsCount() || col>getColumnsCount() || row<1 || col < 1) {
			throw new NoSuchCellInTableException(row,col);
		}
		Interact interact = null; 

		try{	
		interact = new Interact(ORMaster.getObject(table, SeleniumLocator.XPath, "tbody/tr[" + row + "]/td[" + col + "]"));
		}catch(StaleElementReferenceException e){
			this.forceProcessTable();
			interact = new Interact(ORMaster.getObject(table, SeleniumLocator.XPath, "tbody/tr[" + row + "]/td[" + col + "]"));
		}
		return interact;		
	}

	public Interact getCellTDElement(String heading,int row) throws IOException, ClassPageMappingNotFoundException, AppNotPresentException, Exception{
		int col = getColumnNumForHeader(heading);
		return getCellTDElement(row,col);
		
	}
		
	/* Enable and enhance this when in need. - Not tested
	public Interact getCellSpecificElement(int row, int col,String fullXPathRelativeToTable) throws NoSuchCellInTableException, ColumnNotFoundInTableForHeading{
		
		if (row > getDataRowsCount() || col>getColumnsCount() || row<1 || col < 1) {
			throw new NoSuchCellInTableException(row,col);
		}
		
		return new Interact(ORMaster.getObject(table, SeleniumLocator.XPath, fullXPathRelativeToTable));
		
	}
	
	public Interact getCellSpecificElement(String heading,int row,String fullXPathRelativeToTable) throws NoSuchCellInTableException, ColumnNotFoundInTableForHeading{
		int col = getColumnNumForHeader(heading);
		return getCellSpecificElement(row,col,fullXPathRelativeToTable);
		
	}
	*/
	
	private void parseTable() {
		System.out.println("--------\n"+table.getText()+"\n-------");
		MirthWait currentWait = General.getBrowserWaitTime();
		long tOut = currentWait.getTimeOut();
		TimeUnit tUnit = currentWait.getTimeUnit();
		General.setBrowserWaitTime(0,TimeUnit.MILLISECONDS);
		
		//Read Headers if present
		List<WebElement> rowElement = table.findElements(By.xpath("thead/tr/th"));
		tableData.put(0,getStringListFromElementList(rowElement,true));

		//Read table data rows.
		List<WebElement> dataRowElements =  table.findElements(By.xpath("tbody/tr"));
		for (int i=0; i<dataRowElements.size();i++){
			rowElement = null;
			rowElement = dataRowElements.get(i).findElements(By.xpath("td"));
			tableData.put(i+1, getStringListFromElementList(rowElement,false));
		}	
		
		General.setBrowserWaitTime(tOut,tUnit);	
		
	}
	
	public boolean isTableEmpty(){
		
		MirthWait currentBrowserWaitTime = General.getBrowserWaitTime();
		long timeOut = currentBrowserWaitTime.getTimeOut();
		TimeUnit timeUnt = currentBrowserWaitTime.getTimeUnit();
		
		General.setBrowserWaitTime(0, TimeUnit.SECONDS);
		List<WebElement> tdList = table.findElements(By.xpath("tbody/tr[@class='empty']"));		
		General.setBrowserWaitTime(timeOut,timeUnt);		
		
		if (tdList.size()>=1){
			return true;
		}
		
		return false;
	}
	
	private List<String> getStringListFromElementList(List<WebElement> webElements,boolean isHeading) {
	
		if (webElements!=null) {			
			
			List<String> stringList = new ArrayList<String>();
			int unNamedNum =1;
			String unNamedName = "UnNamed_"; 
						
			for (int i=0;i<webElements.size();i++) {
				
				String text = webElements.get(i).getText();
				if (text ==null || text.equalsIgnoreCase("")){
					
					if (isHeading){
						text = unNamedName + (unNamedNum++) ;
					}
					
					else {
						text = "";
					}					
				}//if (text ==null || text.equalsIgnoreCase("")){
				
				stringList.add(text);
			}//for (int i=0;i<webElements.size();i++) 
			
			return stringList;		
		
		}//if (webElements!=null) {
		
		return null;
	}//private List<String> getStringListFromElementList(List<WebElement> webElements,boolean isHeading)
	
	
	public List<String> getHeadingsAsList() {
		return tableData.get(0);
	}
	
	//here row num starts from 1
	public List<String> MultiSelectSeparator(int rowNum) {
		return tableData.get(rowNum);
	}
	
	public int getColumnNumForHeader(String heading) throws ColumnNotFoundInTableForHeading, IOException {
		List<String> headingList = tableData.get(0);
		int columnNum = 0;
		boolean foundColumn = false;
		
		columnLoop:
		for (int i=0;i<headingList.size();i++) {
			if (headingList.get(i).equalsIgnoreCase(heading)){
				columnNum = i+1;
				foundColumn = true;
				break columnLoop;
			}
		}
		
		if (!foundColumn) {
			throw new ColumnNotFoundInTableForHeading("Heading: '"+ heading +"' NOT Found!!");
		}
		
		return columnNum;	
			
	}
	
	public String getValueFromTable(String heading,int rowNum) throws ColumnNotFoundInTableForHeading, IOException {
		
		int columnNum = getColumnNumForHeader(heading);
		return tableData.get(rowNum).get(columnNum-1);
		
	}
	

	public String getValueFromTable(int rowNum, int colNum) {
		return tableData.get(rowNum).get(colNum-1);
	}
	
	public List<String> getAllColumnValuesAsListForHeader(String heading) throws ColumnNotFoundInTableForHeading, IOException {
		
		
		List<String> stringList = new ArrayList<String>();
		int columnNum = getColumnNumForHeader(heading);
		
		//deliberately not starting from 0 in order to ignore header itself
		for (int i=1;i<tableData.size();i++) {
			stringList.add(getValueFromTable(i,columnNum));
		}
		
		return stringList;
		
	}
	
	public String getHeaderForColumn(int colNum) {
		return tableData.get(0).get(colNum-1);
	}
	
	public int getDataRowsCount() {
		return (tableData.size() - 1); //-1 is to remove heading data from the count
	}
	
	public int getColumnsCount() {
		return (tableData.get(0).size());
	}
	
	public void clickRow(String heading,int row) throws Exception{
		this.getCellTDElement(heading,row).click();		
	}
	
	public void clickFirstRowOfUsersTable(String heading) throws Exception{
		this.clickRow(heading,1);
	}
	
	
	public int findRowHavingText(String text,String heading,boolean isCaseSensitive) throws ColumnNotFoundInTableForHeading, IOException{
		return findRowHavingText(text, heading, isCaseSensitive,false);
	}
	
	//search functions
	public int findRowHavingText(String text,String heading,boolean isCaseSensitive,boolean ignoreNotFound) throws ColumnNotFoundInTableForHeading, IOException{
			
		boolean isRowFound = false;

		for (int row=1;row<=getDataRowsCount();row++){			
			
			if (isCaseSensitive){
				if (text.equals(getValueFromTable(heading, row))){
					isRowFound = true;
				}
			}//if (isCaseSensitive)
			else {
				if (text.equals(getValueFromTable(heading, row))){
					isRowFound = true;
				}
			}//else
			
			if(isRowFound){
				return row;
			}//if(isRowFound)
			
		}//for (int row=1;row<=getDataRowsCount();row++)
		
		if (ignoreNotFound){
			return -1;
		}
		
		else{
			throw new IllegalArgumentException("Given text: " + text+" is NOT present in the Identifier columun of the table");
		}
		
		
				
	}


}//class
