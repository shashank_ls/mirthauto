package com.mirth.utility.seleniumLib;


public class LinkS extends SeleniumElement{
	
	public static final Setter setter = Setter.link;
		
	public LinkS(String data){
		super(data);
	}
	
	
	@Override
	public Setter getSetter(){
		return setter;
	}
	

}
