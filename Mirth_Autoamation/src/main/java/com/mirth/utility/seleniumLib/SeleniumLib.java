package com.mirth.utility.seleniumLib;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;

import com.mirth.application.master.General;
import com.mirth.utility.general.SleepLib;


public class SeleniumLib {

	public static boolean waitForVisiblity(SeleniumElement selElement) throws Exception {
		
		return selElement.interact().isDisplayed();
		
	}

	public static void highlightElement(WebElement element) {
		for (int i = 0; i < 2; i++) {
			JavascriptExecutor js = (JavascriptExecutor) General.browser();
			js.executeScript(
					"arguments[0].setAttribute('style', arguments[1]);",
					element, "color: red; border: 3px solid red;");
			js.executeScript(
					"arguments[0].setAttribute('style', arguments[1]);",
					element, "");
		}
	}
	
	public static void highlightElementForSec(WebElement element,long iterations) throws InterruptedException{
		for(int i =0; i <iterations;i++){
			SeleniumLib.highlightElement(element);
			SleepLib.sleepMilliSeconds(200);
		}
	}
	  
	
	
}
