package com.mirth.utility.seleniumLib;


public class RadioButtonS extends SeleniumElement {
	
	public static final Setter setter = Setter.radioButton;

	
	public RadioButtonS(String data){
		super(data);
	}
	
	@Override
	public Setter getSetter(){
		return setter;
	}
	

}
