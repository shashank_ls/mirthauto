package com.mirth.utility.seleniumLib;


public class SimpleDropDownS extends SeleniumElement {
	
	public static final Setter setter = Setter.singleSelectDropdownText;
		
	
	public SimpleDropDownS(String data){
		super(data);
	}
	
	@Override
	public Setter getSetter(){
		return setter;
	}
	


}
