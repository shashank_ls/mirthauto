package com.mirth.utility.seleniumLib;



public class CheckBoxS extends SeleniumElement {
	
	public static final Setter setter = Setter.checkBox;
	
	public CheckBoxS(String data){
		super(data);
	}
		
	@Override
	public Setter getSetter(){
		return setter;
	}
	


}
