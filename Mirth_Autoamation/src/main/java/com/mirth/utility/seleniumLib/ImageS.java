package com.mirth.utility.seleniumLib;


public class ImageS extends SeleniumElement{
	
	public static final Setter setter = Setter.image;
		
	public ImageS(String data) {
		super(data);
	}
	
	
	@Override
	public Setter getSetter(){
		return setter;
	}
	


}
