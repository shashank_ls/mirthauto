package com.mirth.utility.seleniumLib;

import java.io.IOException;

import com.mirth.application.master.PageObject;
import com.mirth.controller.Interact;
import com.mirth.exceptions.AppNotPresentException;
import com.mirth.exceptions.ClassPageMappingNotFoundException;
import com.mirth.utility.general.MirthLib;
import com.mirth.utility.general.StaticLib;

public abstract class SeleniumElement {
	
	private String value = null;
	private Class<? extends PageObject> pageClass = null;
	private String appName = null;
	
	public SeleniumElement(String elementValue){
		
	
			try {
				this.createElement(elementValue);
			} catch (ClassNotFoundException | IOException | AppNotPresentException e) {
				System.out.println("Issue with initializing SeleniumElement: " + elementValue + ". Terminating Program");
				e.printStackTrace();
				
				System.exit(1);	
			}

				
	}
	
	public String get(){
		return value;
	}
	
	public String toString(){
		return this.get();
	}
	
	public Class<? extends PageObject> getPageClass(){
		return this.pageClass;
	}
	
	public String getAppName(){
		return this.appName;
	}
	
	@SuppressWarnings("unchecked")
	private final void createElement(String elementValue) throws ClassNotFoundException, IOException, AppNotPresentException{
				
		String canonicalOrigin = StaticLib.getPreviousCanonicalOrigin(4);
		
		this.value = elementValue;
		this.pageClass = ( (Class<? extends PageObject>) Class.forName(canonicalOrigin) );
		this.appName = MirthLib.getAppNameForCanonical(canonicalOrigin);	
	}
	
	public final Interact interact() throws IOException, ClassPageMappingNotFoundException, AppNotPresentException, Exception {
		return PageObject.interactWithFromOutsidePage(appName,PageObject.getPageNameOfClass(pageClass),value);
	}
	
	public abstract Setter getSetter();
		
}
