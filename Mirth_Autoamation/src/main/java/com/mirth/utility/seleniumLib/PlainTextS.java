package com.mirth.utility.seleniumLib;


public class PlainTextS extends SeleniumElement {
	
	public static final Setter setter = Setter.plainText;
	
	public PlainTextS(String data){
		super(data);
	}
	
	@Override
	public Setter getSetter(){
		return setter;
	}
	

}
