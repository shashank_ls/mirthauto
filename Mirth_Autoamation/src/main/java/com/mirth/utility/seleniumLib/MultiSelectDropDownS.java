package com.mirth.utility.seleniumLib;


public class MultiSelectDropDownS extends SeleniumElement {
	
	public static final Setter setter = Setter.multiSelectDropdownText;	
	
	public MultiSelectDropDownS(String data){
		super(data);
	}
	
	
	@Override
	public Setter getSetter(){
		return setter;
	}
	

}
	
