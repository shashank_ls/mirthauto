package com.mirth.utility.microsoftDocX;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.xwpf.usermodel.Document;
import org.apache.poi.xwpf.usermodel.ParagraphAlignment;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;


public class CreateImageDoc {
	CustomXWPFDocument document = null;
	
	public CreateImageDoc (String[] images,String docFilePath,String startingText,String endText) throws FileNotFoundException {
        document = new CustomXWPFDocument();
		
     // Createa a para -1
        XWPFParagraph paragraphOne = document.createParagraph();
        paragraphOne.setAlignment(ParagraphAlignment.LEFT);
        XWPFRun paragraphOneRunOne = paragraphOne.createRun();
        paragraphOneRunOne.setFontSize(8);
        //paragraphOneRunOne.setFontFamily("Verdana");
        //paragraphOneRunOne.setColor("000070");
        paragraphOneRunOne.setText(startingText);
        
        {//Block Code
				for (int i=0;i<images.length;i++) {
						// Adding a file
				        try {			        
					        // Working addPicture Code below...
					        XWPFParagraph paragraphX = document.createParagraph();
					        paragraphX.setAlignment(ParagraphAlignment.CENTER);
					
					        String blipId = paragraphX.getDocument().addPictureData(
					                new FileInputStream(new File(images[i])),
					                Document.PICTURE_TYPE_JPEG);
					       // System.out.println(document
					     //           .getNextPicNameNumber(Document.PICTURE_TYPE_JPEG));
					        document.createPicture(blipId,
					                document.getNextPicNameNumber(Document.PICTURE_TYPE_JPEG),
					                570, 338);
				        } catch (InvalidFormatException e1) {
				        // TODO Auto-generated catch block
				        e1.printStackTrace();
				        }
				}

        }
        
        
        XWPFParagraph paragraphTwo = document.createParagraph();
        paragraphTwo.setAlignment(ParagraphAlignment.LEFT);
        XWPFRun paragraphTwoRunOne = paragraphTwo.createRun();
        paragraphTwoRunOne.setFontSize(8);
        paragraphTwoRunOne.setText(endText);
        
        
        FileOutputStream outStream = null;
        try {
            String fileName = docFilePath;
            outStream = new FileOutputStream(fileName);
        } catch (FileNotFoundException e) {
            System.out.println("First Catch");
            e.printStackTrace();
        }
        
        try {
            document.write(outStream);
            outStream.close();
        } catch (FileNotFoundException e) {
            System.out.println("Second Catch");
            e.printStackTrace();
        } catch (IOException e) {
            System.out.println("Third Catch");
            e.printStackTrace();
        }
        
		
	}

	/*
	public static void main(String[] args) throws FileNotFoundException {
		
		String folderName = "/users/shashanks/Documents/000_Drive2/MRC Mapping/Screenshots/";
		
		String[] a = {folderName+"1.png",folderName+"2.png",folderName+"3.png",folderName+"4.png"};
		
		new CreateImageDoc(a,"test.docx","This is a sample text to verify..","End Data");
	}
	*/
	
	
}
