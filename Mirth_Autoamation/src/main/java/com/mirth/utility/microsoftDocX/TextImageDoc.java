package com.mirth.utility.microsoftDocX;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.xwpf.usermodel.Document;
import org.apache.poi.xwpf.usermodel.ParagraphAlignment;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.apache.xerces.impl.dv.util.Base64;

public class TextImageDoc {
	CustomXWPFDocument document = null;
	String defaultDocPath="";
	boolean setPageBreak=false;

	
	public TextImageDoc (String defaultDocPath) {
		document = new CustomXWPFDocument(); 
        this.defaultDocPath = defaultDocPath;
	}
	
	public void writeText(String text) throws IOException {
		writeText(text,"000000");
       
	}
	
	public void writeText(String text,String colorInHex) throws IOException {
        XWPFParagraph paragraphOne = document.createParagraph();
        XWPFRun paragraphOneRunOne = paragraphOne.createRun();
        paragraphOneRunOne.setFontSize(10);
        paragraphOneRunOne.setColor(colorInHex);
        paragraphOne.setAlignment(ParagraphAlignment.LEFT);
        if (setPageBreak) {
        	 paragraphOne.setPageBreak(true);
        	 setPageBreak=false;
        }
        
        String[] lines = text.split("\n");
        for (String line: lines) {
        	paragraphOneRunOne.setText(line);
        	paragraphOneRunOne.addBreak();
        }
        
        writeToFile();
       
	}
	
	public void insertImage(InputStream imageStream,int width,int height) throws InvalidFormatException,IOException {
        // Working addPicture Code below...
        XWPFParagraph paragraphX = document.createParagraph();
        paragraphX.setAlignment(ParagraphAlignment.CENTER);
		if (setPageBreak) {
			paragraphX.setPageBreak(true);
	      }
      
        String blipId = paragraphX.getDocument().addPictureData(
        		imageStream , Document.PICTURE_TYPE_JPEG);
       // System.out.println(document
     //           .getNextPicNameNumber(Document.PICTURE_TYPE_JPEG));
        document.createPicture(blipId,
                document.getNextPicNameNumber(Document.PICTURE_TYPE_JPEG),
                width, height);
        
        imageStream.close();
        
        //setPageBreak = true;
        
        writeToFile();
	}
	
	public void insertImage(File imageFile,int width, int height) throws InvalidFormatException, IOException {
		
		InputStream imageStream = new FileInputStream(imageFile);
		insertImage(imageStream, width, height);
	}
	
	public void insertImage(byte[] imageBytes,int width, int height) throws InvalidFormatException, IOException {
  		
		ByteArrayInputStream imageStream = new ByteArrayInputStream(imageBytes);
		insertImage(imageStream, width, height);
 
	}
	
	public void insertImage(String base64String,int width, int height) throws InvalidFormatException, IOException {
  		
		byte[] imageByte = Base64.decode(base64String);
		insertImage(imageByte,width,height);      
	}
	
	private void writeToFile() throws IOException {
		
        FileOutputStream outStream = new FileOutputStream(defaultDocPath);
        document.write(outStream);
        outStream.close();
        
        FileInputStream inStream = new FileInputStream(defaultDocPath);
        document = new CustomXWPFDocument(inStream);
        inStream.close();
	}

	
	public void writeToFile(String fileName) throws IOException {
		
        FileOutputStream outStream = new FileOutputStream(fileName);
        document.write(outStream);
        outStream.close();
	}

}
