/*
 * Author: Shashank L S
 * Creation Date: 06/Jan/2016
 * Description: Xls and Xlsx reader class 
 * Last Modified date: 11/Jan/2016
 */

package com.mirth.utility.spreadsheet;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;

public class MicrosoftSpreadsheet {
	
	
	//Some Constants
	
	private String filePath="";
	private File exlFile = null;
	private FileInputStream exlFileInputStream = null;
	private FileOutputStream exlFileOutputStream = null; 
	private MicrosoftSpreadsheetFormatHandler excelFileHandler = null;
	
	private boolean isWorkbookOpen = false;
	private String currentSheet = null;
	
	
	private void classConstructor(String filePathString) {
    	
		this.filePath = filePathString;		
		this.exlFile = new File(filePathString);
		
    	String fileExtension = filePath.substring(filePath.lastIndexOf('.') + 1);
    	
    	
    	boolean isXlsx = true;
    	if (fileExtension.equalsIgnoreCase("xls")) {
    		isXlsx = false;
    	}
    	
    	else {
    		isXlsx = true;
    	}
    	
    	this.excelFileHandler = new MicrosoftSpreadsheetFormatHandler(isXlsx);

	}
	
	
	public MicrosoftSpreadsheet(String filePath) {
		
		 classConstructor(filePath);

    }
		 
	
	private void createWorkbook(String sheetName) throws IOException 	{		
			
		this.exlFileOutputStream = new FileOutputStream(this.exlFile);
		excelFileHandler.createNewWorkbook();
		excelFileHandler.writeToFileOutputStream(exlFileOutputStream);
		
		this.createSheet(sheetName);
	}
	
	
	public void createWorkbook(String sheetName, boolean replace) throws IOException 	{		
		
		
		if (this.exlFile.exists() && !replace) {
			throw new IllegalMonitorStateException ("Workbook already present. Hence, not recreating the workbook.");
		}
		
		createWorkbook(sheetName);

	}
	
	public String getFilePath() {
		return this.filePath;
	}
  
	public void createSheet(String sheetName) throws IOException 	{
		
		if (!isWorkbookOpen){
    		//open the workbook spreadsheet if it NOT already open
        	this.exlFileInputStream = new FileInputStream(this.exlFile);
        	this.excelFileHandler.createNewWorkbook(exlFileInputStream);
        	this.isWorkbookOpen = true;
		}
		
		this.excelFileHandler.createSheet(sheetName);
		
		this.exlFileInputStream.close();
		this.exlFileOutputStream = new FileOutputStream(this.exlFile);
       	this.excelFileHandler.writeToFileOutputStream(exlFileOutputStream);
       	
    	this.exlFileInputStream = new FileInputStream(this.exlFile);
    	this.excelFileHandler.createNewWorkbook(exlFileInputStream);
    	this.isWorkbookOpen = true;
    	
    	if (this.currentSheet != null) {
    		this.swithchToSheet(currentSheet);
    	}

		
	}
	
	public void changeWorkbook(String filePath) throws IOException 	{
		
		if (isWorkbookOpen){
				this.exlFileInputStream.close();
				isWorkbookOpen = false;
		}
		
		classConstructor(filePath);
				
	}
	
	public int getMaxRows() throws IOException {
	  	if(!this.isWorkbookOpen)  {        	
	    		
	    		//open the workbook spreadsheet if it NOT already open
	        	this.exlFileInputStream = new FileInputStream(this.exlFile);
	        	this.excelFileHandler.createNewWorkbook(exlFileInputStream);
	        	this.isWorkbookOpen = true;
	    }    	
	  	
	  	return this.excelFileHandler.getLastRowNum(this.currentSheet);  	
	  	
	}
	
    //Opens the specified sheet by sheetName
    public void swithchToSheet(String sheetName) throws IOException,IllegalArgumentException {
    	
    	if(!this.isWorkbookOpen)  {        	

    		//open the workbook spreadsheet if it NOT already open
        	this.exlFileInputStream = new FileInputStream(this.exlFile);
        	this.excelFileHandler.createNewWorkbook(exlFileInputStream);
        	
        	this.isWorkbookOpen = true;
    	}    	
    	
    	if ( this.excelFileHandler.isSheetPresent(sheetName) ) {
    		this.excelFileHandler.pointToSheet(sheetName);    
    		currentSheet = sheetName;
    	}
    	
    	else
    	{
    		this.exlFileInputStream.close();
    		throw new IllegalArgumentException("Not a valid sheet name.");
    	}    	
    	
    }
    
    public boolean isSheetPresent(String sheetName) throws IOException {
	   	if(!this.isWorkbookOpen)  {        	
	    		
	    		//open the workbook spreadsheet if it NOT already open
	        	this.exlFileInputStream = new FileInputStream(this.exlFile);
	        	this.excelFileHandler.createNewWorkbook(exlFileInputStream);
	        	this.isWorkbookOpen = true;
	    	}    	
	   	
	   	return this.excelFileHandler.isSheetPresent(sheetName);

    }
    
    public boolean isWorkBookPresent() {
    	return this.exlFile.exists();
    }
    
    public String[] getAllSheetNames() throws IOException{
    	
    	if (!isWorkBookPresent()) {
    		return null;
    	}
    	
    	if(!this.isWorkbookOpen)  {        	
    		
    		//open the workbook spreadsheet if it NOT already open
        	this.exlFileInputStream = new FileInputStream(this.exlFile);
        	this.excelFileHandler.createNewWorkbook(exlFileInputStream);

        	this.isWorkbookOpen = true;
    	}    	
    	
    	int numberOfSheets = this.excelFileHandler.getNumberOfSheets();
    	String[] sheetNames = new String[numberOfSheets];
    	for (int i = 0; i < numberOfSheets; i++) {
    		sheetNames[i] = this.excelFileHandler.getSheetName(i);
    	}
    	
		return sheetNames;
    	
    }
    
    public int getNumberOfSheets() throws IOException{
    	
    	if(!this.isWorkbookOpen)  {        	
    		//open the workbook spreadsheet if it NOT already open
        	this.exlFileInputStream = new FileInputStream(this.exlFile);
        	this.excelFileHandler.createNewWorkbook(exlFileInputStream);

        	this.isWorkbookOpen = true;
    	} 
    	
    	return this.excelFileHandler.getNumberOfSheets();
    }
    
    //Opens the specified sheet by sheetNumber
    public void swithchToSheet(int sheetNum) throws IOException,IllegalArgumentException{
    	
    	if (sheetNum < 1 ) {
    		throw new IllegalArgumentException("Not a valid sheet number. Sheet number starts from 1.");
    	}
    	
    	if(!this.isWorkbookOpen)  {        	
    		
    		//open the workbook spreadsheet if it NOT already open
        	this.exlFileInputStream = new FileInputStream(this.exlFile);
        	this.excelFileHandler.createNewWorkbook(exlFileInputStream);
        	this.isWorkbookOpen = true;
    	}    	

    	
    	if(sheetNum > this.excelFileHandler.getNumberOfSheets()) {
    		this.exlFileInputStream.close();
    		throw new IllegalArgumentException("Not a valid sheet number. Sheet number starts from 1.");
    	}
    	
    	this.excelFileHandler.pointToSheet(sheetNum-1);
    	
    	currentSheet = this.excelFileHandler.getSheetName(sheetNum-1);
    }
    
	public short getMaxColumns(int rowNum) {
		
		return excelFileHandler.getMaxColumns(rowNum);
		
	}
    
    //Closes the sheet and the workbook
    public void closeXl() throws IOException {
    	if (this.isWorkbookOpen)
    	{
    		
    		if (this.exlFileInputStream!=null) {
    			this.exlFileInputStream.close();
    		}
    		
    		if (this.exlFileOutputStream!=null) {
        		this.exlFileOutputStream.close();
    		}

    		this.isWorkbookOpen = false;
    	}
    }
    
    public String readCell(String colHeading, int rowNum) throws IllegalAccessException, IllegalArgumentException {
    	return readCell(1, colHeading, rowNum);
    	
    }    
    
    public String readCell(int headingRow, String colHeading, int rowNum) throws IllegalAccessException,IllegalArgumentException {
    	
    	int colNum = 1;
    	boolean gotHeading = false;
    	if (!this.isWorkbookOpen){
			throw new IllegalAccessException("Work is NOT open to access the cell.");
		}
		
		if (rowNum < 1 ) {
			throw new IllegalArgumentException("Row or Column number is NOT valid.");
		}
		
		Row row = this.excelFileHandler.getRow(headingRow-1);
		int maxColumns = row.getLastCellNum();
		for (int column=1; column<= maxColumns; column++) {
			
			String cellValue = 	readCell(headingRow,column);

			if (cellValue!=null && cellValue.trim().equalsIgnoreCase(colHeading)) {
				gotHeading = true;
				colNum = column;
				break;
			}						
		}		
		
		if (!gotHeading){
			throw new IllegalArgumentException("Column heading given does NOT exist in the row " + headingRow + " of the sheet.");
		}
		
		return readCell(rowNum,colNum);		
    }
    
 
       
	public String readCell(int rowNum, int colNum) throws IllegalAccessException, IllegalArgumentException{
		
		String cellValue;
		if (!this.isWorkbookOpen){
			throw new IllegalAccessException("Work is NOT open to access the cell.");
		}
		
		if (rowNum < 1 || colNum < 1) {
			throw new IllegalArgumentException("Row or Column number is NOT valid.");
		}
		
		Row row = this.excelFileHandler.getRow(rowNum-1);

		try{
			Cell cell = row.getCell(colNum-1);		
			cell.setCellType(Cell.CELL_TYPE_STRING);
			cellValue =  cell.getStringCellValue();	
		} 
		catch(NullPointerException Nse) {
			cellValue = null;
		}
		
		return cellValue;
				
	}
	
	public String readCell(String cell) throws IllegalAccessException {
		
		String temp = cell.toLowerCase();
		temp = MicrosoftSpreadsheet.validateAndSplitCell(temp);
		
		int colNum = MicrosoftSpreadsheet.findSequenceCount( temp.split("-")[0] );
		int rowNum = Integer.parseInt( temp.split("-")[1] );
		
		return readCell(rowNum,colNum);
	}

	private static int findSequenceCount(String seq) {
		
		String lowerSeq = seq.toLowerCase();
		int seqCount = 0;
		
		//2 power string length-1
				for (int i=0;i<lowerSeq.length();i++) {		
					
					int charAscci = (int)(lowerSeq.charAt(i));
					if( charAscci >=97 &&  charAscci <=122 ) {
										
						seqCount *= 26;
						seqCount += Integer.parseInt(String.valueOf(((int)lowerSeq.charAt(i))-96));
					}				
					
					else{
					throw new IllegalArgumentException("Argument supplied is NOT valid.");
					}
					
				}//for		
		
		return seqCount;
	}
	
	private static String validateAndSplitCell(String s) {
			
		String col = "";
		int row = 0;
		
		boolean didRowStart = false;
			
		for (int i=0;i<s.length();i++) 		{
						
			if ( !didRowStart ) {
				if( (int)(s.charAt(i)) >=97 &&  (int)(s.charAt(i)) <=122 ) {
					col+= String.valueOf(s.charAt(i));
				}
				
				else if ( (int)(s.charAt(i)) >=49 &&  (int)(s.charAt(i)) <=57 )  {
					didRowStart = true;
					row = Integer.parseInt(String.valueOf(s.charAt(i)));
					continue;
				}
				
				else {
					throw new IllegalArgumentException("Given arguement[" + s.toUpperCase() + "] is not of correct cell value");
				}
				
				
			}//if(!didRowStart)
			
			else if (didRowStart && (int)(s.charAt(i)) >=49 &&  (int)(s.charAt(i)) <=57 )  {
				row = ( (row * 10) + Integer.parseInt(String.valueOf(s.charAt(i))) );
			}
			
			else {
				throw new IllegalArgumentException("Given arguement[" + s.toUpperCase() + "] is not of correct cell value");
			}				
		}	
		
		if ( row <1 ) {
			throw new IllegalArgumentException("Given arguement[" + s.toUpperCase() + "] is not of correct cell value");
		}	
		
		return (col+"-"+row);
	}
	
	

	//--------------------------------------//
	//Writing functionalities
	//--------------------------------------//

	public void writeCell(int rowNum, int colNum, String data,CellFormatting format,boolean isWithSave) throws IllegalAccessException, IllegalArgumentException, IOException{
		
		if (!this.isWorkbookOpen){
			throw new IllegalAccessException("Work is NOT open to access the cell.");
		}
		
		if (rowNum < 1 || colNum < 1) {
			throw new IllegalArgumentException("Row or Column number is NOT valid.");
		}
		
		if (format == null) {
			format = new CellFormatting();
		}
		
		formatCell(rowNum,colNum,format);
		
		Row row = this.excelFileHandler.getRow(rowNum-1);
		if (row == null){
			row = this.excelFileHandler.createRow(rowNum-1);
		}

		Cell cell = row.getCell(colNum-1);
		if (cell==null) {
			cell = row.createCell(colNum-1);		
		}
		
		cell.setCellValue(data);	
		
		if (isWithSave){
			this.save();
		}
	    
		
	}
		
	public void writeCell(String cell,String data,CellFormatting format,boolean isWithSave) throws IllegalAccessException, IllegalArgumentException, IOException {
		
		String temp = cell.toLowerCase();
		temp = MicrosoftSpreadsheet.validateAndSplitCell(temp);
		
		int colNum = MicrosoftSpreadsheet.findSequenceCount( temp.split("-")[0] );
		int rowNum = Integer.parseInt( temp.split("-")[1] );
		
		writeCell(rowNum,colNum,data,format,isWithSave);
	}

    public void writeCell(String colHeading, int rowNum,String data,CellFormatting format,boolean isWithSave) throws IllegalAccessException, IllegalArgumentException, IOException {
    	
    	writeCell(1, colHeading, rowNum,data,format,isWithSave);
    	
    	
    }
    
    public void writeCell(int headingRow, String colHeading, int rowNum,String data,CellFormatting format,boolean isWithSave) throws IllegalAccessException,IllegalArgumentException, IOException {
        
        int colNum = 1;
        boolean gotHeading = false;
        if (!this.isWorkbookOpen){
	    throw new IllegalAccessException("Work is NOT open to access the cell.");
	    }
	
	    if (rowNum < 1 ) {
	    throw new IllegalArgumentException("Row or Column number is NOT valid.");
	    }
	
	    //Point to 1st row
	
	    Row rowLocal = this.excelFileHandler.getRow(headingRow-1);
	
	    int maxColumns = rowLocal.getLastCellNum();
	    for (int column=1; column<= maxColumns; column++) {
	
	    String cellValue = readCell(headingRow,column);
	
	    if (cellValue!=null && cellValue.trim().equalsIgnoreCase(colHeading)) {
	    gotHeading = true;
	    colNum = column;
	    break;
	    }
	    }
	
	    if (!gotHeading){
	    throw new IllegalArgumentException("Column heading given does NOT exist in the row " + headingRow + " of the sheet.");
	    }
	
	
	    writeCell(rowNum,colNum,data,format, isWithSave);
    } 
       
    public void unFreezePanesInCurrentSheet() throws IllegalAccessException, IOException {
       
	    if (!this.isWorkbookOpen){
	    throw new IllegalAccessException("WorkBook is NOT open to access the cell.");
	    }
	
	       try {
	           this.excelFileHandler.createFreezePane(0, 0);
	       } catch(IndexOutOfBoundsException e)
	       {
	           System.out.println("previously didn't have a freeze pane.");
	       }
	       
		    this.save();  
     }
       
    //Formatting functions
    public void formatCell(int rowNum, int colNum,CellFormatting formatData) throws IOException, IllegalAccessException {
	    if (!this.isWorkbookOpen){
	    throw new IllegalAccessException("Work is NOT open to access the cell.");
	    }
	
	    if (rowNum < 1 || colNum < 1) {
	    throw new IllegalArgumentException("Row or Column number is NOT valid.");
	    }
	
	    Row row = this.excelFileHandler.getRow(rowNum-1);
	    if (row == null){
	    row = this.excelFileHandler.createRow(rowNum-1);
	    }
	
	    Cell cell = row.getCell(colNum-1);
	    if (cell==null) {
	    cell = row.createCell(colNum-1);
	    }
	
	    CellStyle cStyle = this.excelFileHandler.getCellStyle();
	
	    //
	    if (formatData.fullBorders) {
	    cStyle.setBorderTop(formatData.borderType);
	    cStyle.setBorderLeft(formatData.borderType);
	    cStyle.setBorderRight(formatData.borderType);
	    cStyle.setBorderBottom(formatData.borderType);
	    } 
	    else if (formatData.bucketBorder) {
	    cStyle.setBorderLeft(formatData.borderType);
	    cStyle.setBorderRight(formatData.borderType);
	    cStyle.setBorderBottom(formatData.borderType);	
	    }
	    //
	
	    if (formatData.backgroundColor!=CellStyle.NO_FILL || formatData.foregroundColor!=CellStyle.NO_FILL) {
	    	cStyle.setFillPattern(formatData.fillPattern);
	    }   

	    cStyle.setAlignment(formatData.alignMent);
	    
	    if (formatData.backgroundColor!=CellStyle.NO_FILL) {
	    cStyle.setFillBackgroundColor(formatData.backgroundColor);
	    }
	
	    if (formatData.foregroundColor!=CellStyle.NO_FILL) {
	    cStyle.setFillForegroundColor(formatData.foregroundColor);
	    }
	
	    Font font = this.excelFileHandler.getFont();
	    font.setColor(formatData.fontColor);
	    font.setItalic(formatData.italicFont);
	
	    if (formatData.boldFont) {
	    	font.setBoldweight(Font.BOLDWEIGHT_BOLD);
	    }
	    
	    if (formatData.fontHeight!=Font.SS_NONE){
	    	font.setFontHeightInPoints(formatData.fontHeight);
	    }
	
	
	
	    cStyle.setFont(font);
	    cell.setCellStyle(cStyle);
	
	    this.save();
	
	}
    
    public boolean isSheetXlsx() {
    	return this.excelFileHandler.isSheetXlsx();
    }
    
	public XSSFSheet getXLSXSheet() {
		return this.excelFileHandler.getXLSXSheet();
		
	}
	
	public HSSFSheet getXLSSheet() {
		return this.excelFileHandler.getXLSSheet();
	}
	
	public void save() throws IOException {
	    this.exlFileOutputStream = new FileOutputStream(this.exlFile);
	    this.excelFileHandler.writeToFileOutputStream(exlFileOutputStream);
	           
	           
	    if (isWorkbookOpen) {
	
	        this.exlFileInputStream.close();
	        this.isWorkbookOpen = false;
	        
	        this.exlFileInputStream = new FileInputStream(this.exlFile);
	        this.excelFileHandler.createNewWorkbook(exlFileInputStream);
	            this.isWorkbookOpen = true;
	            this.swithchToSheet(this.currentSheet);
	    }
	}
	
	public void autoSizeSheet(String sheetName, int rowForRef) throws IOException {
		
		String currentSheetName = this.currentSheet;

		this.swithchToSheet(sheetName);
		this.excelFileHandler.autoSizeSheet(rowForRef-1);		
		this.swithchToSheet(currentSheetName);	
		this.save();

	}
	
	public void mergeColumnsInRow (int row,int fromCol,int toCol) throws IOException {
		this.excelFileHandler.mergeColumnsInRow(row-1,fromCol-1,toCol-1);
		this.save();
	}
	
	   
}//ExlReader


