package com.mirth.utility.spreadsheet;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Iterator;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class MicrosoftSpreadsheetFormatHandler {
	
	private HSSFWorkbook xlsWorkbook = null;
	private HSSFSheet xlsSheet = null;
	private XSSFWorkbook xlsxWorkbook = null;
	private XSSFSheet xlsxSheet = null;
	private boolean isXlsx=true;	
			
	
	public MicrosoftSpreadsheetFormatHandler (boolean isXlsxFormat) {
		isXlsx = isXlsxFormat;
	}
	
	public void autoSizeSheet(int rowForRef) {
		
		for (int col=0;col<getMaxColumns(rowForRef);col++) {
			if (isXlsx){
				xlsxSheet.autoSizeColumn(col);
			}
			else {
				xlsSheet.autoSizeColumn(col);
			}

		}//for
		
	}//autoSizeSheet
	
	public boolean isSheetXlsx() {
		return isXlsx;
	}
	
	public CellStyle getCellStyle() {
		if (isXlsx) {
			
			return xlsxWorkbook.createCellStyle();
		}
		
		return xlsWorkbook.createCellStyle();
	
	}
	
	public Font getFont() {
		if (isXlsx) {
			
			return xlsxWorkbook.createFont();
		}
		
		return xlsWorkbook.createFont();
	}
	

	public void setSheet(XSSFSheet xlsxSheet) {
		this.xlsxSheet = xlsxSheet;
	}
	
	public void setSheet(HSSFSheet xlsSheet) {
		this.xlsSheet = xlsSheet;
	}	
	
	public XSSFSheet getXLSXSheet() {
		return this.xlsxSheet;
	}
	
	public HSSFSheet getXLSSheet() {
		return this.xlsSheet;
	}
	

	
	
	public void createNewWorkbook () {
		
		if (isXlsx) {
			this.xlsxWorkbook = new XSSFWorkbook();
			return;
		}
		
		this.xlsWorkbook = new HSSFWorkbook();
	
	}
	
	
	
	public void writeToFileOutputStream (FileOutputStream exlFileOutputStream) throws IOException {
		
		if (isXlsx) {
			this.xlsxWorkbook.write(exlFileOutputStream);
			return;
		}
		
		this.xlsWorkbook.write(exlFileOutputStream);
		
	}
	
	public void createNewWorkbook (FileInputStream exlFileInputStream) throws IOException {
		
		if (isXlsx) {
			this.xlsxWorkbook = new XSSFWorkbook(exlFileInputStream);
			return;
		}
		
		this.xlsWorkbook = new HSSFWorkbook(exlFileInputStream);
		
	}
	
	public void createSheet(String sheetName) {
		
		if (isXlsx) {
			this.xlsxWorkbook.createSheet(sheetName);
			return;
		}
		
			this.xlsWorkbook.createSheet(sheetName);
	}
	
	
	public int getNumberOfSheets() {
		
		if (isXlsx) {
			return this.xlsxWorkbook.getNumberOfSheets();
		}
		
			return this.xlsWorkbook.getNumberOfSheets();
	}
	
	
	
	public int getLastRowNum(String sheetName) {
		
		int lastRowNum;
		
		if (isXlsx) {
			lastRowNum = this.xlsxWorkbook.getSheet(sheetName).getLastRowNum();
		}
		
		else {
			lastRowNum = this.xlsWorkbook.getSheet(sheetName).getLastRowNum();
		}
		
		//Presently POI incorrectly sends one magnitude less than the actual lastRowNum. So, adding +1
		return lastRowNum+1;
	}
	
	
	public String getSheetName(int sheetNum) {
		
		if (isXlsx) {
			return this.xlsxWorkbook.getSheetName(sheetNum);
		}
		
		return this.xlsWorkbook.getSheetName(sheetNum);
		
	}
	
	
	public Iterator<Row> getSheetIterator() {
		
		if (isXlsx) {
			return this.xlsxSheet.iterator();
		}
		
		return this.xlsSheet.iterator();
	}
	
	public Row getRow(int rowNum) {
		
		if (isXlsx) {
		
			return this.xlsxSheet.getRow(rowNum);

		}

		return this.xlsSheet.getRow(rowNum);
	}
	
	public short getMaxColumns(int rowNum) {
		
		Row myRow = this.getRow(rowNum);
		return myRow.getLastCellNum();
		
	}
	
	public Row createRow(int rowNum) {
		
		if (isXlsx) {
			return this.xlsxSheet.createRow(rowNum);
		}
		
		return this.xlsSheet.createRow(rowNum);
		
	}
	
	public void createFreezePane(int x, int y) {
		
		if (isXlsx) {
			this.xlsxSheet.createFreezePane(x,y);
			return;
		}
		
		this.xlsSheet.createFreezePane(x,y);
		
	}


	public void pointToSheet(int sheetNum) {
		
		if (isXlsx) {
			this.xlsxSheet = this.xlsxWorkbook.getSheetAt(sheetNum);
			return;
		}
		
		this.xlsSheet = this.xlsWorkbook.getSheetAt(sheetNum);
		
	}
	
	public void pointToSheet(String sheetName) {
		
		if (isXlsx) {
			this.xlsxSheet = this.xlsxWorkbook.getSheet(sheetName);
			return;
		}
		
		this.xlsSheet = this.xlsWorkbook.getSheet(sheetName);
		
	}
	
	public boolean isSheetPresent(String sheetName) {
		
		if (isXlsx) {
			return ( this.xlsxWorkbook.getSheet(sheetName)!=null );
		}
		
		return ( this.xlsWorkbook.getSheet(sheetName)!=null );
	}

	public void mergeColumnsInRow(int row,int fromRow, int toRow) {
		
		if (isXlsx) {
			xlsxSheet.addMergedRegion(new CellRangeAddress(row,row,fromRow,toRow));
		}
		else {
			xlsSheet.addMergedRegion(new CellRangeAddress(row,row,fromRow,toRow));
		}
		
	}
	
}
