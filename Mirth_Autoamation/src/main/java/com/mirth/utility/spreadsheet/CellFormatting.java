package com.mirth.utility.spreadsheet;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;

public class CellFormatting {

	
	public boolean boldFont = false;
	public boolean italicFont = false;
	public short fontColor = IndexedColors.BLACK.getIndex();
	
	public short borderType = CellStyle.BORDER_THIN;
	public boolean fullBorders = false;
	public boolean bucketBorder = false;
	public short backgroundColor = CellStyle.NO_FILL;
	public short foregroundColor=CellStyle.NO_FILL;
	public short alignMent=CellStyle.ALIGN_GENERAL;
	public short fillPattern = CellStyle.SOLID_FOREGROUND;
	public int cellType = Cell.CELL_TYPE_STRING;
	public short fontHeight = Font.SS_NONE;	
}
