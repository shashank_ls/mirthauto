package com.mirth.utility.dumb;

public class DeclarationConverter {

	public String value = null;
	
	public DeclarationConverter(String val) {
		this.value = val;
	}	
	
	public String get() {
		
		return "public static PlainTextS $" + value + " = new PlainTextS(\"" +value+ "\");";

	}
	
}
