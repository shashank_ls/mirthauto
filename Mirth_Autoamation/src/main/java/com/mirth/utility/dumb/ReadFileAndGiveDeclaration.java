package com.mirth.utility.dumb;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;


public class ReadFileAndGiveDeclaration{
	
	public static String process() throws IOException{
		String finalVal = "";
		BufferedReader in = new BufferedReader(new FileReader("src/declareConverter.txt"));
		String val = in.readLine();
		
		while(val != null){
			
			finalVal += new DeclarationConverter(val).get() + "\n";
			val = in.readLine();
		}
		in.close();
		return finalVal;
	}
	
	public static void main(String[] args) throws IOException{
		System.out.println(ReadFileAndGiveDeclaration.process());
	}
	
}
