package com.mirth.dataObjects;

import java.util.ArrayList;
import java.util.List;

import com.mirth.controller.Executor;

public abstract class MutexObject {

	private static List<String> testsRunningCurrently = new ArrayList<String>();

	private static boolean isTestSlotLocked=false;
	private static String appTestDelimeter = "";

	public static synchronized boolean requestPermissionToRun(TestInventoryData testInventory) {
		
		if (isTestSlotLocked) {
			return false;
		}//if (isTestSlotLocked)
		
		return true;
		
	}//public synchronized boolean requestPermissionToRun(TestInventoryData testInventory)
	
	
	
	public static synchronized boolean canIStartRunning(TestInventoryData testInventory) {
	
		if (testInventory.isRunsAlone) {
			if (testsRunningCurrently.size() != 0) {
				return false;
			}

			testsRunningCurrently.add(testInventory.appName+appTestDelimeter+testInventory.automationScriptID);
			System.out.println("Started Thread "+Thread.currentThread().getId()+" (" + testInventory.automationScriptID + ") which is running  alone!!!!!!!!!");
		}
		
		else {
			System.out.println("Starting run of: " + Thread.currentThread().getId()  + ":" + testInventory.automationScriptID);
		}
		
		return true;		
	}//public synchronized boolean canIStartRunning(TestInventoryData testInventory)	
	
	
	public static synchronized void iFinishedRunning(TestInventoryData testInventory) {
		testsRunningCurrently.remove(testInventory.appName+appTestDelimeter+testInventory.automationScriptID);
		
		if (isTestSlotLocked) {
			
			if (testInventory.isRunsAlone) {
				isTestSlotLocked = false;
			}
			
		}//if (isTestSlotLocked) 
		
		
		//Remove dontRun testCases:
		if (testInventory.dontRunWithList!=null && testInventory.dontRunWithList.size()>0) {
			for(String dontRunId: testInventory.dontRunWithList) {
				Executor.dontRunTests.remove(testInventory.appName+appTestDelimeter+dontRunId);
			}
		}//if (testInventory.dontRunWithList!=null && testInventory.dontRunWithList.size()>0)	
		
		System.out.println("Finished with Run of: " + Thread.currentThread().getId()  + ":" + testInventory.automationScriptID);
	}//public synchronized void iFinishedRunning(TestInventoryData testInventory)
	
	
	public static synchronized List<String> getCurrentlyRunningTests() {
		return testsRunningCurrently;
	}

	
	public static synchronized boolean decideWhetherFitToRun(TestInventoryData testInventory) {
		
		if (Executor.dontRunTests.contains(testInventory.appName+appTestDelimeter+testInventory.automationScriptID)) {
			
			Executor.waitingTests.add(testInventory);
			return false;	
			
		}
		
		//If the control reaches here, then test is FIT to Run
		
		if (!testInventory.isRunsAlone) {
			testsRunningCurrently.add(testInventory.appName+appTestDelimeter+testInventory.automationScriptID);
			System.out.println("Giving permission to: " + Thread.currentThread().getId()  + ":" + testInventory.automationScriptID);
			
			if (testInventory.dontRunWithList!=null && testInventory.dontRunWithList.size()>0) {
				for(String dontRunId: testInventory.dontRunWithList) {
					Executor.dontRunTests.add(testInventory.appName+appTestDelimeter+dontRunId);
				}
			}//if (testInventory.dontRunWithList!=null && testInventory.dontRunWithList.size()>0)	
			
		}//if (!testInventory.isRunsAlone)
		
		else {
			isTestSlotLocked = true;
			System.out.println("Wait started for running lone thread: " + Thread.currentThread().getId()  + ":" + testInventory.automationScriptID);
		}//else ==> if (!testInventory.isRunsAlone)
		
	
		return true;
	}
	
	
	
	/*
	public static synchronized boolean decideWhetherFitToRun(TestInventoryData testInventory) {
		
		boolean isFit = true;
		
		if (Assertions.isNull(testInventory.dontRunWithList)) {
			return true;
		}
		
		DontRunIDLoop:
		for(String dontRunId: testInventory.dontRunWithList) {
			
			if (testsRunningCurrently.contains(testInventory.appName+appTestDelimeter+dontRunId) ) {
				
				Executor.waitingTests.add(testInventory);
				isFit = false;
				break DontRunIDLoop;				
			
			}//if (testsRunningCurrently.contains(testInventory.appName+appTestDelimeter+dontRunId) ) 		
			
		}//for(String dontRunId: dontRunTests)
		
		
		if (isFit) {
			
			if (!testInventory.isRunsAlone) {
				testsRunningCurrently.add(testInventory.appName+appTestDelimeter+testInventory.automationScriptID);
				System.out.println("Giving permission to: " + Thread.currentThread().getId()  + ":" + testInventory.automationScriptID);
			}//if (!testInventory.isRunsAlone)
			
			else {
				isTestSlotLocked = true;
				System.out.println("Wait started for running lone thread: " + Thread.currentThread().getId()  + ":" + testInventory.automationScriptID);
			}//else ==> if (!testInventory.isRunsAlone)
			
		}//if (isFit) 
		
		
		return isFit;	
		
	}
	
	*/
	
	public synchronized static TestInventoryData getAnyPendingTests() {
		
		if (Executor.waitingTests.size() == 0) {
			return null;
		}
				
		WaitingTestForLoop:		
		for (TestInventoryData testInventory: Executor.waitingTests) {
			
			if (testInventory.dontRunWithList!=null && testInventory.dontRunWithList.size()>0) {
				//Dont runID for loop
				for(String dontRunId: testInventory.dontRunWithList) {
					
					if (testsRunningCurrently.contains(testInventory.appName+appTestDelimeter+dontRunId) ) {
						continue WaitingTestForLoop;					
					}		
					
				}//for(String dontRunId: dontRunTests)
			}
			

			
			Executor.waitingTests.remove(testInventory);
			return testInventory;			
			
		}//for (TestInventoryData testInventory: Executor.waitingTests)
		
		return null;
		
	}//public static TestInventoryData getAnyPendingTests()
	
}//class
