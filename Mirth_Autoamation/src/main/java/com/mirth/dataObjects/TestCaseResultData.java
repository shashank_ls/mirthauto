package com.mirth.dataObjects;

import java.io.IOException;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;

import com.mirth.controller.MirthLogger;
import com.mirth.properties.ResultConfig;
import com.mirth.results.QAProof;

public class TestCaseResultData {	
	
	public String appName;
	public String startDateTime="";
	public String endDateTime="";
	public String jiraID="";
	public String scriptID="";
	public String scriptName="";
	public String belongsToModules="";
	public String browserName="";
	
	private boolean isResultPass=true;
	private String issues="";
	private String miscData="";
	private String issueHeadingPrefix = "Issue with ";
	private String issueHeadingSuffix = ": ";
	private String issueColorInProofFormat = "FF0000";//Red
	private String miscDataColorInProofFormat = "FFA500";//Orange
	
	@Override
	public String toString() {
		return ("=> " + appName + "," + startDateTime + ","+ endDateTime + ","+ jiraID + ","+ scriptID + ","+ scriptName + ","
				+ "<"+belongsToModules+">" + ","+ browserName + ","+ this.getResultString() + ","+ issues + ","+ miscData );
	}	
	
	public String getIssues() {
		return issues;
	}
	
	public String getMiscData() {
		return miscData;
	}
	
	public String getResultString() {
		
		if (isResultPass) {
			return ResultConfig.Pass;
		}
		
		return ResultConfig.Fail;		
	}
	
	public boolean getResult() {
		return isResultPass;
	}
	
	public void setResult(boolean boolResult) {
		
		//once the value is changed to valse, it should stay that way regardless of future passes.
		if(isResultPass==false) {
			return;
		}
		
		isResultPass = boolResult;
	}
		
	public void reportIssue(String headingString, String issueString) throws IOException, InvalidFormatException{
		
		this.setResult(false);
		
		this.issues +=  ((issues.length()>0)?"\n\n":"");
		this.issues += ">> " + issueHeadingPrefix+headingString+issueHeadingSuffix;
		//System.out.print("-------\n"+ThreadController.getThreadHeadingScreenShot());
		reportIssue(issueString);
		
	}
	
	public void reportIssue(String issueString) throws InvalidFormatException, IOException{
		//System.out.println(":"+ThreadController.getThreadHeadingScreenShot()+"\n-------");
		
		this.setResult(false);
		
		QAProof.writeText(issueString,issueColorInProofFormat);	
		MirthLogger.log(issueString);
		
		this.issues += (issues.length()>0)?"\n":"";
		this.issues += issueString;
		
	}
	

	public void reportMiscData(String miscData) throws IOException{
		
		this.miscData += (miscData.length()>0)?"\n":"";
		this.miscData += miscData;
		
		QAProof.writeText(">> " + miscData,miscDataColorInProofFormat);
		MirthLogger.log(miscData);
	
	}
	
	
	
	
	
	
}
