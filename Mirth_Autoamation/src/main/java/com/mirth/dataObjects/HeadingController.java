package com.mirth.dataObjects;

public class HeadingController {
	private String verifyingText = null;
	private boolean isProofHeadingWritten = false;
	private boolean isIssueHeadingWritten = false;
	
	
	public HeadingController(String heading){
		verifing(heading);
	}
	
	public void verifing(String heading) {
		
		if (heading == null){
			heading = "";
		}
		
		goToInitialState(heading);
	}
	
	public String getVerifyingText(){
		return this.verifyingText;
	}
	
	public boolean isProofHeadingWritten(){
		return this.isProofHeadingWritten;
	}
	
	public void setIsProofHeadingWritten(boolean status){
		this.isProofHeadingWritten = status;
	}
	
	public boolean isIssueHeadingWritten(){
		return this.isIssueHeadingWritten;
	}
	
	public void setIsIssueHeadingWritten(boolean status){
		this.isIssueHeadingWritten = status;
	}
	
	public void goToInitialState(String heading){
		this.verifyingText = heading;
		this.isProofHeadingWritten = false;
		this.isIssueHeadingWritten = false;
	}
	
	public void goToInitialState(){
		goToInitialState("");
	}
	
	public void noProofHeading(){
		this.setIsProofHeadingWritten(true);
	}
	
	public void noIssueHeading(){
		this.setIsIssueHeadingWritten(true);
	}
	
}
