package com.mirth.dataObjects;

import java.util.ArrayList;

//This class is thread safe
public class DataContainer<T> {

	private ArrayList<T> container;
	
	public DataContainer() {
		container = new ArrayList<T>();
	}
	
	public synchronized void putData(T data) {
		container.add(data);
	}
	
	public synchronized int getSize() {
		return container.size();
	}
	
	public synchronized T getData(int index) {
		
		if (container.size() > 0) {
			return container.get(index);
		}
		
		return null;
	}
	
	public synchronized boolean isDataExists(T data) {
		
		if (container.contains(data)) {
			return true;
		}
		
		return false;		
	}
	
	public synchronized void removeDataAtIndex(int index) {
		
		container.remove(index);
		
	}
	
}
