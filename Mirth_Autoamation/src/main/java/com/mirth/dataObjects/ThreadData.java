package com.mirth.dataObjects;

import java.io.PrintWriter;
import java.util.Map;

import com.mirth.controller.DriverController;
import com.mirth.controller.Verification;
import com.mirth.utility.microsoftDocX.TextImageDoc;

public class ThreadData {
		
	public DriverController driverController = null;
	public PrintWriter printWriter = null;
	public TestInventoryData testInventoryData = null;
	public TextImageDoc textImageDoc = null;
	public Map<String,String> dataMap = null;
	public Map<String,Object> objMap = null;
	public Verification verify = null;
	
	public HeadingController headingController = new HeadingController("");
	
	
	public ThreadData() {
		
	}
	
}
