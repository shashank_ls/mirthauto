package com.mirth.dataObjects;

import java.util.List;

public class TestInventoryData {

	public String appName;
	public boolean isQAProofNeed = false;
	public Browser browser;
	
	public String manualTestID;
	public String automationScriptID;
	public String automationScriptName;
	public boolean isExecutionFlag = false;
	public String browserString;
	public int priority = 1;
	public String qaProofNeeded;
	public List<String> belongsToModulesList;
	public List<String> dependentOnTestCasesList;
	public boolean isRunsAlone = false;
	public List<String> dontRunWithList;
	
	@Override
	public String toString() {
		return ("=> " + appName + "," + manualTestID + ","+ automationScriptID + ","+ automationScriptName + ","+ isExecutionFlag + ","+ browser + ","
				+ priority + ","+ qaProofNeeded + ","+ belongsToModulesList + ","+ dependentOnTestCasesList + ","+ isRunsAlone + ","+ dontRunWithList);
	}	
}
