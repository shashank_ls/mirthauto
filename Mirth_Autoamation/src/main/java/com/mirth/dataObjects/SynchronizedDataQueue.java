package com.mirth.dataObjects;

import java.util.LinkedList;

import com.mirth.utility.general.StaticLib;

//This class is thread safe
public class SynchronizedDataQueue<T> {

	private LinkedList<T> container;
	
	public SynchronizedDataQueue() {
		container = new LinkedList<T>();
	}
	
	public synchronized void putData(T data) {
		container.add(data);
	}
	
	public synchronized int getSize() {
		return container.size();
	}
	
		
	public synchronized T getNextData() {
		
		if (container.size() > 0) {
			return container.poll();
		}
		
		return null;
	}
	
	public synchronized boolean isContainerEmpty() {
		return (this.getSize()==0);
	}
	
	public LinkedList<T> getCopyOfAllData() throws InstantiationException, IllegalAccessException, ClassNotFoundException {
		return ( (LinkedList<T>) StaticLib.cloneList(container));		
	}
	
}
