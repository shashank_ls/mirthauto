package com.mirth.dataObjects;

public class AppAttributesForTestInv {
	
	public boolean isModuleExecutionOnlyActual;
	public boolean isExecutionFlagForModuleActual;
	public boolean isPriorityBasedExecutionActual;
	public boolean isPriorityBasedModuleExecutionActual;
	public boolean isTakeQAProofActual;
	
	@Override
	public String toString() {
		return ("=> " + isModuleExecutionOnlyActual + ","+ isExecutionFlagForModuleActual  
				+ ","+ isPriorityBasedExecutionActual + ","+ isPriorityBasedExecutionActual + ","+ isTakeQAProofActual);
	}
}
