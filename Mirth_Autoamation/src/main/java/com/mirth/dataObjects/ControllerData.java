package com.mirth.dataObjects;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class ControllerData {

	private static List<String> appsToRunList = new ArrayList<String>();
	private static Map<String,String> generalAttributesMap = new LinkedHashMap<String,String>();
	private static Map<String,String> globalApplicationAttributesMap = new LinkedHashMap<String,String>();	
	private static Map<String,ApplicationData> appDataMap = new LinkedHashMap<String,ApplicationData>();
	
	
	public static List<String> getAppsToRunList() {
		return appsToRunList;
	}
	
	public static Map<String,String> getGeneralAttributesMap() {
		return generalAttributesMap;
	}
	
	public static Map<String,String> getGlobalApplicationAttributesMap() {
		return globalApplicationAttributesMap;
	}

	public static Map<String,ApplicationData> getAppDataMap() {
		return appDataMap;
	}


	
}
