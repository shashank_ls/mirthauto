package com.mirth.dataObjects;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;
import java.util.TreeSet;

public class ProductSetData {

	TreeMap<Integer,ArrayList<String>> productMap = new TreeMap<Integer,ArrayList<String>>();

	
	public void addProduct(String product,int priority){
		
		ArrayList<String> listProduct = productMap.get(priority);
		if (listProduct == null) {
			listProduct = new ArrayList<String>();
			productMap.put(priority, listProduct);
		}
		listProduct.add(product);
	}
	
	public List<String> getSortedProductList() {
		
		List<String> finalProductList = new ArrayList<String>();
		for (Integer i: new TreeSet<Integer>(productMap.keySet())) {
			
			for (String product: productMap.get(i)) {
				finalProductList.add(product);
			}//for (String product: productMap.get(i))					
		}//for (Integer i: new TreeSet<Integer>(productMap.keySet()))
		
		return finalProductList;
	}
}
