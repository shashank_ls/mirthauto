package com.mirth.dataObjects;

public class ModuleInventoryData {

	public String moduleID;
	public String moduleName;
	
	@Override
	public String toString() {
		return ("=> " + moduleName + ","+ moduleID );
	}
	
}
