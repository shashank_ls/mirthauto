package com.mirth.dataObjects;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class ApplicationData {

	private List<TestInventoryData> testInventoryList = new ArrayList<TestInventoryData>();
	private Map<String,String> localApplicationAttributesMap = new LinkedHashMap<String,String>();
	private List<ModuleInventoryData> moduleInventoryList = new ArrayList<ModuleInventoryData>();
	
	public Map<String,String> getLocalApplicationAttributesMap() {
		return localApplicationAttributesMap;
	}
	
	public List<TestInventoryData> getTestInventory() {
		return testInventoryList;
	}
	
	public List<ModuleInventoryData> getModuleInventoryList() {
		return moduleInventoryList;
	}

	
}
