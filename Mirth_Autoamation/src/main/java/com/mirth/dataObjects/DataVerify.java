package com.mirth.dataObjects;

public class DataVerify {
	
	public String fieldIdentifier=null;
	public String[] expectedSetterDataPair;
	public String headingString = null;
	
	public boolean isCaseSensitiveComparision = false;
	public boolean isDocumentScreenshotAndHeading = true;
	
	public DataVerify(String fieldId,String[] expectedSDPair){
		this.fieldIdentifier = fieldId;
		this.expectedSetterDataPair = expectedSDPair;
	}
	
	public DataVerify(String fieldId,String[] expectedSDPair,String heading){
		this(fieldId,expectedSDPair);
		this.headingString = heading;
	}
	
	public DataVerify(String fieldId,String[] expectedSDPair,String heading,boolean isCaseComp,boolean isDocScrnHeading){
		this(fieldId,expectedSDPair,heading);
		this.isCaseSensitiveComparision = isCaseComp;
		this.isDocumentScreenshotAndHeading = isDocScrnHeading;
	}
	
	
}//class
