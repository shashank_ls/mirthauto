package com.mirth.dataObjects;

public class ApplicationResultData {	
	
	public String appName;
	public String environment;
	public String buildNumber;
	public String runStart;
	public String runEnd;
	public String runID;
	
	public boolean isWritten = false;
	
}
