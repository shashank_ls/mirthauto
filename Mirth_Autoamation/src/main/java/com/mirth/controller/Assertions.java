package com.mirth.controller;

import java.util.List;
import java.util.Map;
import java.util.Set;

public class Assertions {

	public static boolean isSame(String expected, String actual,boolean isCaseSensitive) {
		
		if (isCaseSensitive){
			return expected.equals(actual);
		}

		return expected.equalsIgnoreCase(actual);
		
	}

	public static boolean isSame(long expected, long actual) {
		return (expected==actual);
	}
	
	public static boolean isSame(double expected, double actual) {
		return (expected==actual);
	}

	public static boolean isSame(double expected, long actual) {
		return (expected==(double)actual);
	}

	public static boolean isSame(long expected, double actual) {
		return isSame(actual,expected);
	}

	public static boolean isSame(boolean expected, boolean actual) {
		return (expected==actual);
	}
	
	public static boolean isSame(Object objA,Object objB) {
		return objA.equals(objB);
	}

	public static boolean isSame(String expected,Long actual, boolean isCaseSensitive) {
		return isSame(expected,actual.toString(),isCaseSensitive);
	}
	
	public static boolean isSame(Long expected,String actual, boolean isCaseSensitive) {
		return isSame(actual,expected,isCaseSensitive);
	}
	
	public static boolean isSame(String expected,Double actual, boolean isCaseSensitive) {
		return isSame(expected,actual.toString(),isCaseSensitive);
	}
	
	public static boolean isSame(Double expected,String actual, boolean isCaseSensitive) {
		return isSame(actual,expected,isCaseSensitive);
	}

	public static boolean isSame(String expected,Boolean actual, boolean isCaseSensitive) {
		return isSame(expected,actual.toString(),isCaseSensitive);
	}
	
	public static boolean isSame(Boolean expected,String actual, boolean isCaseSensitive) {
		return isSame(actual,expected,isCaseSensitive);
	}

	public static boolean isSame(String expected,long actual) {
		
		try{
			return isSame(Long.parseLong(expected),actual);
		}catch(Exception e) {
			//non parsable if the control comes here.
		}		
		return false;		
	}
	
	public static boolean isSame(long expected,String actual) {
		return isSame(actual,expected);
	}
	
	public static boolean isSame(String expected,double actual) {
		
		try{
			return isSame(Double.parseDouble(expected),actual);
		}catch(Exception e) {
			//non parsable if the control comes here.
		}		
		return false;		
	}
	
	public static boolean isSame(double expected,String actual) {
		return isSame(actual,expected);
	}


	public static boolean isTrue(boolean expression) {
		if (expression) {
			return true;
		}
		
		return false;
	}
	

	public static boolean isFalse(boolean expression) {
		
		return !isTrue(expression);
	}
	
	public static boolean isNull(Object obj) {
		return (obj==null);
	}
	
	public static boolean isNotNull(Object obj) {
		return !isNull(obj);
	}
	
	public static boolean isNotEmpty(String str) {
		
		if (isNull(str)) {
			return false;
		}
		return !str.equalsIgnoreCase("");
	}
	
	public static boolean isNotEmpty(List<? extends Object> c) {
		if (isNull(c)) {
			return false;
		}
		return (c.size()>0);
	}
	
	public static boolean isNotEmpty(Set<? extends Object> s) {
		if (isNull(s)) {
			return false;
		}
		return (s.size()>0);
	}
	
	
	public static boolean isNotEmpty(Map<? extends Object,? extends Object> m) {
		if (isNull(m)) {
			return false;
		}
		return (m.size()>0);
	}

	public static boolean isNegetive(double num) {
		return (num<0);
	}
	
	public static boolean isNotNegetive(double num) {
		return !isNegetive(num);
	}
	
	public static boolean isPositive(double num) {
		return (num<0);
	}
	
	public static boolean isNotPositive(double num) {
		return !isPositive(num);
	}

	public static boolean isZero(double num) {
		return (num == 0);
	}

	public static boolean isNotZero(double num) {
		return !isZero(num);
	}


}
