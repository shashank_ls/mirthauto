package com.mirth.controller;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import com.mirth.properties.LogConfig;
import com.mirth.utility.general.StaticLib;
import com.mirth.utility.general.TimeLib;

public final class MirthLogger {
	
	private static String separator = " | ";
	
	public static void setSpearator(String separatorString) {
		if (Assertions.isNotEmpty(separatorString)) {
			separator = separatorString;
		}
	}
	
	public static String getSeparatorString() {
		return separator;
	}
	
	private static Integer totalLogFilesUntilNow = 0; 
	private static String runLogFolder = null;
	
	private static String getNextLogNum() {
		++totalLogFilesUntilNow;
		return totalLogFilesUntilNow.toString();
	}	
	
	public static PrintWriter getNewTestCaseThreadLogFile() throws IOException {
		return getNewLogFile(LogConfig.getValue("TestCaseThreadsLogFileSuffix")+MirthLogger.getNextLogNum());
	}
	
	public static PrintWriter getNewMainThreadLogFile() throws IOException {
		return getNewLogFile(LogConfig.getValue("MainLogFileSuffix"));
	}
	
	public static PrintWriter getNewResultsThreadLogFile() throws IOException {
		return getNewLogFile(LogConfig.getValue("ResultsLogFileSuffix"));
	}
	
	public static PrintWriter getNewLogFile(String suffix) throws IOException {
		
		if (Assertions.isNull(runLogFolder)) {
			
			synchronized(MirthLogger.class) {
				String logFolderString = StaticLib.appendFolderSeparatorIfNotPresent(StaticLib.getFrameWrorkFolderWithSeparator() + LogConfig.getValue("LogFolderName"));
				File logFolder = new File(logFolderString);
				if (!logFolder.exists()) {
					logFolder.mkdir();
				}
				
				String runLogFolderString = logFolderString 
									  +StaticLib.getCustomDateTimeNameInFormat(
										DriverScript.executionStartTime,
										LogConfig.getValue("TestCaseLogFolderPrefix"),
										LogConfig.getValue("LogFolderDateTimeFormat")
										);
				
				runLogFolderString = StaticLib.appendFolderSeparatorIfNotPresent(runLogFolderString);				
				
				File runLogFolder = new File(runLogFolderString);
				if (!runLogFolder.exists()){
					runLogFolder.mkdir();
					MirthLogger.runLogFolder = runLogFolderString;
				}
							
			}//if (Assertions.isNull(runLogFolder))
			
			//Here the log folders are created if not created already!!
			}//synchronized(Logger.class) {
			
		//Now, we need to create the file inside log folder and send it back 
		String newLogFileString = runLogFolder + LogConfig.getValue("LogFilePrefix") 
								+ suffix + "." + LogConfig.getValue("logFileExtension");

		File newLogFile = new File(newLogFileString);
		PrintWriter fileWriter = null;
		
		if (!newLogFile.exists()){
			fileWriter = new PrintWriter(new FileWriter(newLogFileString,true),true);
		}
		
		return fileWriter;		
	}//public static File getNewLogFile(String suffix)
	
	public static void log(String data) throws IOException {		
		
		StackTraceElement stackTrace =  Thread.currentThread().getStackTrace()[2];
		writeLog(stackTrace,data);
		//System.out.println("==> "+" [Method: " +  stackTrace.getMethodName() + "()]");
	}//public static void log(String data) 
	
	private static void writeLog(StackTraceElement stackTrace,String data) throws IOException{
		
		PrintWriter logWriter = MirthLogger.getCorrectLogWriter();
		String logString = "#" 
					   + separator + TimeLib.getDateTimeInFormat(LogConfig.getValue("LoggerDateTimeFormat"))
					   + separator + stackTrace.getClassName() + "." +  stackTrace.getMethodName() + "()"
					   + separator + "Line: " + stackTrace.getLineNumber()
					   + separator + data;
	
		logWriter.println(logString);

	}
	
	public static void methodEntry() throws IOException{
		
		StackTraceElement stackTrace =  Thread.currentThread().getStackTrace()[2];
		String data = "Method Entry";
		writeLog(stackTrace, data);
	}
	
	public static void methodExit() throws IOException{
		StackTraceElement stackTrace =  Thread.currentThread().getStackTrace()[2];
		String data = "Method Exit";
		writeLog(stackTrace, data);
	}

	
	private static PrintWriter getCorrectLogWriter() {
		return ThreadController.getLogFileOfThread(Thread.currentThread().getId());
	}//private static File getCorrectLogFile()
	

	
	
}
