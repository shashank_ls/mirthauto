package com.mirth.controller.objects;

public enum SeleniumLocator {
		
	XPath,ID,LinkText,Name,CssSelctor,PartialLinkText,ClassName,TagName;

}
