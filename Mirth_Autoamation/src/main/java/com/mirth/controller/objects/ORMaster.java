package com.mirth.controller.objects;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.mirth.exceptions.ORSheetNotPresentForApplicationException;
import com.mirth.properties.ObjectConfig;
import com.mirth.utility.general.StaticLib;
import com.mirth.utility.spreadsheet.MicrosoftSpreadsheet;

public class ORMaster {

	//Master xpath Map
	private static final Map<String,String> masterOR = new LinkedHashMap<String,String>();
	private static String orSpreadsheetPath;
	
	private static final String valueDelimeter = ":#:";
	private static final String keyDelimeter = "";
	
	public static String[] getSelectorAndValueWithSeparator(String applicationName, String pageName,
			String objectName) {
	
		System.out.println(applicationName+"-"+pageName+"-"+objectName);
		return masterOR.get(applicationName+keyDelimeter+pageName+keyDelimeter+objectName).split(valueDelimeter);
	}

	public static SeleniumLocator getSelector(String applicationName, String pageName,
			String objectName) {

		return SeleniumLocator.valueOf(getSelectorAndValueWithSeparator(applicationName,pageName,objectName)[0]);
	}
	
	public static String getObjectValue(String applicationName, String pageName,
			String objectName) {

		return getSelectorAndValueWithSeparator(applicationName,pageName,objectName)[1];
	}		
	
	
	public static WebElement getObject(WebDriver driver,WebElement sourceElement,SeleniumLocator locatorType,String locatorString) {
		
		WebElement element = null;
		
		if (locatorType == SeleniumLocator.XPath) {				
			if (sourceElement == null) element = driver.findElement(By.xpath(locatorString));
			else element = sourceElement.findElement(By.xpath(locatorString));
		}
		
		else if (locatorType == SeleniumLocator.ID) {
			if (sourceElement == null) element = driver.findElement(By.id(locatorString));
			else element = sourceElement.findElement(By.id(locatorString));
		}
		
		else if (locatorType == SeleniumLocator.Name) {
			if (sourceElement == null) element = driver.findElement(By.name(locatorString));
			else element = sourceElement.findElement(By.name(locatorString));
		}
		
		else if (locatorType == SeleniumLocator.CssSelctor) {
			if (sourceElement == null) element = driver.findElement(By.cssSelector(locatorString));
			else element = sourceElement.findElement(By.cssSelector(locatorString));
		}
		
		else if (locatorType == SeleniumLocator.LinkText) {
			if (sourceElement == null) element = driver.findElement(By.linkText(locatorString));
			else element = sourceElement.findElement(By.linkText(locatorString));
		}
		
		else if (locatorType == SeleniumLocator.PartialLinkText) {
			if (sourceElement == null) element = driver.findElement(By.partialLinkText(locatorString));
			else element = sourceElement.findElement(By.partialLinkText(locatorString));
		}
		
		else if (locatorType == SeleniumLocator.ClassName) {
			if (sourceElement == null) element = driver.findElement(By.className(locatorString));
			else element = sourceElement.findElement(By.className(locatorString));
		}
		
		else if (locatorType == SeleniumLocator.TagName) {
			if (sourceElement == null) element = driver.findElement(By.tagName(locatorString));
			else element = sourceElement.findElement(By.tagName(locatorString));
		}	

		
		return element;
	}
	
	public static WebElement getObject(WebDriver driver,SeleniumLocator locatorType,String locatorString) {
		if (driver == null){
			throw new NullPointerException();
		}
		
		return getObject(driver,null, locatorType, locatorString);
	}
	
	public static WebElement getObject(WebElement sourceElement,SeleniumLocator locatorType,String locatorString) {
		if (sourceElement == null){
			throw new NullPointerException();
		}
		
		return getObject(null,sourceElement, locatorType, locatorString);
	}
	
	public static WebElement getObject(WebDriver driver, WebElement sourceElement,String applicationName, String pageName,String objectName) throws Exception {
 		
		String[] selectorValueWithSeparator = getSelectorAndValueWithSeparator(applicationName,pageName,objectName);
		SeleniumLocator locator =  getSelector(applicationName,pageName,objectName);
		String objectValue =  selectorValueWithSeparator[1];
		WebElement element = getObject(driver,sourceElement,locator,objectValue);
		
		return element;
		
	}
	
	public static WebElement getObject(WebDriver driver,String applicationName, String pageName,String objectName) throws Exception {
		if (driver == null){
			throw new NullPointerException();
		}
		
		return getObject(driver, null, applicationName, pageName, objectName);
	}
	
	public static WebElement getObject(WebElement sourceElement,String applicationName, String pageName,String objectName) throws Exception {
		if (sourceElement == null){
			throw new NullPointerException();
		}
		
		return getObject(null, sourceElement, applicationName, pageName, objectName);
	}
	
	public static synchronized void addAppObjectsToRepo(String applicationName,boolean ignore) throws IOException, ORSheetNotPresentForApplicationException, IllegalAccessException, IllegalArgumentException {
		
		//This method runs based on following assumption
		/*
		col=2 will have page start
		col=2 will have corresponding page end
		col=2 will have object name
		col=3 will have selector
		col=4 will have object value		
		*/
		
		//if Object Repository is NOT readbefore, then read it!!
		if (orSpreadsheetPath == null) {			
			String orFolderPathWithSeparator = StaticLib.appendFolderSeparatorIfNotPresent(StaticLib.getFrameWorkPackageWithSeparator() 
												+ ObjectConfig.getValue("ObjectRepositoryFolderName"));
			ORMaster.orSpreadsheetPath = orFolderPathWithSeparator + ( ObjectConfig.getValue("ObjectRepositoryFileName") );			
		}
		
		MicrosoftSpreadsheet objectRepository = new MicrosoftSpreadsheet(ORMaster.orSpreadsheetPath);
		
		if (objectRepository.isSheetPresent(applicationName)) {
			objectRepository.swithchToSheet(applicationName);
		}
		else {
			
			if (ignore){ return; }
				
			throw new ORSheetNotPresentForApplicationException(applicationName);
		}
		
		
		int maxRows = objectRepository.getMaxRows();
		
		//iterate through the entire page and collect object in the Map
		RowbyRowIterateForLoop:
		for (int row=1;row<=maxRows;row++) {
			
			//If the row is NOT a Start Page, skip it!!
			String rowValue = objectRepository.readCell(row, 2);
			if (rowValue==null || !rowValue.trim().equalsIgnoreCase(ObjectConfig.getValue("StartPageH"))) {
				continue RowbyRowIterateForLoop;
			}
			
			String pageName = objectRepository.readCell(row, 3).trim();
			//If page name is blank, then ignore that page.
			if (pageName == null || pageName.equalsIgnoreCase("")) {
				continue RowbyRowIterateForLoop;
			}
			
			//skip current row!!!
			row++; //this row is the 2nd heading row
			
			String objectVal = null;
			ObjectsReadLoop:
			do{
				 row++; //go to next actual row
				 objectVal = objectRepository.readCell(row, 2);
				
				//If blank values in between objects, then ignore
				while(objectVal==null || objectVal.trim().equalsIgnoreCase("")) {
					row++;
					if (row>maxRows) {
						break RowbyRowIterateForLoop;
					}
					
					objectVal = objectRepository.readCell(row, 2);
				}//while(objectVal==null || objectVal.trim().equalsIgnoreCase(ObjectConfig.getValue("")))
				
				if (objectVal.trim().equalsIgnoreCase(ObjectConfig.getValue("EndPageH"))) {
					break ObjectsReadLoop;
				}
				
				//Here, the actual reading of objects takes place
				String key = applicationName+keyDelimeter+pageName+keyDelimeter+objectVal.trim();
				String value = objectRepository.readCell(row, 3).trim() + valueDelimeter + objectRepository.readCell(row, 4).trim();
				masterOR.put(key, value);
				
			}while(true); //breaking out of this will happen inside the loop						
			
		}//RowbyRowIterateForLoop:	for (int row=1;row<=maxRows;row++) {
		
		objectRepository.closeXl();
		
	}
	
	public static void printAllContentsOfORMap() {
		
		for(String key: masterOR.keySet()) {
			System.out.println(key + "="+ masterOR.get(key));
		}
		
		System.out.println(masterOR.size());
		
	}
	
	
}
