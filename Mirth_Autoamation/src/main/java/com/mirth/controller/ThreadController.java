package com.mirth.controller;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.mirth.dataObjects.HeadingController;
import com.mirth.dataObjects.TestInventoryData;
import com.mirth.dataObjects.ThreadData;
import com.mirth.utility.microsoftDocX.TextImageDoc;


public class ThreadController {
	
	public static final List<Thread> allThreads = new ArrayList<Thread>();
	private static final Map<Long,ThreadData> threadDataMap = new HashMap<Long,ThreadData>();
		

	public static synchronized void removeThreadDriverMapping (Long threadID) {		
		threadDataMap.get(threadID).driverController = null;		
	}
	
	public static synchronized void removeThreadInventoryMapping (Long threadID) {		
		threadDataMap.get(threadID).testInventoryData = null;	
	}
	
	public static boolean isThreadDataMapEmpty() {
		if (threadDataMap==null) {
			return true;
		}
		
		for(Long id: threadDataMap.keySet()) {
			if (threadDataMap.get(id).driverController!=null) {
				return false;
			}
		}
		
		return true;
	}
	
	//use this only if you want to update existing thread to new DriverController.
	public static synchronized void updateThreadDriverMapping(Long threadID, DriverController cDriver) {
		
		ThreadData threadData = getThreadData(threadID);
		threadData.driverController = cDriver;				
	}
	
	public static synchronized void updateThreadLogMapping(Long threadID, PrintWriter logWriter) {	
		ThreadData threadData = getThreadData(threadID);
		threadData.printWriter = logWriter;						
	}
	
	public static synchronized void updateThreadImageDocMapping(Long threadID, TextImageDoc textImageDoc) {	
		ThreadData threadData = getThreadData(threadID);
		threadData.textImageDoc = textImageDoc;						
	}
	
	public static synchronized void updateThreadInventoryMapping(TestInventoryData tInventory) {
		ThreadData threadData = getThreadData(Thread.currentThread().getId());
		threadData.testInventoryData = tInventory;			
	}
	
	public static synchronized DriverController getDriverControllerForThread (Long threadID) {		
		ThreadData threadData = getThreadData(threadID);
		return threadData.driverController;			
	}

	public static synchronized TestInventoryData getInventoryOfThread (Long threadID) {		
		ThreadData threadData = getThreadData(threadID);
		return threadData.testInventoryData;			
	}
	
	public static synchronized TestInventoryData getInventoryOfCurrentThread () {		
		ThreadData threadData = getThreadData(Thread.currentThread().getId());
		return threadData.testInventoryData;			
	}
	
	public static synchronized String getAppNameOfCurrentThread () {		
		ThreadData threadData = getThreadData(Thread.currentThread().getId());
		return threadData.testInventoryData.appName;			
	}
	
	
	public static synchronized PrintWriter getLogFileOfThread (Long threadID) {		
		ThreadData threadData = getThreadData(threadID);
		return threadData.printWriter;			
	}
	
	public static synchronized TextImageDoc getTextImageDocOfThread (Long threadID) {		
		ThreadData threadData = getThreadData(threadID);
		return threadData.textImageDoc;	
	}
	
	/*
	public static synchronized Map<String,String> getStringMapOfCurrentThread () {		
		ThreadData threadData = getThreadData(Thread.currentThread().getId());
		return threadData.dataMap;	
	}
	
	public static synchronized Map<String,Object> getObjectMapOfCurrentThread () {		
		ThreadData threadData = getThreadData(Thread.currentThread().getId());
		return threadData.objMap;	
	}	
	*/
	
	public static synchronized void updateThreadStringMap(long id,
			HashMap<String, String> stringMap) {	
		
		ThreadData threadData = getThreadData(id);
		threadData.dataMap = stringMap;
		
	}

	public static synchronized void updateThreadObjectMap(long id,
			HashMap<String, Object> objMap) {

		ThreadData threadData = getThreadData(id);
		threadData.objMap = objMap;

	}

	public static synchronized void removeThreadStringMap(long id) {
		threadDataMap.get(id).dataMap = null;
	}

	public static synchronized void removeThreadObjectMap(long id) {
		threadDataMap.get(id).objMap = null;		
	}
	
	public static synchronized ThreadData getThreadData(long id) {
		
		ThreadData threadData = threadDataMap.get(id);
		if (threadData == null) {
			threadData = new ThreadData();
			threadDataMap.put(id, threadData);
		}		
		return threadData;		
	}
	
	public static synchronized void updateThreadVerifyMap(long id,
			Verification verify) {	
		
		ThreadData threadData = getThreadData(id);
		threadData.verify = verify;		
	}
	
	public static synchronized void updateCurrentThreadVerifyMap(Verification verify) {	
		
		ThreadData threadData = getThreadData(Thread.currentThread().getId());
		threadData.verify = verify;		
	}
	
	public static synchronized Verification getVerifyOfThread (Long threadID) {		
		ThreadData threadData = getThreadData(threadID);
		return threadData.verify;			
	}
	
	public static synchronized Verification getVerifyOfCurrentThread () {		
		ThreadData threadData = getThreadData(Thread.currentThread().getId());
		return threadData.verify;			
	}
	
	public static synchronized void removeVerifyOfCurrentThread () {		
		ThreadData threadData = getThreadData(Thread.currentThread().getId());
		threadData.verify = null;			
	}
	
	public static synchronized HeadingController getHeadingControllerOfCurrentThread () {		
		ThreadData threadData = getThreadData(Thread.currentThread().getId());
		return threadData.headingController;			
	}
	
	
}
