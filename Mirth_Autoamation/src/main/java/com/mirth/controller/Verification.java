package com.mirth.controller;

import java.io.IOException;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;

import com.mirth.dataObjects.HeadingController;
import com.mirth.dataObjects.TestCaseResultData;
import com.mirth.results.QAProof;

public class Verification {

	private static String noItemString = "";
	TestCaseResultData resultData;
	String itemCurrentlyBeingVerified = noItemString;
	HeadingController headingController = ThreadController.getHeadingControllerOfCurrentThread();
	private String headingStringPrefix = "Verifying ";
	private String headingStringSuffix = ": ";
	private String headingStringColorFormatForProof = "0000FF";
	public boolean isRelaventHeadingWritten=false;

	
	public Verification(TestCaseResultData data) {
		resultData = data;
	}
	
	public Verification withReport(String item){
		
		if (Assertions.isNull(item)) {
			item = "None";
		}
		
		itemCurrentlyBeingVerified = item;
		return this;
		 
	}

	
	public boolean isSame(String expected, String actual,boolean isCaseSensitive) throws InvalidFormatException, IOException {
		
		boolean isResultPass = Assertions.isSame(expected, actual,isCaseSensitive);
		reportResult(expected,actual,isResultPass);
		return isResultPass;
	}

	private void reportResult(String expected,String actual,boolean result) throws InvalidFormatException, IOException {
		if (result) {
			reportSuccess(expected,actual);
		} 
		else {
			reportIssue(expected,actual);
		}		
	}

	public void reportSuccess(String expected,String actual) throws IOException, InvalidFormatException {
		
		String successString = itemCurrentlyBeingVerified + " Expected: " + expected + "\n" 
				+ itemCurrentlyBeingVerified + " Actual: " + actual;
		
		this.reportSuccess(successString);
				
	}
	
	public void reportSuccess(String successString) throws IOException, InvalidFormatException {
		
		if (!headingController.isProofHeadingWritten()){
			String heading = headingStringPrefix+headingController.getVerifyingText()+headingStringSuffix;
			QAProof.writeText(heading,headingStringColorFormatForProof);
			MirthLogger.log(heading);
			headingController.setIsProofHeadingWritten(true);
		}
		
		
		QAProof.writeText(successString);
		MirthLogger.log(successString);

		//Mandatory Last Line
		resetMutableValues();		
	}

	
	public void reportMiscData(String miscData) throws IOException {
		
		resultData.reportMiscData(miscData);	
				
		//Mandatory Last Line
		resetMutableValues();
	}
	
	private void resetMutableValues() {
		itemCurrentlyBeingVerified = noItemString;
	}


	public void reportIssue(String expected,String actual) throws InvalidFormatException, IOException {
		
		String issueString = itemCurrentlyBeingVerified + " Expected: " + expected + "\n" 
							 + itemCurrentlyBeingVerified + " Actual: " + actual;
		
		reportIssue(issueString);
		
	}//public void reportIssue(String expected,String actual)
	

	public void reportIssue(String issueString) throws InvalidFormatException, IOException{
		
		resultData.setResult(false);
		
		if (!headingController.isIssueHeadingWritten()){
			resultData.reportIssue(headingController.getVerifyingText(),issueString);
			headingController.setIsIssueHeadingWritten(true);
		}
		else {
			resultData.reportIssue(issueString);
		} 		
			
		
		//Mandatory Last Line
		resetMutableValues();
	}//public void reportIssue(String expected,String actual)

	
	
	
	//------------------Comparator functions that needs to be used as an when required---------------------
	/*
	public void isSame(long expected, long actual) {
		boolean isResultPass = Assertions.isSame(expected, actual);
		
	}
	
	public void isSame(double expected, double actual) {
		boolean isResultPass = Assertions.isSame(expected, actual);
	}

	public void isSame(double expected, long actual) {
		boolean isResultPass = Assertions.isSame(expected, actual);
	}

	public void isSame(long expected, double actual) {
		boolean isResultPass = Assertions.isSame(expected, actual);
	}

	public void isSame(boolean expected, boolean actual) {
		boolean isResultPass = Assertions.isSame(expected, actual);
	
	}
	
	public void isSame(Object objA,Object objB) {
		boolean isResultPass = Assertions.isSame(objA, objB);
	}

	public void isSame(String expected,Long actual, boolean isCaseSensitive) {
		boolean isResultPass = Assertions.isSame(expected, actual,isCaseSensitive);
	}
	
	public void isSame(Long expected,String actual, boolean isCaseSensitive) {
		boolean isResultPass = Assertions.isSame(expected, actual,isCaseSensitive);
	}
	
	public void isSame(String expected,Double actual, boolean isCaseSensitive) {
		boolean isResultPass = Assertions.isSame(expected, actual,isCaseSensitive);
	}
	
	public void isSame(Double expected,String actual, boolean isCaseSensitive) {
		boolean isResultPass = Assertions.isSame(expected, actual,isCaseSensitive);
	}

	public void isSame(String expected,Boolean actual, boolean isCaseSensitive) {
		boolean isResultPass = Assertions.isSame(expected, actual,isCaseSensitive);
	}
	
	public void isSame(Boolean expected,String actual, boolean isCaseSensitive) {
		boolean isResultPass = Assertions.isSame(expected, actual,isCaseSensitive);
	}

	public void isSame(String expected,long actual) {
		boolean isResultPass = Assertions.isSame(expected, actual);
	}
	
	public void isSame(long expected,String actual) {
	
		boolean isResultPass = Assertions.isSame(expected, actual);
	}
	
	public void isSame(String expected,double actual) {
		boolean isResultPass = Assertions.isSame(expected, actual);
	}
	
	public void isSame(double expected,String actual) {
		boolean isResultPass = Assertions.isSame(expected, actual);
	}


	public void isTrue(boolean expression) {
		boolean isResultPass = Assertions.isTrue(expression);

	}
	

	public void isFalse(boolean expression) {
		boolean isResultPass = Assertions.isFalse(expression);
	}
	
	public void isNull(Object obj) {
		boolean isResultPass = Assertions.isNull(obj);
	
	}
	
	public void isNotNull(Object obj) {
		boolean isResultPass = Assertions.isNotNull(obj);
	
	}
	
	public void isNotEmpty(String str) {
		boolean isResultPass = Assertions.isNotEmpty(str);
	}
	
	public void isNotEmpty(List<? extends Object> c) {
		boolean isResultPass = Assertions.isNotEmpty(c);
	}
	
	public void isNotEmpty(Set<? extends Object> s) {
		boolean isResultPass = Assertions.isNotEmpty(s);
	}
	
	
	public void isNotEmpty(Map<? extends Object,? extends Object> m) {
		boolean isResultPass = Assertions.isNotEmpty(m);	
	}

	public void isNegetive(double num) {
		boolean isResultPass = Assertions.isNegetive(num);
	}
	
	public void isNotNegetive(double num) {
		boolean isResultPass = Assertions.isNotNegetive(num);
	}
	
	public void isPositive(double num) {
		boolean isResultPass = Assertions.isPositive(num);
	}
	
	public void isNotPositive(double num) {
		boolean isResultPass = Assertions.isNotPositive(num);
	}

	public void isZero(double num) {
		boolean isResultPass = Assertions.isZero(num);
	}

	public void isNotZero(double num) {
		boolean isResultPass = Assertions.isNotZero(num);
	}
	
	*/
	
} //class
