package com.mirth.controller;


import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import com.mirth.controller.objects.ORMaster;
import com.mirth.dataObjects.AppAttributesForTestInv;
import com.mirth.dataObjects.ApplicationData;
import com.mirth.dataObjects.Browser;
import com.mirth.dataObjects.ControllerData;
import com.mirth.exceptions.ORSheetNotPresentForApplicationException;
import com.mirth.exceptions.UnknownBrowserException;
import com.mirth.properties.ApplicationConfig;
import com.mirth.properties.ControllerConfig;
import com.mirth.utility.general.StaticLib;

public class TestOrganizer {

	
	private static Map<String,AppAttributesForTestInv> appLocalAttributesMap = new HashMap<String,AppAttributesForTestInv>();
	
	public static AppAttributesForTestInv getActualApplicationAttributeForApp(String appName) {
		return appLocalAttributesMap.get(appName);
	}
	
	//General Attributes
	private static int numberOfThreads;
	public static Browser defaultBrowser;
	public static boolean isOneAppAtATimeExecution;
	public static String runResultPrefix;
	public static String resultReportType;
	
	//Global Application Attributes	
	private static boolean isModuleExecutionOnlyGlobal = false;
	private static boolean isExecFlagForModule = false;
	private static boolean isPriorityBasedExecutionGlobal = false;
	private static boolean isPriorityBasedModuleExecutionGlobal = false;
	private static boolean isTakeQAProofGlobal = false;	
		
	public static int getNumberOfThreads() {
		return numberOfThreads;
	}
	
	//Start execution
	public static void organizeAndUpdateTestInventory() throws UnknownBrowserException, IllegalAccessException, IllegalArgumentException, IOException, ORSheetNotPresentForApplicationException {
		MirthLogger.methodEntry();
		//Get the general attributes--------
		String noOfThreadsString =  ControllerData.getGeneralAttributesMap().get(ControllerConfig.NumberOfThreadsSH);
		numberOfThreads = Integer.parseInt(noOfThreadsString);
		
		String generalBrowser = ControllerData.getGeneralAttributesMap().get(ControllerConfig.DefaultBrowserSH);
		defaultBrowser = StaticLib.getBrowserFromString(generalBrowser);
		
		String oneAppAtATime = ControllerData.getGeneralAttributesMap().get(ControllerConfig.OneAppAtATimeSH);
		isOneAppAtATimeExecution = oneAppAtATime.equalsIgnoreCase(ControllerConfig.Yes)?true:false;
		
		runResultPrefix = ControllerData.getGeneralAttributesMap().get(ControllerConfig.RunNameSH);;
		resultReportType = ControllerData.getGeneralAttributesMap().get(ControllerConfig.ResultReportType);;
		//--------------------------------------
				
		//Get the global application attributes.
		String moduleExecutionOnlyStringGlobal = ControllerData.getGlobalApplicationAttributesMap().get(ControllerConfig.ModuleExecutionOnlySH);
		if ( moduleExecutionOnlyStringGlobal!=null && moduleExecutionOnlyStringGlobal.equalsIgnoreCase(ControllerConfig.Yes) ) {
			isModuleExecutionOnlyGlobal = true;
		}
		
		String considerExecFlagForModuleStringGlobal = ControllerData.getGlobalApplicationAttributesMap().get(ControllerConfig.ExecFlagDuringModuleExecSH);
		if ( considerExecFlagForModuleStringGlobal!=null && considerExecFlagForModuleStringGlobal.equalsIgnoreCase(ControllerConfig.Yes) ) {
			isExecFlagForModule = true;
		}
		
		String priorityBasedExecutionStringGlobal = ControllerData.getGlobalApplicationAttributesMap().get(ControllerConfig.PriorityBasedExecutionSH);
		if ( priorityBasedExecutionStringGlobal!=null && priorityBasedExecutionStringGlobal.equalsIgnoreCase(ControllerConfig.Yes) ) {
			isPriorityBasedExecutionGlobal = true;
		}
		
		String considerPriorityForModuleStringGlobal = ControllerData.getGlobalApplicationAttributesMap().get(ControllerConfig.PriorityDuringModuleExecutionSH);
		if ( considerPriorityForModuleStringGlobal!=null && considerPriorityForModuleStringGlobal.equalsIgnoreCase(ControllerConfig.Yes) ) {
			isPriorityBasedModuleExecutionGlobal = true;
		}
		
		String takeQAProofStringGlobal = ControllerData.getGlobalApplicationAttributesMap().get(ControllerConfig.TakeQAProofSH);
		if ( takeQAProofStringGlobal!=null && takeQAProofStringGlobal.equalsIgnoreCase(ControllerConfig.Yes) ) {
			isTakeQAProofGlobal = true;
		}
		
		//-----------------------------------------------------
		
		
		//start looping through the applications
		for (String appToRun: ControllerData.getAppsToRunList()) {
					
			AppAttributesForTestInv appAttributeActual = new AppAttributesForTestInv();
			appAttributeActual.isModuleExecutionOnlyActual = TestOrganizer.isModuleExecutionOnlyGlobal;
			appAttributeActual.isExecutionFlagForModuleActual = TestOrganizer.isExecFlagForModule;
			appAttributeActual.isPriorityBasedExecutionActual = TestOrganizer.isPriorityBasedExecutionGlobal;
			appAttributeActual.isPriorityBasedModuleExecutionActual = TestOrganizer.isPriorityBasedModuleExecutionGlobal;
			appAttributeActual.isTakeQAProofActual = TestOrganizer.isTakeQAProofGlobal;
			
			ApplicationData appData = ControllerData.getAppDataMap().get(appToRun);
			
			String overRideString = appData.getLocalApplicationAttributesMap().get(ControllerConfig.OverrideControllerConfigSH);
			boolean isOverRideGlobal = overRideString.equalsIgnoreCase(ControllerConfig.Yes)?true:false;
			
			//If Global application attributes needs to be overwritten
			if (isOverRideGlobal) {
				
				String moduleExecOnlyStringLocal = appData.getLocalApplicationAttributesMap().get(ControllerConfig.ModuleExecutionOnlySH);
				if (!moduleExecOnlyStringLocal.equalsIgnoreCase(ControllerConfig.Default)) {					
					appAttributeActual.isModuleExecutionOnlyActual = moduleExecOnlyStringLocal.equalsIgnoreCase(ControllerConfig.Yes)?true:false;					
				}
				
				String considerExecFlagForModuleStringLocal = appData.getLocalApplicationAttributesMap().get(ControllerConfig.ExecFlagDuringModuleExecSH);
				if (!considerExecFlagForModuleStringLocal.equalsIgnoreCase(ControllerConfig.Default)) {					
					appAttributeActual.isExecutionFlagForModuleActual = considerExecFlagForModuleStringLocal.equalsIgnoreCase(ControllerConfig.Yes)?true:false;					
				}
				
				String priorityBasedExecutionStringLocal = appData.getLocalApplicationAttributesMap().get(ControllerConfig.PriorityBasedExecutionSH);
				if (!priorityBasedExecutionStringLocal.equalsIgnoreCase(ControllerConfig.Default)) {					
					appAttributeActual.isPriorityBasedExecutionActual = priorityBasedExecutionStringLocal.equalsIgnoreCase(ControllerConfig.Yes)?true:false;					
				}
				//
				String priorityBasedModuleExecutionStringLocal = appData.getLocalApplicationAttributesMap().get(ControllerConfig.PriorityDuringModuleExecutionSH);
				if (!priorityBasedModuleExecutionStringLocal.equalsIgnoreCase(ControllerConfig.Default)) {					
					appAttributeActual.isPriorityBasedModuleExecutionActual = priorityBasedModuleExecutionStringLocal.equalsIgnoreCase(ControllerConfig.Yes)?true:false;					
				}
				
				String takeQAProofStringLocal = appData.getLocalApplicationAttributesMap().get(ControllerConfig.TakeQAProofSH);
				if (!takeQAProofStringLocal.equalsIgnoreCase(ControllerConfig.Default)) {					
					appAttributeActual.isTakeQAProofActual = takeQAProofStringLocal.equalsIgnoreCase(ControllerConfig.Yes)?true:false;					
				}	
				
			}
			
			//put the appAttributes to the Map
			appLocalAttributesMap.put(appToRun , appAttributeActual);
			
			//put application details in InventoryManger
			InventoryManager.updateInventoryForApp(appToRun, appAttributeActual);			
			
		}//for (String appToRun: ControllerData.getAppsToRunList())
				
		//This needs to be fixed - Seriously!!!
		ORMaster.addAppObjectsToRepo(ApplicationConfig.getValue("MSO"),true);
		ORMaster.addAppObjectsToRepo(ApplicationConfig.getValue("Results"),true);
		ORMaster.addAppObjectsToRepo(ApplicationConfig.getValue("Match"),true);		
				
		MirthLogger.methodExit();
	}//public static void startExecution() throws UnknownBrowserException	
	
}//Executor (Class)

