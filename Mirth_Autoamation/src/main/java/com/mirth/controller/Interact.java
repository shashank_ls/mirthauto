package com.mirth.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import com.mirth.controller.objects.ORMaster;
import com.mirth.controller.objects.SeleniumLocator;
import com.mirth.dataObjects.SetterDataPair;
import com.mirth.exceptions.ClassPageMappingNotFoundException;
import com.mirth.exceptions.InvalidCheckBoxValueException;
import com.mirth.exceptions.InvalidRadioButtonValueException;
import com.mirth.exceptions.NotATableElementException;
import com.mirth.exceptions.TestDataNotPresentException;
import com.mirth.properties.InteractConfig;
import com.mirth.utility.general.RandomLib;
import com.mirth.utility.general.StaticLib;
import com.mirth.utility.seleniumLib.ButtonS;
import com.mirth.utility.seleniumLib.CheckBoxS;
import com.mirth.utility.seleniumLib.ImageS;
import com.mirth.utility.seleniumLib.LinkS;
import com.mirth.utility.seleniumLib.MultiSelectDropDownS;
import com.mirth.utility.seleniumLib.PlainTextS;
import com.mirth.utility.seleniumLib.RadioButtonS;
import com.mirth.utility.seleniumLib.SeleniumElement;
import com.mirth.utility.seleniumLib.Setter;
import com.mirth.utility.seleniumLib.SimpleDropDownS;
import com.mirth.utility.seleniumLib.TableS;
import com.mirth.utility.seleniumLib.TextBoxS;

public class Interact {
	
	private WebElement webElement = null;
	
	public WebElement webElement(){
		return webElement;
	}
	
	public Interact(WebElement element) {
		webElement = element;		
	}

	public Interact getInteractElement(SeleniumLocator selLocator, String locatorString){
		return new Interact(ORMaster.getObject(webElement,selLocator,locatorString));
	}

	//static method to get the driver
	public static WebDriver getDriver() {
		return ThreadController.getDriverControllerForThread(Thread.currentThread().getId()).getCurrentMirthDriver().getDriver();
	}
	
	//Element manipulators
	public void click() {
		webElement.click();
	}
	
	public boolean isDisplayed(){
		return webElement.isDisplayed();
	}
	
	//TextBox's and Text Area's
	public void clearText() {
		webElement.clear();
	}
	
	public void setText(String data) {
		
		if (data==null){
			return;
		}
		
		this.clearText();
		//System.out.println("Interact -> SetText(String data): "+ data);
		webElement.sendKeys(data);
	}
	
	public void setTextWithoutClearing(String data){
		if (data==null){
			return;
		}
		
		webElement.sendKeys(data);
	}

	public void appendText(String data) {
		
		data = this.getText() + data;
		this.setText(data);
	}	


	public String getText() {
		
		return webElement.getText();		
	}
	
	public String getTextBoxValue() {
		
		return webElement.getAttribute("value");		
	}
	
	public String getButtonText() {
		
		return webElement.getAttribute("value");		
	}
	
	
	//Drop down Elements
	
	public void setDropdownByText(String dropdownText) {
		
		String alreadySelectedOption = null;
		try{
			alreadySelectedOption = this.getDropdownSelectedText();
			
			
		}catch(NoSuchElementException e){
			alreadySelectedOption = null;
		}
		
		if (alreadySelectedOption!=null && dropdownText.equals(alreadySelectedOption)) {
			return;
		}	
		new Select(this.webElement).selectByVisibleText(dropdownText);
	
	}//public void setDropdownByText(String visibleText)
	
	public void setMultiSelectListByText(String[] items) {

		Select selectElement = new Select(this.webElement);
		List<String> alreadySelectedValues = this.getAllDropdownSelectedTextAsList();
		
		
		for (int i=0; i< items.length; i++) {
			
			if (!alreadySelectedValues.contains(items[i])){
				selectElement.selectByVisibleText(items[i]);
			}
			
		}//for (int i=0; i< items.length; i++) 
		
	}
	
	public String getDropdownSelectedText() {
		
			return new Select(this.webElement).getFirstSelectedOption().getText();
	}
	
	public List<String> getAllDropdownSelectedTextAsList() {
		
		List<WebElement> allSelectedElements = new Select(this.webElement).getAllSelectedOptions();
		List<String> allValues = new ArrayList<String>();	
		
		if (allSelectedElements != null) {
			
			for (WebElement element: allSelectedElements) {
				allValues.add(element.getText());
			}//for 

		}//if (allSelectedElements != null)
		
		
		return allValues;
	}
	
	public String getDropdownSelectedOptionValue() {
		return new Select(this.webElement).getFirstSelectedOption().getAttribute("value");
	}
	
	public void setDropdownByElementValue(String value) {
		new Select(this.webElement).selectByValue(value);
	}
	
	public void setMultiSelectListByValue(String[] values) {
		for (int i=0; i< values.length; i++) {
			new Select(this.webElement).selectByValue(values[i]);
		}
	}
	
	public List<String> getAllDropdownSelectedOptionValuesAsList() {
		
		List<WebElement> allSelectedElements = new Select(this.webElement).getAllSelectedOptions();
		List<String> allValues = new ArrayList<String>();	
		
		if (allSelectedElements != null) {
			
			for (WebElement element: allSelectedElements) {
				allValues.add(element.getAttribute("value"));
			}//for 

		}//if (allSelectedElements != null)
		
		
		return allValues;
	}
	
	//CheckBox's and Radio Buttons.
	public void setCheckBox_ON() {
	
		if (!this.getIsCheckBox_ON()) {
			this.click();
		}
		
	}

	public void setCheckBox_OFF() {
		if (this.getIsCheckBox_ON()) {
			this.click();
		}
	}

	public boolean getIsCheckBox_ON() {
		
		if (this.webElement.isSelected()) {
			return true;
		}
		
		return false;
		
	}

	public void setRadioButton_ON() {
		if (!this.getIsCheckBox_ON()) {
			this.click();
		}
	}

	public void setRadioButton_OFF() {
		if (this.getIsCheckBox_ON()) {
			this.click();
		}
	}
	
	public boolean getIsRadioButton_ON() {
		
		if (this.webElement.isSelected()) {
			return true;
		}
		
		return false;
		
	}
	
	public String getImageSource(){
		return this.webElement.getAttribute("src");
	}

	//End - Element manipulators

	//New getData() 
	public String getElementData(Setter setter) throws IOException, NotATableElementException {
		
		String finalStringToReturn = "";
		
		if (setter == Setter.checkBox) {
		
			if (this.getIsCheckBox_ON()) {
				finalStringToReturn =  InteractConfig.getValue("ON");
			}
			else {
				finalStringToReturn =  InteractConfig.getValue("OFF");
			}
			
		}//if (setter == Setter.checkBox) 
		
		else if (setter == Setter.radioButton) {
			
			if (this.getIsCheckBox_ON()) {
				finalStringToReturn =  InteractConfig.getValue("ON");
			}
			else {
				finalStringToReturn =  InteractConfig.getValue("OFF");
			}
			
		}//if (setter == Setter.checkBox) 
		
		
		else if (setter == Setter.singleSelectDropdownText) {
			List<String> stringList = getAllDropdownSelectedTextAsList();
			
			if (stringList == null || stringList.size()<0) {
				finalStringToReturn=  null;
			}
			
			else {
			
				for (int i=0;i<stringList.size();i++) {
					
					if (i!=0){
						finalStringToReturn += InteractConfig.getValue("MultiSelectSeparator");
					}				
					finalStringToReturn += stringList.get(i);
				}//for
				
			}
		}//else if (setter == Setter.dropdownText)
		
		else if (setter == Setter.multiSelectDropdownText) {
			List<String> stringList = getAllDropdownSelectedTextAsList();
			
			if (stringList == null || stringList.size()<0) {
				finalStringToReturn=  null;
			}
			
			else {
			
				for (int i=0;i<stringList.size();i++) {
					
					if (i!=0){
						finalStringToReturn += InteractConfig.getValue("MultiSelectSeparator");
					}				
					finalStringToReturn += stringList.get(i);
				}//for
				
			}
		}//else if (setter == Setter.multiSelectDropdownText)
		
		else if (setter == Setter.textBox) {
			finalStringToReturn =  this.getTextBoxValue();
		}
		
		else if (setter == Setter.button) {
			finalStringToReturn =  this.getButtonText();
		}
		
		else if (setter == Setter.link) {
			finalStringToReturn =  this.getText();
		}
		
		else if (setter == Setter.image) {
			finalStringToReturn =  this.getImageSource();
		}
		
		else { //Assume as plainText
			finalStringToReturn = this.getText();
		}		
		
		return finalStringToReturn;
		
	}
		
	//END - New getData end
	
	
	public void setElementData(Setter actualSetter,String data) throws IOException, InvalidCheckBoxValueException, InvalidRadioButtonValueException {
		
		
		if (actualSetter == Setter.blank){
			return;
		}
		
		//CHECKBOX
		else if (actualSetter == Setter.checkBox) {
						
			if (data.equals(InteractConfig.getValue("ON"))) {
				this.setCheckBox_ON();
			}
			
			else if (data.equals(InteractConfig.getValue("OFF"))){
				this.setCheckBox_OFF();
			}
			
			else {
				throw new InvalidCheckBoxValueException(data);
			}
			
		}
		
		//RADIO BUTTON
		else if (actualSetter == Setter.radioButton) {
			
			if (data.equals(InteractConfig.getValue("ON"))) {
				this.setRadioButton_ON();
			}
			
			else if (data.equals(InteractConfig.getValue("OFF"))){
				this.setRadioButton_OFF();
			}
			
			else {
				throw new InvalidRadioButtonValueException(data);
			}
		}		
		
		//DropDown by Text
		else if (actualSetter == Setter.singleSelectDropdownText) {
			this.setDropdownByText(data);			
		}

	
		//Comma Separated multi select List By Text
		else if (actualSetter == Setter.multiSelectDropdownText) {
			System.out.println("hehe");
			this.setMultiSelectListByText(data.split(Pattern.quote(InteractConfig.getValue("MultiSelectSeparator"))));	
			
		}//else if (patternData.startsWith(InteractConfig.getValue("commaSeparatedList")))
		
		//TextBox   Value
		else if (actualSetter == Setter.textBox) {
			this.setText(data);
		}
		
		//Button  Value (upload file)
				else if (actualSetter == Setter.button) {
					this.setTextWithoutClearing(data);
				}
	
		//Unknown or plainText
		else if (actualSetter == Setter.plainText) {
			this.setText(data);
		}		
		
		else {
			//Here, since we do not know the prefix, we are assuming the entire data to be the actual data.			
			this.setText(data);
		}		
		
	}
	
	
	
	//----------------------------------
	//Actual Setter data play starts here.
	
	//Function to call appropriate setting method
	public static String[] getRawSetterDataPair(String patternData) throws IOException, InvalidRadioButtonValueException, InvalidCheckBoxValueException {

		String setter = InteractConfig.getValue("plainText");
		String data = patternData;	
		
		
		//Blanks
		if (patternData.startsWith(InteractConfig.getValue("blankValue"))) {
			setter = InteractConfig.getValue("blankValue");
			data = "";
		}
		
		//CHECKBOX
		if (patternData.startsWith(InteractConfig.getValue("checkBox"))) {
			setter = InteractConfig.getValue("checkBox");
			data = patternData.replaceFirst(setter, "");	
		}
		
		//RADIO BUTTON
		else if (patternData.startsWith(InteractConfig.getValue("radioButton"))) {
			setter = InteractConfig.getValue("radioButton");
			data = patternData.replaceFirst(setter, "");	
		}
		
		//DropDown by Text
		else if (patternData.startsWith(InteractConfig.getValue("dropDownText"))) {
			setter = InteractConfig.getValue("dropDownText");
			data = patternData.replaceFirst(setter, "");			
		}
		
		//DropDown by Value
		else if (patternData.startsWith(InteractConfig.getValue("dropDownValue"))) {
			setter = InteractConfig.getValue("dropDownValue");
			data = patternData.replaceFirst(setter, "");	
		}
		
		//Unique String
		else if (patternData.startsWith(InteractConfig.getValue("uniqueString"))) {
			setter = InteractConfig.getValue("uniqueString");
			data = patternData.replaceFirst(setter, "");			
		}
		
		//Random Email
		else if (patternData.startsWith(InteractConfig.getValue("randomEmail"))) {
			setter = InteractConfig.getValue("randomEmail");
			data = patternData.replaceFirst(setter, "");		
		}
		
		//Random Number
		else if (patternData.startsWith(InteractConfig.getValue("randomNum"))) {
			setter = InteractConfig.getValue("randomNum");
			data = patternData.replaceFirst(setter, "");		
		}
		
		//Comma Separated multi select List
		else if (patternData.startsWith(InteractConfig.getValue("multiSelectText"))) {
			setter = InteractConfig.getValue("multiSelectText");
			data = patternData.replaceFirst(setter, "");		
		}//else if (patternData.startsWith(InteractConfig.getValue("commaSeparatedList")))
	
		
		else if (patternData.startsWith(InteractConfig.getValue("plainText"))) {
			setter = InteractConfig.getValue("plainText");
			data = patternData.replaceFirst(setter, "");	
		}
		
		else if (patternData.startsWith(InteractConfig.getValue("textBox"))) {
			setter = InteractConfig.getValue("textBox");
			data = patternData.replaceFirst(setter, "");	
			//data = getSetterDataPair(patternData.replaceFirst(setter, ""))[1];			
		}
		
		else if (patternData.startsWith(InteractConfig.getValue("testDataPath"))) {
			setter = InteractConfig.getValue("testDataPath");
			System.out.println(setter);
			data = patternData.replaceFirst(setter, "");	
			//data = getSetterDataPair(patternData.replaceFirst(setter, ""))[1];			
		}
		
		else {
			//Here, since we do not know the prefix, we are assuming the entire data to be the actual data.
			setter = InteractConfig.getValue("plainText");
			data=patternData; //Replace should not be made here
			
		}		
		
		String[] setterData = new String[2];
		setterData[0] = setter;
		setterData[1]=data;
		
		return setterData;
	}//public void setTestData(String patternData) throws IOException
	
	public static Setter getSetter(SeleniumElement selElement){
		
		Setter setter = null;
		
		if (selElement instanceof TextBoxS){
			setter = TextBoxS.setter;
		}
		
		else if (selElement instanceof CheckBoxS){
			setter = CheckBoxS.setter;
		}
		
		else if (selElement instanceof RadioButtonS){
			setter = RadioButtonS.setter;
		}
		
		else if (selElement instanceof SimpleDropDownS){
			setter = SimpleDropDownS.setter;
		}
		
		else if (selElement instanceof MultiSelectDropDownS){
			setter = MultiSelectDropDownS.setter;
		}
		
		else if (selElement instanceof ButtonS){
			setter = ButtonS.setter;
		}
		
		else if (selElement instanceof TableS){
			setter = TableS.setter;
		}
		
		else if (selElement instanceof LinkS){
			setter = LinkS.setter;
		}
		
		else if (selElement instanceof ImageS){
			setter = ImageS.setter;
		}
		
		else { //assume setter is PlainTextS
			setter = PlainTextS.setter;
		}	
		
		return setter;
		
	}
	
	public static String getRunTimeTestData(String setter,String remaining) throws NumberFormatException, IOException {
		
		String data = null;
		
		if (setter.startsWith(InteractConfig.getValue("uniqueString"))) {
			data = StaticLib.getGloballyUniqueStringValue(true);	
		}
		
		//Random Email
		else if (setter.startsWith(InteractConfig.getValue("randomEmail"))) {
			data = StaticLib.getGloballyUniqueEmailValue(InteractConfig.getValue("emailDomain"));
		}
		
		//Random Number
		else if (setter.startsWith(InteractConfig.getValue("randomNum"))) {
			data = RandomLib.getRandomNumString(Integer.parseInt(remaining));
		}
		
		//Blank value
		else if (setter.startsWith(InteractConfig.getValue("blankValue"))) {
			data = "";
		}
		
		else if (setter.startsWith(InteractConfig.getValue("testDataPath"))) {
			data = remaining;
		}
		
		return data;
		
	}//public static Strgn getRunTimeTestData(String setter)
	
	public static SetterDataPair getActualSetterDataPair(SeleniumElement selElement,String testDataString) throws IOException, InvalidRadioButtonValueException, InvalidCheckBoxValueException, IllegalAccessException, IllegalArgumentException, TestDataNotPresentException, ClassPageMappingNotFoundException {
		Setter actualSetter = getSetter(selElement); 
		String data = null;
		//Start finding actual setter and Data
		String[] setterDataPair = Interact.getRawSetterDataPair(testDataString);
		System.out.println(actualSetter +" inside getactualsetterdata: "+testDataString+"----> "+setterDataPair[0]);
		data = getRunTimeTestData(setterDataPair[0],setterDataPair[1]);
		
		if (data ==null) {
			
			//If control reaches here, then setter might be specified
			data = setterDataPair[1];
			
			if (setterDataPair[0].equals(InteractConfig.getValue("textBox"))) {
				
				String[] secondPair = Interact.getRawSetterDataPair(data);
				
				if (secondPair[0]!=InteractConfig.getValue("plainText")){
					data = getRunTimeTestData(secondPair[0], secondPair[1]);
				}
				
			}// else if (setterDataPair[0]==InteractConfig.getValue("textBox")) {
			
		}//if (data ==null) {
		
		//END - Finished with actual setter and Data
		
		SetterDataPair setDatPair = new SetterDataPair();
		setDatPair.setter = actualSetter;
		setDatPair.data = data;
		
		System.out.println(actualSetter +"<-------------------------------->" +data);
		
		return setDatPair;
	}
	
}//Class
