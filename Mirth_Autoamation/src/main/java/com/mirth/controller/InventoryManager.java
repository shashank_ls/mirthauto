package com.mirth.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import com.mirth.dataObjects.AppAttributesForTestInv;
import com.mirth.dataObjects.ControllerData;
import com.mirth.dataObjects.ModuleInventoryData;
import com.mirth.dataObjects.TestInventoryData;
import com.mirth.exceptions.UnknownBrowserException;
import com.mirth.properties.ControllerConfig;
import com.mirth.utility.general.StaticLib;

public class InventoryManager {

	//since liveInventoryForAllApps is a Tree map 1 will always get a higher priority
	public static int basePriorityValue=1;
	
	// format for testcaseExecutionStatusMap is==> (appName:AutomationScriptID , false);
	private static Map<String,Boolean> testcaseExecutionStatusMap = new LinkedHashMap<String,Boolean>();
	private static Map<Integer,LinkedHashMap<String,List<TestInventoryData>>> liveInventoryForAllApps = new TreeMap<Integer,LinkedHashMap<String,List<TestInventoryData>>>();	
	
	public static Map<String,Boolean> getTestcaseExecutionStatusMap() {
		return testcaseExecutionStatusMap;
	}
	
	public static Map<Integer,LinkedHashMap<String,List<TestInventoryData>>> getLiveInventoryForAllApps() {
		return liveInventoryForAllApps;
	}
	
	public static void updateInventoryForApp(String appName, AppAttributesForTestInv localAttributes) throws UnknownBrowserException, IOException {
		MirthLogger.methodEntry();
		List<TestInventoryData> appInventoryList =  ControllerData.getAppDataMap().get(appName).getTestInventory();
		List<String> modulesToExecute = new ArrayList<String>();
		boolean isConsiderPriority = localAttributes.isPriorityBasedExecutionActual;
		
		//if moduleExecution only - Collect all moduleID's
		if (localAttributes.isModuleExecutionOnlyActual) {
			
			//add moduleID's to the module Execution List
			for (ModuleInventoryData module: ControllerData.getAppDataMap().get(appName).getModuleInventoryList()) {
				modulesToExecute.add(module.moduleID);
			}
			
			//Set Priority based on ModuleExecutionPriority setting
			isConsiderPriority = localAttributes.isPriorityBasedModuleExecutionActual;		
			
		}					
	
		//Loop through the TestInventoryList and add the relevant tests based on controller spreadsheet.
		TestInventoryLoop:
 		for (int test = 0; test < appInventoryList.size(); test++) {
			
 			TestInventoryData testcase = appInventoryList.get(test);
			
 			//if test case is already added as a part of other module, then skip the processing of it.
			if (testcaseExecutionStatusMap.get(testcase.appName+":"+testcase.automationScriptID) != null) {
				continue TestInventoryLoop;
			}
			
			
			//Start processing of the test cases
			//Is module Execution only??
			if (localAttributes.isModuleExecutionOnlyActual) {
				
				boolean isModuleTestCase = false;
				
				//Check if the test case belongs any module that needs to run
				ModuleCheckLoop:
				for(String moduleID: modulesToExecute) {
					List<String> moduleList = testcase.belongsToModulesList;
					if (moduleList!=null && moduleList.contains(moduleID)) {
						isModuleTestCase = true;
						break ModuleCheckLoop;
					}
				}//for(String moduleID: testcase.belongsToModulesList)				
				
				//if this is not a moudle testcase, then skip this test case;
				if (!isModuleTestCase) {
					continue TestInventoryLoop;
				}	
				
				//Execution flag during moduleExecution
				//if we need to consider execution flag during module execution and if execFlag is false, then skip this test.
				if (localAttributes.isExecutionFlagForModuleActual && (!testcase.isExecutionFlag) ) {
					continue TestInventoryLoop;
				}
				
				/*if control reaches here, then executionFlag should be set to Yes as we are sure at this point 
				 * that this test case needs to be executed regardless of value in Execution flag. So, if reagrdless of
				 * the current executionFlag value, set it to true.
				 */			
				testcase.isExecutionFlag = true;
				
			}//if (localAttributes.isModuleExecutionOnlyActual)
			
			
			/*When the control reaches here, if execution flag is false, then that test case should not be executed as the 
			 * module execution settings are already handled and execution flag is set to true based on it wherever 
			 * necessary.
			 */
			
			//If execution flag is NOT set to "Yes", then skip this test.
			if (!testcase.isExecutionFlag) {
				continue TestInventoryLoop;
			}
				
			
			//If control reaches here, then we need to execute the test for sure.
			
			//Add QA proof boolean data to TestInventory
			if (testcase.qaProofNeeded.equalsIgnoreCase(ControllerConfig.Default)) {
				testcase.isQAProofNeed = localAttributes.isTakeQAProofActual;
			}
			
			else if (testcase.qaProofNeeded.equalsIgnoreCase(ControllerConfig.Yes)){
				testcase.isQAProofNeed = true;
			}
			
			else {
				testcase.isQAProofNeed = false;
			}
			//QA proof Setting collected in TestInventoryData
			
			//Update browser value of testcase if pointed to default
			if (testcase.browserString.equalsIgnoreCase(ControllerConfig.Default)) {
				testcase.browser = TestOrganizer.defaultBrowser;
			}
			
			else  {
				testcase.browser = StaticLib.getBrowserFromString(testcase.browserString);
			}				
			
			//----
				// Check depends on, Runs alone, Don't run with 
			//----
			
		
			//Finally, add this test to test executable testInventory
			addTestCaseToLiveInventory(testcase, isConsiderPriority);
			
		}//for (int test = 0; test < appInventoryList.size(); test++) {
		MirthLogger.methodExit();		
	}//updateInventoryForApp(String appName, ActualApplicationAttributes localAttributes) {
	
	
	private static void addTestCaseToLiveInventory(TestInventoryData testcase,boolean isConsiderPriority) throws IOException {		
		MirthLogger.methodEntry();
		MirthLogger.log("Adding testcase: " + testcase.automationScriptID +" of Application: "+testcase.appName);
		int insertPriorityVal = basePriorityValue;
		
		if (isConsiderPriority) {
			insertPriorityVal = testcase.priority;
		}
		
		LinkedHashMap<String,List<TestInventoryData>> appsMap = liveInventoryForAllApps.get(insertPriorityVal);
		if (appsMap == null) {
			//if no entry is present then create a Map and add it.
			appsMap = new LinkedHashMap<String,List<TestInventoryData>>();
			liveInventoryForAllApps.put(insertPriorityVal,appsMap);
		}
			
		List<TestInventoryData> testList = appsMap.get(testcase.appName);
		//if no entry is present then create a list and add it.
		if (testList == null) {
			testList = new ArrayList<TestInventoryData>();
			appsMap.put(testcase.appName, testList);
		}
		
		testList.add(testcase);		
		testcaseExecutionStatusMap.put(testcase.appName+":"+testcase.automationScriptID,false);
		MirthLogger.methodExit();

	}//addTestCaseToLiveInventory(TestInventoryData test case,boolean isConsiderPriority)
	
	//--------For deletion.. Testing methods...
	
	public static void printInventory() {
		
		System.out.println("\n\n\n");
			
		Set<Integer> allPriorities = (Set<Integer>) liveInventoryForAllApps.keySet(); 
		for (Integer a:   allPriorities) {
			
			System.out.println("\n\n"+a);
			
			HashMap<String,List<TestInventoryData>> b = liveInventoryForAllApps.get(a);
			Set<String> c = b.keySet();
			for (String d: c) {
				System.out.println("\n"+d);
				
				for (TestInventoryData e: b.get(d)) {
					System.out.print(" \"" + e.automationScriptID + ":"+e.isQAProofNeed + "\" ");
				}
				
			}
			
		}
		System.out.println();
		for (String a: testcaseExecutionStatusMap.keySet()) {
			System.out.println (a + " : " + testcaseExecutionStatusMap.get(a));
		}
		
		System.out.println("Default Browser: " + TestOrganizer.defaultBrowser );
		System.out.println("Number of Threads: " + TestOrganizer.getNumberOfThreads() );
		System.out.println("Run only one app: " + TestOrganizer.isOneAppAtATimeExecution );
		
	}
	
}//class
