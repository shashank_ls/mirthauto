package com.mirth.controller;

import java.io.IOException;
import java.util.Calendar;

import com.mirth.dataObjects.ControllerData;
import com.mirth.dataObjects.ModuleInventoryData;
import com.mirth.dataObjects.TestInventoryData;
import com.mirth.exceptions.NoSuchProductException;
import com.mirth.exceptions.ORSheetNotPresentForApplicationException;
import com.mirth.exceptions.ProductSheetNotPresentException;
import com.mirth.exceptions.UnknownBrowserException;
import com.mirth.properties.ControllerConfig;
import com.mirth.properties.OtherConfig;
import com.mirth.properties.ResultConfig;
import com.mirth.utility.general.StaticLib;

public class DriverScript {
	
	public static Calendar executionStartTime = Calendar.getInstance();
	public static void main(String[] args) {
		System.out.println("Starting DriverScript Now..");
		try {		
				DriverScript.initiateConfigsAndSettings();
				
				//Start Logger.
				ThreadController.updateThreadLogMapping(
													Thread.currentThread().getId(),
													MirthLogger.getNewMainThreadLogFile()
													);
			
				
				MirthLogger.log("Started with DriverScript");
				
				//Config initializations (These config needs external initializations)
				MirthLogger.log("Parsing SpreadSheet data now.");
				DriverScript.parseControllerSpreadsheetData();	
				MirthLogger.log("Parsing succesfull. Organizing and updating Test Inventory");
				//printControllerSheetData();				 
				TestOrganizer.organizeAndUpdateTestInventory();
				MirthLogger.log("Finished with Inventory Update");
				//InventoryManager.printInventory();
				//ORMaster.printAllCo  ntentsOfORMap();
				MirthLogger.log("Starting Test case execution");
				Executor.executeTestCases();	

				//------------
				
		} catch (NullPointerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchProductException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ProductSheetNotPresentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnknownBrowserException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ORSheetNotPresentForApplicationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		finally {
			System.out.println("*****************");
			System.out.println(Thread.getAllStackTraces().keySet());
			System.out.println(ThreadController.allThreads.size());
			System.out.println("\n==> Exceptions Unhandled in public static void main of DriverScript class!!");
			System.out.println("*****************");			

			ThreadController.getLogFileOfThread(Thread.currentThread().getId()).close();
			//Make sure to kill all threads before quitting!
			System.exit(0);
		}//finally	
		
	}//main
	
	private static void initiateConfigsAndSettings() throws IOException {
		
		ControllerConfig.initiateControllerConfig();
		ResultConfig.initiateControllerConfig();
		
		String driverFolder = StaticLib.appendFolderSeparatorIfNotPresent(StaticLib.getFrameWorkPackageWithSeparator()+OtherConfig.getValue("DriverFolderName"));
		
		String ieDriverPath = driverFolder + OtherConfig.getValue("WindowsIEDriverFileName");		
		String winChromeDriverPath = driverFolder + OtherConfig.getValue("WindowsChromeDriverFileName");
		String macChromeDriverPath = driverFolder + OtherConfig.getValue("MacChromeDriverFileName");
		
		if (StaticLib.isThisMachineWindows()) {
			System.setProperty("webdriver.chrome.driver", winChromeDriverPath);
			System.setProperty("webdriver.ie.driver", ieDriverPath);
		}//if (StaticLib.isThisMachineWindows()) 
		
		else { //At this point, only Mac
			System.setProperty("webdriver.chrome.driver", macChromeDriverPath);
		
		}//else => if (StaticLib.isThisMachineWindows())
		
	}//	private static void initiateConfigsAndSettings()

	//Get the SpreadSheet Path from controllerConfig and Read Controller values to controllerData class
	private static void parseControllerSpreadsheetData() throws IllegalArgumentException, IllegalAccessException, IOException, NoSuchProductException, ProductSheetNotPresentException
	{		
		MirthLogger.methodEntry();		
		//Get the SpreadSheet Path from controllerConfig
		String controllerSpreadsheetPath = StaticLib.getFrameWorkPackageWithSeparator();
		controllerSpreadsheetPath += ControllerConfig.ControllerSpreadSheetName;		
		
		//Read Controller values to controllerData class
		Controller.readControllerData(controllerSpreadsheetPath);
		
		MirthLogger.methodExit();
	}
	
	//Test methods only.. Can be deleted after testing.
	public static void printControllerSheetData() throws IllegalArgumentException, IllegalAccessException, IOException, NoSuchProductException, ProductSheetNotPresentException {
			MirthLogger.methodEntry();
			System.out.println(ControllerData.getAppsToRunList());
			System.out.println(ControllerData.getGeneralAttributesMap());
			System.out.println(ControllerData.getGlobalApplicationAttributesMap());
			System.out.println("\nMSO data");
			System.out.println(ControllerData.getAppDataMap().get("MSO").getLocalApplicationAttributesMap());
			
			System.out.println("\nTest Inventory:");
			for (TestInventoryData t: ControllerData.getAppDataMap().get("MSO").getTestInventory()) {			
				System.out.println(t.toString());
			}

			System.out.println("\nModule Inventory:");
			for (ModuleInventoryData t: ControllerData.getAppDataMap().get("MSO").getModuleInventoryList()) {			
				System.out.println(t.toString());
			} 
			
			MirthLogger.methodExit();
	}
	
}//public static void printControllerSheetData()


