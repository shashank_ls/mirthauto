package com.mirth.controller;

import java.io.IOException;
import java.util.HashMap;

import com.mirth.application.master.TestCaseMapping;
import com.mirth.application.master.Testcase;
import com.mirth.application.version2.mso.testcases.MSOTestCaseMapping;
import com.mirth.application.version2.results.testcases.ResultsTestCaseMapping;
import com.mirth.dataObjects.TestCaseResultData;
import com.mirth.dataObjects.TestInventoryData;
import com.mirth.exceptions.TestMappingNotFoundException;
import com.mirth.properties.ApplicationConfig;
import com.mirth.properties.ResultConfig;
import com.mirth.results.QAProof;
import com.mirth.results.ResultWriter;
import com.mirth.utility.general.StaticLib;
import com.mirth.utility.general.TimeLib;

public class TestRunner implements Runnable{
	
	@Override
	public void run() {		
			
			try {
				MirthLogger.log("Starting Test Case thread: " + Thread.currentThread().getId());
				this.startExecution();
				this.doHouseKeeping();	
			} catch (Exception e) {
				//This exception Should be handled
				e.printStackTrace();
				try {
					MirthLogger.log(e.getStackTrace().toString());
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
			
	}
	
	private void startExecution() throws Exception {
		MirthLogger.methodEntry();
		TestInventoryData testInventory = null;
		
		TestInventoryWhileLoop:
		while(true) {
			
				try {			
				
					testInventory = Executor.testContainer.getNextData();
					if (testInventory == null) {
						break TestInventoryWhileLoop;
					}
					
					this.runTest(testInventory);

				} catch (InstantiationException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (ClassNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (TestMappingNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} finally {
					
				}//finally
				
		}//while(true)
		
		MirthLogger.methodExit();
	}//startExecution()
		
	private void runTest(TestInventoryData testInventory) throws Exception {
		MirthLogger.methodEntry();
		ThreadController.updateThreadInventoryMapping(testInventory);
		ThreadController.updateThreadDriverMapping(Thread.currentThread().getId(), new DriverController());
		ThreadController.updateThreadImageDocMapping(Thread.currentThread().getId(), QAProof.getCorrespondingImageDoc(testInventory));
		ThreadController.updateThreadStringMap(Thread.currentThread().getId(),new HashMap<String,String>());
		ThreadController.updateThreadObjectMap(Thread.currentThread().getId(),new HashMap<String,Object>());
		
		
		Testcase test = TestRunner.getTesstCaseClassObject(testInventory.appName, testInventory.automationScriptID);
		final TestCaseResultData resultData = new TestCaseResultData();
		
		//Pass on appropriate data required for reporting.
		resultData.appName = testInventory.appName;
		resultData.startDateTime = TimeLib.getDateTimeInFormat(ResultConfig.StartEndDateTimeFormat);
		resultData.jiraID = testInventory.manualTestID;
		resultData.scriptID = testInventory.automationScriptID;
		resultData.scriptName = testInventory.automationScriptName;
		resultData.belongsToModules = StaticLib.getStringListValuesAsString(testInventory.belongsToModulesList, ",");
		resultData.browserName = testInventory.browser.toString();
		//execute test case
		test.startTestCaseExecution(resultData);
		
		resultData.endDateTime = TimeLib.getDateTimeInFormat(ResultConfig.StartEndDateTimeFormat);
		
		ResultWriter.addTestResult(resultData);
		ThreadController.removeThreadInventoryMapping(Thread.currentThread().getId());
		ThreadController.removeThreadStringMap(Thread.currentThread().getId());
		ThreadController.removeThreadObjectMap(Thread.currentThread().getId());
		MirthLogger.methodExit();
	}

	private static Testcase getTesstCaseClassObject(String appName, String automationScriptID) throws IOException, InstantiationException, IllegalAccessException, ClassNotFoundException, TestMappingNotFoundException {

		Testcase test = null;
		String testcaseCanonicalPathName = null;
		TestCaseMapping testcaseMap = getTestCaseMap(appName);

		
		testcaseCanonicalPathName = testcaseMap.getTestClassRelativeName("TestCasePackagePath");
		String testCaseClassName = testcaseMap.getTestClassRelativeName(automationScriptID);
		testcaseCanonicalPathName += "." + testCaseClassName;
	
		test = (Testcase) Class.forName(testcaseCanonicalPathName).newInstance();
		
		return test;
		
	}	

	private static TestCaseMapping getTestCaseMap(String appName) throws IOException, TestMappingNotFoundException {
		
		TestCaseMapping testcaseMap = null;
		if (appName.equalsIgnoreCase(ApplicationConfig.getValue("MSO"))) {
			testcaseMap = MSOTestCaseMapping.getObject();
		}
		
		else if (appName.equalsIgnoreCase(ApplicationConfig.getValue("Results"))) {
			testcaseMap = ResultsTestCaseMapping.getObject();
		}
		
		else {
			throw new TestMappingNotFoundException(appName);
		}
		
		return testcaseMap;
	}

	private void doHouseKeeping() throws IOException {
		MirthLogger.methodExit();
		ThreadController.removeThreadDriverMapping(Thread.currentThread().getId());		
		ThreadController.allThreads.remove(Thread.currentThread());
		ThreadController.getLogFileOfThread(Thread.currentThread().getId()).close();
		MirthLogger.methodExit();
	}//doHouseKeeping()
}
