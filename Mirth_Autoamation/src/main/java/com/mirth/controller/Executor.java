package com.mirth.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;

import com.mirth.dataObjects.ControllerData;
import com.mirth.dataObjects.SynchronizedDataQueue;
import com.mirth.dataObjects.TestInventoryData;
import com.mirth.results.ResultWriter;
import com.mirth.utility.general.SleepLib;

public class Executor {
	
	//Data Container is thread safe, so no need to worry about thread overwrite
	public static SynchronizedDataQueue<TestInventoryData> testContainer = new SynchronizedDataQueue<TestInventoryData>();
	public static List<TestInventoryData> waitingTests = new ArrayList<TestInventoryData>();
	public static List<String> dontRunTests = new ArrayList<String>();
	
	public static boolean isExecutionCompleted=false;
	
	public static boolean getIsExecutionCompleted() {
		return isExecutionCompleted;
	}
	
	public static void executeTestCases() throws InterruptedException, IOException {
		
		Executor.prepareTestContainer();
		Executor.startTestExecution();
		Executor.houseKeeping();
		
	}
	
	private static void houseKeeping() {
		
		System.out.println("Main Thread: All threads finished!!");
		
	}

	private static void startTestExecution() throws InterruptedException, IOException {
		
		/*Test Data*/
		/*
		ApplicationResultData appResData = new ApplicationResultData();
		
		appResData.appName = "MSO";
		appResData.environment = "Test Environment";
		appResData.buildNumber = "Test 1.12.36";
		appResData.runStart = "Date Time 1";
		appResData.runEnd = "Date Time 2";
		appResData.runID = "Pizza_Dominos";
		
		ResultWriter.addAppResult(appResData);
		*/
		
		
		//Create resultFoldersFirst
		ResultWriter.createRequiredResultFolders();
		
		//Start ResultWriter Thread first
		Thread resultThread = new Thread(ResultWriter.getWriter());
		resultThread.start();		
		
		//for each test in test container
		final int noOfThreads = TestOrganizer.getNumberOfThreads();
		
		
		//Wait for result thread to be ready before starting execution
		while(!ResultWriter.isResultThreadReady()){
			SleepLib.sleep1Second();
		}
		
		
		for (int i=0; i < noOfThreads; i++) {
			Thread newThread = new Thread(new TestRunner());
			ThreadController.updateThreadDriverMapping(newThread.getId(), new DriverController());
			ThreadController.updateThreadLogMapping(newThread.getId(), MirthLogger.getNewTestCaseThreadLogFile()); 	
			
			//Below line may be deleted in future once we are sure that allThreads Object is NOT required.
			ThreadController.allThreads.add(newThread);
			
			newThread.start();			
		}		
		
		//find if the execution is complete every 5 seconds
		while (!ThreadController.isThreadDataMapEmpty()) {
			SleepLib.sleep1Second();
		}
		//If the control reaches here, then all the Threads have completed their task and are dead!!
		
		//Execution completed here.
		Executor.isExecutionCompleted = true;
		while(!ResultWriter.getIsResultWriterFinished()) {
			SleepLib.sleep1Second();
		}

		//If the control reaches here, then all the Threads are dead and results have been written.
		
		System.out.println("Finished Main Thread!!");		
	}

	/*
	public static void printAndEmptyContainer() {
				
		TestInventoryData t = testContainer.getNextData();
		while(t!=null) {
			System.out.println(t);
			t = testContainer.getNextData();
		}
		
	}
*/	
	private static void prepareTestContainer() {
		
		final Map<Integer,LinkedHashMap<String,List<TestInventoryData>>> liveInventoryForAllApps = InventoryManager.getLiveInventoryForAllApps();		
		TreeSet<Integer> prioirtySet = new TreeSet<Integer>(liveInventoryForAllApps.keySet());
		List<String> appsListInOrder = ControllerData.getAppsToRunList();
		
		// if need to run only one application at a time without taking priority across multiple applications
		if (TestOrganizer.isOneAppAtATimeExecution) {
			
			//Loop through each application
			for (String appName: appsListInOrder) {
				
				//Loop through each priority to find if TestInventoryData is present for that priority in this App.
				PriorityForLoopInsideAppForLoop:
				for (int priority : prioirtySet) {
					
					//Check if test cases of this app is present for this priority. 
					List<TestInventoryData> appInventoryForThisPriority = liveInventoryForAllApps.get(priority).get(appName);					
					if (appInventoryForThisPriority == null) {
						continue PriorityForLoopInsideAppForLoop;
					}
					int sizeOfAppInventoryForThisPriority = appInventoryForThisPriority.size();
					
					//put the TestInventoryData inside the testContainer.
					for (int i=0; i< sizeOfAppInventoryForThisPriority; i++) {
						testContainer.putData(appInventoryForThisPriority.get(i));
					}//for (int i=0; i< sizeOfAppInventoryForThisPriority; i++)					
				}//for (int priority : prioirtySet)
			}//for (String appName: appsListInOrder)
			
		}//if (TestOrganizer.isOneAppAtATimeExecution)
		
		
		// if need to run multiple applications at once shot based on priority
		else {

			//for each priority
			for (int priority : prioirtySet)   {
				
				//for each app across each prirority
				AppForLoopInsidePrioirtyForLoop:
				for (String appName: appsListInOrder) {
					
					//Check if test cases of this app is present for this priority. 
					List<TestInventoryData> appInventoryForThisPriority = liveInventoryForAllApps.get(priority).get(appName);
					if (appInventoryForThisPriority == null) {
						continue AppForLoopInsidePrioirtyForLoop;
					}
					int sizeOfAppInventoryForThisPriority = appInventoryForThisPriority.size();
					
					//put the TestInventoryData inside the testContainer.
					for (int i=0; i< sizeOfAppInventoryForThisPriority; i++) {
						testContainer.putData(appInventoryForThisPriority.get(i));
					}//for (int i=0; i< sizeOfAppInventoryForThisPriority; i++)
				} //for (String appName: appsListInOrder) 
			
			}//for (int priority : prioirtySet)
			
		}//else ==>  if(TestOrganizer.isOneAppAtATimeExecution)
		
	}//prepareTestContainer()
	
}
