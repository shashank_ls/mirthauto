package com.mirth.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.mirth.dataObjects.ApplicationData;
import com.mirth.dataObjects.ControllerData;
import com.mirth.dataObjects.ModuleInventoryData;
import com.mirth.dataObjects.ProductSetData;
import com.mirth.dataObjects.TestInventoryData;
import com.mirth.exceptions.NoSuchProductException;
import com.mirth.exceptions.ProductSheetNotPresentException;
import com.mirth.properties.ApplicationConfig;
import com.mirth.properties.ControllerConfig;
import com.mirth.utility.spreadsheet.MicrosoftSpreadsheet;

public class Controller {
	
	private static boolean isControllerNotActive = true;
	
	//Disable the class from ever getting initiated
	private Controller() {
		
	}
	
	public static void readControllerData(String controllerSpreadSheetPath) throws IllegalArgumentException, IllegalAccessException, IOException, NoSuchProductException, ProductSheetNotPresentException {
		MirthLogger.methodEntry();
		if (isControllerNotActive) {
			
			Controller.readControllerSpreadSheetData(controllerSpreadSheetPath);
			isControllerNotActive = false;	
			
		}
		MirthLogger.methodExit();
	}
		
	private static void readControllerSpreadSheetData(String controllerSpreadSheetPath) throws IllegalArgumentException, IOException, 
						IllegalAccessException, NoSuchProductException, ProductSheetNotPresentException {
		
		MirthLogger.methodEntry();
		
		//If the conifg object is empty, then initiate it and read config contents into it.
		if (ControllerConfig.isConfigPropertyNull())  {
			ControllerConfig.initiateControllerConfig();
		}
		
		//Available Product Set
		List<String> productSet = new ArrayList<String>();
		productSet.add(ApplicationConfig.getValue("MSO"));
		productSet.add(ApplicationConfig.getValue("Results"));
		productSet.add(ApplicationConfig.getValue("Mail"));
		productSet.add(ApplicationConfig.getValue("Match"));
		productSet.add(ApplicationConfig.getValue("Care"));
		productSet.add(ApplicationConfig.getValue("Appliance"));
		//-------------------------------------------------------------	
		
		//General attributes List
		List<String> generalAttributes = new ArrayList<String>();
		generalAttributes.add(ControllerConfig.NumberOfThreadsSH);
		generalAttributes.add(ControllerConfig.DefaultBrowserSH);
		generalAttributes.add(ControllerConfig.OneAppAtATimeSH);
		generalAttributes.add(ControllerConfig.RunNameSH);
		generalAttributes.add(ControllerConfig.ResultReportType);
		//-------------------------------------------------------------	
		
		//Application attributes List
		List<String> applicationAttributes = new ArrayList<String>();
		applicationAttributes.add(ControllerConfig.OverrideControllerConfigSH);
		applicationAttributes.add(ControllerConfig.ModuleExecutionOnlySH);
		applicationAttributes.add(ControllerConfig.ExecFlagDuringModuleExecSH);
		applicationAttributes.add(ControllerConfig.PriorityDuringModuleExecutionSH);
		applicationAttributes.add(ControllerConfig.PriorityBasedExecutionSH);
		applicationAttributes.add(ControllerConfig.TakeQAProofSH);
		applicationAttributes.add(ControllerConfig.TestServerSH);
		//-------------------------------------------------------------	
		
		//Browsers List
		List<String> browsersList = new ArrayList<String>();
		browsersList.add(ControllerConfig.Firefox);
		browsersList.add(ControllerConfig.Chrome);
		browsersList.add(ControllerConfig.IE);
		browsersList.add(ControllerConfig.Safari);
		browsersList.add(ControllerConfig.Default);
		//-------------------------------------------------------------	
		
		
		//Point to controller sheet
		String mainControllerSheet = ControllerConfig.MainController;
		MicrosoftSpreadsheet controller = new MicrosoftSpreadsheet(controllerSpreadSheetPath);
		//-------------------------------------------------------------
		
		//Read Product Set 
		//check which products have the execution flags set to Yes put it inside appsToRun List of Controller class
		controller.swithchToSheet(mainControllerSheet);
		int maxNumOfRowsInConfig = controller.getMaxRows();
		ProductSetData productData = new ProductSetData();
		for (int row = 3; row<=maxNumOfRowsInConfig; row++) {
			
			String rowVal = controller.readCell(2,ControllerConfig.TestExecutionFlagH, row);
			
			if (rowVal!=null && rowVal.equalsIgnoreCase(ControllerConfig.Yes)) {

				String product = controller.readCell(2,ControllerConfig.ProductH, row);
				int priority = Integer.parseInt(controller.readCell(2,ControllerConfig.ProductPriorityH, row));
				
				if (priority < 1) {priority =1;}
				//Check if product is a valid one and if NOT throw exception
				
				if (product==null || !productSet.contains(product)) {
					throw new NoSuchProductException(product);
				}
				productData.addProduct(product, priority);
			
			}//if (rowVal.equalsIgnoreCase("Yes"))	
		
		}//Product Set - for (int row = 3; row<=maxNumOfRows; row++)
		//Add the product to the executable List.
		ControllerData.getAppsToRunList().addAll(productData.getSortedProductList());
		//-------------------------------------------------------------
		
		
		//Read General Attributes from controller
		//Capture General Attributes in controllerAttributes Map of ControllerData class				
		for (int row = 3; row<=maxNumOfRowsInConfig; row++) {
			
			String key = controller.readCell(2,ControllerConfig.GeneralAttributeH, row);
			if (key!=null && generalAttributes.contains(key.trim())) {
				
				String value = controller.readCell(2,ControllerConfig.GeneralValueH, row);
				ControllerData.getGeneralAttributesMap().put(key, (value==null)?"":value);	
				
			}//controllerData - General Attributes: if (key!=null && applicationAttributes.contains(key.trim()))
	
		}//controllerData - General Attributes: for (int row = 3; row<=maxNumOfRowsInConfig; row++)
		//-------------------------------------------------------------
		
		
		//Read Application Attributes from controller
		//Capture Application Attributes in controllerAttributes Map of ControllerData class				
		for (int row = 3; row<=maxNumOfRowsInConfig; row++) {
			
			String key = controller.readCell(2,ControllerConfig.ApplicationAttributeH, row);
			if (key!=null && applicationAttributes.contains(key.trim())) {
				
				String value = controller.readCell(2,ControllerConfig.ValueH, row);
				ControllerData.getGlobalApplicationAttributesMap().put(key, (value==null)?"":value);							
			}//controllerData - Application Attributes: if (key!=null && applicationAttributes.contains(key.trim()))
	
		}//controllerData - Application Attributes: for (int row = 3; row<=maxNumOfRowsInConfig; row++)			
		//-------------------------------------------------------------		
		
		
		//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=//
		
		
		//Start collecting Application data based on appsToRun List in controllerData.		
		for (String appName: ControllerData.getAppsToRunList()) {
			ApplicationData appData = new ApplicationData();
			
			//throw Product Sheet NOT present exception when product sheet is NOT present.
			if (!controller.isSheetPresent(appName)) {
				throw new ProductSheetNotPresentException(appName);
			}
			
			controller.swithchToSheet(appName);
			int maxNumOfRowsInProductSheet = controller.getMaxRows();
			
			
			//Capture TestInventory and add it to appData Map of controllerData
			ReadInventoryData:
			for (int row=3;row<=maxNumOfRowsInProductSheet; row++) {
				
				String autoID = controller.readCell(2,ControllerConfig.AutomationScriptIDH, row);
								
				//If automation scriptID is NOT present, then skip the row 
				if (autoID == null  || autoID.equalsIgnoreCase("")) {
					continue ReadInventoryData;
				}
				
				TestInventoryData appInventoryData = new TestInventoryData();
				
				//appInventoryData.appName
				appInventoryData.appName = appName;
				
				//appInventoryData.manualTestID
				appInventoryData.manualTestID = controller.readCell(2,ControllerConfig.ManualTestCaseIDH, row);
				if (appInventoryData.manualTestID == null) {
					appInventoryData.manualTestID = "";
				}
				//----------------------------------
				
				
				//appInventoryData.automationScriptID
				appInventoryData.automationScriptID = autoID;
				if (appInventoryData.automationScriptID == null) {
					appInventoryData.automationScriptID = "";
				}
				//----------------------------------
				
				
				//appInventoryData.automationScriptName
				appInventoryData.automationScriptName = controller.readCell(2,ControllerConfig.AutomationScriptNameH, row);
				if (appInventoryData.automationScriptName == null) {
					appInventoryData.automationScriptName = "";
				}
				//----------------------------------
				
				
				//appInventoryData.isExecutionFlag
				String executionFlag = controller.readCell(2,ControllerConfig.ExecutionFlagH, row);
				if (executionFlag != null && executionFlag.equalsIgnoreCase(ControllerConfig.Yes)) {
					appInventoryData.isExecutionFlag = true;
				}
				else {
					appInventoryData.isExecutionFlag = false;
				}
				//----------------------------------
				
				
				//appInventoryData.browser
				String browser = controller.readCell(2,ControllerConfig.BrowserH, row);
				if (browser != null && browsersList.contains(browser)) {
					appInventoryData.browserString = browser;
				}
				else {
					appInventoryData.browserString =  ControllerConfig.Default;
				}
				//----------------------------------
				
				
				//appInventoryData.priority
				String intPriority = controller.readCell(2,ControllerConfig.PriorityH, row);
				if (intPriority!=null) {
					
					try {
						int priority = Integer.parseInt(intPriority);
						
						if (priority<1) {
							priority = 1;
						}
						
						appInventoryData.priority = priority;
						
					} catch (Exception parseException) {
						appInventoryData.priority = 1;
					}//catch (Exception parseException)
					
				}//if (intPriority!=null) 
				//----------------------------------
				
							
				//appInventoryData.qaProofNeeded
				appInventoryData.qaProofNeeded = controller.readCell(2,ControllerConfig.QAProofH, row);
				//---------------------------------
							
				
				//appInventoryData.belongsToModulesList
				String moduleMappingList = 	controller.readCell(2,ControllerConfig.ModuleMappingH, row);			
				if (moduleMappingList!=null && !moduleMappingList.equalsIgnoreCase("")) {
					
					try {
						
						String[] modules = moduleMappingList.split(",");
						
						appInventoryData.belongsToModulesList = new ArrayList<String>();
						
						for (String module: modules) {
							appInventoryData.belongsToModulesList.add(module);
						}
						
					}catch(Exception splitException) {
						appInventoryData.belongsToModulesList = null;
					}
					
				}//if (moduleMappingList!=null && !moduleMappingList.equalsIgnoreCase(""))
				//----------------------------------
				
				
				//appInventoryData.dependentOnTestCasesList
				String dependingOnList = 	controller.readCell(2,ControllerConfig.DependentOnH, row);			
				if (dependingOnList!=null && !dependingOnList.equalsIgnoreCase("")) {
					
					try {

						String[] dependsOnArray = dependingOnList.split(",");
						appInventoryData.dependentOnTestCasesList = new ArrayList<String>();
						
						for (String dependOn: dependsOnArray) {
							appInventoryData.dependentOnTestCasesList.add(dependOn);
						}
						
					}catch(Exception splitException) {
						appInventoryData.dependentOnTestCasesList = null;

					}

				}//if (dependingOnList!=null && !dependingOnList.equalsIgnoreCase(""))
				//----------------------------------
				
				
				//appInventoryData.dontRunWithList
				String runsAlone = controller.readCell(2,ControllerConfig.RunsAloneH, row);
				if (runsAlone != null && runsAlone.equalsIgnoreCase(ControllerConfig.Yes)) {
					appInventoryData.isRunsAlone = true;
				}
				else {
					appInventoryData.isRunsAlone = false;
				}
				//----------------------------------
				
				
				//appInventoryData.automationScriptID
				String dontRunWithList = 	controller.readCell(2,ControllerConfig.DontRunWithH, row);			
				if (dontRunWithList!=null && !dontRunWithList.equalsIgnoreCase("")) {
					
					try {

						String[] dontRunArray = dontRunWithList.split(",");
						
						
						appInventoryData.dontRunWithList = new ArrayList<String>();
						
						for (String dontRunOn: dontRunArray) {
							appInventoryData.dontRunWithList.add(dontRunOn);
						}
						
					}catch(Exception splitException) {

						appInventoryData.dontRunWithList = null;						
					}
					
				}//if (dontRunWithList!=null && !dontRunWithList.equalsIgnoreCase(""))
				//----------------------------------
					
				//Add the test to TestInventory of our AppData
				appData.getTestInventory().add(appInventoryData);
				
			}//ReadInventroyData - for (int row=3;row<=maxNumOfRowsInProductSheet; row++)		
			
			
			//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=//
			
			//Read Application configuration to applicationAttributesMap of Application Data				
			for (int row = 3; row<=maxNumOfRowsInProductSheet; row++) {
				
				String key = controller.readCell(2,ControllerConfig.ApplicationAttributeH, row);
				if (key!=null && !key.equalsIgnoreCase("") && applicationAttributes.contains(key.trim())) {
					
					String value = controller.readCell(2,ControllerConfig.ValueH, row);
					appData.getLocalApplicationAttributesMap().put(key, (value==null)?"":value);							
				}//appData-Application Attributes: if (key!=null && applicationAttributes.contains(key.trim()))
		
			}// appData - Application attributes - (int row=3; row<=maxNumOfRowsInProductSheet; row++)				
			//-------------------------------------------------------------	
			
			//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=//
			
			//Read Module data to moduleInventoryList of ApplicationsData
			ReadModuleInventoryData:
			for (int row = 3; row<=maxNumOfRowsInProductSheet; row++) {
				
				String moduleID = controller.readCell(2,ControllerConfig.ModuleIDH, row);
				if (moduleID != null && !moduleID.equalsIgnoreCase("")) {
					
					ModuleInventoryData mData = new ModuleInventoryData();
					mData.moduleID = moduleID;
					
					String moduleFlag = controller.readCell(2,ControllerConfig.ModuleExecutionFlagH, row);
					
					if (moduleFlag==null || !moduleFlag.equalsIgnoreCase(ControllerConfig.Yes)) {
						
						//Don't Read this!! Just Skip..
						continue ReadModuleInventoryData;	
					
					}//mData					
					
					String moduleName = controller.readCell(2,ControllerConfig.ModuleNameH, row);		
					if (moduleName==null) {
						moduleName="";
					}
					mData.moduleName = moduleName;			
					
					//Add the moduleData to Applications Data
					appData.getModuleInventoryList().add(mData);
					
				}//ModuleData - if (moduleID != null && !moduleID.equalsIgnoreCase(""))
			}//Applications Data - Module Data: for (int row = 3; row<=maxNumOfRowsInProductSheet; row++)
			//-------------------------------------------------------------	
						
			//Write the appData to AppData of ControllerData
			ControllerData.getAppDataMap().put(appName, appData);					
			
			//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=//			
			
		}//AppsToRun - for (String appName: ControllerData.getAppsToRun())		
	
		//close spreadsheet and make controller object eligible for garbage collection
		controller.closeXl();
		controller = null;
		MirthLogger.methodExit();
	}
}
