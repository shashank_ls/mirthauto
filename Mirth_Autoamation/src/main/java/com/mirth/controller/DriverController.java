package com.mirth.controller;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.safari.SafariDriver;

import com.mirth.dataObjects.Browser;
import com.mirth.exceptions.InternetExplorerNotSupportedException;
import com.mirth.exceptions.UnknownBrowserException;
import com.mirth.properties.OtherConfig;
import com.mirth.utility.general.StaticLib;
import com.mirth.utility.seleniumLib.MirthDriver;

public class DriverController {


	private final Map<String,MirthDriver> drivers = new LinkedHashMap<String,MirthDriver>();
	private String currentDriver = null;
	private static int browserXPosition=0;
	private static int browserYPosition=0;	
	
	
	public void createNewDriver(String name, Browser browser) throws UnknownBrowserException, InternetExplorerNotSupportedException, NumberFormatException, IOException {	
		
		MirthDriver driver = DriverController.initiateDriver(browser);
		this.drivers.put(name, driver);

	}
	
	public void pointToDriver(String name) {		
		this.currentDriver = name;		
	}
	
	public void createAndPointToNewDriver(String name,Browser browser) throws UnknownBrowserException, InternetExplorerNotSupportedException, NumberFormatException, IOException {	
		
		this.createNewDriver(name,browser);
		this.pointToDriver(name);
		
	}
	
	public void closeDriver(String name) {
		
		WebDriver driver = this.drivers.get(name).getDriver();
		
		if (driver!=null) {
			driver.quit();
		}
		
		//this.drivers.get(name).close();
	}
	
	public void closeAndRemoveDriver(String name) {
		this.closeDriver(name);		
		this.drivers.remove(name);
	}
	
	public MirthDriver getCurrentMirthDriver(){
		return this.drivers.get(currentDriver);
	}
	
	public MirthDriver getMirhDriver(String name) {						
		return this.drivers.get(name);
	}
	
	private static synchronized MirthDriver initiateDriver(Browser browser) throws InternetExplorerNotSupportedException, UnknownBrowserException, NumberFormatException, IOException {
		
		int width = Integer.parseInt(OtherConfig.getValue("BrowserWidth")); 
		int height = Integer.parseInt(OtherConfig.getValue("BrowserHeight")); 
		WebDriver driver = null;
		
		int spacing = Integer.parseInt(OtherConfig.getValue("BrowserSpacing")); 
		Dimension dimension= new Dimension(width,height);
			
		if (browser == Browser.Firefox) {
			driver = new FirefoxDriver();	

		}		
		
		else if (browser == Browser.Chrome) {
			driver = new ChromeDriver();

			
		}//else if (browser == Browser.Chrome)
		
		
		else if (browser == Browser.IE) {
			
			if (!StaticLib.isThisMachineWindows()) {
				throw new InternetExplorerNotSupportedException();
			}
			driver = new InternetExplorerDriver();

		}//else if (browser == Browser.IE)
		
		
		else if (browser == Browser.Safari) {
			driver = new SafariDriver();
			
		}//else if (browser == Browser.Safari)
		
		//if browser is unknown
		else {
			throw new UnknownBrowserException(browser.toString());
		}

		
		driver.manage().window().setPosition(new Point(browserXPosition,browserYPosition));
		driver.manage().window().setSize(dimension);
				
		if (browserXPosition > width && browserYPosition ==height) {
			browserXPosition=0;
			browserYPosition=0;

		}
		else if (browserXPosition ==0 && browserYPosition ==0) {
			browserXPosition=width+spacing;
			browserYPosition=0;

		}
		else if (browserXPosition > width && browserYPosition ==0) {
			browserXPosition=0;
			browserYPosition=height;
			 
		}
		else if (browserXPosition == 0 && browserYPosition ==height) {
			browserXPosition=width+spacing;
			browserYPosition=height;
		}
		
		return new MirthDriver(driver);
		
	}//public static synchronized void setDriverProperties(WebDriver driver)
		
}//class
