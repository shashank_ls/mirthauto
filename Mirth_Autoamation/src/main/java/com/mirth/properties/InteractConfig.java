package com.mirth.properties;

import java.io.IOException;
import java.util.Properties;

public class InteractConfig{
	
	private InteractConfig() {
		//Singleton
	}

	//singleton object
	private static InteractConfig appConfig =null; 
	private Properties config;
	private String configFileName= "InteractConfig.properties";
	//Config Variable END----------------------
	
	public synchronized static InteractConfig getObject() {
		
		if (InteractConfig.appConfig == null) {
			InteractConfig.appConfig = new InteractConfig();
		}
		
		return InteractConfig.appConfig;
	}
	
	
	private synchronized void initiateControllerConfig() throws IOException {
		config = new Properties();
		config.load(InteractConfig.class.getResourceAsStream(configFileName));
		
	}//initiateControllerConfig() throws IOException
	
	
	public synchronized String getVal(String configKey) throws IOException {
		
		if (isConfigPropertyNull()) {
			initiateControllerConfig();
		}
		
		return config.getProperty(configKey);
	}
	
	public static synchronized String getValue(String configKey) throws IOException {		
		return InteractConfig.getObject().getVal(configKey);	
	}

	public synchronized boolean isConfigPropertyNull() {
		return (config==null)?true:false;
	}
	
}
