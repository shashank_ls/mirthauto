package com.mirth.properties;

import java.io.IOException;
import java.util.Properties;

public class OtherConfig{
	
	private OtherConfig() {
		//Singleton
	}

	//singleton object
	private static OtherConfig appConfig =null; 
	private Properties config;
	private String configFileName= "OtherConfig.properties";
	//Config Variable END----------------------
	
	public synchronized static OtherConfig getObject() {
		
		if (OtherConfig.appConfig == null) {
			OtherConfig.appConfig = new OtherConfig();
		}
		
		return OtherConfig.appConfig;
	}
	
	
	private synchronized void initiateControllerConfig() throws IOException {
		config = new Properties();
		config.load(OtherConfig.class.getResourceAsStream(configFileName));
		
	}//initiateControllerConfig() throws IOException
	
	
	public synchronized String getVal(String configKey) throws IOException {
		
		if (isConfigPropertyNull()) {
			initiateControllerConfig();
		}
		
		return config.getProperty(configKey);
	}
	
	public static synchronized String getValue(String configKey) throws IOException {		
		return OtherConfig.getObject().getVal(configKey);	
	}

	public synchronized boolean isConfigPropertyNull() {
		return (config==null)?true:false;
	}
	
}
