package com.mirth.properties;

import java.io.IOException;
import java.util.Properties;

public class ObjectConfig{
	
	private ObjectConfig() {
		//Singleton
	}

	//singleton object
	private static ObjectConfig appConfig =null; 
	private Properties config;
	private String configFileName= "OR.properties";
	//Config Variable END----------------------
	
	public synchronized static ObjectConfig getObject() {
		
		if (ObjectConfig.appConfig == null) {
			ObjectConfig.appConfig = new ObjectConfig();
		}
		
		return ObjectConfig.appConfig;
	}
	
	
	private synchronized void initiateControllerConfig() throws IOException {
		config = new Properties();
		config.load(ObjectConfig.class.getResourceAsStream(configFileName));
		
	}//initiateControllerConfig() throws IOException
	
	
	public synchronized String getVal(String configKey) throws IOException {
		
		if (isConfigPropertyNull()) {
			initiateControllerConfig();
		}
		
		return config.getProperty(configKey);
	}
	
	public static synchronized String getValue(String configKey) throws IOException {		
		return ObjectConfig.getObject().getVal(configKey);	
	}

	public synchronized boolean isConfigPropertyNull() {
		return (config==null)?true:false;
	}
	
}
