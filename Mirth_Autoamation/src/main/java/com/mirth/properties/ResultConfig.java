package com.mirth.properties;

import java.io.IOException;
import java.util.Properties;

public class ResultConfig {
	
	private static Properties config;
	private static String configFileName= "ResultConfig.properties";
	
	//Configuration Variable Start-----------------------
	
	public static String RunInformationH;
	public static String AppHeadingConfigH;
	public static String AppHeadingValueH;
	
	public static String ResultFolderName;
	public static String RunPrefix;
	public static String RunDateTimeFormat;
	
	public static String TestCaseNumber;
	public static String StartDateTime;
	public static String EndDateTime;
	public static String JiraID;
	public static String ScriptID;
	public static String TestName;
	public static String BelongsToModule;
	public static String Browser;
	public static String Result;
	public static String Issues;
	public static String MiscData;
	
	public static String StartEndDateTimeFormat;
	
	public static String Pass;
	public static String Fail;
	public static String ApplicationNameH;
	public static String EnvironmentH;
	public static String BuildNumberH;
	public static String RunStartH;
	public static String RunEndH;
	public static String RunIDH;
	public static String ResultSpreadSheetFormat;

	
	//Screenshot related properties are NOT hard referenced here.
	
	//Config Variable END----------------------
	
	public static void initiateControllerConfig() throws IOException {
		
		readConfigFile();
		mapConfigToVariables();
		
	}
	
	
	//Initiate config object and read configuration data from controllerConfig.properites
	private static void readConfigFile() throws IOException {
		
		config = new Properties();
		config.load(ResultConfig.class.getResourceAsStream(configFileName));	
		
	}
	
	private static void mapConfigToVariables() throws IOException {
		
		//Configuration Variable Start-----------------------
		RunInformationH = getConfigVal("RunInformationH");
		AppHeadingConfigH = getConfigVal("AppHeadingConfigH");
		AppHeadingValueH = getConfigVal("AppHeadingValueH");	
		
		
		ApplicationNameH = getConfigVal("ApplicationNameH");
		EnvironmentH = getConfigVal("EnvironmentH");
		BuildNumberH = getConfigVal("BuildNumberH");
		RunStartH = getConfigVal("RunStartH");
		RunEndH = getConfigVal("RunEndH");
		RunIDH = getConfigVal("RunIDH");
		
		ResultFolderName = getConfigVal("ResultFolderName");
		RunPrefix= getConfigVal("RunPrefix");
		RunDateTimeFormat= getConfigVal("RunDateTimeFormat");		
		
		TestCaseNumber = getConfigVal("TestCaseNumberH"); 
		StartDateTime = getConfigVal("StartDateTimeH"); 
		EndDateTime = getConfigVal("EndDateTimeH");
		JiraID = getConfigVal("JiraIDH");
		ScriptID = getConfigVal("ScriptIDH"); 
		TestName = getConfigVal("TestNameH");
		BelongsToModule = getConfigVal("BelongsToModuleH"); 
		Browser = getConfigVal("BrowserH"); 
		Result = getConfigVal("ResultH"); 
		Issues = getConfigVal("IssuesH"); 
		MiscData = getConfigVal("MiscDataH");
		
		StartEndDateTimeFormat =  getConfigVal("StartEndDateTimeFormat");
		
		Pass = getConfigVal("Pass");
		Fail = getConfigVal("Fail");
		
		ResultSpreadSheetFormat = getConfigVal("ResultSpreadSheetFormat");
		
	}
	
	public static String getConfigVal(String configKey) {
		
		return config.getProperty(configKey);
	}

	public static boolean isConfigPropertyNull() {
		return (config==null)?true:false;
	}
	
}
