package com.mirth.properties;

import java.io.IOException;
import java.util.Properties;

public class LogConfig{
	
	private LogConfig() {
		//Singleton
	}

	//singleton object
	private static LogConfig appConfig =null; 
	private Properties config;
	private String configFileName= "LogConfig.properties";
	//Config Variable END----------------------
	
	public synchronized static LogConfig getObject() {
		
		if (LogConfig.appConfig == null) {
			LogConfig.appConfig = new LogConfig();
		}
		
		return LogConfig.appConfig;
	}
	
	
	private synchronized void initiateControllerConfig() throws IOException {
		config = new Properties();
		config.load(LogConfig.class.getResourceAsStream(configFileName));
		
	}//initiateControllerConfig() throws IOException
	
	
	public synchronized String getVal(String configKey) throws IOException {
		
		if (isConfigPropertyNull()) {
			initiateControllerConfig();
		}
		
		return config.getProperty(configKey);
	}
	
	public static synchronized String getValue(String configKey) throws IOException {		
		return LogConfig.getObject().getVal(configKey);	
	}

	public synchronized boolean isConfigPropertyNull() {
		return (config==null)?true:false;
	}
	
}
