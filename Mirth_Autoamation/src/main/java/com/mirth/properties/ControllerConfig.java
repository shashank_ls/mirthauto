package com.mirth.properties;

import java.io.IOException;
import java.util.Properties;

public class ControllerConfig {

	private static Properties config;
	private static String configFileName= "ControllerConfig.properties";
	
	//Configuration Variable Start-----------------------
	
	//Framework FileNames and File Path
	public static String ControllerSpreadSheetName;
	public static String MirthAutomationSuitePathWin;
	public static String MirthAutomationSuitePathMac;
	
	//Controller/Application Spreadsheet Headers
	public static String MainController;
	
	//FrameWorkPackage Locations
	public static String FrameWorkPackage;

	//Product Set
	public static String ProductH;
	public static String ProductPriorityH;
	public static String TestExecutionFlagH;
	public static String GeneralAttributeH;
	public static String GeneralValueH;
	public static String ApplicationAttributeH;
	public static String ValueH;
	
	//TestInventroy Headings
	public static String ManualTestCaseIDH;
	public static String AutomationScriptIDH;
	public static String AutomationScriptNameH;
	public static String ExecutionFlagH;
	public static String BrowserH;
	public static String PriorityH;
	public static String QAProofH;
	public static String ModuleMappingH;
	public static String DependentOnH;
	public static String RunsAloneH;
	public static String DontRunWithH;
	
	//ModuleExecutionMapping Headings
	public static String ModuleNameH;
	public static String ModuleIDH;
	public static String ModuleExecutionFlagH;
	
	//Product Names 
	public static String MSO;
	public static String Results;
	public static String Match;
	public static String Mail;
	public static String Care;
	public static String Appliance;
	
	//General Attributes
	public static String NumberOfThreadsSH;
	public static String DefaultBrowserSH;
	public static String OneAppAtATimeSH;
	public static String RunNameSH;
	public static String ResultReportType;
	
	
	//Application Attributes
	public static String OverrideControllerConfigSH;
	public static String ModuleExecutionOnlySH;
	public static String ExecFlagDuringModuleExecSH;
	public static String PriorityBasedExecutionSH;
	public static String PriorityDuringModuleExecutionSH;
	public static String TakeQAProofSH;
	public static String TestServerSH;
	
	//Browsers
	public static String Firefox;
	public static String Chrome;
	public static String IE;
	public static String Safari;
	
	//Yes or No values
	public static String Default;
	public static String Yes;
	public static String No;
	
	//ResultReportTypeValues
	public static String Excel;
	public static String HTML;
	public static String ExHTML;

	
	//Config Variable END----------------------
	
	public static void initiateControllerConfig() throws IOException {
		
		readConfigFile();
		mapConfigToVariables();
		
	}
	
	
	//Initiate config object and read configuration data from controllerConfig.properites
	private static void readConfigFile() throws IOException {
		
		config = new Properties();
		config.load(ControllerConfig.class.getResourceAsStream(configFileName));	
		
	}
	
	private static void mapConfigToVariables() {
		
		//Configuration Variable Start-----------------------

		//Framework FileNames and File Path"); 
		ControllerSpreadSheetName = getConfigVal("ControllerSpreadSheetName"); 
		MirthAutomationSuitePathWin = getConfigVal("MirthAutomationSuitePathWin"); 
		MirthAutomationSuitePathMac = getConfigVal("MirthAutomationSuitePathMac");

		//Controller/Application Spreadsheet Headers
		MainController = getConfigVal("MainController");
		
		//FrameWork Package Locations
		FrameWorkPackage = getConfigVal("FrameWorkPackage");

		//Product Set
		ProductH = getConfigVal("ProductH"); 
		ProductPriorityH = getConfigVal("ProductPriorityH");
		TestExecutionFlagH = getConfigVal("TestExecutionFlagH"); 
		GeneralAttributeH = getConfigVal("GeneralAttributeH"); 
		GeneralValueH = getConfigVal("GeneralValueH"); 
		ApplicationAttributeH = getConfigVal("ApplicationAttributeH"); 
		ValueH = getConfigVal("ValueH");

		//TestInventroy Headings
		ManualTestCaseIDH = getConfigVal("ManualTestCaseIDH"); 
		AutomationScriptIDH = getConfigVal("AutomationScriptIDH"); 
		AutomationScriptNameH = getConfigVal("AutomationScriptNameH"); 
		ExecutionFlagH = getConfigVal("ExecutionFlagH"); 
		BrowserH = getConfigVal("BrowserH"); 
		PriorityH = getConfigVal("PriorityH"); 
		QAProofH = getConfigVal("QAProofH"); 
		ModuleMappingH = getConfigVal("ModuleMappingH"); 
		DependentOnH = getConfigVal("DependentOnH"); 
		RunsAloneH = getConfigVal("RunsAloneH"); 
		DontRunWithH = getConfigVal("DontRunWithH");

		//ModuleExecutionMapping Headings
		ModuleNameH = getConfigVal("ModuleNameH"); 
		ModuleIDH = getConfigVal("ModuleIDH"); 
		ModuleExecutionFlagH = getConfigVal("ModuleExecutionFlagH");

		//Product Names
		MSO = getConfigVal("MSO"); 
		Results = getConfigVal("Results"); 
		Match = getConfigVal("Match"); 
		Mail = getConfigVal("Mail"); 
		Care = getConfigVal("Care"); 
		Appliance = getConfigVal("Appliance");

		//General Attributes
		NumberOfThreadsSH = getConfigVal("NumberOfThreadsSH"); 
		DefaultBrowserSH = getConfigVal("DefaultBrowserSH"); 
		OneAppAtATimeSH = getConfigVal("OneAppAtATimeSH");
		RunNameSH = getConfigVal("RunNameSH");
		ResultReportType = getConfigVal("ResultReportType");		

		//Application Attributes
		OverrideControllerConfigSH = getConfigVal("OverrideControllerConfigSH"); 
		ModuleExecutionOnlySH = getConfigVal("ModuleExecutionOnlySH"); 
		ExecFlagDuringModuleExecSH = getConfigVal("ExecFlagDuringModuleExecSH"); 
		PriorityBasedExecutionSH = getConfigVal("PriorityBasedExecutionSH"); 
		PriorityDuringModuleExecutionSH = getConfigVal("PriorityDuringModuleExecutionSH"); 
		TakeQAProofSH = getConfigVal("TakeQAProofSH");
		TestServerSH = getConfigVal("TestServerSH");
		
		//Browsers
		Firefox = getConfigVal("Firefox"); 
		Chrome = getConfigVal("Chrome"); 
		IE = getConfigVal("IE");
		Safari = getConfigVal("Safari");

		//Yes or No values
		Default = getConfigVal("Default"); 
		Yes = getConfigVal("Yes"); 
		No = getConfigVal("No");
		
		//Excel or Html
		Excel = getConfigVal("Excel"); 
		HTML = getConfigVal("HTML"); 
		ExHTML = getConfigVal("ExHTML");

		
	}
	
	private static String getConfigVal(String configKey) {
		return config.getProperty(configKey);
	}

	public static boolean isConfigPropertyNull() {
		return (config==null)?true:false;
	}
	
}
