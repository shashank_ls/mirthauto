package com.mirth.properties;

import java.io.IOException;
import java.util.Properties;

public class ApplicationConfig{
	
	private ApplicationConfig() {
		//Singleton
	}

	//singleton object
	private static ApplicationConfig appConfig =null; 
	private Properties config;
	private String configFileName= "ApplicationConfig.properties";
	//Config Variable END----------------------
	
	public synchronized static ApplicationConfig getObject() {
		
		if (ApplicationConfig.appConfig == null) {
			ApplicationConfig.appConfig = new ApplicationConfig();
		}
		
		return ApplicationConfig.appConfig;
	}
	
	
	private synchronized void initiateControllerConfig() throws IOException {
		config = new Properties();
		config.load(ApplicationConfig.class.getResourceAsStream(configFileName));
		
	}//initiateControllerConfig() throws IOException
	
	
	public synchronized String getVal(String configKey) throws IOException {
		
		if (isConfigPropertyNull()) {
			initiateControllerConfig();
		}
		
		return config.getProperty(configKey);
	}
	
	public static synchronized String getValue(String configKey) throws IOException {		
		return ApplicationConfig.getObject().getVal(configKey);	
	}

	public synchronized boolean isConfigPropertyNull() {
		return (config==null)?true:false;
	}
	
}
