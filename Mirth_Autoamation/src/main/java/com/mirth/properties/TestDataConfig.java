package com.mirth.properties;

import java.io.IOException;
import java.util.Properties;

public class TestDataConfig{
	
	private TestDataConfig() {
		//Singleton
	}

	//singleton object
	private static TestDataConfig appConfig =null; 
	private Properties config;
	private String configFileName= "TestData.properties";
	//Config Variable END----------------------
	
	public synchronized static TestDataConfig getObject() {
		
		if (TestDataConfig.appConfig == null) {
			TestDataConfig.appConfig = new TestDataConfig();
		}
		
		return TestDataConfig.appConfig;
	}
	
	
	private synchronized void initiateControllerConfig() throws IOException {
		config = new Properties();
		config.load(TestDataConfig.class.getResourceAsStream(configFileName));
		
	}//initiateControllerConfig() throws IOException
	
	
	public synchronized String getVal(String configKey) throws IOException {
		
		if (isConfigPropertyNull()) {
			initiateControllerConfig();
		}
		
		return config.getProperty(configKey);
	}
	
	public static synchronized String getValue(String configKey) throws IOException {		
		return TestDataConfig.getObject().getVal(configKey);	
	}

	public synchronized boolean isConfigPropertyNull() {
		return (config==null)?true:false;
	}
	
}
