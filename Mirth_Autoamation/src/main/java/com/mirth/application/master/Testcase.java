package com.mirth.application.master;

import java.io.IOException;
import java.util.Date;

import com.mirth.controller.ThreadController;
import com.mirth.controller.Verification;
import com.mirth.dataObjects.HeadingController;
import com.mirth.dataObjects.TestCaseResultData;
import com.mirth.dataObjects.TestInventoryData;
import com.mirth.results.QAProof;

public abstract class Testcase{
	
	protected final TestInventoryData testInventory = ThreadController.getInventoryOfThread(Thread.currentThread().getId());
	protected TestCaseResultData resultData;
	protected abstract void steps() throws Exception;
	protected Verification verify;
	protected final HeadingController headingController = ThreadController.getHeadingControllerOfCurrentThread();
	protected String proofDocColorFormat = "A52A2A";//Brown
	
	public void startTestCaseExecution(TestCaseResultData data) throws Exception {
		headingController.goToInitialState();
		System.out.println(new Date());
		resultData = data;
		verify = new Verification(resultData);
		ThreadController.updateCurrentThreadVerifyMap(verify);	
		this.steps();	
		ThreadController.removeVerifyOfCurrentThread();
		System.out.println(new Date());
		//PageObject.removetestData(testInventory);
	}//public void startTestCaseExecution(TestCaseResultData data)
	
	public final void writeText(String data) throws IOException{
		QAProof.writeText(data,proofDocColorFormat);
	}
	
}//class
