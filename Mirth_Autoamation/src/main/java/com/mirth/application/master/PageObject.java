package com.mirth.application.master;

import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.mirth.application.version2.match.pages.pagemaster.MatchClassPageMapping;
import com.mirth.application.version2.mso.pages.pagemaster.MSOClassPageMapping;
import com.mirth.application.version2.results.pages.pagemaster.ResultsClassPageMapping;
import com.mirth.controller.Assertions;
import com.mirth.controller.DriverController;
import com.mirth.controller.Interact;
import com.mirth.controller.ThreadController;
import com.mirth.controller.Verification;
import com.mirth.controller.objects.ORMaster;
import com.mirth.dataObjects.SetterDataPair;
import com.mirth.dataObjects.TestInventoryData;
import com.mirth.exceptions.AppNotPresentException;
import com.mirth.exceptions.ClassPageMappingNotFoundException;
import com.mirth.exceptions.NotATableElementException;
import com.mirth.exceptions.TestDataNotPresentException;
import com.mirth.properties.ApplicationConfig;
import com.mirth.properties.TestDataConfig;
import com.mirth.utility.general.MirthLib;
import com.mirth.utility.general.SleepLib;
import com.mirth.utility.general.StaticLib;
import com.mirth.utility.general.StringLib;
import com.mirth.utility.seleniumLib.SeleniumElement;
import com.mirth.utility.seleniumLib.SeleniumLib;
import com.mirth.utility.seleniumLib.Setter;
import com.mirth.utility.spreadsheet.MicrosoftSpreadsheet;

public abstract class PageObject{
	
	//Master TestData Map
	//Appname/TestName -> Page -> Key-Value Pair
	private static final Map<String,Map<String,Map<String,String>>> testData = new HashMap<String,Map<String,Map<String,String>>>();
	private static final String keyDelimeter = "";
	
	public static void removetestData(TestInventoryData testInv) {
		testData.remove(testInv.appName+testInv.automationScriptID);
	}
		
	public static void clickElement(SeleniumElement selElement) throws Exception  {
		Class<? extends PageObject> pageClass = selElement.getPageClass();
		String canonicalOrigin = pageClass.getCanonicalName();
		String page = getPageNameOfClass(pageClass);
		String appName = MirthLib.getAppNameForCanonical(canonicalOrigin);
		interactWith(appName, page, selElement).click();
	}
	
	private static String td(String tdAppName,String page,String key) throws IOException, IllegalAccessException, IllegalArgumentException, TestDataNotPresentException, ClassPageMappingNotFoundException {
 		TestInventoryData testInv = ThreadController.getInventoryOfThread(Thread.currentThread().getId());
 		
 		String mapKey = tdAppName+keyDelimeter+testInv.automationScriptID;
 		Map<String,Map<String,String>> pageMap = testData.get(mapKey);
 		
 		if (Assertions.isNotNull(pageMap)) {
 			
 			Map<String,String> keyMap =  pageMap.get(page);
 			if (Assertions.isNotNull(keyMap)) { 				
 				String value = keyMap.get(key); 
 				
 				if (Assertions.isNotEmpty(value)) {
 					return value;
 				} 				
 				else {
 					throw new TestDataNotPresentException(key);
 				} 				
 			}//if (Assertions.isNotNull(keyMap)) 
 		}//if (Assertions.isNotNull(pageMap))
 		 		
 		return extractTestDataAndGetValue(tdAppName,testInv,page,key);

	}
	
	protected static String td(SeleniumElement eleKey) throws IOException, ClassPageMappingNotFoundException, AppNotPresentException, IllegalAccessException, IllegalArgumentException, TestDataNotPresentException {
		String page =  getCurrentPage();	
		return td(MirthLib.getAppNameForCanonical(StaticLib.getPreviousCanonicalOrigin(2)),page,eleKey.get());
	}
	
	protected static String td(String key) throws IOException, IllegalAccessException, IllegalArgumentException, TestDataNotPresentException, ClassPageMappingNotFoundException, AppNotPresentException {
		String page =  getCurrentPage();	
		return td(MirthLib.getAppNameForCanonical(StaticLib.getPreviousCanonicalOrigin(2)),page,key);
 		
 	}//public static String td(String key)
	
	public static String td(Class<? extends PageObject> pageClass,String key) throws IOException, IllegalAccessException, IllegalArgumentException, TestDataNotPresentException, ClassPageMappingNotFoundException, AppNotPresentException {
		
		String page = getPageNameOfClass(pageClass);
		return td(MirthLib.getAppNameForCanonical(pageClass.getCanonicalName()),page,key);
	
	}
	
	public static String td(Class<? extends PageObject> pageClass,SeleniumElement eleKey) throws IOException, IllegalAccessException, IllegalArgumentException, TestDataNotPresentException, ClassPageMappingNotFoundException, AppNotPresentException {
		
		String page = getPageNameOfClass(pageClass);
		return td(MirthLib.getAppNameForCanonical(pageClass.getCanonicalName()),page,eleKey.get());
	
	}
	
	public static String getTestDataFolderNameForApp(String appName) throws IOException{
 		String testDataFolder = StaticLib.appendFolderSeparatorIfNotPresent(StaticLib.getFrameWorkPackageWithSeparator()+TestDataConfig.getValue("TestDataFolderName"));
 		return StaticLib.appendFolderSeparatorIfNotPresent(testDataFolder+appName);

	}
 	
	private static String extractTestDataAndGetValue(String tdAppName,TestInventoryData testInv,String page,String key) throws IOException, IllegalAccessException, IllegalArgumentException, TestDataNotPresentException {
 		
		String mapKey=tdAppName+keyDelimeter+testInv.automationScriptID;
 		Map<String,Map<String,String>> pageMap = testData.get(mapKey);		
 		
 		if (Assertions.isNull(pageMap)) {
 			pageMap = new HashMap<String,Map<String,String>>();
 			testData.put(mapKey,pageMap);
 		}
 		
 		Map<String,String> keyMap = testData.get(mapKey).get(page);
 		
 		if (Assertions.isNull(keyMap)) {
 			keyMap = new HashMap<String,String>();
 			testData.get(mapKey).put(page,keyMap);
 		}
 				
 		String testDataSpreadSheet = getTestDataFolderNameForApp(tdAppName)+page+"."+TestDataConfig.getValue("SpreadSheetType");
			
		MicrosoftSpreadsheet testSheet = new MicrosoftSpreadsheet(testDataSpreadSheet);
		testSheet.swithchToSheet(TestDataConfig.getValue("DataMappingSheet"));
		int maxRows = testSheet.getMaxRows();
		
		String defaultValueHeader = TestDataConfig.getValue("DefaultValueHeader");
		String dataToUseHeader = defaultValueHeader;
		
		boolean isTestCasePresent=false;
		
		//start iterating through the test case rows to find if the test case mapping is present for this object
		DataMappingSearchLoop:
		for (int row = 2; row<= maxRows; row++) {
			
			String testCaseString = testSheet.readCell(row,1);
			if (Assertions.isNotEmpty(testCaseString) && Assertions.isSame(testInv.automationScriptID, testCaseString, false)) {

				dataToUseHeader = testSheet.readCell(row,2);

				if (!Assertions.isNotEmpty(dataToUseHeader)) {
					dataToUseHeader = defaultValueHeader;
				}
				
				isTestCasePresent = true;
				break DataMappingSearchLoop;
			}			
			
		}//DataMappingSearchLoop: for (int row = 2; row<= maxRows; row++)

		//Now, read the Data for the test case
		testSheet.swithchToSheet(TestDataConfig.getValue("TestDataSheet"));
		int testDataMaxRows = testSheet.getMaxRows();
		
		TestDataReadLoop:		
		for (int row = 2; row<= testDataMaxRows; row++) {
			
			String keyString = testSheet.readCell(row, 1);
			if (!Assertions.isNotEmpty(keyString)) {
				continue TestDataReadLoop;
			}
			String valueString = testSheet.readCell(dataToUseHeader, row);
			if (isTestCasePresent) {				
				if (!Assertions.isNotEmpty(valueString)) {
					valueString = testSheet.readCell(defaultValueHeader, row);					
				}//if (Assertions.isNotEmpty(valueString))
			}//if (isTestCasePresent)
			
			valueString = StringLib.getNotNullString(valueString, "");
			
			keyMap.put(keyString, valueString);
			System.out.println(keyString+"--"+valueString);
		}//TestDataReadLoop: for (int row = 2; row<= maxRows; row++)
		testSheet.closeXl();
		
		String value = keyMap.get(key);
		
		if (!Assertions.isNotEmpty(value)) {
			throw new TestDataNotPresentException(key);
		} 	

		return value;
		
 	}//private static String extractTestDataAndGetValue()
		
 	

	protected static DriverController getDriverController() {		
		return ThreadController.getDriverControllerForThread(Thread.currentThread().getId());
	}
	
	protected static WebDriver getDriver() {		
		return ThreadController.getDriverControllerForThread(Thread.currentThread().getId()).getCurrentMirthDriver().getDriver();
	}
	
	protected static WebDriver getDriver(String name) {		
		return ThreadController.getDriverControllerForThread(Thread.currentThread().getId()).getMirhDriver(name).getDriver();
	}
 	
	
	protected static WebElement getObject(WebDriver driver, String applicationName, String pageName,
			String objectName) throws Exception {

		return ORMaster.getObject(driver, applicationName, pageName,objectName);

	}
	
	protected static WebElement getObject(WebElement element, String applicationName, String pageName,
			String objectName) throws Exception {

		return ORMaster.getObject(element, applicationName, pageName,objectName);

	}	

	
	protected static Interact interactWith(WebDriver driver, String applicationName, String pageName,
			String objectName) throws Exception {

		return new Interact(getObject(driver, applicationName, pageName,objectName));

	}		
	
	
 	private static WebElement getObject(String appName,String page, String objectName) throws Exception {

 		return ORMaster.getObject(getDriver(), appName, page, objectName);
 		
 	}
 	
	
 	private static Interact interactWith(String appName,String page, String objectName) throws Exception {
 		
 		return new Interact(getObject(appName,page,objectName));
 		
 	}

 	public static Interact interactWithFromOutsidePage(String appName,String page, String objectName) throws Exception {
 		
 		return new Interact(getObject(appName,page,objectName));
 		
 	}

 	
 	private static Interact interactWith(String appName,String page, SeleniumElement selElement) throws Exception {
 		
 		return new Interact(getObject(appName,page,selElement.get()));
 		
 	}

 	//Extra method for ease
 	protected static WebElement getObject(SeleniumElement selElement) throws Exception {		
 		
 		String page = getCurrentPage(); 
 		return getObject(MirthLib.getAppNameForCanonical(StaticLib.getPreviousCanonicalOrigin(2)),page, selElement.get());
 		
 	}
 	//End - Extra method
 	
 	protected static WebElement getObject(String objectName) throws Exception {		
 		
 		String page = getCurrentPage(); 
 		return getObject(MirthLib.getAppNameForCanonical(StaticLib.getPreviousCanonicalOrigin(2)),page, objectName);
 		
 	}
 	
 	protected static WebElement getObject(WebElement sourceElement,String objectName) throws Exception {		
 		
 		String page = getCurrentPage(); 
 		return getObject(sourceElement,MirthLib.getAppNameForCanonical(StaticLib.getPreviousCanonicalOrigin(2)),page, objectName);
 		
 	}
 	
 	protected static Interact interactWith(String objectName) throws Exception {		
 		
 		String page = getCurrentPage(); 
 		return new Interact(getObject(MirthLib.getAppNameForCanonical(StaticLib.getPreviousCanonicalOrigin(2)),page,objectName));
 		
 	}
 	
 	protected static Interact interactWith(SeleniumElement elementName) throws Exception {		
 		
 		String page = getCurrentPage(); 
 		return new Interact(getObject(MirthLib.getAppNameForCanonical(StaticLib.getPreviousCanonicalOrigin(2)),page,elementName.get()));
 		
 	}
 	

 	private static String getCurrentPage() throws IOException, ClassPageMappingNotFoundException, AppNotPresentException {
 		
 		String canonicalOrigin = StaticLib.getPreviousCanonicalOrigin(3); 
 		String appName = MirthLib.getAppNameForCanonical(canonicalOrigin);
 		String callingClassName = StaticLib.getValueAfterLastIndex(canonicalOrigin,'.'); 	
 		System.out.println(getClassPageMap(appName));
 		return getClassPageMap(appName).getPageNameForClass(callingClassName); 		 		
 	}
 	
 	public static String getPageNameOfClass(Class<? extends PageObject> className) throws IOException, ClassPageMappingNotFoundException, AppNotPresentException {
 		
 		String canonicalOrigin = className.getCanonicalName();
 		String appName =MirthLib.getAppNameForCanonical(canonicalOrigin);
		String callingClassName = StaticLib.getValueAfterLastIndex(canonicalOrigin,'.');
 		return getClassPageMap(appName).getPageNameForClass(callingClassName); 
 		 		
 	}
 	
	private static ClassPageMapping getClassPageMap(String appName) throws IOException, ClassPageMappingNotFoundException {
		ClassPageMapping classPageMap = null;
		if (appName.equalsIgnoreCase(ApplicationConfig.getValue("MSO"))) {
			classPageMap = MSOClassPageMapping.getObject();
		}
		
		else if (appName.equalsIgnoreCase(ApplicationConfig.getValue("Results"))) {
			classPageMap = ResultsClassPageMapping.getObject();
		}
		
		else if (appName.equalsIgnoreCase(ApplicationConfig.getValue("Match"))) {
			classPageMap = MatchClassPageMapping.getObject();
		}
		
		else {
			throw new ClassPageMappingNotFoundException(appName);
		}
		
		return classPageMap;
	}	
	
		
	   //////////////////////////////////////////////////////
	  //    Interlinked overloaded verification methods	  //
	 // Please be careful before changing anything below // 
	//////////////////////////////////////////////////////
	
	//simple StraightForwardComparision
	public static boolean verifyStringData(String fieldName,String expected,String actual,boolean isCaseSensitiveComparision) throws InvalidFormatException, IOException{
		Verification verify = ThreadController.getVerifyOfCurrentThread();
		return verify.withReport(fieldName).isSame(expected, actual, isCaseSensitiveComparision);
	}
	
	public static void printSuccessReport(String fieldName,String string1,String string2) throws InvalidFormatException, IOException {
		Verification verify = ThreadController.getVerifyOfCurrentThread();
		verify.withReport(fieldName).reportSuccess(string1, string2);
	}
	
	public static void printFailureReport(String fieldName,String string1,String string2) throws InvalidFormatException, IOException {
		Verification verify = ThreadController.getVerifyOfCurrentThread();
		verify.withReport(fieldName).reportIssue(string1, string2);
	}

	public static void printFailureReport(String fieldName,String value) throws InvalidFormatException, IOException {
		Verification verify = ThreadController.getVerifyOfCurrentThread();
		verify.withReport(fieldName).reportIssue(value);
	}

	
	//verifying string data with page - called from this own class
	private static boolean verifyStringData(String tdAppName,String page,String expected,boolean isCaseSensitiveComparision,SeleniumElement selElement) throws Exception {
		String actual = getData(tdAppName,page,selElement);
		return verifyStringData(selElement.get(), expected, actual, isCaseSensitiveComparision);
		
	}
	
	public static boolean verifyStringData(SeleniumElement selElement,String expected,boolean isCaseSensitiveComparision) throws Exception {
		
		Class<? extends PageObject> pageClass = selElement.getPageClass();
		String page = PageObject.getPageNameOfClass(pageClass);
		return verifyStringData(MirthLib.getAppNameForCanonical(pageClass.getCanonicalName()),page, expected, isCaseSensitiveComparision, selElement);
	}
	
	//verifying string data on Map with page - called from this own class
	private static Map<SeleniumElement,Boolean> verifyStringData(String tdAppName,String page,Map<SeleniumElement,String> selElements,boolean isCaseSensitiveComparision) throws Exception {
		Map<SeleniumElement,Boolean> resultMap = new LinkedHashMap<SeleniumElement,Boolean>();
		
		for (SeleniumElement selElement: selElements.keySet()){
			resultMap.put(selElement,verifyStringData(tdAppName,page, selElements.get(selElement), isCaseSensitiveComparision, selElement));
		}
		
		return resultMap;	
	}
	
	//verifying string data on Map without page - to be called from outside
	protected static Map<SeleniumElement,Boolean> verifyStringData(Map<SeleniumElement,String> selElements,boolean isCaseSensitiveComparision) throws Exception {
		return verifyStringData(MirthLib.getAppNameForCanonical(StaticLib.getPreviousCanonicalOrigin(2)),getCurrentPage(), selElements,isCaseSensitiveComparision);
	}
	
	public static Map<SeleniumElement,Boolean> verifyStringData(Class<? extends PageObject> pageClass,Map<SeleniumElement,String> selElements,boolean isCaseSensitiveComparision) throws Exception {
		String page = PageObject.getPageNameOfClass(pageClass);
		return verifyStringData(MirthLib.getAppNameForCanonical(pageClass.getCanonicalName()),page, selElements,isCaseSensitiveComparision);
	}
		
	/* Explicipt map verfication of elements -> aiming to depricate-->
	//=======Verify data as per Map
	//by default case will be ignored	
	protected static void verifyDataAsPerMap(Map<String,Object[]> dataMap,boolean isCaseSensitive,String headingString,boolean isDocumentScreenshot) throws Exception {
		verifyDataAsPerMap(getCurrentPage(),dataMap,isCaseSensitive,headingString,isDocumentScreenshot);
	}
	
	protected static void verifyDataAsPerMap(Map<String,Object[]> dataMap,String headingString,boolean isDocumentScreenShot) throws Exception {
		verifyDataAsPerMap(getCurrentPage(),dataMap,false,headingString,isDocumentScreenShot);
	}
	

	public static void verifyDataAsPerMap(String page,Map<String,Object[]> dataMap,boolean isCaseSensitiveComparision,String headingString,boolean isDocumentScreenShot) throws Exception {
		

		Boolean isNoFailureTillNow = true;
		Map<Integer,Boolean> isHeadingScreenMap = new HashMap<Integer,Boolean>();
		isHeadingScreenMap.put(0,false);
		
		if (dataMap==null){
			return;
		}
		
		//need to work on boolean return and screenshot problem
		verifyingDataLoop:
		for (String fieldIdentifier: dataMap.keySet()){

			
			Object[] valMap = dataMap.get(fieldIdentifier);
			Boolean isGetEnabled = StaticLib.convertToType(valMap[3], Boolean.class);
			if (!isGetEnabled){
				continue verifyingDataLoop;
			}			
			
			String[] setterDataPair = new String[2];
			System.arraycopy(dataMap.get(fieldIdentifier), 0, setterDataPair, 0, 2);		
			if (!isNoFailureTillNow) {
				headingString = null;
			}//if (!isNoFailureTillNow)
			
			DataVerify vData = new DataVerify(fieldIdentifier,setterDataPair,headingString,isCaseSensitiveComparision,ThreadController.getThreadHeading()); 
			isNoFailureTillNow = verifyData(page,vData,isHeadingScreenMap);
			if (isHeadingScreenMap.get(0)){
				ThreadController.updateThreadHeading(false);
			}		
			
 			vData = null;
			
		}//for (String fieldIdentifier: dataMap.keySet())
		
		ThreadController.updateThreadHeading(true);
		ThreadController.updateThreadScreenShot(true);
		
	}//public static void verifyDataAsPerMap(String page,Map<String,String[]> dataMap,,boolean isCaseSensitiveComparision)

//=========Verify data as per map finishes.
	
	//verifiable=true by default
	protected static void addTestDataToMap(Map<String,Object[]> valMap, String testKey) throws IllegalAccessException, IllegalArgumentException, IOException, TestDataNotPresentException, ClassPageMappingNotFoundException, InvalidRadioButtonValueException, InvalidCheckBoxValueException {
		addTestDataToMap(getCurrentPage(),valMap, testKey,true,true);
	}
	
	protected static void addTestDataToMap(Map<String,Object[]> valMap, String testKey,Boolean isSetData,Boolean isVerifiable) throws IllegalAccessException, IllegalArgumentException, IOException, TestDataNotPresentException, ClassPageMappingNotFoundException, InvalidRadioButtonValueException, InvalidCheckBoxValueException {
		addTestDataToMap(getCurrentPage(),valMap, testKey,isSetData,isVerifiable);
	}
	
	public static void addTestDataToMap(String page,Map<String,Object[]> valMap, String testKey,Boolean isSetData,Boolean isVerifiable) throws IllegalAccessException, IllegalArgumentException, IOException, TestDataNotPresentException, ClassPageMappingNotFoundException, InvalidRadioButtonValueException, InvalidCheckBoxValueException {
		
		String patternData = td(page,testKey);
		
		if (valMap!=null) {
			
			if (!testKey.equals(InteractConfig.getValue("blankValue"))) {			
				
				Object[] origArray = Interact.getRawSetterDataPair(patternData);
				int origArrayLength = origArray.length;
				Object[] newArray = new Object[origArrayLength+2];
				System.arraycopy(origArray, 0, newArray, 0, origArrayLength);
				
				newArray[origArrayLength] = isSetData;
				newArray[origArrayLength+1] = isVerifiable;
				
				valMap.put(testKey, newArray);
			}			
		}//if (valMap!=null)	
	}//public static void addTestDataToMap(Map<String,String[]> valMap, String testKey) 
	
	public static Object[] updateSetterForvalMapObject(Object[] obj,String setter){
		obj[0] = setter;
		return obj;
	}
	
		
	//--	
	public static synchronized void setTdStringVal (String key,String val) {		
		ThreadData threadData = ThreadController.getThreadData(Thread.currentThread().getId());
		if (threadData!=null) {
			threadData.dataMap.put(Thread.currentThread().getId() + "_" +key,val);
		}			
	}
	
	public static synchronized void setTdObjStrVal (String key, Object val) {		
		ThreadData threadData = ThreadController.getThreadData(Thread.currentThread().getId());
		if (threadData!=null) {
			threadData.objMap.put(Thread.currentThread().getId() +  "_" +key,val);
			
			@SuppressWarnings("unchecked")
			Map<String,Object[]> o = (Map<String,Object[]>)val;
			
			for (String s: o.keySet()){
				
				String strVal = (String)o.get(s)[1];
				setTdStringVal(s,strVal);
			}			
		}					
	}
		
	public static synchronized void setTdObjVal (String key, Object val) {		
		ThreadData threadData = ThreadController.getThreadData(Thread.currentThread().getId());
		if (threadData!=null) {
			threadData.objMap.put(Thread.currentThread().getId() + "_" +key,val);			
		}					
	}
	
	public static synchronized String getTdStringVal (String key) {		
		ThreadData threadData = ThreadController.getThreadData(Thread.currentThread().getId());
		if (threadData!=null) {
			return threadData.dataMap.get(Thread.currentThread().getId() + "_" + key);
		}
		
		return null;			
	}
	
	public static synchronized Object getTdObjVal (String key) {		
		ThreadData threadData = ThreadController.getThreadData(Thread.currentThread().getId());
		if (threadData!=null) {
			return threadData.objMap.get(Thread.currentThread().getId() + "_" + key);
		}
		
		return null;			
	}
	
	//----	
	 
	 */
	
	
	
	 //End - depreciated items
	 //End of following functions. Need to re-visit and take a call as to what to retain and what not.
	 
	
	//NEED TO Write Verify data functions and re-visit above
	
	//GUI Setters and Getters for Page Object classes
	//Starting with setters
	
	//Get TestData formatted
	//get Processed Data from test data
	private static String getProcessedTestData(String tdAppName,String page,SeleniumElement selElement) throws NotATableElementException, Exception {
		
		String fieldId = selElement.get();
		String testDataString = td(tdAppName,page,fieldId);
		SetterDataPair setterData = Interact.getActualSetterDataPair(selElement, testDataString);
		//String dataToSet = setterData.data;
		
		return setterData.data;
		
		
		//This needs to be fixed for multi select drop down.
	
	}//public String setData(SeleniumElement selElement)
	
	public static String getProcessedTestData(SeleniumElement selElement) throws NotATableElementException, Exception {
		Class<? extends PageObject> pageClass= selElement.getPageClass();
		return getProcessedTestData(MirthLib.getAppNameForCanonical(pageClass.getCanonicalName()),PageObject.getPageNameOfClass(pageClass), selElement);
	}
	
	
	//Get subset of test Data
	private static Map<SeleniumElement,String> getProcessedTestData(String tdAppName,String page,SeleniumElement... selElements) throws NotATableElementException, Exception {
		
		Map<SeleniumElement,String> dataMap = new LinkedHashMap<SeleniumElement,String>();
		
		for (SeleniumElement selElement: selElements) {
			dataMap.put(selElement, getProcessedTestData(tdAppName,page,selElement));
		}
		
		return dataMap;
		
	}//public String setData(SeleniumElement selElement) 
	
	protected static Map<SeleniumElement,String> getProcessedTestData(SeleniumElement... selElements) throws NotATableElementException, Exception {
		return getProcessedTestData(MirthLib.getAppNameForCanonical(StaticLib.getPreviousCanonicalOrigin(2)),getCurrentPage(), selElements);
	}
	
	public static Map<SeleniumElement,String> getProcessedTestData(Class<? extends PageObject> pageClass, SeleniumElement... selElements) throws NotATableElementException, Exception {
		return getProcessedTestData(MirthLib.getAppNameForCanonical(pageClass.getCanonicalName()),PageObject.getPageNameOfClass(pageClass), selElements);
	}
	
	//---
	
	//Get subset of test Data
	private static Map<SeleniumElement,String> getProcessedTestData(String tdAppName,String page,List<SeleniumElement> selElementList) throws NotATableElementException, Exception {
		
		Map<SeleniumElement,String> dataMap = new LinkedHashMap<SeleniumElement,String>();
		
		for (SeleniumElement selElement: selElementList) {
			dataMap.put(selElement, getProcessedTestData(tdAppName,page,selElement));
		}
		
		return dataMap;
		
	}//public String setData(SeleniumElement selElement) 
	
	protected static Map<SeleniumElement,String> getProcessedTestData(List<SeleniumElement> selElementList) throws NotATableElementException, Exception {
		return getProcessedTestData(MirthLib.getAppNameForCanonical(StaticLib.getPreviousCanonicalOrigin(2)),getCurrentPage(), selElementList);
	}
	
	public static Map<SeleniumElement,String> getProcessedTestData(Class<? extends PageObject> pageClass, List<SeleniumElement> selElementList) throws NotATableElementException, Exception {
		return getProcessedTestData(MirthLib.getAppNameForCanonical(pageClass.getCanonicalName()),PageObject.getPageNameOfClass(pageClass), selElementList);
	}
	
	//---
	
	//set Data from test data
	private static String setData(String tdAppName,String page,SeleniumElement selElement) throws NotATableElementException, Exception {
		
		String fieldId = selElement.get();
		String testDataString = td(tdAppName,page,fieldId);
		SetterDataPair setterData = Interact.getActualSetterDataPair(selElement, testDataString);
		Setter actualSetter = setterData.setter;
		String dataToSet = setterData.data;
		
		interactWith(tdAppName,page,fieldId).setElementData(actualSetter,dataToSet);	
		
		
		return dataToSet;
	
	}//public String setData(SeleniumElement selElement) 
	
	public static String setData(SeleniumElement selElement) throws NotATableElementException, Exception {
		Class<? extends PageObject> pageClass = selElement.getPageClass();
		return setData(MirthLib.getAppNameForCanonical(pageClass.getCanonicalName()),PageObject.getPageNameOfClass(pageClass), selElement);
	}
	
	//Set subset of test Data
	private static Map<SeleniumElement,String> setData(String tdAppName,String page,SeleniumElement... selElements) throws NotATableElementException, Exception {
		
		Map<SeleniumElement,String> dataMap = new LinkedHashMap<SeleniumElement,String>();
		
		for (SeleniumElement selElement: selElements) {
			dataMap.put(selElement, setData(tdAppName,page,selElement));
		}
		
		return dataMap;
		
	}//public String setData(SeleniumElement selElement) 
	
	protected static Map<SeleniumElement,String> setData(SeleniumElement... selElements) throws NotATableElementException, Exception {
		return setData(MirthLib.getAppNameForCanonical(StaticLib.getPreviousCanonicalOrigin(2)),getCurrentPage(), selElements);
	}
	
	public static Map<SeleniumElement,String> setData(Class<? extends PageObject> pageClass, SeleniumElement... selElements) throws NotATableElementException, Exception {
		return setData(MirthLib.getAppNameForCanonical(pageClass.getCanonicalName()),PageObject.getPageNameOfClass(pageClass), selElements);
	}
	
	//Set subset of test Data
	private static Map<SeleniumElement,String> setData(String tdAppName,String page,List<SeleniumElement> selElementList) throws NotATableElementException, Exception {
		
		Map<SeleniumElement,String> dataMap = new LinkedHashMap<SeleniumElement,String>();
		
		for (SeleniumElement selElement: selElementList) {
			dataMap.put(selElement, setData(tdAppName,page,selElement));
		}
		
		return dataMap;
		
	}//public String setData(SeleniumElement selElement) 
	
	protected static Map<SeleniumElement,String> setData(List<SeleniumElement> selElementList) throws NotATableElementException, Exception {
		return setData(MirthLib.getAppNameForCanonical(StaticLib.getPreviousCanonicalOrigin(2)),getCurrentPage(), selElementList);
	}
	
	public static Map<SeleniumElement,String> setData(Class<? extends PageObject> pageClass, List<SeleniumElement> selElementList) throws NotATableElementException, Exception {
		return setData(MirthLib.getAppNameForCanonical(pageClass.getCanonicalName()),PageObject.getPageNameOfClass(pageClass), selElementList);
	}
	
	//set literal data
	private static String setData(String tdAppName,String page,SeleniumElement selElement,String data) throws NotATableElementException, Exception {
		System.out.println("In Setdata for field " + selElement.get() +"=-------->"+data);
		String fieldId = selElement.get();
		Setter actualSetter = selElement.getSetter();		
		interactWith(tdAppName,page,fieldId).setElementData(actualSetter,data);		
		return data;
	
	}//public String setData(SeleniumElement selElement) 
	
	public static String setData(SeleniumElement selElement,String data) throws NotATableElementException, Exception {
		Class<? extends PageObject> pageClass = selElement.getPageClass();
		return setData(MirthLib.getAppNameForCanonical(pageClass.getCanonicalName()),PageObject.getPageNameOfClass(pageClass), selElement, data);
	}
	
	//Set subset of literal Data
	private static Map<SeleniumElement,String> setData(String tdAppName,String page,Map<SeleniumElement,String> inputMap) throws NotATableElementException, Exception {
		
		for (SeleniumElement selElement: inputMap.keySet()) {
			 SeleniumLib.waitForVisiblity(selElement);
			 
			 if (selElement.get().equals("Chk_ChangePasswordNextLogin")){
				 System.out.println("Writing ob");
				 System.out.println(selElement.interact().getIsCheckBox_ON());
				 selElement.interact().click();
				 System.out.println(selElement.interact().getIsCheckBox_ON());
				 SleepLib.sleep5Seconds();		
				 System.out.println("start");
			 }
			 
			 setData(selElement,inputMap.get(selElement));
			 System.out.println("Done");
			 
			 
			 System.out.println("???????"+selElement.get()+" : " + inputMap.get(selElement));
		}
		
		return inputMap;
		
	}//public String setData(SeleniumElement selElement) 

	protected static Map<SeleniumElement,String> setData(Map<SeleniumElement,String> inputMap) throws NotATableElementException, Exception {
		return setData(MirthLib.getAppNameForCanonical(StaticLib.getPreviousCanonicalOrigin(2)),getCurrentPage(),inputMap);	
	}
	
	public static Map<SeleniumElement,String> setData(Class<? extends PageObject> pageClass, Map<SeleniumElement,String> inputMap) throws NotATableElementException, Exception {
		return setData(MirthLib.getAppNameForCanonical(pageClass.getCanonicalName()),PageObject.getPageNameOfClass(pageClass),inputMap);	
	}
	//End 
	
	
	/**********************
	Setting processed data
	**********************/
	/*
	//set Data from test data
	//set literal data
	private static String setProcessedData(String tdAppName,String page,SeleniumElement selElement,String data,Setter actualSetter) throws NotATableElementException, Exception {
		System.out.println("In setProcessedData for field " + selElement.get() +"=-------->"+data);
		String fieldId = selElement.get();
		interactWith(tdAppName,page,fieldId).setElementData(actualSetter,data);		
		return data;
	
	}//public String setProcessedData(SeleniumElement selElement) 
	
	public static String setProcessedData(SeleniumElement selElement,String data,Setter actualSetter) throws NotATableElementException, Exception {
		Class<? extends PageObject> pageClass = selElement.getPageClass();
		return setProcessedData(MirthLib.getAppNameForCanonical(pageClass.getCanonicalName()),PageObject.getPageNameOfClass(pageClass), selElement, data,actualSetter);
	}
	
	
	//set literal data Setter data pair
	private static String setProcessedData(String tdAppName,String page,SeleniumElement selElement,SetterDataPair sdPair) throws NotATableElementException, Exception {
		System.out.println("In setProcessedData for field " + selElement.get() +"=-------->"+sdPair.data);
		String fieldId = selElement.get();
		interactWith(tdAppName,page,fieldId).setElementData(sdPair.setter,sdPair.data);		
		return sdPair.data;
	
	}//public String setProcessedData(SeleniumElement selElement) 
	
	public static String setProcessedData(SeleniumElement selElement,SetterDataPair sdPair) throws NotATableElementException, Exception {
		Class<? extends PageObject> pageClass = selElement.getPageClass();
		return setProcessedData(MirthLib.getAppNameForCanonical(pageClass.getCanonicalName()),PageObject.getPageNameOfClass(pageClass), selElement, sdPair);
	}
	
	//Set subset of literal Data
	private static Map<SeleniumElement,SetterDataPair> setProcessedData(String tdAppName,String page,Map<SeleniumElement,SetterDataPair> inputMap) throws NotATableElementException, Exception {
		
		for (SeleniumElement selElement: inputMap.keySet()) {
			SetterDataPair sdPair = inputMap.get(selElement);
			 setProcessedData(selElement,sdPair.data,sdPair.setter);
		}
		
		return inputMap;
		
	}//public String setProcessedData(SeleniumElement selElement) 

	protected static Map<SeleniumElement,SetterDataPair> setProcessedData(Map<SeleniumElement,SetterDataPair> inputMap) throws NotATableElementException, Exception {
		return setProcessedData(MirthLib.getAppNameForCanonical(StaticLib.getPreviousCanonicalOrigin(2)),getCurrentPage(),inputMap);	
	}
	
	public static Map<SeleniumElement,SetterDataPair> setProcessedData(Class<? extends PageObject> pageClass, Map<SeleniumElement,SetterDataPair> inputMap) throws NotATableElementException, Exception {
		return setProcessedData(MirthLib.getAppNameForCanonical(pageClass.getCanonicalName()),PageObject.getPageNameOfClass(pageClass),inputMap);	
	}
	
	//End of processedData Setters.
	*/
	
	
	//End of Setters.
	
	//Now starting with getters	
	//one getter
	private static String getData(String tdAppname,String page,SeleniumElement selElement) throws NotATableElementException, Exception {		
		
		return interactWith(tdAppname,page,selElement).getElementData(Interact.getSetter(selElement));
	}
	
	public static String getData(SeleniumElement selElement) throws NotATableElementException, Exception {		
		Class<? extends PageObject> pageClass = selElement.getPageClass();
		return getData(MirthLib.getAppNameForCanonical(pageClass.getCanonicalName()),PageObject.getPageNameOfClass(pageClass), selElement);
	}
	

	//subset of getter
	private static Map<SeleniumElement,String> getData(String tdAppName,String page,SeleniumElement... selElements) throws NotATableElementException, Exception {	
		
		Map<SeleniumElement,String> dataMap = new LinkedHashMap<SeleniumElement,String>();
		
		for (SeleniumElement selElement: selElements) {
			dataMap.put(selElement, getData(tdAppName,page,selElement));
		}
		
		return dataMap;
	}//public static Map<SeleniumElement,String> getData(SeleniumElement... selElements) throws NotATableElementException, Exception {

	protected static Map<SeleniumElement,String> getData(SeleniumElement... selElements) throws NotATableElementException, Exception {	
		return getData(MirthLib.getAppNameForCanonical(StaticLib.getPreviousCanonicalOrigin(2)),getCurrentPage(), selElements);
	}
	
	public static Map<SeleniumElement,String> getData(Class<? extends PageObject> pageClass, SeleniumElement... selElements) throws NotATableElementException, Exception {	
		return getData(MirthLib.getAppNameForCanonical(pageClass.getCanonicalName()),PageObject.getPageNameOfClass(pageClass), selElements);
	}
	
	//END - GUI Setters and Getters for Page Object classes
	
	
}//class
