package com.mirth.application.master;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;

import com.mirth.controller.DriverController;
import com.mirth.controller.ThreadController;
import com.mirth.dataObjects.Browser;
import com.mirth.exceptions.AppNotPresentException;
import com.mirth.exceptions.ClassPageMappingNotFoundException;
import com.mirth.exceptions.InternetExplorerNotSupportedException;
import com.mirth.exceptions.InvalidSeleniumLocatorError;
import com.mirth.exceptions.UnknownBrowserException;
import com.mirth.utility.seleniumLib.MirthWait;
import com.mirth.utility.seleniumLib.SeleniumElement;

public final class General extends PageObject{

	public static void openFreshBrowser(String name,Browser browser) throws InvalidSeleniumLocatorError, IOException, UnknownBrowserException, InterruptedException, InternetExplorerNotSupportedException {
		
		ThreadController.updateThreadDriverMapping(Thread.currentThread().getId(),new DriverController());
		getDriverController().createAndPointToNewDriver(name,browser);		
	}	

	public static void openAndPointToAddtionalBrowser(String name,Browser browser) throws InvalidSeleniumLocatorError, IOException, UnknownBrowserException, InterruptedException, InternetExplorerNotSupportedException {
		getDriverController().createAndPointToNewDriver(name,browser);		
	}
	
	public static void switchToBrowser(String name) {
		getDriverController().pointToDriver(name);
	}
	
	public static void closeAndDestroyBrowser(String name) {
		getDriverController().closeAndRemoveDriver(name);
	}
	
	public static WebDriver browser() {
		return getDriverController().getCurrentMirthDriver().getDriver();
	}
	
	public static MirthWait getBrowserWaitTime(){
		return getDriverController().getCurrentMirthDriver().getWaitTime();
	}
	
	public static void setBrowserWaitTime(long timeOut, TimeUnit tUnit){
		getDriverController().getCurrentMirthDriver().setWaitTime(timeOut, tUnit);
	}

	public static void scrollBrowser(int pixels) {
		((JavascriptExecutor)browser()).executeScript("scroll(0, "+pixels+");");
	}

	public static void acceptAlert() {	
		String winHandleBefore = browser().getWindowHandle();
		browser().switchTo().alert().accept();	
		browser().switchTo().window(winHandleBefore);		
	}
	
	public static boolean checkIfElementExistsImmediately(SeleniumElement selElement) throws IOException, ClassPageMappingNotFoundException, AppNotPresentException, Exception{
		
		long time = General.getBrowserWaitTime().getTimeOut();
		TimeUnit timeUnit = General.getBrowserWaitTime().getTimeUnit();
		boolean elementExists = false;
		
		General.setBrowserWaitTime(0, TimeUnit.SECONDS);
		
		try{
			selElement.interact();
			elementExists = true;
		}catch(ElementNotVisibleException | NoSuchElementException envE){
			elementExists = false;
		}
		finally{
			General.setBrowserWaitTime(time,timeUnit);
		}
		
		return elementExists;				
	}	
	
	
	//Common Functions
	public static boolean checkIfElementVisibleImmediately(SeleniumElement selElement) throws IOException, ClassPageMappingNotFoundException, AppNotPresentException, Exception{
		
		boolean isDisplayed = false;
		long time = General.getBrowserWaitTime().getTimeOut();
		TimeUnit timeUnit = General.getBrowserWaitTime().getTimeUnit();
		General.setBrowserWaitTime(0, TimeUnit.SECONDS);
		try{
			isDisplayed =  selElement.interact().webElement().isDisplayed();
		}catch(ElementNotVisibleException | NoSuchElementException envE){
			isDisplayed = false;
		}finally{
			General.setBrowserWaitTime(time, timeUnit);	
		}
		
		return isDisplayed;	
		
	}

	
}
