package com.mirth.application.master;

import java.io.IOException;

public interface TestCaseMapping {

	public String getTestClassRelativeName(String configKey) throws IOException;
	public boolean isConfigPropertyNull();
}
