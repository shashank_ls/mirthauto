package com.mirth.application.master;

import java.io.IOException;

public interface ClassPageMapping {

	public String getPageNameForClass(String configKey) throws IOException;
	public boolean isConfigPropertyNull();
}
