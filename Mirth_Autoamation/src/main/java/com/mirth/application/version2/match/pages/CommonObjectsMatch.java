
package com.mirth.application.version2.match.pages;

import com.mirth.application.master.PageObject;
import com.mirth.utility.seleniumLib.ButtonS;
import com.mirth.utility.seleniumLib.LinkS;
import com.mirth.utility.seleniumLib.TextBoxS;

public final class CommonObjectsMatch extends PageObject{
	
	public static TextBoxS $SearchUsersTextBox = new TextBoxS("SearchUsersTextBox");
	public static ButtonS $QuickSearchMagnifyingButton = new ButtonS("QuickSearchMagnifyingButton");
	public static LinkS $Administration = new LinkS("Administration"); 
	public static LinkS $UsersLinkInAdministrationDropDown = new LinkS("UsersLinkInAdministrationDropDown");
	public static LinkS $SignOutButtonOnKanaToolBar = new LinkS("SignOutButtonOnKanaToolBar");
	public static final LinkS $MirthResultsInKanaToolBar = new LinkS("MirthResultsInKanaToolBar");
	public static final LinkS $MirthMatchInKanaToolBar = new LinkS("MirthMatchInKanaToolBar");
		
	
	public static void goToUsersPage() throws Exception{
		interactWith($Administration).click();
		interactWith($UsersLinkInAdministrationDropDown).click();
	}
	
	public static void setSearchTextBox(String text) throws Exception {		
		interactWith($SearchUsersTextBox).setText(text);
	}
	
	public static void clickMagnigyingSearchButton() throws Exception {
		interactWith($QuickSearchMagnifyingButton).click();
	}
	
	/*Search and advanced Search*/	
	//1. Basic searches	
	public static void searchTextBoxFor(String text) throws Exception {
		setSearchTextBox(text);
		clickMagnigyingSearchButton();
	}	
	

	
}//class
