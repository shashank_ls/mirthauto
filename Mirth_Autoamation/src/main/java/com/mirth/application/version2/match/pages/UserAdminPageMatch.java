
package com.mirth.application.version2.match.pages;

import java.util.LinkedHashMap;
import java.util.Map;

import com.mirth.application.master.PageObject;
import com.mirth.application.version2.mso.pages.CommonObjectsMSO;
import com.mirth.application.version2.mso.pages.NewUserPageMSO;
import com.mirth.results.QAProof;
import com.mirth.utility.general.SleepLib;
import com.mirth.utility.general.StaticLib;
import com.mirth.utility.seleniumLib.ImageS;
import com.mirth.utility.seleniumLib.SeleniumElement;
import com.mirth.utility.seleniumLib.TableS;

public final class UserAdminPageMatch extends PageObject{
	
	public static final TableS $UsersTable =   new TableS ("UsersTable"); 
	
	//InsideUsersTable
	public static ImageS $StatusImageInFirstRowOfUsersTable = new ImageS("StatusImageInFirstRowOfUsersTable");
	
	//Element Constants
	public static String usersTableStatus = "Status";
	public static String usersTableName = "Name";
	public static String usersTableAccountID = "Account ID";
	public static String usersTableEmail = "Email";
	public static String usersTableRole = "Roles";	
	
	
	public static Map<SeleniumElement,Boolean> verifyUserTableAsPerMso(Map<SeleniumElement,String> actualMap) throws Exception{
		Map<SeleniumElement,String> verificationMap = StaticLib.getNewMapCopy(actualMap);
		actualMap=null;
		
		Map<SeleniumElement,Boolean> verificationResult = new LinkedHashMap<SeleniumElement,Boolean>();
		
		CommonObjectsMSO.searchTextBoxFor(verificationMap.get(NewUserPageMSO.$AccountIDTextBox));
		SleepLib.sleep2Seconds();
		
		$UsersTable.forceProcessTable();
		QAProof.screenshot();
		
		String nameActual = $UsersTable.getValueFromTable(usersTableName, 1);
		String accountIDActual = $UsersTable.getValueFromTable(usersTableAccountID, 1);
		String emailActual = $UsersTable.getValueFromTable(usersTableEmail, 1);
		String RolesActual = $UsersTable.getValueFromTable(usersTableRole, 1);
		
		String statusSourceActual = StaticLib.getValueAfterLastIndex(getData($StatusImageInFirstRowOfUsersTable),'/');
		String statusSourceExpected = "object_active.png";
		
		verificationResult.put($StatusImageInFirstRowOfUsersTable,verifyStringData(usersTableStatus,statusSourceExpected,statusSourceActual,false));
		verificationResult.put(NewUserPageMSO.$AccountIDTextBox,verifyStringData(usersTableAccountID,verificationMap.get(NewUserPageMSO.$AccountIDTextBox),accountIDActual,false));
		verificationResult.put(NewUserPageMSO.$Name,verifyStringData(usersTableName,verificationMap.get(NewUserPageMSO.$Name),nameActual,false));
		verificationResult.put(NewUserPageMSO.$Email,verifyStringData(usersTableEmail,verificationMap.get(NewUserPageMSO.$Email),emailActual,false));
		//Results Role might be different, need to be careful here
		verificationResult.put(NewUserPageMSO.$SelectListInAddSecurityRolePopup,verifyStringData(usersTableRole,verificationMap.get(NewUserPageMSO.$SelectListInAddSecurityRolePopup),RolesActual,false));	
		
		return verificationResult;
		
	}	
}//class