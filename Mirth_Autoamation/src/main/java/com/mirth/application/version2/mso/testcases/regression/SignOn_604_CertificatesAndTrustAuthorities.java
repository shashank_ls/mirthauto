
package com.mirth.application.version2.mso.testcases.regression;

import java.util.Map;
import java.util.concurrent.TimeUnit;

import com.mirth.application.master.General;
import com.mirth.application.master.PageObject;
import com.mirth.application.master.Testcase;
import com.mirth.application.version2.mso.pages.CertificatesPageMSO;
import com.mirth.application.version2.mso.pages.CommonObjectsMSO;
import com.mirth.application.version2.mso.pages.LoginPageMSO;
import com.mirth.application.version2.mso.pages.TrustAuthoritiesPageMSO;
import com.mirth.results.QAProof;
import com.mirth.utility.general.SleepLib;
import com.mirth.utility.general.StaticLib;
import com.mirth.utility.seleniumLib.SeleniumElement;

public class SignOn_604_CertificatesAndTrustAuthorities extends Testcase {
	

	@Override
	protected void steps() throws Exception {		

		QAProof.writeInfo(">>Step 1 to 6");
		General.openFreshBrowser("main", super.testInventory.browser);
		General.setBrowserWaitTime(10,TimeUnit.SECONDS);
		LoginPageMSO.launch();
		LoginPageMSO.login();
		CommonObjectsMSO.clickUsersTab();
		CommonObjectsMSO.$CertificatesTab.interact().click();
		SleepLib.sleep1Second();
		CertificatesPageMSO.loadCertificates();
		String cert1FingerPrint = StaticLib.getPEMFingerprintWithOutSpaces(CertificatesPageMSO.cert1Path);
		
		CertificatesPageMSO.uploadCertificate(CertificatesPageMSO.cert1Path,true);
		CommonObjectsMSO.$CertificatesTab.interact().click();
		
		
		CommonObjectsMSO.searchTextBoxFor(cert1FingerPrint);
		CertificatesPageMSO.$CertificatesTable.forceProcessTable();	
		if (CertificatesPageMSO.$CertificatesTable.isTableEmpty()){
			this.writeText("Certificate not added succesfully");
		}
		else {
			if (cert1FingerPrint.equalsIgnoreCase(CertificatesPageMSO.$CertificatesTable.getValueFromTable(CertificatesPageMSO.fingerPrintHeading, 1))) {
				QAProof.writeText("Certificate Added succesfully.");			
			}
			else{
				QAProof.writeText("Incorrect Certificate Added.");
			}
		}
		QAProof.screenshot();	
		CertificatesPageMSO.$CertificatesTable.getCellTDElement(1, 2).click();
		
		PageObject.verifyStringData(CertificatesPageMSO.$CertificateFingerPrintInEditCertificate, cert1FingerPrint, false);
				
		SleepLib.sleep1Second();
		CommonObjectsMSO.$BackToListLinkInLeftPane.interact().click();
		
		QAProof.writeInfo(">>Step 7");
		CertificatesPageMSO.$CertificatesTable.getCellTDElement(1,1).click();
		CommonObjectsMSO.$DeleteLinkInLeftPane.interact().click();
		SleepLib.sleepMilliSeconds(500);
		General.acceptAlert();
		QAProof.screenshot();
		CommonObjectsMSO.searchTextBoxFor(cert1FingerPrint);
		QAProof.screenshot(CertificatesPageMSO.$CertificatesTable);
		CertificatesPageMSO.$CertificatesTable.forceProcessTable();
		
		if (CertificatesPageMSO.$CertificatesTable.isTableEmpty() ){
			QAProof.screenshot();
		}
		
		QAProof.writeInfo(">>Step 8");
		CertificatesPageMSO.uploadCertificate(CertificatesPageMSO.cert1Path,true);
		
		QAProof.writeInfo(">>Step 9 to 15");
		CommonObjectsMSO.$TrushAuthoritiesTab.interact().click();
		TrustAuthoritiesPageMSO.$CreateTrustAuthorityLink.interact().click();
		
		
		Map<SeleniumElement,String> trustAuthority1 = TrustAuthoritiesPageMSO.createTrustAuthorityWithSave(
				StaticLib.getGloballyUniqueStringValue(false), 
				StaticLib.getGloballyUniqueStringValue(false),
				StaticLib.getGloballyUniqueEmailValue("mirth"),
				CertificatesPageMSO.cert1Path);
		QAProof.screenshot(TrustAuthoritiesPageMSO.$FingerPrintInEditTrustAuthority);
		
		PageObject.verifyStringData(TrustAuthoritiesPageMSO.$FingerPrintInEditTrustAuthority, cert1FingerPrint, false);
		CommonObjectsMSO.$BackToListLinkInLeftPane.interact().click();
		
		CommonObjectsMSO.searchTextBoxFor(trustAuthority1.get(TrustAuthoritiesPageMSO.$NameTextBoxInNewTrustAuthority));
		QAProof.screenshot(TrustAuthoritiesPageMSO.$TrustAuthoritiesTable);
		TrustAuthoritiesPageMSO.$TrustAuthoritiesTable.forceProcessTable();		
		QAProof.screenshot(TrustAuthoritiesPageMSO.$TrustAuthoritiesTable);		
		
		PageObject.verifyStringData(TrustAuthoritiesPageMSO.samlIssuerHeading, 
					trustAuthority1.get(TrustAuthoritiesPageMSO.$SamLIssuerTextBoxInNewTrustAuthority),
					TrustAuthoritiesPageMSO.$TrustAuthoritiesTable.getValueFromTable(TrustAuthoritiesPageMSO.samlIssuerHeading, 1)
					,false);		
					
		PageObject.verifyStringData(TrustAuthoritiesPageMSO.nameHeading, 
				trustAuthority1.get(TrustAuthoritiesPageMSO.$NameTextBoxInNewTrustAuthority),
				TrustAuthoritiesPageMSO.$TrustAuthoritiesTable.getValueFromTable(TrustAuthoritiesPageMSO.nameHeading, 1)
				,false);		

		
		PageObject.verifyStringData(TrustAuthoritiesPageMSO.emailHeading, 
				trustAuthority1.get(TrustAuthoritiesPageMSO.$EmailTextBoxInNewTrustAuthority),
				TrustAuthoritiesPageMSO.$TrustAuthoritiesTable.getValueFromTable(TrustAuthoritiesPageMSO.emailHeading, 1)
				,false);		

		
		CommonObjectsMSO.searchTextBoxFor("");

				QAProof.writeInfo(">>Step 16 to 19");
		TrustAuthoritiesPageMSO.$CreateTrustAuthorityLink.interact().click();
		Map<SeleniumElement,String> trustAuthority2 = TrustAuthoritiesPageMSO.createTrustAuthorityWithSave(
				StaticLib.getGloballyUniqueStringValue(false), 
				StaticLib.getGloballyUniqueStringValue(false),
				StaticLib.getGloballyUniqueEmailValue("mirth"),
				CertificatesPageMSO.cert2Path);
		
		String cert2FingerPrint = StaticLib.getPEMFingerprintWithOutSpaces(CertificatesPageMSO.cert2Path);
		PageObject.verifyStringData(TrustAuthoritiesPageMSO.$FingerPrintInEditTrustAuthority,cert2FingerPrint,false);
		CommonObjectsMSO.$BackToListLinkInLeftPane.interact().click();
		
		QAProof.writeInfo(">>Step 20 to 21");
		CommonObjectsMSO.$CertificatesTab.interact().click();
		
		CommonObjectsMSO.searchTextBoxFor(cert2FingerPrint);
		QAProof.screenshot(CertificatesPageMSO.$CertificatesTable);
		CertificatesPageMSO.$CertificatesTable.forceProcessTable();		
		QAProof.screenshot();
		if (!CertificatesPageMSO.$CertificatesTable.isTableEmpty()) {
			if (cert2FingerPrint.equalsIgnoreCase(CertificatesPageMSO.$CertificatesTable.getValueFromTable(CertificatesPageMSO.fingerPrintHeading, 1))) {
				QAProof.writeText("Certificate Added succesfully when added via trust authority");			
			}
			else {
				QAProof.writeText("Certificate was not Added succesfully when added via trust authority");
			}
		}
		
		else{
			QAProof.writeText("Certificate was not Added succesfully when added via trust authority");
		}
		
		QAProof.writeInfo(">>Step 22 to 24");
		CertificatesPageMSO.$CertificatesTable.getCellTDElement(1,1).click();
		CommonObjectsMSO.$DeleteLinkInLeftPane.interact().click();
		SleepLib.sleepMilliSeconds(500);
		General.acceptAlert();
		QAProof.screenshot();
		
		String deleteErrorExpected = "1 Certificates cannot be deleted, due to association with one or more third party trust authorities";
		PageObject.verifyStringData(CommonObjectsMSO.$InformationAlertAtTop, deleteErrorExpected, false);
		
		
		QAProof.writeInfo(">>Step 23");
		CommonObjectsMSO.$TrushAuthoritiesTab.interact().click();
		TrustAuthoritiesPageMSO.$CreateTrustAuthorityLink.interact().click();
		
		Map<SeleniumElement,String> trustAuthority3 = TrustAuthoritiesPageMSO.createTrustAuthorityWithSave(
				StaticLib.getGloballyUniqueStringValue(false), 
				StaticLib.getGloballyUniqueStringValue(false),
				StaticLib.getGloballyUniqueEmailValue("mirth"),
				CertificatesPageMSO.cert1Path);
		QAProof.screenshot(TrustAuthoritiesPageMSO.$FingerPrintInEditTrustAuthority);
		
		CommonObjectsMSO.$CertificatesTab.interact().click();
		CommonObjectsMSO.searchTextBoxFor(cert1FingerPrint);
		CertificatesPageMSO.$CertificatesTable.forceProcessTable();
		
		if (CertificatesPageMSO.$CertificatesTable.getDataRowsCount() != 1){
			if (!cert1FingerPrint.equalsIgnoreCase(CertificatesPageMSO.$CertificatesTable.getValueFromTable(CertificatesPageMSO.fingerPrintHeading, 1))) {
				QAProof.writeText("Incorrect Certificate present");
			}		
			else{
				writeText("Duplicate certificate not created");
			}
		}//if (CertificatesPageMSO.$CertificatesTable.getDataRowsCount() != 1)
		
		else{
			QAProof.writeText("Looks like duplicate certificate is added for certificate with fingerprint: "+cert1FingerPrint);
		}
				
		QAProof.writeInfo(">>Step 25");
		CertificatesPageMSO.$CertificatesTable.getCellTDElement(1,1).click();
		CommonObjectsMSO.$DeleteLinkInLeftPane.interact().click();
		SleepLib.sleepMilliSeconds(500);
		General.acceptAlert();
		QAProof.screenshot();
		
		PageObject.verifyStringData(CommonObjectsMSO.$InformationAlertAtTop, deleteErrorExpected, false);
		
		QAProof.writeInfo(">>Step 26");
		
		CommonObjectsMSO.$TrushAuthoritiesTab.interact().click();
		CommonObjectsMSO.searchTextBoxFor(trustAuthority3.get(TrustAuthoritiesPageMSO.$NameTextBoxInNewTrustAuthority));
		TrustAuthoritiesPageMSO.$TrustAuthoritiesTable.forceProcessTable();
		
		if (TrustAuthoritiesPageMSO.$TrustAuthoritiesTable.isTableEmpty() || !trustAuthority3.get(TrustAuthoritiesPageMSO.$NameTextBoxInNewTrustAuthority)
							.equalsIgnoreCase(TrustAuthoritiesPageMSO.$TrustAuthoritiesTable
									.getCellTDElement(TrustAuthoritiesPageMSO.nameHeading, 1).getText())) {
			
			verify.reportIssue("Trust athority3 with name: "+trustAuthority3.get(TrustAuthoritiesPageMSO.$NameTextBoxInNewTrustAuthority) + " is NOT seen.");
		}
		
		TrustAuthoritiesPageMSO.$TrustAuthoritiesTable.getCellTDElement(1, 1).click();
		CommonObjectsMSO.$DeleteLinkInLeftPane.interact().click();
		SleepLib.sleepMilliSeconds(500);
		General.acceptAlert();
		QAProof.screenshot();
		
		QAProof.writeInfo(">>Step 27");
		CommonObjectsMSO.$CertificatesTab.interact().click();
		CommonObjectsMSO.searchTextBoxFor(cert1FingerPrint);
		CertificatesPageMSO.$CertificatesTable.forceProcessTable();
		QAProof.screenshot();
		if (CertificatesPageMSO.$CertificatesTable.getDataRowsCount() == 1){
			if (!cert1FingerPrint.equalsIgnoreCase(CertificatesPageMSO.$CertificatesTable.getValueFromTable(CertificatesPageMSO.fingerPrintHeading, 1))) {
				writeText("Incorrect Certificate present");
			}		
			else{
				QAProof.writeText("Associated certificate 1 not deleted as expected when the Trust authority is deleted.");
			}
		}//if (CertificatesPageMSO.$CertificatesTable.getDataRowsCount() != 1)
		
		else{
			writeText("Looks like duplicate certificate is added for certificate with fingerprint: "+cert1FingerPrint);
		}
				
		QAProof.writeInfo(">>Step 28");
		CommonObjectsMSO.searchTextBoxFor("");
		CertificatesPageMSO.$CertificatesTable.forceProcessTable();
		
		//Click all Certificates
		for (int row=1;row<=CertificatesPageMSO.$CertificatesTable.getDataRowsCount();row++){
			CertificatesPageMSO.$CertificatesTable.getCellTDElement(row, 1).click();
		}
		QAProof.screenshot(CommonObjectsMSO.$DeleteLinkInLeftPane);
		CommonObjectsMSO.$DeleteLinkInLeftPane.interact().click();
		SleepLib.sleepMilliSeconds(500);
		General.acceptAlert();
		QAProof.screenshot();
		
		deleteErrorExpected = "Certificates cannot be deleted, due to association with one or more third party trust authorities";
		if (!CommonObjectsMSO.$InformationAlertAtTop.interact().getText().contains(deleteErrorExpected)){
			verify.reportIssue("Certificates deleted eventhough they were NOT supposed to");
		}
		else {
			verify.reportSuccess("Certificates not deleted as expected");
		}
		
		QAProof.writeInfo(">>Step 29 and 30");
		CommonObjectsMSO.$TrushAuthoritiesTab.interact().click();
		CommonObjectsMSO.searchTextBoxFor(trustAuthority2.get(TrustAuthoritiesPageMSO.$NameTextBoxInNewTrustAuthority));
		TrustAuthoritiesPageMSO.$TrustAuthoritiesTable.forceProcessTable();
		TrustAuthoritiesPageMSO.$TrustAuthoritiesTable.getCellTDElement(1, 1).click();
		CommonObjectsMSO.$EditLinkInLeftPane.interact().click();
		
		
		trustAuthority2 = TrustAuthoritiesPageMSO.createTrustAuthorityWithSave(
				StaticLib.getGloballyUniqueStringValue(false), 
				StaticLib.getGloballyUniqueStringValue(false),
				StaticLib.getGloballyUniqueEmailValue("mirth"),
				CertificatesPageMSO.cert3Path);
		
		String cert3FingerPrint = StaticLib.getPEMFingerprintWithOutSpaces(CertificatesPageMSO.cert3Path);
		PageObject.verifyStringData(TrustAuthoritiesPageMSO.$FingerPrintInEditTrustAuthority, cert3FingerPrint, false);
		
		CommonObjectsMSO.$BackToListLinkInLeftPane.interact().click();
		
		QAProof.writeInfo(">>Step 31");
		CommonObjectsMSO.searchTextBoxFor(trustAuthority2.get(TrustAuthoritiesPageMSO.$NameTextBoxInNewTrustAuthority));
		QAProof.screenshot(TrustAuthoritiesPageMSO.$TrustAuthoritiesTable);
		TrustAuthoritiesPageMSO.$TrustAuthoritiesTable.forceProcessTable();		
		QAProof.screenshot(TrustAuthoritiesPageMSO.$TrustAuthoritiesTable);		
		
		PageObject.verifyStringData(TrustAuthoritiesPageMSO.samlIssuerHeading, 
					trustAuthority2.get(TrustAuthoritiesPageMSO.$SamLIssuerTextBoxInNewTrustAuthority),
					TrustAuthoritiesPageMSO.$TrustAuthoritiesTable.getValueFromTable(TrustAuthoritiesPageMSO.samlIssuerHeading, 1)
					,false);		
					
		PageObject.verifyStringData(TrustAuthoritiesPageMSO.nameHeading, 
				trustAuthority2.get(TrustAuthoritiesPageMSO.$NameTextBoxInNewTrustAuthority),
				TrustAuthoritiesPageMSO.$TrustAuthoritiesTable.getValueFromTable(TrustAuthoritiesPageMSO.nameHeading, 1)
				,false);		

		
		PageObject.verifyStringData(TrustAuthoritiesPageMSO.emailHeading, 
				trustAuthority2.get(TrustAuthoritiesPageMSO.$EmailTextBoxInNewTrustAuthority),
				TrustAuthoritiesPageMSO.$TrustAuthoritiesTable.getValueFromTable(TrustAuthoritiesPageMSO.emailHeading, 1)
				,false);	
		
		QAProof.writeInfo(">>Step 32");
		CommonObjectsMSO.$CertificatesTab.interact().click();
		CommonObjectsMSO.searchTextBoxFor(cert2FingerPrint);
		CertificatesPageMSO.$CertificatesTable.forceProcessTable();
		CertificatesPageMSO.$CertificatesTable.getCellTDElement(1,1).click();
		CommonObjectsMSO.$DeleteLinkInLeftPane.interact().click();
		SleepLib.sleepMilliSeconds(500);
		General.acceptAlert();
		QAProof.screenshot();
		
		CertificatesPageMSO.$CertificatesTable.forceProcessTable();
		if (!CertificatesPageMSO.$CertificatesTable.isTableEmpty()) {
			verify.reportIssue("Certificate 2 is NOT deleted succesfully eventhough its previous associated trustauthority is linked to some other certificate.");
		}
		
		QAProof.writeInfo(">>Step 33");
		CommonObjectsMSO.$TrushAuthoritiesTab.interact().click();
		CommonObjectsMSO.searchTextBoxFor("");
		TrustAuthoritiesPageMSO.$TrustAuthoritiesTable.forceProcessTable();
		
		//Click all Certificates
		for (int row=1;row<=TrustAuthoritiesPageMSO.$TrustAuthoritiesTable.getDataRowsCount();row++){
			TrustAuthoritiesPageMSO.$TrustAuthoritiesTable.getCellTDElement(row, 1).click();
		}
		CommonObjectsMSO.$DeleteLinkInLeftPane.interact().click();
		SleepLib.sleepMilliSeconds(500);
		General.acceptAlert();
		QAProof.screenshot();
		
		TrustAuthoritiesPageMSO.$TrustAuthoritiesTable.forceProcessTable();
		if (!TrustAuthoritiesPageMSO.$TrustAuthoritiesTable.isTableEmpty()) {
			verify.reportIssue("All trust authorities are NOT deleted succesfully when they are supposed to");
		}
		
		QAProof.writeInfo(">>Step 34");
		CommonObjectsMSO.$CertificatesTab.interact().click();
		CommonObjectsMSO.searchTextBoxFor("");
		CertificatesPageMSO.$CertificatesTable.forceProcessTable();
		
		for (int row=1;row<=CertificatesPageMSO.$CertificatesTable.getDataRowsCount();row++){
			CertificatesPageMSO.$CertificatesTable.getCellTDElement(row, 1).click();
		}
		
		QAProof.screenshot(CommonObjectsMSO.$DeleteLinkInLeftPane);
		CommonObjectsMSO.$DeleteLinkInLeftPane.interact().click();
		SleepLib.sleepMilliSeconds(500);
		General.acceptAlert();
		QAProof.screenshot();
		
		CertificatesPageMSO.$CertificatesTable.forceProcessTable();
		if (!CertificatesPageMSO.$CertificatesTable.isTableEmpty()) {
			verify.reportIssue("Certificate 2 is NOT deleted succesfully eventhough its previous associated trustauthority is linked to some other certificate.");
		}
		
		General.closeAndDestroyBrowser("main");
		
		QAProof.writeInfo(">>Test Case End<<");
	}
	

	

	
	
		
}//class
