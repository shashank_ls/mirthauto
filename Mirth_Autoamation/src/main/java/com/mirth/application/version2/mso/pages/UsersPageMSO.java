
package com.mirth.application.version2.mso.pages;
import java.util.LinkedHashMap;
import java.util.Map;

import com.mirth.application.master.General;
import com.mirth.application.master.PageObject;
import com.mirth.exceptions.NotATableElementException;
import com.mirth.results.QAProof;
import com.mirth.utility.general.SleepLib;
import com.mirth.utility.general.StaticLib;
import com.mirth.utility.seleniumLib.ButtonS;
import com.mirth.utility.seleniumLib.ImageS;
import com.mirth.utility.seleniumLib.LinkS;
import com.mirth.utility.seleniumLib.RadioButtonS;
import com.mirth.utility.seleniumLib.SeleniumElement;
import com.mirth.utility.seleniumLib.SeleniumLib;
import com.mirth.utility.seleniumLib.SimpleDropDownS;
import com.mirth.utility.seleniumLib.TableS;

public class UsersPageMSO extends PageObject {

	public static LinkS $CreateUserLinkInUserActions = new LinkS("CreateUserLinkInUserActions");
	public static TableS $UsersTable = new TableS("UsersTable");	
	
	public static LinkS $UserPageClearSearchCriteria = new LinkS("UserPageClearSearchCriteria");
	public static LinkS $LockORUnlockUserAccountsLink = new LinkS("LockORUnlockUserAccountsLink");
	public static LinkS $SyncUserAccountsLink = new LinkS("SyncUserAccountsLink");
	public static RadioButtonS $LockRadioButtonInLockUnlockPopup = new RadioButtonS("LockRadioButtonInLockUnlockPopup");
	public static RadioButtonS $UnlockRadioButtonInLockUnlockPopup = new RadioButtonS("UnlockRadioButtonInLockUnlockPopup");
	public static SimpleDropDownS $SelectTemplateListInLockUnlockPopup = new SimpleDropDownS("SelectTemplateListInLockUnlockPopup");
	public static SimpleDropDownS $SelectRoelsListInLockUnlockPopup = new SimpleDropDownS("SelectRoelsListInLockUnlockPopup");
	public static RadioButtonS $AllUsersExceptAdminRadioButtonInLockUnlockPopup = new RadioButtonS("AllUsersExceptAdminRadioButtonInLockUnlockPopup");
	public static RadioButtonS $AllUsersOfTemplateRadioButtonInLockUnlockPopup = new RadioButtonS("AllUsersOfTemplateRadioButtonInLockUnlockPopup");
	public static RadioButtonS $AllUsersOfRolesRadioButtonInLockUnlockPopup = new RadioButtonS("AllUsersOfRolesRadioButtonInLockUnlockPopup");
	public static RadioButtonS $AllUsersExceptAdminRadioButtonInAccountSyncPopup = new RadioButtonS("AllUsersExceptAdminRadioButtonInAccountSyncPopup");
	
	
	public static ButtonS $SubmitButtonInLockUnlockPopup = new ButtonS("SubmitButtonInLockUnlockPopup");
	public static ButtonS $CancelButtonInLockUnlockPopup = new ButtonS("CancelButtonInLockUnlockPopup");
	
	public static RadioButtonS $AllUsersOfTemplateRadioButtonInAccountSyncPopUp = new RadioButtonS("AllUsersOfTemplateRadioButtonInAccountSyncPopUp");
	public static ButtonS $SubmitButtonInAccountSyncPopup  = new ButtonS("SubmitButtonInAccountSyncPopup");
	public static ButtonS $CancelButtonInAccountSyncPopup = new ButtonS("CancelButtonInAccountSyncPopup");
	public static SimpleDropDownS $SelectListInAccountSyncPopup = new SimpleDropDownS("SelectListInAccountSyncPopup");

	
	
	//InsideUsersTable
	public static ImageS $StatusImageInFirstRowOfUsersTable = new ImageS("StatusImageInFirstRowOfUsersTable");
	
	//Element Constants
	public static String usersTableStatus = "Status";
	public static String usersTableName = "Name";
	public static String usersTableAccountID = "Account ID";
	public static String usersTableEmail = "Email";
	public static String usersTableRole = "Roles";	
	
	public static void clickCreateNewUser() throws Exception {		
		interactWith($CreateUserLinkInUserActions).click();		
	}
	
	public static void clearSearchCriteria() throws Exception {		
		interactWith($UserPageClearSearchCriteria).click();		
	}
	
	public static void syncAllUsers() throws Exception {
		
		if (!General.checkIfElementVisibleImmediately(UsersPageMSO.$AllUsersOfTemplateRadioButtonInAccountSyncPopUp)){
			$SyncUserAccountsLink.interact().click();
		}
		
		SeleniumLib.waitForVisiblity($AllUsersExceptAdminRadioButtonInLockUnlockPopup);
		setData($AllUsersExceptAdminRadioButtonInAccountSyncPopup,"ON");
		QAProof.screenshot($SubmitButtonInAccountSyncPopup);
		$SubmitButtonInAccountSyncPopup.interact().click();
		SleepLib.sleep3Seconds();
		
		String syncSuccess = "Successfully synced the user accounts";
		PageObject.verifyStringData(CommonObjectsMSO.$InformationAlertAtTop, syncSuccess, false);
		QAProof.screenshot(CommonObjectsMSO.$InformationAlertAtTop);
		SleepLib.sleep2Seconds();
		
	}
	
	public static void syncAllUsersOfTemplate(String templateIdentifier,boolean isSubmit) throws Exception{
		
		if (!General.checkIfElementVisibleImmediately(UsersPageMSO.$AllUsersOfTemplateRadioButtonInAccountSyncPopUp)){
			$SyncUserAccountsLink.interact().click();
		}
		
		setData($AllUsersOfTemplateRadioButtonInAccountSyncPopUp,"ON");
		setData($SelectListInAccountSyncPopup,templateIdentifier);
		
		QAProof.screenshot($SubmitButtonInAccountSyncPopup);
		SleepLib.sleep2Seconds();
		
		if (isSubmit){
			
			$SubmitButtonInAccountSyncPopup.interact().click();
		}
		
		String syncSuccess = "Successfully synced the user accounts";
		PageObject.verifyStringData(CommonObjectsMSO.$InformationAlertAtTop, syncSuccess, false);
		QAProof.screenshot(CommonObjectsMSO.$InformationAlertAtTop);
		SleepLib.sleep2Seconds();
		
	}
	
	public static Map<SeleniumElement, Boolean > verifyUserDataAsPerCreation(Map<SeleniumElement, String> actualMap) throws NotATableElementException, Exception {
		Map<SeleniumElement,String> verificationMap = StaticLib.getNewMapCopy(actualMap);
		actualMap=null;
		
		Map<SeleniumElement,Boolean> verificationResult = new LinkedHashMap<SeleniumElement,Boolean>();
		
		CommonObjectsMSO.searchTextBoxFor(verificationMap.get(NewUserPageMSO.$AccountIDTextBox));
		SleepLib.sleep2Seconds();
		
		QAProof.screenshot($UsersTable);
		$UsersTable.forceProcessTable();
		
		String nameActual = $UsersTable.getValueFromTable(usersTableName, 1);
		String accountIDActual = $UsersTable.getValueFromTable(usersTableAccountID, 1);
		String emailActual = $UsersTable.getValueFromTable(usersTableEmail, 1);
		String RolesActual = $UsersTable.getValueFromTable(usersTableRole, 1);
		
		String statusSourceActual = StaticLib.getValueAfterLastIndex(getData($StatusImageInFirstRowOfUsersTable),'/');
		String statusSourceExpected = "object_active.png"; 
		
		if (verificationMap.get(NewUserPageMSO.$AccountStatusDropDown).equalsIgnoreCase("Locked")){
			statusSourceExpected = "lock.png";
		}
		
		
		verificationResult.put($StatusImageInFirstRowOfUsersTable,verifyStringData(usersTableStatus,statusSourceExpected,statusSourceActual,false));
		verificationResult.put(NewUserPageMSO.$Name,verifyStringData(usersTableName,verificationMap.get(NewUserPageMSO.$Name),nameActual,false));
		verificationResult.put(NewUserPageMSO.$AccountIDTextBox,verifyStringData(usersTableAccountID,verificationMap.get(NewUserPageMSO.$AccountIDTextBox),accountIDActual,false));
		verificationResult.put(NewUserPageMSO.$Email,verifyStringData(usersTableEmail,verificationMap.get(NewUserPageMSO.$Email),emailActual,false));
		//Results Role might be different, need to be careful here
		verificationResult.put(NewUserPageMSO.$SelectListInAddSecurityRolePopup,verifyStringData(usersTableRole,verificationMap.get(NewUserPageMSO.$SelectListInAddSecurityRolePopup),RolesActual,false));	
		
		return verificationResult;
		
	}
	
	
	
	
	//2. Advanced Searches
	
	/* END -Search functionalities */	
	
	
}//class

