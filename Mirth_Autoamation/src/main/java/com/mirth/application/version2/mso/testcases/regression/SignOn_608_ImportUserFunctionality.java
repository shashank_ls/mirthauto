
package com.mirth.application.version2.mso.testcases.regression;

import java.util.concurrent.TimeUnit;

import com.mirth.application.master.General;
import com.mirth.application.master.Testcase;
import com.mirth.application.version2.mso.pages.LoginPageMSO;
import com.mirth.results.QAProof;

public class SignOn_608_ImportUserFunctionality extends Testcase {
	
	@Override
	protected void steps() throws Exception {		
		
		QAProof.writeInfo(">>Step 1");
		General.openFreshBrowser("main", super.testInventory.browser);
		General.setBrowserWaitTime(10,TimeUnit.SECONDS);
		LoginPageMSO.launch();
		LoginPageMSO.login();
		
		QAProof.writeInfo(">>Step 2");			
		
	}
	
	
}//class
