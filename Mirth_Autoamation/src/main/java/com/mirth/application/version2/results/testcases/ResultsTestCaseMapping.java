package com.mirth.application.version2.results.testcases;

import java.io.IOException;
import java.util.Properties;

import com.mirth.application.master.TestCaseMapping;

public class ResultsTestCaseMapping implements TestCaseMapping{
	
	private ResultsTestCaseMapping() {
		//Singleton
	}

	//singleton object
	private static ResultsTestCaseMapping testCaseObject =null; 
	private Properties config;
	private String configFileName= "resultsTestCaseConfig.properties";
	//Config Variable END----------------------
	
	public synchronized static ResultsTestCaseMapping getObject() {
		
		if (ResultsTestCaseMapping.testCaseObject == null) {
			ResultsTestCaseMapping.testCaseObject = new ResultsTestCaseMapping();
		}
		
		return ResultsTestCaseMapping.testCaseObject;
	}
	
	
	private synchronized void initiateControllerConfig() throws IOException {
		config = new Properties();
		config.load(ResultsTestCaseMapping.class.getResourceAsStream(configFileName));
		
	}//initiateControllerConfig() throws IOException
	
	
	public synchronized String getTestClassRelativeName(String configKey) throws IOException {
		
		if (isConfigPropertyNull()) {
			initiateControllerConfig();
		}
		
		return config.getProperty(configKey);
	}

	public synchronized boolean isConfigPropertyNull() {
		return (config==null)?true:false;
	}
	
}
