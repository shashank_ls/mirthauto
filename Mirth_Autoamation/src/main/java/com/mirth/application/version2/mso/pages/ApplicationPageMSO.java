package com.mirth.application.version2.mso.pages;

import com.mirth.application.master.PageObject;
import com.mirth.utility.seleniumLib.TableS;

public class ApplicationPageMSO extends PageObject {
	
	public static final TableS $ApplicationTable = new TableS("ApplicationTable");
		
	public static final String identifierHeading = "Identifier";
	public static final String nameHeading = "Name";		
}
