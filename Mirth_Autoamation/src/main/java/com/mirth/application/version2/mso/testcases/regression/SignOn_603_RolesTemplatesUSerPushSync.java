package com.mirth.application.version2.mso.testcases.regression;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import com.mirth.application.master.General;
import com.mirth.application.master.PageObject;
import com.mirth.application.master.Testcase;
import com.mirth.application.version2.match.pages.CommonObjectsMatch;
import com.mirth.application.version2.match.pages.EditUserPageMatch;
import com.mirth.application.version2.match.pages.LoginPageMatch;
import com.mirth.application.version2.match.pages.SiteSelectorPageMatch;
import com.mirth.application.version2.match.pages.UserAdminPageMatch;
import com.mirth.application.version2.mso.pages.ApplicationPageMSO;
import com.mirth.application.version2.mso.pages.CommonObjectsMSO;
import com.mirth.application.version2.mso.pages.LoginPageMSO;
import com.mirth.application.version2.mso.pages.NewEditApplicaionMSO;
import com.mirth.application.version2.mso.pages.NewTemplatePageMSO;
import com.mirth.application.version2.mso.pages.NewUserPageMSO;
import com.mirth.application.version2.mso.pages.RolesPageMSO;
import com.mirth.application.version2.mso.pages.TemplatesPageMSO;
import com.mirth.application.version2.mso.pages.UsersPageMSO;
import com.mirth.application.version2.results.pages.CommonObjectsResults;
import com.mirth.application.version2.results.pages.EditUserPageResults;
import com.mirth.application.version2.results.pages.LoginPageResults;
import com.mirth.application.version2.results.pages.PatientsPageResults;
import com.mirth.application.version2.results.pages.SiteSelectorPageResults;
import com.mirth.application.version2.results.pages.UserAdminPageResults;
import com.mirth.exceptions.NotATableElementException;
import com.mirth.properties.ApplicationConfig;
import com.mirth.properties.InteractConfig;
import com.mirth.results.QAProof;
import com.mirth.utility.general.SleepLib;
import com.mirth.utility.general.StaticLib;
import com.mirth.utility.seleniumLib.SeleniumElement;
import com.mirth.utility.seleniumLib.SeleniumLib;


public class SignOn_603_RolesTemplatesUSerPushSync extends Testcase{

	@Override
	protected void steps() throws Exception {
		
		QAProof.writeInfo("Steps 1");
		General.openFreshBrowser("main", super.testInventory.browser);
		General.setBrowserWaitTime(10, TimeUnit.SECONDS);
		LoginPageMSO.launch();
		LoginPageMSO.login();
		
		QAProof.writeInfo("Step 2");
		CommonObjectsMSO.$RolesTab.interact().click();
		
		Map<SeleniumElement,String> mrProvRoleMap = RolesPageMSO.createDefaultRole();
		String mrProvDescription = mrProvRoleMap.get(RolesPageMSO.$RoleDescriptionTextBox);
		String mrProv = mrProvRoleMap.get(RolesPageMSO.$NameTextBox);
		RolesPageMSO.$SecurityRolesTable.forceProcessTable();
		int mrProvRow = RolesPageMSO.$SecurityRolesTable.findRowHavingText(mrProvRoleMap.get(RolesPageMSO.$NameTextBox), RolesPageMSO.nameHeading, false);
		PageObject.verifyStringData("Name column", mrProv, RolesPageMSO.$SecurityRolesTable.getValueFromTable(RolesPageMSO.nameHeading,mrProvRow), false);
		PageObject.verifyStringData("User column", "0", RolesPageMSO.$SecurityRolesTable.getValueFromTable(RolesPageMSO.userHeading,mrProvRow), false);
		PageObject.verifyStringData("Description column", mrProvDescription, RolesPageMSO.$SecurityRolesTable.getValueFromTable(RolesPageMSO.descriptionHeading,mrProvRow), false);
		
		QAProof.writeInfo("Step 3");
		Map<SeleniumElement,String> mmAdminRoleMap = RolesPageMSO.createDefaultRole();
		String mmAdminDescription = mmAdminRoleMap.get(RolesPageMSO.$RoleDescriptionTextBox);
		String mmAdmin = mmAdminRoleMap.get(RolesPageMSO.$NameTextBox);
		RolesPageMSO.$SecurityRolesTable.forceProcessTable();
		int mmAdminRow = RolesPageMSO.$SecurityRolesTable.findRowHavingText(mmAdmin, RolesPageMSO.nameHeading, false);
		PageObject.verifyStringData("Name column", mmAdmin, RolesPageMSO.$SecurityRolesTable.getValueFromTable(RolesPageMSO.nameHeading,mmAdminRow), false);
		PageObject.verifyStringData("User column", "0", RolesPageMSO.$SecurityRolesTable.getValueFromTable(RolesPageMSO.userHeading,mmAdminRow), false);
		PageObject.verifyStringData("Description column", mmAdminDescription, RolesPageMSO.$SecurityRolesTable.getValueFromTable(RolesPageMSO.descriptionHeading,mmAdminRow), false);
		
		QAProof.writeInfo("Step 4");
		CommonObjectsMSO.$ApplicaitonTab.interact().click();
		ApplicationPageMSO.$ApplicationTable.forceProcessTable();
		int mirthResultsRow = ApplicationPageMSO.$ApplicationTable.findRowHavingText(ApplicationPageMSO.td(ApplicationPageMSO.class, "_resultsIdentifier"), 
																ApplicationPageMSO.identifierHeading,false);

		QAProof.writeInfo("Step 5");		
		ApplicationPageMSO.$ApplicationTable.clickRow(ApplicationPageMSO.identifierHeading, mirthResultsRow);
		NewEditApplicaionMSO.addAppRole("Provider User","ProviderUser To "+mrProv+" Mapping",mrProv);
		NewEditApplicaionMSO.$ApplicationRolesTable.forceProcessTable();
		int mrAppRoleRow = NewEditApplicaionMSO.$ApplicationRolesTable.findRowHavingText(mrProv, NewEditApplicaionMSO.msoRoleHeading, false);
		PageObject.verifyStringData("Application Role column", "Provider User", NewEditApplicaionMSO.$ApplicationRolesTable.getValueFromTable(NewEditApplicaionMSO.appRoleHeading,mrAppRoleRow), false);
		PageObject.verifyStringData("Description column", "ProviderUser To "+mrProv+" Mapping", NewEditApplicaionMSO.$ApplicationRolesTable.getValueFromTable(NewEditApplicaionMSO.descriptionHeading,mrAppRoleRow), false);
		PageObject.verifyStringData("MirthSignOn Role column", mrProv, NewEditApplicaionMSO.$ApplicationRolesTable.getValueFromTable(NewEditApplicaionMSO.msoRoleHeading,mrAppRoleRow), false);
		
		QAProof.writeInfo("Step 6");
		RegressionUtilities.setApplicaitonInstanceVals(null,true,true);
		CommonObjectsMSO.$BackToListLinkInLeftPane.interact().click();
		
		
		QAProof.writeInfo("Step 7");		
		ApplicationPageMSO.$ApplicationTable.forceProcessTable();
		int mirthMatchRow = ApplicationPageMSO.$ApplicationTable.findRowHavingText(ApplicationPageMSO.td(ApplicationPageMSO.class, "_matchIdentifier"), 
				ApplicationPageMSO.identifierHeading,false);
		ApplicationPageMSO.$ApplicationTable.clickRow(ApplicationPageMSO.identifierHeading, mirthMatchRow);
		
		QAProof.writeInfo("Step 8");
		NewEditApplicaionMSO.addAppRole("Administrator","AdminTo"+mrProv+"Mapping",mmAdmin);
		NewEditApplicaionMSO.$ApplicationRolesTable.forceProcessTable();
		int mmAppRoleRow = NewEditApplicaionMSO.$ApplicationRolesTable.findRowHavingText(mmAdmin, NewEditApplicaionMSO.msoRoleHeading, false);
		PageObject.verifyStringData("Application Role column", "Administrator", NewEditApplicaionMSO.$ApplicationRolesTable.getValueFromTable(NewEditApplicaionMSO.appRoleHeading,mmAppRoleRow), false);
		PageObject.verifyStringData("Description column", "AdminTo"+mrProv+"Mapping", NewEditApplicaionMSO.$ApplicationRolesTable.getValueFromTable(NewEditApplicaionMSO.descriptionHeading,mmAppRoleRow), false);
		PageObject.verifyStringData("MirthSignOn Role column", mmAdmin, NewEditApplicaionMSO.$ApplicationRolesTable.getValueFromTable(NewEditApplicaionMSO.msoRoleHeading,mmAppRoleRow), false);
		
		
		QAProof.writeInfo("Step 9");
		RegressionUtilities.setApplicaitonInstanceVals(null,true,true);
		CommonObjectsMSO.$BackToListLinkInLeftPane.interact().click();
		
		QAProof.writeInfo("Step 10 and 11");
		CommonObjectsMSO.$TemplatesTab.interact().click();
		TemplatesPageMSO.$CreateUserTemplateLinkInActionsBar.interact().click();
		Map<SeleniumElement,String> changeableFields = new LinkedHashMap<SeleniumElement,String>();
		changeableFields.put(NewTemplatePageMSO.$SelectListInSecurityRolesPopup,mrProv+InteractConfig.getValue("MultiSelectSeparator")+mmAdmin);
		changeableFields.put(NewTemplatePageMSO.$Chk_ChangePasswordNextLogin,"OFF");
		Map<SeleniumElement,String> newTemplateVals = NewTemplatePageMSO.createNewTemplateWithCustomReplace(changeableFields);
		SeleniumLib.waitForVisiblity(CommonObjectsMSO.$SaveChangesLinkInLeftPane);
		CommonObjectsMSO.$SaveChangesLinkInLeftPane.interact().click();
		QAProof.screenshot(NewTemplatePageMSO.$LabelTextBox);
		NewTemplatePageMSO.verifyNewTemplateValues(newTemplateVals);
		QAProof.writeText("Succesfully created a new Template.");
		CommonObjectsMSO.$BackToListLinkInLeftPane.interact().click();
		
		QAProof.writeInfo("Step 12 to 14");
		CommonObjectsMSO.clickUsersTab();
		UsersPageMSO.clickCreateNewUser();
		
		String user1Password = "test1234";
		changeableFields.clear();
		changeableFields = getNewChangeableUserMap(newTemplateVals.get(NewTemplatePageMSO.$LabelTextBox), user1Password);
			
		Map<SeleniumElement,String> newUserVals = NewUserPageMSO.createNewUserWithLiteralSelectedFields(changeableFields,true);
		QAProof.screenshot(NewUserPageMSO.$SecurityRolesTable);
		
		QAProof.writeInfo("Step 15");
		General.openAndPointToAddtionalBrowser("spBrowser", super.testInventory.browser);
		General.setBrowserWaitTime(10, TimeUnit.SECONDS);
		verifyUserPresence(newUserVals,ApplicationConfig.getValue("Results"),true);
		
		QAProof.writeInfo("Step 16");
		verifyUserPresence(newUserVals,ApplicationConfig.getValue("Match"),true);
		
		QAProof.writeInfo("Step 17");
		General.switchToBrowser("main");
		RegressionUtilities.setApplicaitonInstanceVals(true,ApplicationPageMSO.td(ApplicationPageMSO.class, "_resultsIdentifier"),false,true);
		RegressionUtilities.setApplicaitonInstanceVals(true,ApplicationPageMSO.td(ApplicationPageMSO.class, "_matchIdentifier"),false,true);
		
		QAProof.writeInfo("Step 18 (Repeating steps 12 to 14)");
		CommonObjectsMSO.clickUsersTab();
		UsersPageMSO.clickCreateNewUser();
		//changeableFields map already present.
		changeableFields.clear();
		changeableFields = getNewChangeableUserMap(newTemplateVals.get(NewTemplatePageMSO.$LabelTextBox), user1Password);
		newUserVals.clear();
		newUserVals = NewUserPageMSO.createNewUserWithLiteralSelectedFields(changeableFields,true);
		QAProof.screenshot(NewUserPageMSO.$SecurityRolesTable);
		
		QAProof.writeInfo("Step 19");
		General.switchToBrowser("spBrowser");
		verifyUserPresence(newUserVals,ApplicationConfig.getValue("Results"),false);
		verifyUserPresence(newUserVals,ApplicationConfig.getValue("Match"),false);	
		
		QAProof.writeInfo("Step 20");
		General.switchToBrowser("main");
		CommonObjectsMSO.$UsersTab.interact().click();
		UsersPageMSO.syncAllUsersOfTemplate(newTemplateVals.get(NewTemplatePageMSO.$IdentifierTextBox ), true);
		
		QAProof.writeInfo("Step 21");
		General.switchToBrowser("spBrowser");
		verifyUserPresence(newUserVals,ApplicationConfig.getValue("Results"),false);
		verifyUserPresence(newUserVals,ApplicationConfig.getValue("Match"),false);	
		
		QAProof.writeInfo("Step 22 and 23");
		General.switchToBrowser("main");
		RegressionUtilities.setApplicaitonInstanceVals(true,ApplicationPageMSO.td(ApplicationPageMSO.class, "_resultsIdentifier"),true,true);
		CommonObjectsMSO.$UsersTab.interact().click();
		SleepLib.sleep2Seconds();
		UsersPageMSO.syncAllUsersOfTemplate(newTemplateVals.get(NewTemplatePageMSO.$IdentifierTextBox ), true);
				
		QAProof.writeInfo("Step 24");
		General.switchToBrowser("spBrowser");
		verifyUserPresence(newUserVals,ApplicationConfig.getValue("Results"),true);
		verifyUserPresence(newUserVals,ApplicationConfig.getValue("Match"),false);
		
		QAProof.writeInfo("Step 25 annd 26");
		General.switchToBrowser("main");
		RegressionUtilities.setApplicaitonInstanceVals(true,ApplicationPageMSO.td(ApplicationPageMSO.class, "_matchIdentifier"),true,true);
		CommonObjectsMSO.$UsersTab.interact().click();
		UsersPageMSO.syncAllUsersOfTemplate(newTemplateVals.get(NewTemplatePageMSO.$IdentifierTextBox ), true);
		
		QAProof.writeInfo("Step 27");
		General.switchToBrowser("spBrowser");
		
		verifyUserPresence(newUserVals,ApplicationConfig.getValue("Results"),true);
		verifyUserPresence(newUserVals,ApplicationConfig.getValue("Match"),true);	
		
		QAProof.writeInfo("Step 28");
		General.switchToBrowser("main");
		RegressionUtilities.setApplicaitonInstanceVals(true,ApplicationPageMSO.td(ApplicationPageMSO.class, "_resultsIdentifier"),false,true);
		RegressionUtilities.setApplicaitonInstanceVals(true,ApplicationPageMSO.td(ApplicationPageMSO.class, "_matchIdentifier"),false,true);
		CommonObjectsMSO.$UsersTab.interact().click();
		CommonObjectsMSO.searchTextBoxFor(newUserVals.get(NewUserPageMSO.$AccountIDTextBox));
		UsersPageMSO.$UsersTable.forceProcessTable();
		UsersPageMSO.$UsersTable.clickFirstRowOfUsersTable(UsersPageMSO.usersTableAccountID);
		PageObject.setData(NewUserPageMSO.$AccountStatusDropDown,"Deactivated");
		CommonObjectsMSO.$SaveChangesLinkInLeftPane.interact().click();
				
		QAProof.writeInfo("Step 29");
		General.switchToBrowser("spBrowser");
		verifyUserPresence(newUserVals,ApplicationConfig.getValue("Results"),true);
		verifyUserPresence(newUserVals,ApplicationConfig.getValue("Match"),true);
		
		QAProof.writeInfo("Step 30");
		General.switchToBrowser("main");
		RegressionUtilities.setApplicaitonInstanceVals(true,ApplicationPageMSO.td(ApplicationPageMSO.class, "_matchIdentifier"),true,true);
		CommonObjectsMSO.$UsersTab.interact().click();
		UsersPageMSO.syncAllUsersOfTemplate(newTemplateVals.get(NewTemplatePageMSO.$IdentifierTextBox ), true);
		General.switchToBrowser("spBrowser");
		verifyUserPresence(newUserVals,ApplicationConfig.getValue("Results"),true);
		verifyUserPresence(newUserVals,ApplicationConfig.getValue("Match"),false);

		QAProof.writeInfo("Step 31");
		General.switchToBrowser("main");
		RegressionUtilities.setApplicaitonInstanceVals(true,ApplicationPageMSO.td(ApplicationPageMSO.class, "_resultsIdentifier"),true,true);
		CommonObjectsMSO.$UsersTab.interact().click();
		CommonObjectsMSO.searchTextBoxFor("");
		UsersPageMSO.syncAllUsersOfTemplate(newTemplateVals.get(NewTemplatePageMSO.$IdentifierTextBox ), true);
		SleepLib.sleep5Seconds();
		General.switchToBrowser("spBrowser");
		verifyUserPresence(newUserVals,ApplicationConfig.getValue("Results"),false);
		verifyUserPresence(newUserVals,ApplicationConfig.getValue("Match"),false);
		General.closeAndDestroyBrowser("spBrowser");
		
		QAProof.writeInfo("Step 32");
		General.switchToBrowser("main");
		TemplatesPageMSO.deleteTemplate(newTemplateVals.get(NewTemplatePageMSO.$IdentifierTextBox));
		RolesPageMSO.deleteRole(mrProv);
		RolesPageMSO.deleteRole(mmAdmin);
		
		QAProof.writeInfo("Step 33");
		CommonObjectsMSO.$UsersTab.interact().click();
		newUserVals.put(NewUserPageMSO.$SecurityRolesTable, "** No Roles Assigned **");
		UsersPageMSO.$UsersTable.clickFirstRowOfUsersTable(UsersPageMSO.usersTableAccountID);
		NewUserPageMSO.$SecurityRolesTable.forceProcessTable();
		QAProof.screenshot(NewUserPageMSO.$SecurityRolesTable);
		if (NewUserPageMSO.$SecurityRolesTable.isTableEmpty()){
			verify.reportSuccess("Deleted roles("+mrProv+" and " + mmAdmin+") NOT seen in user details. (Expected)");
		}else{
			verify.reportIssue("Deleted roles("+mrProv+" and " + mmAdmin+") is still associated with user. (Not Expected)");
		}	
		

		QAProof.writeInfo("Step 34");
		
		//creating new Test template.
		CommonObjectsMSO.$TemplatesTab.interact().click();
		TemplatesPageMSO.$CreateUserTemplateLinkInActionsBar.interact().click();
		
		changeableFields.clear();
		changeableFields.put(NewTemplatePageMSO.$SelectListInSecurityRolesPopup,"System");
		changeableFields.put(NewTemplatePageMSO.$Chk_ChangePasswordNextLogin,"OFF");
		newTemplateVals.clear();
		newTemplateVals = NewTemplatePageMSO.createNewTemplateWithCustomReplace(changeableFields);
		SeleniumLib.waitForVisiblity(CommonObjectsMSO.$SaveChangesLinkInLeftPane);
		CommonObjectsMSO.$SaveChangesLinkInLeftPane.interact().click();
		QAProof.screenshot(NewTemplatePageMSO.$LabelTextBox);
		NewTemplatePageMSO.verifyNewTemplateValues(newTemplateVals);
		QAProof.writeText("Succesfully created a new Template.");
		CommonObjectsMSO.$BackToListLinkInLeftPane.interact().click();
		
		//create a user with this new Template.
		CommonObjectsMSO.clickUsersTab();
		UsersPageMSO.clickCreateNewUser();
		//changeableFields map already present.
		changeableFields.clear();
		changeableFields = getNewChangeableUserMap(newTemplateVals.get(NewTemplatePageMSO.$LabelTextBox), user1Password);
		newUserVals.clear();
		newUserVals = NewUserPageMSO.createNewUserWithLiteralSelectedFields(changeableFields,true);
		QAProof.screenshot(NewUserPageMSO.$SecurityRolesTable);
		
		QAProof.writeInfo("Step 35");
		TemplatesPageMSO.addRoleToExistingTemplate(newTemplateVals.get(NewTemplatePageMSO.$IdentifierTextBox),"Administrator");
		
		QAProof.writeInfo("Step 36");
		CommonObjectsMSO.$UsersTab.interact().click();
		newUserVals.put(NewUserPageMSO.$SecurityRolesTable, "Administrator | System");
		CommonObjectsMSO.searchTextBoxFor(newUserVals.get(NewUserPageMSO.$AccountIDTextBox));
		UsersPageMSO.verifyUserDataAsPerCreation(newUserVals);
		UsersPageMSO.$UsersTable.clickFirstRowOfUsersTable(UsersPageMSO.usersTableAccountID);
		NewUserPageMSO.$SecurityRolesTable.forceProcessTable();
		QAProof.screenshot(NewUserPageMSO.$SecurityRolesTable);
		
		int rowNum = NewUserPageMSO.$SecurityRolesTable.findRowHavingText("Administrator", NewUserPageMSO.securityRoleNameColumnHeading,false,true);
		if (rowNum<1){
			verify.reportIssue("The newly added role(Administrator) to the template("+newTemplateVals.get(NewTemplatePageMSO.$IdentifierTextBox)+") is NOT seen in the security roles table of the user. [NOT Expected]");
		}
		else{
			verify.reportSuccess("The newly added role(Administrator) to the template("+newTemplateVals.get(NewTemplatePageMSO.$IdentifierTextBox)+") is seen in the security roles table of the user. [Expected]");
		}
				
		QAProof.writeInfo("Step 37 and 38");
		TemplatesPageMSO.deleteRoleOfTemplate(newTemplateVals.get(NewTemplatePageMSO.$IdentifierTextBox),"System");
			
		
		QAProof.writeInfo("Step 39");
		CommonObjectsMSO.$UsersTab.interact().click();
		newUserVals.put(NewUserPageMSO.$SecurityRolesTable, "Administrator");
		CommonObjectsMSO.searchTextBoxFor(newUserVals.get(NewUserPageMSO.$AccountIDTextBox));
		UsersPageMSO.verifyUserDataAsPerCreation(newUserVals);
		UsersPageMSO.$UsersTable.clickFirstRowOfUsersTable(UsersPageMSO.usersTableAccountID);
		NewUserPageMSO.$SecurityRolesTable.forceProcessTable();
		QAProof.screenshot(NewUserPageMSO.$SecurityRolesTable);
		
		rowNum = NewUserPageMSO.$SecurityRolesTable.findRowHavingText("System", NewUserPageMSO.securityRoleNameColumnHeading,false,true);
		if (rowNum<1){
			verify.reportIssue("The newly deleted role(System) from the template("+newTemplateVals.get(NewTemplatePageMSO.$IdentifierTextBox)+") is NOT seen in the security roles table of the user. [Expected]");
		}
		else{
			verify.reportSuccess("The newly deleted role(System) from the template("+newTemplateVals.get(NewTemplatePageMSO.$IdentifierTextBox)+") is seen in the users table of the User. [NOT Excpeted]");
		}	
		
		QAProof.writeInfo("Test Case END");
		
	}
	
	
	
	public void verifyUserPresence(Map<SeleniumElement,String> dataMap,String appName,boolean isPresent) throws Exception{
		SleepLib.sleep3Seconds();				
		
		if (appName.equalsIgnoreCase(ApplicationConfig.getValue("Results"))){
			
			dataMap.put(NewUserPageMSO.$SelectListInAddSecurityRolePopup, "Provider User");
			
			LoginPageResults.launch();
			LoginPageResults.login();
			SiteSelectorPageResults.$SiteSelectorTable.forceProcessTable();
			SiteSelectorPageResults.selectSite();
			PatientsPageResults.$CancelButtonOnAdvancedPatientFilter.interact().click();
			SleepLib.sleep1Second();
			CommonObjectsResults.goToUsersPage();
			
			if (isPresent){			

				headingController.verifing("updated values in Users table of Users Page in Results Applicaiton after filtering the appropriate patient");
				QAProof.screenshot(UserAdminPageResults.$UsersTable);	
				
				UserAdminPageResults.$UsersTable.forceProcessTable();
				if (UserAdminPageResults.$UsersTable.isTableEmpty()){
					verify.reportIssue("User("+dataMap.get(NewUserPageMSO.$AccountIDTextBox)+") is Not seen when he/she is expected to be seen in Results Users tab.");
					return;
				}

				UserAdminPageResults.verifyUserTableAsPerMso(dataMap);	
				UserAdminPageResults.$UsersTable.clickFirstRowOfUsersTable(UserAdminPageResults.usersTableAccountID);
				headingController.verifing("updated values in User details in Edit User Page of Results Applicaiton");
				QAProof.screenshot(EditUserPageResults.$AccountIDTextValue);
				EditUserPageResults.verifyUserDetailsAsPerMso(dataMap);
			}//if (isPresent){			
 
			else{
				headingController.verifing("User's presence in the Users table");
				CommonObjectsMSO.searchTextBoxFor(dataMap.get(NewUserPageMSO.$AccountIDTextBox));
				SleepLib.sleep2Seconds();
				UserAdminPageResults.$UsersTable.forceProcessTable();
				QAProof.screenshot(UserAdminPageResults.$UsersTable);			
								
				if (UserAdminPageResults.$UsersTable.isTableEmpty()){
					verify.reportSuccess("User NOT seen in the Users table as expected.");
				}
				else {
					verify.reportIssue("User '"+dataMap.get(NewUserPageMSO.$AccountIDTextBox)+"' is seen in the Users table which is NOT expected.");
				}//else - if (UserAdminPageResults.$UsersTable.isTableEmpty())
				
			}//else - if(isPresent)
		
			CommonObjectsResults.$SignOutButtonOnKanaToolBar.interact().click();
		}//if (appName.equalsIgnoreCase(ApplicationConfig.getValue("Results")))

		else if (appName.equalsIgnoreCase(ApplicationConfig.getValue("Match"))){

			dataMap.put(NewUserPageMSO.$SelectListInAddSecurityRolePopup, "Administrator");
			
			LoginPageMatch.launch();
			LoginPageMatch.login();
			SiteSelectorPageMatch.$SiteSelectorTable.forceProcessTable();
			SiteSelectorPageMatch.selectSite();
			SleepLib.sleep1Second();
			CommonObjectsMatch.goToUsersPage();
			
			if(isPresent){
				headingController.verifing("updated values in Users table of Users Page in Match Applicaiton after filtering the appropriate patient");
				QAProof.screenshot(UserAdminPageMatch.$UsersTable);
				
				UserAdminPageMatch.$UsersTable.forceProcessTable();
				if (UserAdminPageMatch.$UsersTable.isTableEmpty()){
					verify.reportIssue("User Not seen when he/she is expected to be seen in Match Users tab.");
					return;
				}
				
				
				UserAdminPageMatch.verifyUserTableAsPerMso(dataMap);	
				UserAdminPageMatch.$UsersTable.clickFirstRowOfUsersTable(UserAdminPageResults.usersTableAccountID);
				headingController.verifing("updated values in User details in Edit User Page of Results Applicaiton");
				QAProof.screenshot(EditUserPageMatch.$AccountIDTextValue);
				EditUserPageMatch.verifyUserDetailsAsPerMso(dataMap);
			}
			
			else{
				headingController.verifing("User's presence in the Users table");
				CommonObjectsMatch.searchTextBoxFor(dataMap.get(NewUserPageMSO.$AccountIDTextBox));
				SleepLib.sleep2Seconds();
				UserAdminPageMatch.$UsersTable.forceProcessTable();
				QAProof.screenshot(UserAdminPageMatch.$UsersTable);			
								
				if (UserAdminPageMatch.$UsersTable.isTableEmpty()){
					verify.reportSuccess("User NOT seen in the Users table as expected.");
				}
				else {
					verify.reportIssue("User '"+dataMap.get(NewUserPageMSO.$AccountIDTextBox)+"' is seen in the Users table which is NOT expected.");
				}//else - if (UserAdminPageResults.$UsersTable.isTableEmpty())
				
			}//else - if(isPresent)
			
			CommonObjectsMatch.$SignOutButtonOnKanaToolBar.interact().click();
		}//else - if (appName.equalsIgnoreCase(ApplicationConfig.getValue("Match")))
		
	}//public void verifyUserPresence(Map<SeleniumElement,String> dataMap,String appName,boolean isPresent) 
	
	
	
	public static Map<SeleniumElement,String> getNewChangeableUserMap(String templateName,String password) throws NotATableElementException, Exception{
		
		Map<SeleniumElement,String> changeableFields = new LinkedHashMap<SeleniumElement,String>();
		
		changeableFields.put(NewUserPageMSO.$UserEntityTemplateNameDropDown, templateName);
		SleepLib.sleep1Second();
		changeableFields.put(NewUserPageMSO.$AccountIDTextBox, StaticLib.getGloballyUniqueStringValue(false));
		changeableFields.put(NewUserPageMSO.$Name, StaticLib.getGloballyUniqueStringValue(false));
		changeableFields.put(NewUserPageMSO.$Email, StaticLib.getGloballyUniqueEmailValue("mirth"));
		changeableFields.put(NewUserPageMSO.$NewPassword, password);
		changeableFields.put(NewUserPageMSO.$ConfirmNewPassword, password);
		changeableFields.put(NewUserPageMSO.$VoiceContact, PageObject.getProcessedTestData(NewUserPageMSO.$VoiceContact));
		changeableFields.put(NewUserPageMSO.$FaxContact, PageObject.getProcessedTestData(NewUserPageMSO.$FaxContact));
		changeableFields.put(NewUserPageMSO.$AddressLine1, "Pritech Park SEZ, Block 11A");
		changeableFields.put(NewUserPageMSO.$AddressLine2, "Outer Ring Rd, Bellandur");
		changeableFields.put(NewUserPageMSO.$City, "Bangalore");
		changeableFields.put(NewUserPageMSO.$State, "KA");
		changeableFields.put(NewUserPageMSO.$Postal, "560103");
		changeableFields.put(NewUserPageMSO.$Country, "United States");
		
		return changeableFields;
	}
	

}
