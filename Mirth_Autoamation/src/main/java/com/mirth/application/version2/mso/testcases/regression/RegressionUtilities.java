package com.mirth.application.version2.mso.testcases.regression;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import com.mirth.application.master.PageObject;
import com.mirth.application.version2.mso.pages.ApplicationPageMSO;
import com.mirth.application.version2.mso.pages.CommonObjectsMSO;
import com.mirth.application.version2.mso.pages.NewEditApplicaionMSO;
import com.mirth.results.QAProof;
import com.mirth.utility.seleniumLib.SeleniumElement;

public class RegressionUtilities {

	public static void setApplicaitonInstanceVals(String msoAppIdentifier,boolean userPushCheck,boolean kanaCheck) throws Exception{
		setApplicaitonInstanceVals(false, msoAppIdentifier, userPushCheck, kanaCheck);
	}
	
	
	public static void setApplicaitonInstanceVals(boolean isFresh,String msoAppIdentifier,boolean userPushCheck,boolean kanaCheck) throws Exception{
		setApplicaitonInstanceVals(isFresh,msoAppIdentifier, userPushCheck, kanaCheck,null);
	}
	
	public static void setApplicaitonInstanceVals(boolean isFresh,String msoAppIdentifier,boolean userPushCheck,boolean kanaCheck,Map<SeleniumElement,String> dataMap) throws Exception{
		
		if (isFresh || msoAppIdentifier!=null){
			CommonObjectsMSO.$ApplicaitonTab.interact().click();
			ApplicationPageMSO.$ApplicationTable.forceProcessTable();
			int mirthResultsRow = ApplicationPageMSO.$ApplicationTable.findRowHavingText(msoAppIdentifier, 
																	ApplicationPageMSO.identifierHeading,false,false);
			ApplicationPageMSO.$ApplicationTable.clickRow(ApplicationPageMSO.identifierHeading, mirthResultsRow);
		}
		
		NewEditApplicaionMSO.$ApplicationInstancesTable.forceProcessTable();
		NewEditApplicaionMSO.$ApplicationInstancesTable.clickFirstRowOfUsersTable(NewEditApplicaionMSO.labelHeading);
		
		if (userPushCheck){
			NewEditApplicaionMSO.$EnableUserPushCheckBox.interact().setCheckBox_ON();	
		}
		else{
			NewEditApplicaionMSO.$EnableUserPushCheckBox.interact().setCheckBox_OFF();
		}		
		
		if (kanaCheck){
			NewEditApplicaionMSO.$ShowKanaToolBarCheckBox.interact().setCheckBox_ON();	
		}
		else{
			NewEditApplicaionMSO.$ShowKanaToolBarCheckBox.interact().setCheckBox_OFF();
		}
		
		if (dataMap!=null){
			for (SeleniumElement selEle: dataMap.keySet()){
				PageObject.setData(selEle, dataMap.get(selEle));
			}
		}
		
				
		QAProof.screenshot(CommonObjectsMSO.$SubmitButtonOnPopup);
		CommonObjectsMSO.$SubmitButtonOnPopup.interact().click();
		CommonObjectsMSO.$SaveChangesLinkInLeftPane.interact().click();
			
	}
	
	public static Map<SeleniumElement,String> getApplicaitonInstanceVals(boolean isFresh,String msoAppIdentifier,Map<SeleniumElement,String> dataMap) throws Exception{
		
		if (isFresh || msoAppIdentifier!=null){
			CommonObjectsMSO.$ApplicaitonTab.interact().click();
			ApplicationPageMSO.$ApplicationTable.forceProcessTable();
			int mirthResultsRow = ApplicationPageMSO.$ApplicationTable.findRowHavingText(msoAppIdentifier, 
																	ApplicationPageMSO.identifierHeading,false,false);
			ApplicationPageMSO.$ApplicationTable.clickRow(ApplicationPageMSO.identifierHeading, mirthResultsRow);
		}
		
		NewEditApplicaionMSO.$ApplicationInstancesTable.forceProcessTable();
		NewEditApplicaionMSO.$ApplicationInstancesTable.clickFirstRowOfUsersTable(NewEditApplicaionMSO.labelHeading);
		
		Map<SeleniumElement,String> resultMap = new HashMap<SeleniumElement,String>();
		if (dataMap!=null){
			
			Set<SeleniumElement> selEleSet = dataMap.keySet();
			dataMap = null;	
			for (SeleniumElement selEle: selEleSet){
				resultMap.put(selEle,PageObject.getData(selEle));
			}
		}
		
		CommonObjectsMSO.$CancelButtonOnPopup.interact().click();
		return resultMap;	
	}
	
	
}//class
