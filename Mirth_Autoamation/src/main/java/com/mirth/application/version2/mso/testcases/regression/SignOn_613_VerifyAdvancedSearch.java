/*
 * TestCase Author: Jessica Dear
 */


package com.mirth.application.version2.mso.testcases.regression;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import com.mirth.application.master.General;
import com.mirth.application.master.Testcase;
import com.mirth.application.version2.mso.pages.CommonObjectsMSO;
import com.mirth.application.version2.mso.pages.LoginPageMSO;
import com.mirth.application.version2.mso.pages.NewUserPageMSO;
import com.mirth.application.version2.mso.pages.UsersPageMSO;
import com.mirth.results.QAProof;
import com.mirth.utility.seleniumLib.SeleniumElement;

public class SignOn_613_VerifyAdvancedSearch extends Testcase{

	@Override
	public void steps() throws Exception {
			
		QAProof.writeInfo("Steps 1 TO 9");
		General.openFreshBrowser("main", super.testInventory.browser);
		General.setBrowserWaitTime(10, TimeUnit.SECONDS);
		LoginPageMSO.launch();
		LoginPageMSO.login();
		CommonObjectsMSO.clickUsersTab();
//		CommonObjectsResults.goToUsersPage();
		
		
		
		/*sample code - delete later
		Map<SeleniumElement,String> changeVals = new LinkedHashMap<SeleniumElement,String>();
		chaneVals.put(NewUserPageMSO.$AddressLine1,"Samlpe address 1");
		chaneVals.put(NewUserPageMSO.$SelectListInAddSecurityRolePopup,"System");
		
		Map<SeleniumElement,String> user1 = NewUserPageMSO.createNewUserFromTestDataWithCustomReplace(chaneVals);
		
		
		PageObject.setData(NewUserPageMSO.$AddressLine1,"");
		
		*/
				
		//create 5 users
		//user1, user2, user3, user4, user5
		
		CommonObjectsMSO.clickUsersTab();
		UsersPageMSO.clickCreateNewUser();
		Map<SeleniumElement,String> changeVals = new LinkedHashMap<SeleniumElement,String>();
		changeVals.put(NewUserPageMSO.$Email,"test@test.com");
		changeVals.put(NewUserPageMSO.$Name,"user1");
		changeVals.put(NewUserPageMSO.$SelectListInAddSecurityRolePopup,"Administrator");
		Map<SeleniumElement,String> user1 = NewUserPageMSO.createNewUserFromTestDataWithCustomReplace(changeVals);
		NewUserPageMSO.verifyNewUserValues(user1);
		
		String accountId = user1.get(NewUserPageMSO.$AccountIDTextValue);
				
		CommonObjectsMSO.clickUsersTab();
		UsersPageMSO.clickCreateNewUser();
		Map<SeleniumElement,String> user2 = NewUserPageMSO.createNewUserFromTestData();
		NewUserPageMSO.verifyNewUserValues(user2);
		
		CommonObjectsMSO.clickUsersTab();
		UsersPageMSO.clickCreateNewUser();
		Map<SeleniumElement,String> user3 = NewUserPageMSO.createNewUserFromTestData();
		NewUserPageMSO.verifyNewUserValues(user3);
		
		CommonObjectsMSO.clickUsersTab();
		UsersPageMSO.clickCreateNewUser();
		Map<SeleniumElement,String> user4 = NewUserPageMSO.createNewUserFromTestData();
		NewUserPageMSO.verifyNewUserValues(user4);
		
		CommonObjectsMSO.clickUsersTab();
		UsersPageMSO.clickCreateNewUser();
		Map<SeleniumElement,String> user5 = NewUserPageMSO.createNewUserFromTestData();
		NewUserPageMSO.verifyNewUserValues(user5);
		
		CommonObjectsMSO.clickUsersTab();
		CommonObjectsMSO.$QuickSearchMagnifyingButton.interact().click();
		//all users should be displayed

		//writeText("use");
		CommonObjectsMSO.searchTextBoxFor("use");
		UsersPageMSO.$UsersTable.forceProcessTable();
		String nameActual = UsersPageMSO.$UsersTable.getValueFromTable(UsersPageMSO.usersTableName, 1);
		String nameExpected = "use";
		verify.isSame(nameExpected,nameActual, false);

		//writeText("anonymous");
		CommonObjectsMSO.searchTextBoxFor("anonymous");
		UsersPageMSO.$UsersTable.forceProcessTable();
		
		if (UsersPageMSO.$UsersTable.isTableEmpty())
		{
			verify.reportSuccess("No User with name as anonymous seen in the Users table");
		}
		else {
			verify.reportIssue("SomeUser is seen in the users table with name as anonymous when we are not supposed to see it.");
		}
		
		/*Sample cpode -  not releavant to this test case - delete later
		UsersPageMSO.$UsersTable.forceProcessTable();
		String nameActual = UsersPageMSO.$UsersTable.getValueFromTable(UsersPageMSO.usersTableName, 1);
		String nameExpected = "test";
		
		verify.isSame(nameExpected,nameActual, false);
		
		
		
		*/
		
		
		CommonObjectsMSO.$AdvancedFilterLink.interact().click();
		//uncheck all checkboxes
		CommonObjectsMSO.$AdvancedSearchCheckboxIncludeAdmins.interact().setCheckBox_OFF();
		CommonObjectsMSO.$AdvancedSearchCheckboxIncludeSystem.interact().setCheckBox_OFF();
		CommonObjectsMSO.$AdvancedSearchCheckboxIncludeLocked.interact().setCheckBox_OFF();
		CommonObjectsMSO.$AdvancedSearchSubmit.interact().click();
		//All the users present in MSO application should be displayed.
		UsersPageMSO.$UsersTable.forceProcessTable();
		//want to verify all users displayed...?
		//take count and then make sure number matches


		QAProof.writeInfo("Steps 9");
		CommonObjectsMSO.$AdvancedFilterLink.interact().click();
		CommonObjectsMSO.$AdvancedSearchName.interact().setText("user1");
		CommonObjectsMSO.$AdvancedSearchCheckboxIncludeAdmins.interact().setCheckBox_ON();
		CommonObjectsMSO.$AdvancedSearchCheckboxIncludeSystem.interact().setCheckBox_OFF();
		CommonObjectsMSO.$AdvancedSearchCheckboxIncludeLocked.interact().setCheckBox_OFF();
		CommonObjectsMSO.$AdvancedSearchSubmit.interact().click();
		//Only user1 data should be displayed.
		UsersPageMSO.$UsersTable.forceProcessTable();
		String nameActual1 = UsersPageMSO.$UsersTable.getValueFromTable(UsersPageMSO.usersTableName, 1);
		String nameExpected1 = "user1";
		verify.isSame(nameExpected1, nameActual1, false);
		UsersPageMSO.clearSearchCriteria();


		QAProof.writeInfo("Steps 10");
		CommonObjectsMSO.$AdvancedFilterLink.interact().click();
		CommonObjectsMSO.$AdvancedSearchName.interact().setText("user1, user2");
		CommonObjectsMSO.$AdvancedSearchCheckboxIncludeAdmins.interact().setCheckBox_OFF();
		CommonObjectsMSO.$AdvancedSearchCheckboxIncludeSystem.interact().setCheckBox_OFF();
		CommonObjectsMSO.$AdvancedSearchCheckboxIncludeLocked.interact().setCheckBox_OFF();
		CommonObjectsMSO.$AdvancedSearchSubmit.interact().click();
		//No data should be displayed.
		UsersPageMSO.$UsersTable.forceProcessTable();
		if (UsersPageMSO.$UsersTable.isTableEmpty())
		{
			verify.reportSuccess("No User with name as 'user1, user2' seen in the Users table");
		}
		else {
			verify.reportIssue("SomeUser is seen in the users table with name as 'user1, user2' when we are not supposed to see it.");
		}
		UsersPageMSO.clearSearchCriteria();

		QAProof.writeInfo("Steps 11");
		CommonObjectsMSO.$AdvancedFilterLink.interact().click();
		CommonObjectsMSO.$AdvancedSearchAccountId.interact().setText(accountId);
		CommonObjectsMSO.$AdvancedSearchCheckboxIncludeAdmins.interact().setCheckBox_OFF();
		CommonObjectsMSO.$AdvancedSearchCheckboxIncludeSystem.interact().setCheckBox_OFF();
		CommonObjectsMSO.$AdvancedSearchCheckboxIncludeLocked.interact().setCheckBox_OFF();
		CommonObjectsMSO.$AdvancedSearchSubmit.interact().click();
		//Only user1 data should be displayed.
		UsersPageMSO.$UsersTable.forceProcessTable();
		verify.isSame(nameExpected1,nameActual1, false);
		UsersPageMSO.clearSearchCriteria();

		QAProof.writeInfo("Steps 12");
		CommonObjectsMSO.$AdvancedFilterLink.interact().click();
		CommonObjectsMSO.$AdvancedSearchEmail.interact().setText("test@test.com");
		CommonObjectsMSO.$AdvancedSearchCheckboxIncludeAdmins.interact().setCheckBox_OFF();
		CommonObjectsMSO.$AdvancedSearchCheckboxIncludeSystem.interact().setCheckBox_OFF();
		CommonObjectsMSO.$AdvancedSearchCheckboxIncludeLocked.interact().setCheckBox_OFF();
		CommonObjectsMSO.$AdvancedSearchSubmit.interact().click();
		//Only user1 data should be displayed.
		UsersPageMSO.$UsersTable.forceProcessTable();
		verify.isSame(nameExpected1,nameActual1, false);
		UsersPageMSO.clearSearchCriteria();

		//enter security role
		QAProof.writeInfo("Steps 13");
		CommonObjectsMSO.$AdvancedFilterLink.interact().click();
		CommonObjectsMSO.$AdvancedSearchSecurityRole.interact().setText("Administrator");
		CommonObjectsMSO.$AdvancedSearchCheckboxIncludeAdmins.interact().setCheckBox_ON();
		CommonObjectsMSO.$AdvancedSearchCheckboxIncludeSystem.interact().setCheckBox_OFF();
		CommonObjectsMSO.$AdvancedSearchCheckboxIncludeLocked.interact().setCheckBox_OFF();
		CommonObjectsMSO.$AdvancedSearchSubmit.interact().click();
		//All the users who have Administrator role should be displayed
		UsersPageMSO.$UsersTable.forceProcessTable();
		
//		Integer sizeOfTable = UsersPageMSO.$UsersTable.getDataRowsCount();
//		
//		for(int i = 0; i<sizeOfTable; i++){
//			String value = UsersPageMSO.$UsersTable.getValueFromTable(i, "Roles");
//		}

		System.out.println("============NEW CODE STARTS HERE");
		
		String roleExpected = "Administrator";
		for(String rowValue: UsersPageMSO.$UsersTable.getAllColumnValuesAsListForHeader("Roles")){
			System.out.println(rowValue);
			verify.isSame(roleExpected,rowValue, false);
		}
		UsersPageMSO.clearSearchCriteria();
		
		
		
		QAProof.writeInfo("Steps 14");
		CommonObjectsMSO.$AdvancedFilterLink.interact().click();
		CommonObjectsMSO.$AdvancedSearchName.interact().setText("user1");
		CommonObjectsMSO.$AdvancedSearchSecurityRole.interact().setText("Administrator");
		CommonObjectsMSO.$AdvancedSearchCheckboxIncludeAdmins.interact().setCheckBox_ON();
		CommonObjectsMSO.$AdvancedSearchCheckboxIncludeSystem.interact().setCheckBox_OFF();
		CommonObjectsMSO.$AdvancedSearchCheckboxIncludeLocked.interact().setCheckBox_OFF();
		CommonObjectsMSO.$AdvancedSearchSubmit.interact().click();
		//Only user1 data should be displayed.
		UsersPageMSO.$UsersTable.forceProcessTable();
		verify.isSame(nameExpected1,nameActual1, false);
		UsersPageMSO.clearSearchCriteria();

		QAProof.writeInfo("Steps 15");
		CommonObjectsMSO.$AdvancedFilterLink.interact().click();
		CommonObjectsMSO.$AdvancedSearchName.interact().setText("user1");
		CommonObjectsMSO.$AdvancedSearchAccountId.interact().setText(accountId);
		CommonObjectsMSO.$AdvancedSearchEmail.interact().setText("test@abc.com");
		CommonObjectsMSO.$AdvancedSearchSecurityRole.interact().setText("Administrator");
		CommonObjectsMSO.$AdvancedSearchCheckboxIncludeAdmins.interact().setCheckBox_OFF();
		CommonObjectsMSO.$AdvancedSearchCheckboxIncludeSystem.interact().setCheckBox_OFF();
		CommonObjectsMSO.$AdvancedSearchCheckboxIncludeLocked.interact().setCheckBox_OFF();
		CommonObjectsMSO.$AdvancedSearchSubmit.interact().click();
		//No data should be displayed as email ID provided in search criteria is wrong.
		UsersPageMSO.$UsersTable.forceProcessTable();
		if (UsersPageMSO.$UsersTable.isTableEmpty())
		{
			verify.reportSuccess("No User with email as 'test@abc.com' seen in the Users table");
		}
		else {
			verify.reportIssue("SomeUser is seen in the users table with email as 'test@abc.com' when we are not supposed to see it.");
		}
		UsersPageMSO.clearSearchCriteria();

		QAProof.writeInfo("Steps 16");
		CommonObjectsMSO.$AdvancedFilterLink.interact().click();
		CommonObjectsMSO.$AdvancedSearchName.interact().setText("user1");
		CommonObjectsMSO.$AdvancedSearchAccountId.interact().setText(accountId);
		CommonObjectsMSO.$AdvancedSearchEmail.interact().setText("test@test.com");
		CommonObjectsMSO.$AdvancedSearchSecurityRole.interact().setText("Administrator");
		CommonObjectsMSO.$AdvancedSearchCheckboxIncludeAdmins.interact().setCheckBox_ON();
		CommonObjectsMSO.$AdvancedSearchCheckboxIncludeSystem.interact().setCheckBox_OFF();
		CommonObjectsMSO.$AdvancedSearchCheckboxIncludeLocked.interact().setCheckBox_OFF();
		CommonObjectsMSO.$AdvancedSearchSubmit.interact().click();
		//Only user1 data should be displayed as email ID provided in search criteria is correct.
		UsersPageMSO.$UsersTable.forceProcessTable();
		verify.isSame(nameExpected1,nameActual1, false);
		UsersPageMSO.clearSearchCriteria();

		QAProof.writeInfo("Steps 17");
		CommonObjectsMSO.$AdvancedFilterLink.interact().click();
		CommonObjectsMSO.$AdvancedSearchCheckboxIncludeAdmins.interact().setCheckBox_ON();
		CommonObjectsMSO.$AdvancedSearchCheckboxIncludeSystem.interact().setCheckBox_OFF();
		CommonObjectsMSO.$AdvancedSearchCheckboxIncludeLocked.interact().setCheckBox_OFF();
		CommonObjectsMSO.$AdvancedSearchSubmit.interact().click();
		//All the users with only Administrator role should be displayed.
		UsersPageMSO.$UsersTable.forceProcessTable();
		String roleActual1 = UsersPageMSO.$UsersTable.getValueFromTable(UsersPageMSO.usersTableRole, 1);
		String roleExpected1 = "Administrator";
		verify.isSame(roleActual1,roleExpected1, false);
		UsersPageMSO.clearSearchCriteria();

		QAProof.writeInfo("Steps 18");
		CommonObjectsMSO.$AdvancedFilterLink.interact().click();
		CommonObjectsMSO.$AdvancedSearchCheckboxIncludeAdmins.interact().setCheckBox_OFF();
		CommonObjectsMSO.$AdvancedSearchCheckboxIncludeSystem.interact().setCheckBox_ON();
		CommonObjectsMSO.$AdvancedSearchCheckboxIncludeLocked.interact().setCheckBox_OFF();
		CommonObjectsMSO.$AdvancedSearchSubmit.interact().click();
		//All the users with only System role should be displayed.
		UsersPageMSO.$UsersTable.forceProcessTable();
		String roleExpected2 = "System";
		
		for(String rowValue: UsersPageMSO.$UsersTable.getAllColumnValuesAsListForHeader("Roles")){
			System.out.println(rowValue);
			verify.isSame(roleExpected2,rowValue, false);
		}
		UsersPageMSO.clearSearchCriteria();
		
		//for loop - roles column needs to be checked only

		QAProof.writeInfo("Steps 19");
		CommonObjectsMSO.$AdvancedFilterLink.interact().click();
		CommonObjectsMSO.$AdvancedSearchCheckboxIncludeAdmins.interact().setCheckBox_ON();
		CommonObjectsMSO.$AdvancedSearchCheckboxIncludeSystem.interact().setCheckBox_ON();
		CommonObjectsMSO.$AdvancedSearchCheckboxIncludeLocked.interact().setCheckBox_ON();
		CommonObjectsMSO.$AdvancedSearchSubmit.interact().click();
		//All the users with role as either Administrator , System or both and account status as locked should be displayed.
		UsersPageMSO.$UsersTable.forceProcessTable();
		
		String statusExpected1 = "Account Deactivated";
		String statusExpected2 = "Account Ok";
		String statusExpected3 = "Account Locked";
		
		int index = 0;
		
		for(String rowValue: UsersPageMSO.$UsersTable.getAllColumnValuesAsListForHeader("Roles")){
			System.out.println(rowValue);
			if (verify.isSame(roleExpected1,rowValue, false) || verify.isSame(roleExpected2,rowValue, false)) {
				
				String rowValue2 = UsersPageMSO.$UsersTable.getAllColumnValuesAsListForHeader("Status").get(index);
				if (verify.isSame(statusExpected3,rowValue2, false) || verify.isSame(statusExpected2,rowValue2, false)) {
				} else {
					verify.reportIssue("Status is incorrect");
				}
			} else {
				verify.reportIssue("Role is incorrect");
			}
			index++;
		}
		UsersPageMSO.clearSearchCriteria();

		QAProof.writeInfo("Steps 20");
		CommonObjectsMSO.$AdvancedFilterLink.interact().click();
		CommonObjectsMSO.$AdvancedSearchCheckboxIncludeAdmins.interact().setCheckBox_ON();
		CommonObjectsMSO.$AdvancedSearchCheckboxIncludeLocked.interact().setCheckBox_OFF();
		CommonObjectsMSO.$AdvancedSearchCheckboxIncludeDeactivated.interact().setCheckBox_ON();
		CommonObjectsMSO.$AdvancedSearchSubmit.interact().click();
		//All the users with role as either Administrator , System or both with account status as deactivated should be displayed.
		UsersPageMSO.$UsersTable.forceProcessTable();
		
		for(String rowValue: UsersPageMSO.$UsersTable.getAllColumnValuesAsListForHeader("Roles")){
			System.out.println(rowValue);
			if (verify.isSame(roleExpected1,rowValue, false) || verify.isSame(roleExpected2,rowValue, false)) {
				
				String rowValue2 = UsersPageMSO.$UsersTable.getAllColumnValuesAsListForHeader("Status").get(index);
				if (verify.isSame(statusExpected1,rowValue2, false) || verify.isSame(statusExpected2,rowValue2, false)) {
				} else {
					verify.reportIssue("Status is incorrect");
				}
			} else {
				verify.reportIssue("Role is incorrect");
			}
			index++;
		}
		UsersPageMSO.clearSearchCriteria();


		QAProof.writeInfo("Test case End");
	}
}
