
package com.mirth.application.version2.mso.pages;

import com.mirth.application.master.PageObject;
import com.mirth.utility.seleniumLib.CheckBoxS;
import com.mirth.utility.seleniumLib.LinkS;
import com.mirth.utility.seleniumLib.TableS;


public class ActiveSessionsPageMSO extends PageObject {

	public static TableS $ActiveSessionsTable = new TableS("ActiveSessionsTable");	
	public static CheckBoxS $ActiveSessionsTableCheckbox = new CheckBoxS("ActiveSessionsTableCheckbox");
	public static LinkS $ActiveSessionsTableDelete = new LinkS("ActiveSessionsTableDelete");
	
	
	/* END -Search functionalities */	
	
	
}//class

