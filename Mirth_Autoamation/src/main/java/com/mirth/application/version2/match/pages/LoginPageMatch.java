//This class is basically same as Login MSO, but a separate class to provide for Test data and other issues.

package com.mirth.application.version2.match.pages;

import com.mirth.application.master.PageObject;
import com.mirth.exceptions.NotATableElementException;
import com.mirth.results.QAProof;
import com.mirth.utility.general.MirthLib;
import com.mirth.utility.seleniumLib.ButtonS;
import com.mirth.utility.seleniumLib.CheckBoxS;
import com.mirth.utility.seleniumLib.PlainTextS;
import com.mirth.utility.seleniumLib.SeleniumLib;
import com.mirth.utility.seleniumLib.SimpleDropDownS;
import com.mirth.utility.seleniumLib.TextBoxS;

public final class LoginPageMatch extends PageObject{
	
	public static final PlainTextS SSOLogoLoginPage =   new PlainTextS ("SSOLogoLoginPage"); 
	public static final PlainTextS $WarningText =   new PlainTextS ("WarningText"); 
	public static final PlainTextS $WarningLogo =   new PlainTextS ("WarningLogo"); 
	public static final PlainTextS $UsernameLabel =   new PlainTextS ("UsernameLabel"); 
	public static final TextBoxS $UsernameTextBox =   new TextBoxS ("UsernameTextBox"); 
	public static final PlainTextS $ForgotUsernameLink =   new PlainTextS ("ForgotUsernameLink"); 
	public static final PlainTextS $PasswordLabel =   new PlainTextS ("PasswordLabel"); 
	public static final TextBoxS $PasswordTextBox =   new TextBoxS ("PasswordTextBox"); 
	public static final PlainTextS $ForgotPasswordLink =   new PlainTextS ("ForgotPasswordLink"); 
	public static final ButtonS $LoginButton =   new ButtonS ("LoginButton"); 
	public static final PlainTextS $FooterText =   new PlainTextS ("FooterText"); 
	public static final PlainTextS $CopyrightText =   new PlainTextS ("CopyrightText"); 
	public static final PlainTextS $AboutLink =   new PlainTextS ("AboutLink"); 
	public static final PlainTextS $AboutPopUp =   new PlainTextS ("AboutPopUp"); 
	public static final PlainTextS $AboutPopUpCloseButton =   new PlainTextS ("AboutPopUpCloseButton"); 
	public static final PlainTextS $AboutPopUpLogo =   new PlainTextS ("AboutPopUpLogo"); 
	public static final PlainTextS $AboutPopUpVersionLabel =   new PlainTextS ("AboutPopUpVersionLabel"); 
	public static final PlainTextS $AboutPopUpVersionNumber =   new PlainTextS ("AboutPopUpVersionNumber"); 
	public static final PlainTextS $AboutPopUpRealmLabel =   new PlainTextS ("AboutPopUpRealmLabel"); 
	public static final PlainTextS $AboutPopUpRealmValue =   new PlainTextS ("AboutPopUpRealmValue"); 
	public static final PlainTextS $AboutPopUpNoticeAndDisclaimerLink =   new PlainTextS ("AboutPopUpNoticeAndDisclaimerLink"); 
	public static final PlainTextS $AboutPopUpNoticeAndDisclaimerPopUp =   new PlainTextS ("AboutPopUpNoticeAndDisclaimerPopUp"); 
	public static final PlainTextS $AboutPopUpNoticeAndDisclaimerPopUpCloseButton =   new PlainTextS ("AboutPopUpNoticeAndDisclaimerPopUpCloseButton"); 
	public static final PlainTextS $AboutPopUpNoticeAndDisclaimerPopUpText =   new PlainTextS ("AboutPopUpNoticeAndDisclaimerPopUpText"); 
	public static final PlainTextS $AboutPopUpPrivacyLink =   new PlainTextS ("AboutPopUpPrivacyLink"); 
	public static final PlainTextS $AboutPopUpPrivacyPopUp =   new PlainTextS ("AboutPopUpPrivacyPopUp"); 
	public static final PlainTextS $AboutPopUpPrivacyPopUpCloseButton =   new PlainTextS ("AboutPopUpPrivacyPopUpCloseButton"); 
	public static final PlainTextS $AboutPopUpPrivacyPopUpText =   new PlainTextS ("AboutPopUpPrivacyPopUpText"); 
	public static final PlainTextS $AboutPopUpCopyrightText =   new PlainTextS ("AboutPopUpCopyrightText ");
	public static final PlainTextS $LoginErrorMessageText = new PlainTextS("LoginErrorMessageText");
	public static final PlainTextS $PasswordErrorMessage = new PlainTextS("PasswordErrorMessage");
		
	public static final TextBoxS $NewPassword =   new TextBoxS ("NewPassword"); 
	public static final TextBoxS $ConfirmNewPassword =   new TextBoxS ("ConfirmNewPassword"); 
	public static final ButtonS $ChangeButton = new ButtonS("ChangeButton");
	public static final SimpleDropDownS $SecQuestion1DropDown = new SimpleDropDownS("SecQuestion1DropDown");
	public static final TextBoxS $SecAnswer1TextBox =   new TextBoxS ("SecAnswer1TextBox");
	public static final SimpleDropDownS $SecQuestion2DropDown = new SimpleDropDownS("SecQuestion2DropDown");
	public static final TextBoxS $SecAnswer2TextBox =   new TextBoxS ("SecAnswer2TextBox");
	public static final ButtonS $SaveButton = new ButtonS("SaveButton");
	
	public static final TextBoxS $TwoFactorChallengeQuestion =   new TextBoxS ("TwoFactorChallengeQuestion");
	public static final TextBoxS $TwoFactorChallengeAnswer =   new TextBoxS ("TwoFactorChallengeAnswer");
	public static final ButtonS $TwoFactorNextButton = new ButtonS("TwoFactorNextButton");
	
	public static final PlainTextS $TwoFactorChallengeQuestionWhileLoggingIn = new PlainTextS("TwoFactorChallengeQuestionWhileLoggingIn");
	public static final TextBoxS $TwoFactorAnswerWhileLoggingIn = new TextBoxS("TwoFactorAnswerWhileLoggingIn");
	public static final ButtonS $VerifyButtonInTwoFactorWhileLoggingIn = new ButtonS("VerifyButtonInTwoFactorWhileLoggingIn");
	public static final ButtonS $CancelButtonInTwoFactorWhileLoggingIn = new ButtonS("CancelButtonInTwoFactorWhileLoggingIn");
	
	public static final CheckBoxS $DontAskForCheckBox = new CheckBoxS("DontAskForCheckBox");
	
	
	public static void launch() throws Exception {
		
		String url = "http://" + MirthLib.getServerNameOrIP()
					 + td("_RelativeApplicationURL");
		
		getDriver().get(url);		
	}
	
	public static void login() throws Exception {
		
		String userName = getProcessedTestData($UsernameTextBox);
		String password = getProcessedTestData($PasswordTextBox);
		login(userName, password);
		
	}
	
	public static void login(String userName,String password) throws Exception {
		
		setData($UsernameTextBox,userName);
		setData($PasswordTextBox,password);
		QAProof.screenshot();
		interactWith($LoginButton).click();
		
	}
	
	public static void setNewPassword(String password) throws Exception {
		setNewPassword(password, true);
	}
	
	public static void setNewPassword(String password,boolean submit) throws Exception {
		setData($NewPassword,password);
		setData($ConfirmNewPassword,password);
		
		if (submit){
			$ChangeButton.interact().click();
		}
	}
	
	
	//Two factor authentication
	public static void setDefaultTwoFactorQuestionAnswer() throws NotATableElementException, Exception{
		setTwoFactorQuestionAnswer(	
				td($TwoFactorChallengeQuestion),
				td($TwoFactorChallengeAnswer)
		);
		
	}
	
	
	public static void setTwoFactorQuestionAnswer(String question, String answer) throws NotATableElementException, Exception{
		setData($TwoFactorChallengeQuestion,question);
		setData($TwoFactorChallengeAnswer,answer);	
	}
	
	
	
	//security questions
	public static void setDefaultSecurityQuestionAnswers() throws Exception{
		setSecurityQuestionAnswers (
				td($SecQuestion1DropDown),
				td($SecAnswer1TextBox),
				td($SecQuestion2DropDown),
				td($SecAnswer2TextBox)
				);		
	}
	
	public static void setSecurityQuestionAnswers(String q1, String a1, String q2, String a2) throws Exception{
		System.out.println(q1+"\n"+a1+"\n"+q2+"\n"+a2);
		
		SeleniumLib.highlightElementForSec(getObject($SecQuestion1DropDown), 10);
		setData($SecQuestion1DropDown,q1);
		setData($SecAnswer1TextBox,a1);
		setData($SecQuestion2DropDown,q2);
		setData($SecAnswer2TextBox,a2);
	}
	
	
}//class