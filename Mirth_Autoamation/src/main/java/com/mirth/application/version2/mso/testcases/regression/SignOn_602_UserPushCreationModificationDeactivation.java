
package com.mirth.application.version2.mso.testcases.regression;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import com.mirth.application.master.General;
import com.mirth.application.master.PageObject;
import com.mirth.application.master.Testcase;
import com.mirth.application.version2.mso.pages.CommonObjectsMSO;
import com.mirth.application.version2.mso.pages.LoginPageMSO;
import com.mirth.application.version2.mso.pages.NewUserPageMSO;
import com.mirth.application.version2.mso.pages.UsersPageMSO;
import com.mirth.application.version2.results.pages.CommonObjectsResults;
import com.mirth.application.version2.results.pages.EditUserPageResults;
import com.mirth.application.version2.results.pages.LoginPageResults;
import com.mirth.application.version2.results.pages.PatientsPageResults;
import com.mirth.application.version2.results.pages.SiteSelectorPageResults;
import com.mirth.application.version2.results.pages.UserAdminPageResults;
import com.mirth.results.QAProof;
import com.mirth.utility.general.SleepLib;
import com.mirth.utility.seleniumLib.SeleniumElement;

public class SignOn_602_UserPushCreationModificationDeactivation extends Testcase {

	@Override
	protected void steps() throws Exception {
		
		QAProof.writeInfo("Running Steps 1 to 7 now");
		General.openFreshBrowser("main", super.testInventory.browser);
		General.setBrowserWaitTime(10, TimeUnit.SECONDS);
		LoginPageMSO.launch();
		LoginPageMSO.login();
		CommonObjectsMSO.clickUsersTab();
		UsersPageMSO.clickCreateNewUser();		
		Map<SeleniumElement,String> newUserVals = NewUserPageMSO.createNewUserFromTestData();
		QAProof.screenshot(NewUserPageMSO.$SecurityRolesTable);
		NewUserPageMSO.clickOnSaveChangesLink();
		headingController.verifing("NewUserPage of MSO Applicaiton after clicking on Save changes link");
		QAProof.screenshot(NewUserPageMSO.$AccountStatusDropDown);
		NewUserPageMSO.verifyNewUserValues(newUserVals);
		NewUserPageMSO.clickOnBackToListLink();
		headingController.verifing("Users table of Users Page in MSO Applicaiton after filtering the appropriate patient");
		UsersPageMSO.verifyUserDataAsPerCreation(newUserVals);
		
		
		//Open new additional browser to test service provider application
		General.openAndPointToAddtionalBrowser("spBrowser", super.testInventory.browser);
		General.setBrowserWaitTime(10, TimeUnit.SECONDS);
		LoginPageResults.launch();
		LoginPageResults.login();
		SiteSelectorPageResults.selectSite();
		PatientsPageResults.$CancelButtonOnAdvancedPatientFilter.interact().click();
		SleepLib.sleep2Seconds();
		CommonObjectsResults.goToUsersPage();
		headingController.verifing("Users table of Users Page in Results Applicaiton after filtering the appropriate patient");
		QAProof.screenshot(UserAdminPageResults.$UsersTable);
		UserAdminPageResults.verifyUserTableAsPerMso(newUserVals);
		UserAdminPageResults.$UsersTable.clickFirstRowOfUsersTable(UserAdminPageResults.usersTableAccountID);
		headingController.verifing("User details in Edit User Page of Results Applicaiton");
		QAProof.screenshot(EditUserPageResults.$AccountIDTextValue);
		EditUserPageResults.verifyUserDetailsAsPerMso(newUserVals);
		CommonObjectsResults.$SignOutButtonOnKanaToolBar.interact().click();
		SleepLib.sleep2Seconds();
		General.closeAndDestroyBrowser("spBrowser");
		General.switchToBrowser("main");

		UsersPageMSO.$UsersTable.clickFirstRowOfUsersTable(UsersPageMSO.usersTableAccountID);
		Map<SeleniumElement,String> userFieldUpdates = new LinkedHashMap<SeleniumElement,String>();
		
		userFieldUpdates.put(NewUserPageMSO.$Chk_ChangePasswordNextLogin, "OFF");
		userFieldUpdates.put(NewUserPageMSO.$Chk_SecurityQuestion, "OFF");
		userFieldUpdates.put(NewUserPageMSO.$Name, PageObject.getProcessedTestData(NewUserPageMSO.$Name));
		userFieldUpdates.put(NewUserPageMSO.$Email, PageObject.getProcessedTestData(NewUserPageMSO.$Email));
		userFieldUpdates.put(NewUserPageMSO.$VoiceContact, PageObject.getProcessedTestData(NewUserPageMSO.$VoiceContact));
		userFieldUpdates.put(NewUserPageMSO.$FaxContact, PageObject.getProcessedTestData(NewUserPageMSO.$FaxContact));
		userFieldUpdates.put(NewUserPageMSO.$AddressLine1, "Pritech Park SEZ, Block 11A");
		userFieldUpdates.put(NewUserPageMSO.$AddressLine2, "Outer Ring Rd, Bellandur");
		userFieldUpdates.put(NewUserPageMSO.$City, "Bangalore");
		userFieldUpdates.put(NewUserPageMSO.$State, "KA");
		userFieldUpdates.put(NewUserPageMSO.$Postal, "560103");
		userFieldUpdates.put(NewUserPageMSO.$Country, "India");
		newUserVals.putAll(userFieldUpdates);
		
		QAProof.writeText("Updating user details now",proofDocColorFormat);
		NewUserPageMSO.updateNewUserPageFor(userFieldUpdates);
		QAProof.screenshot(NewUserPageMSO.$SecurityRolesTable);
		SleepLib.sleep2Seconds();
		NewUserPageMSO.clickOnSaveChangesLink();
		
		headingController.verifing("updated values in NewUserPage of MSO Applicaiton after clicking on Save changes link");
		QAProof.screenshot(NewUserPageMSO.$SecurityRolesTable);
		NewUserPageMSO.verifyNewUserValues(newUserVals);
		NewUserPageMSO.clickOnBackToListLink();
		//SleepLib.sleep2Seconds();
		headingController.verifing("for updated values in Users table of Users Page in MSO Applicaiton after filtering the appropriate patient");
		UsersPageMSO.verifyUserDataAsPerCreation(newUserVals);
		
				
		//Open new additional browser to test service provider application
		General.openAndPointToAddtionalBrowser("spBrowser", super.testInventory.browser);
		General.setBrowserWaitTime(10, TimeUnit.SECONDS);
		LoginPageResults.launch();
		LoginPageResults.login();
		SiteSelectorPageResults.$SiteSelectorTable.forceProcessTable();
		SiteSelectorPageResults.selectSite();
		PatientsPageResults.$CancelButtonOnAdvancedPatientFilter.interact().click();
		SleepLib.sleep3Seconds();
		CommonObjectsResults.goToUsersPage();
		headingController.verifing("updated values in Users table of Users Page in Results Applicaiton after filtering the appropriate patient");
		QAProof.screenshot(UserAdminPageResults.$UsersTable);
		UserAdminPageResults.verifyUserTableAsPerMso(newUserVals);	
		UserAdminPageResults.$UsersTable.clickFirstRowOfUsersTable(UserAdminPageResults.usersTableAccountID);
		headingController.verifing("updated values in User details in Edit User Page of Results Applicaiton");
		QAProof.screenshot(EditUserPageResults.$AccountIDTextValue);
		EditUserPageResults.verifyUserDetailsAsPerMso(newUserVals);
		CommonObjectsResults.$SignOutButtonOnKanaToolBar.interact().click();
		SleepLib.sleep2Seconds();
		LoginPageResults.login(newUserVals.get(NewUserPageMSO.$AccountIDTextValue),newUserVals.get(NewUserPageMSO.$NewPassword));
		QAProof.screenshot(SiteSelectorPageResults.$SiteSelectorTable);
		SiteSelectorPageResults.$SiteSelectorTable.forceProcessTable();
		SiteSelectorPageResults.selectSite();
		PatientsPageResults.$CancelButtonOnAdvancedPatientFilter.interact().click();
		SleepLib.sleep2Seconds();
		QAProof.screenshot(CommonObjectsResults.$SignOutButtonOnKanaToolBar);
		headingController.verifing("login of the created user.");
		verify.reportSuccess("Succesfully Logged in to Mirth Results with newly created user: "+newUserVals.get(NewUserPageMSO.$AccountIDTextValue)+" (Account ID).");
		CommonObjectsResults.$SignOutButtonOnKanaToolBar.interact().click();
		
		
		General.closeAndDestroyBrowser("spBrowser");
		General.switchToBrowser("main");
		UsersPageMSO.$UsersTable.clickFirstRowOfUsersTable(UsersPageMSO.usersTableAccountID);
		QAProof.writeText("Changing the value of AccountStatus to Deactivated.",proofDocColorFormat);
		PageObject.setData(NewUserPageMSO.$AccountStatusDropDown, "Deactivated");
		CommonObjectsMSO.$SaveChangesLinkInLeftPane.interact().click();
		SleepLib.sleep1Second();
		headingController.verifing("AccountStatus DropDown in NewUserPage of MSO Application");
		QAProof.screenshot(NewUserPageMSO.$AccountStatusDropDown);
		PageObject.verifyStringData(NewUserPageMSO.$AccountStatusDropDown,"Deactivated",false);
		CommonObjectsMSO.$BackToListLinkInLeftPane.interact().click();
		CommonObjectsMSO.searchTextBoxFor(newUserVals.get(NewUserPageMSO.$AccountIDTextBox));
		UsersPageMSO.$UsersTable.forceProcessTable();
		headingController.verifing("User Table of Users Page in MSO after deactivating the user");
		QAProof.screenshot(UsersPageMSO.$UsersTable);
		if (UsersPageMSO.$UsersTable.getDataRowsCount() == 1 && UsersPageMSO.$UsersTable.getValueFromTable(1, 1).equalsIgnoreCase("Nothing found to display.")) {
			verify.reportSuccess("The deactivated user is NOT seen in user table of MSO as expected");
		}
		else{
			verify.reportIssue("Users table is not empty. So, either filtering is not happening correctly OR Deactivated user is seen in table OR duplicate accountID");			
			
		}
		CommonObjectsMSO.$LogoutLink.interact().click();
		General.closeAndDestroyBrowser("main");
		
		//Open new additional browser to test service provider application
		General.openAndPointToAddtionalBrowser("spBrowser", super.testInventory.browser);
		General.setBrowserWaitTime(10, TimeUnit.SECONDS);
		LoginPageResults.launch();
		LoginPageResults.login(newUserVals.get(NewUserPageMSO.$AccountIDTextValue),newUserVals.get(NewUserPageMSO.$NewPassword));
		SleepLib.sleep1Second();
		String expectedErrorMessage = PageObject.td(LoginPageResults.class,"_DeactivationErrorMessage");
		expectedErrorMessage = expectedErrorMessage.replace("<ActualAccountID>", newUserVals.get(NewUserPageMSO.$AccountIDTextBox));
		String errorMessageActual = PageObject.getData(LoginPageResults.$LoginErrorMessageText);
		headingController.verifing("Login with Deactivated User");
		QAProof.screenshot();
		boolean loginResult = verify.withReport(LoginPageResults.$LoginErrorMessageText.get()).isSame(expectedErrorMessage, errorMessageActual, false);
		if (loginResult){
			verify.reportSuccess("User NOT able to login as expected");
		}else{
			verify.reportIssue("Login with Deactivated user did NOT behave as expected");
		}
		LoginPageResults.login();
		SiteSelectorPageResults.$SiteSelectorTable.forceProcessTable();
		SiteSelectorPageResults.selectSite();
		PatientsPageResults.$CancelButtonOnAdvancedPatientFilter.interact().click();
		SleepLib.sleep2Seconds();
		CommonObjectsResults.goToUsersPage();
		CommonObjectsResults.searchTextBoxFor(newUserVals.get(NewUserPageMSO.$AccountIDTextBox));
		headingController.verifing("User Table of Users Page in Results application (deactivated user)");
		QAProof.screenshot(UserAdminPageResults.$UsersTable);
		UserAdminPageResults.$UsersTable.forceProcessTable();
		if (UserAdminPageResults.$UsersTable.getDataRowsCount() == 1 && UserAdminPageResults.$UsersTable.getValueFromTable(1, 1).equalsIgnoreCase("Nothing found to display.")) {
			verify.reportSuccess("The deactivated user is NOT seen in user table of Results as expected");
		}
		else{
			verify.reportIssue("Users table is not empty. So, either filtering is not happening correctly OR Deactivated user is seen in table OR duplicate accountID");			
			
		}
		
		CommonObjectsResults.$SignOutButtonOnKanaToolBar.interact().click();
		General.closeAndDestroyBrowser("spBrowser");
		headingController.noProofHeading();
		QAProof.writeText("Test case finished executing.",proofDocColorFormat);
				
	}	
	
		
}//class
