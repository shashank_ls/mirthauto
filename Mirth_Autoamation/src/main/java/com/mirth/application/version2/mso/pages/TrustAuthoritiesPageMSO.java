package com.mirth.application.version2.mso.pages;

import java.util.LinkedHashMap;
import java.util.Map;

import com.mirth.application.master.PageObject;
import com.mirth.utility.seleniumLib.ButtonS;
import com.mirth.utility.seleniumLib.LinkS;
import com.mirth.utility.seleniumLib.PlainTextS;
import com.mirth.utility.seleniumLib.SeleniumElement;
import com.mirth.utility.seleniumLib.TableS;
import com.mirth.utility.seleniumLib.TextBoxS;

public final class TrustAuthoritiesPageMSO extends PageObject{
	
	public static final TableS $TrustAuthoritiesTable = new TableS("TrustAuthoritiesTable");
	public static final LinkS $CreateTrustAuthorityLink = new LinkS("CreateTrustAuthorityLink");
	public static final TextBoxS $SamLIssuerTextBoxInNewTrustAuthority = new TextBoxS("SamLIssuerTextBoxInNewTrustAuthority");
	public static final TextBoxS $NameTextBoxInNewTrustAuthority = new TextBoxS("NameTextBoxInNewTrustAuthority");
	public static final TextBoxS $EmailTextBoxInNewTrustAuthority = new TextBoxS("EmailTextBoxInNewTrustAuthority");
	public static final ButtonS $CertificateUploadBrowseButtonInNewTrustAuthority = new ButtonS("CertificateUploadBrowseButtonInNewTrustAuthority");
	public static final PlainTextS $FingerPrintInEditTrustAuthority = new PlainTextS("FingerPrintInEditTrustAuthority");
	
	
	public static final String samlIssuerHeading = "SAML Issuer";
	public static final String nameHeading = "Name";
	public static final String emailHeading = "Email";
	public static final String createdOn = "Created On";
	
	
	
	public static Map<SeleniumElement,String> createTrustAuthorityWithSave(String samlIssuer,String name,String email, String certficatePath) throws Exception {
		
		Map<SeleniumElement,String> dataMap =  createTrustAuthorityWithOutSave(samlIssuer, name, email, certficatePath);
		CommonObjectsMSO.$SaveChangesLinkInLeftPane.interact().click();
		
		return dataMap;
	}
	
	public static Map<SeleniumElement,String> createTrustAuthorityWithOutSave(String samlIssuer,String name,String email, String certficatePath) throws Exception{
		
		Map<SeleniumElement,String> createTrustAuthorityMap = new LinkedHashMap<SeleniumElement,String>();
		createTrustAuthorityMap.put($SamLIssuerTextBoxInNewTrustAuthority, samlIssuer);
		createTrustAuthorityMap.put($NameTextBoxInNewTrustAuthority, name);
		createTrustAuthorityMap.put($EmailTextBoxInNewTrustAuthority, email);
		createTrustAuthorityMap.put($CertificateUploadBrowseButtonInNewTrustAuthority, certficatePath);
		
		setData(createTrustAuthorityMap);
		
		return createTrustAuthorityMap;				
		
	}
		
	
}//class