
package com.mirth.application.version2.mso.pages;
import java.util.Map;

import com.mirth.application.master.PageObject;
import com.mirth.properties.InteractConfig;
import com.mirth.utility.general.SleepLib;
import com.mirth.utility.general.StaticLib;
import com.mirth.utility.seleniumLib.ButtonS;
import com.mirth.utility.seleniumLib.CheckBoxS;
import com.mirth.utility.seleniumLib.LinkS;
import com.mirth.utility.seleniumLib.MultiSelectDropDownS;
import com.mirth.utility.seleniumLib.RadioButtonS;
import com.mirth.utility.seleniumLib.SeleniumElement;
import com.mirth.utility.seleniumLib.TableS;
import com.mirth.utility.seleniumLib.TextBoxS;

public class NewTemplatePageMSO extends PageObject {
	
	public static TextBoxS $IdentifierTextBox = new TextBoxS("IdentifierTextBox");
	public static TextBoxS $LabelTextBox = new TextBoxS("LabelTextBox");
	public static TextBoxS $DescriptionTextBox = new TextBoxS("DescriptionTextBox");
	public static CheckBoxS $Chk_AccountLockState = new CheckBoxS("Chk_AccountLockState");
	public static CheckBoxS $Chk_ChangePasswordNextLogin = new CheckBoxS("Chk_ChangePasswordNextLogin");
	public static CheckBoxS $Chk_TwoFactorAuthentication = new CheckBoxS("Chk_TwoFactorAuthentication");
	public static RadioButtonS $Rad_OTP = new RadioButtonS("Rad_OTP");
	public static RadioButtonS $Rad_ChallengeResponse = new RadioButtonS("Rad_ChallengeResponse");
	public static CheckBoxS $Chk_SecurityQuestion = new CheckBoxS("Chk_SecurityQuestion");
	public static TableS $UsersTable = new TableS("UsersTable");
	public static MultiSelectDropDownS $SelectListInSecurityRolesPopup = new MultiSelectDropDownS("SelectListInSecurityRolesPopup");
	
	public static TableS $SecurityRolesTable = new TableS("SecurityRolesTable");
	
	public static LinkS $AddSecurityRoleLinkInTemplateActions = new LinkS("AddSecurityRoleLinkInTemplateActions");
	
	
	public static ButtonS $SubmitButtonInAddSecurityRolePopup = new ButtonS("SubmitButtonInAddSecurityRolePopup");
	public static ButtonS $CancelButtonInAddSecurityRolePopup = new ButtonS("$CancelButtonInAddSecurityRolePopup");
	public static LinkS $DeleteSecurityRolesLinkInUserActions = new LinkS("DeleteSecurityRolesLinkInUserActions");
	
	//Element Constants
	public static String securityRoleNameColumnHeading = "Name";
	
	//methods
	
	public static Map<SeleniumElement,String> createNewTemplateFromTestData() throws Exception{
		return createNewTemplateWithCustomReplace(null);
	}
	
	public static Map<SeleniumElement,String> createNewTemplateWithCustomReplace(Map<SeleniumElement,String> changableFields) throws Exception{
		Map<SeleniumElement,String> resultMap = null;
		
		resultMap = getProcessedTestData(
						$IdentifierTextBox,
						$LabelTextBox,
						$DescriptionTextBox,
						$Chk_AccountLockState,
						$Chk_ChangePasswordNextLogin,
						$Chk_TwoFactorAuthentication,
						$Rad_OTP,
						$Rad_ChallengeResponse,
						$Chk_SecurityQuestion
					);

		String securityRolesToBeSelected = getProcessedTestData($SelectListInSecurityRolesPopup);


		if (changableFields!=null){
			String secRoles = changableFields.remove($SelectListInSecurityRolesPopup);
			
			for(SeleniumElement selEle: changableFields.keySet()){
				resultMap.put(selEle, changableFields.get(selEle));
			}
			
			if (secRoles!=null){
				securityRolesToBeSelected = secRoles;
			}
		}//if (changableFields!=null)
		
		setData(resultMap);
		
		$AddSecurityRoleLinkInTemplateActions.interact().click();
		SleepLib.sleep1Second();
		setData($SelectListInSecurityRolesPopup,securityRolesToBeSelected);
		resultMap.put($SelectListInSecurityRolesPopup, securityRolesToBeSelected);
		$SubmitButtonInAddSecurityRolePopup.interact().click();
		
		return resultMap;		
		
	}
	
	public static Map<SeleniumElement,String> updateTemplatePageFor(Map<SeleniumElement,String> changableFields) throws Exception {
		if (changableFields == null){
			return null;
		}
		
		String securityRolesToBeSelected = changableFields.remove($SelectListInSecurityRolesPopup);
		
		for(SeleniumElement selEle: changableFields.keySet()){
			setData(selEle,changableFields.get(selEle));
		}
		
		if (securityRolesToBeSelected !=null){
			$AddSecurityRoleLinkInTemplateActions.interact().click();
			SleepLib.sleep1Second();
			setData($SelectListInSecurityRolesPopup,securityRolesToBeSelected);
			changableFields.put($SelectListInSecurityRolesPopup, securityRolesToBeSelected);
			$SubmitButtonInAddSecurityRolePopup.interact().click();	
		}
		
		return changableFields;		
	}
	
	public static Map<SeleniumElement,Boolean> verifyNewTemplateValues(Map<SeleniumElement,String> actualMap) throws Exception {
		Map<SeleniumElement,String> verificationMap = StaticLib.getNewMapCopy(actualMap);
		actualMap = null;
		
		Map<SeleniumElement,Boolean> resultBooleanMap;
		
		String rolesExpected = verificationMap.remove($SelectListInSecurityRolesPopup);
		
		resultBooleanMap = verifyStringData(verificationMap,false);
		$SecurityRolesTable.processTable();
		String rolesActual = StaticLib.getStringListValuesAsString($SecurityRolesTable.getAllColumnValuesAsListForHeader(securityRoleNameColumnHeading),InteractConfig.getValue("MultiSelectSeparator"));
		
		resultBooleanMap.put($SecurityRolesTable, verifyStringData($SecurityRolesTable.get(), rolesExpected, rolesActual, false) );
		
		return resultBooleanMap;
		
	}
	

}//class

