package com.mirth.application.version2.results.pages.pagemaster;

import java.io.IOException;
import java.util.Properties;

import com.mirth.application.master.ClassPageMapping;

public class ResultsClassPageMapping implements ClassPageMapping{
	
	private ResultsClassPageMapping() {
		//Singleton
	}

	//singleton object
	private static ResultsClassPageMapping msoClassPageObject =null; 
	private Properties config;
	private String configFileName= "resultsClassPageMapping.properties";
	//Config Variable END----------------------
	
	
	public synchronized static ClassPageMapping getObject() {
		
		if (ResultsClassPageMapping.msoClassPageObject == null) {
			ResultsClassPageMapping.msoClassPageObject = new ResultsClassPageMapping();
		}
		
		return ResultsClassPageMapping.msoClassPageObject;
	}
	
	
	private synchronized void initiateControllerConfig() throws IOException {
		config = new Properties();
		config.load(ResultsClassPageMapping.class.getResourceAsStream(configFileName));
		
	}//initiateControllerConfig() throws IOException
	
	@Override
	public synchronized String getPageNameForClass(String configKey) throws IOException {
		
		if (isConfigPropertyNull()) {
			initiateControllerConfig();
		}
		
		return config.getProperty(configKey);
	}

	public synchronized boolean isConfigPropertyNull() {
		return (config==null)?true:false;
	}
	
}
