package com.mirth.application.version2.mso.pages;

import com.mirth.application.master.General;
import com.mirth.application.master.PageObject;
import com.mirth.utility.general.SleepLib;
import com.mirth.utility.seleniumLib.TableS;

public class ProvisioningErrorsMSO extends PageObject {
	
	public static TableS $ProvisioningErrorsTable = new TableS("ProvisioningErrorsTable");
	
	//table label headings
	public static String userPrincipalHeading = "User Principal";
	public static String operationHeading = "Operation";
	public static String applicationNameHeading = "Applicaition Name";
	public static String errorMessageHeading = "Error Message";
	
	public static void clearTable() throws Exception {
		
		$ProvisioningErrorsTable.forceProcessTable();
		
		while (!$ProvisioningErrorsTable.isTableEmpty()){
			
			$ProvisioningErrorsTable.getHeadingCellTDElement(1).click();
			CommonObjectsMSO.$DeleteLinkInLeftPane.interact().click();
			SleepLib.sleep1Second();
			General.acceptAlert();

			SleepLib.sleep1Second();
			$ProvisioningErrorsTable.forceProcessTable();

			
		}//if (!$ProvisioningErrorsTable.isTableEmpty()){
		
	}//clearTable()
	
	
}
