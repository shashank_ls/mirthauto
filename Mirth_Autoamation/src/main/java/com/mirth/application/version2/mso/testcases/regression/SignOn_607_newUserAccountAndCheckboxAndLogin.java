
package com.mirth.application.version2.mso.testcases.regression;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Cookie;

import com.mirth.application.master.General;
import com.mirth.application.master.PageObject;
import com.mirth.application.master.Testcase;
import com.mirth.application.version2.mso.pages.CommonObjectsMSO;
import com.mirth.application.version2.mso.pages.LoginPageMSO;
import com.mirth.application.version2.mso.pages.NewUserPageMSO;
import com.mirth.application.version2.mso.pages.SystemSettingsPageMSO;
import com.mirth.application.version2.mso.pages.UsersPageMSO;
import com.mirth.application.version2.results.pages.CommonObjectsResults;
import com.mirth.application.version2.results.pages.LoginPageResults;
import com.mirth.application.version2.results.pages.PatientsPageResults;
import com.mirth.application.version2.results.pages.SiteSelectorPageResults;
import com.mirth.results.QAProof;
import com.mirth.utility.general.SleepLib;
import com.mirth.utility.seleniumLib.SeleniumElement;
import com.mirth.utility.seleniumLib.SeleniumLib;

public class SignOn_607_newUserAccountAndCheckboxAndLogin extends Testcase {
	
	public void selectSiteAndCancelPatientFilter() throws Exception {
		QAProof.screenshot(SiteSelectorPageResults.$SiteSelectorTable);
		SiteSelectorPageResults.$SiteSelectorTable.forceProcessTable();
		SiteSelectorPageResults.selectSite();
		PatientsPageResults.$CancelButtonOnAdvancedPatientFilter.interact().click();
		System.out.println("{Site selector}");	}
	
	@Override
	protected void steps() throws Exception {		
		
		QAProof.writeInfo(">>Step 1 to 7");
		General.openFreshBrowser("main", super.testInventory.browser);
		General.setBrowserWaitTime(10,TimeUnit.SECONDS);
		LoginPageMSO.launch();
		LoginPageMSO.login();
		CommonObjectsMSO.clickUsersTab();
		UsersPageMSO.clickCreateNewUser();	
		
		//getTheCurrentPassword
		String defaultPassword = NewUserPageMSO.$NewPassword.interact().getTextBoxValue();
		System.out.println("Def pass -> "+ defaultPassword);
		Map<SeleniumElement,String> changableFields = new LinkedHashMap<SeleniumElement,String>();
		changableFields.put(NewUserPageMSO.$Chk_ChangePasswordNextLogin, "ON");
		changableFields.put(NewUserPageMSO.$Chk_TwoFactorAuthentication, "ON");
		changableFields.put(NewUserPageMSO.$Rad_ChallengeResponse, "ON");
		changableFields.put(NewUserPageMSO.$Chk_SecurityQuestion, "ON");
		changableFields.put(NewUserPageMSO.$NewPassword, null);
		changableFields.put(NewUserPageMSO.$ConfirmNewPassword, null);			
		
		Map<SeleniumElement,String> user1Vals = NewUserPageMSO.createNewUserFromTestDataWithCustomReplace(changableFields);
		NewUserPageMSO.clickOnSaveChangesLink();
		headingController.verifing("NewUserPage of MSO Applicaiton after clicking on Save changes link");
		QAProof.screenshot(NewUserPageMSO.$AccountStatusDropDown);
		user1Vals.put(NewUserPageMSO.$NewPassword, defaultPassword);
		user1Vals.put(NewUserPageMSO.$ConfirmNewPassword, defaultPassword);			
		//CommonObjectsMSO.$LogoutLink.interact().click();
		
		General.openAndPointToAddtionalBrowser("spBrowser", super.testInventory.browser);
		General.setBrowserWaitTime(10,TimeUnit.SECONDS);
		LoginPageResults.launch();
		LoginPageResults.login(user1Vals.get(NewUserPageMSO.$AccountIDTextValue),user1Vals.get(NewUserPageMSO.$NewPassword));
		
		QAProof.writeInfo(">>Step 8 to 13");
		String newPassword1 = "test1234";
		LoginPageResults.setNewPassword(newPassword1);
		LoginPageResults.$ChangeButton.interact().click();
		LoginPageResults.setDefaultSecurityQuestionAnswers();
		LoginPageResults.$SaveButton.interact().click();
		LoginPageResults.setDefaultTwoFactorQuestionAnswer();	
		LoginPageResults.$TwoFactorNextButton.interact().click();
		//this.selectSiteAndCancelPatientFilter();
		CommonObjectsResults.$SignOutButtonOnKanaToolBar.interact().click();
		///---
		
		QAProof.writeInfo(">>Step 14 to 16");
		headingController.verifing("Two factor security Question/Answer");
		LoginPageResults.login(user1Vals.get(NewUserPageMSO.$AccountIDTextValue),newPassword1);
		String twoFactorQuestionExpected = PageObject.td(LoginPageResults.class,LoginPageResults.$TwoFactorChallengeQuestion);
		String twoFactorQuestionActual = LoginPageResults.$TwoFactorChallengeQuestionWhileLoggingIn.interact().getText();
		PageObject.verifyStringData("Two factor question", twoFactorQuestionExpected, twoFactorQuestionActual, false);
		LoginPageResults.$TwoFactorAnswerWhileLoggingIn.interact().setText(PageObject.td(LoginPageResults.class,LoginPageResults.$TwoFactorChallengeAnswer));
		LoginPageResults.$DontAskForCheckBox.interact().setCheckBox_ON();
		
		SleepLib.sleep5Seconds();
		LoginPageResults.$VerifyButtonInTwoFactorWhileLoggingIn.interact().click();
		SleepLib.sleep5Seconds();
		
		Set<Cookie> cookies = General.browser().manage().getCookies();
		
		System.out.println("Cookies--->");
		for (Cookie coo: cookies){
			System.out.println(coo.getName()+": " + coo.getValue());
			
		}
		
		
		System.exit(1);
		//this.selectSiteAndCancelPatientFilter();
		CommonObjectsResults.$SignOutButtonOnKanaToolBar.interact().click();
		
		QAProof.writeInfo(">>Step 17");
		LoginPageResults.login(user1Vals.get(NewUserPageMSO.$AccountIDTextValue),newPassword1);
		//>> Here it is asking 2 factor answer where it is not supposed to ask
		
		this.selectSiteAndCancelPatientFilter();
		CommonObjectsResults.$SignOutButtonOnKanaToolBar.interact().click();
		
		QAProof.writeInfo(">>Step 18 to 22");
		General.switchToBrowser("main");
		NewUserPageMSO.$Chk_IgnoreTwoFactorToken.interact().setCheckBox_ON();
		CommonObjectsMSO.$SaveChangesLinkInLeftPane.interact().click();
		
		General.switchToBrowser("spBrowser");
		LoginPageResults.login(user1Vals.get(NewUserPageMSO.$AccountIDTextValue),newPassword1);
		LoginPageResults.$TwoFactorAnswerWhileLoggingIn.interact().setText(PageObject.td(LoginPageResults.class,LoginPageResults.$TwoFactorChallengeAnswer));
		LoginPageResults.$DontAskForCheckBox.interact().click();
				
		this.selectSiteAndCancelPatientFilter();
		CommonObjectsResults.$SignOutButtonOnKanaToolBar.interact().click();
		LoginPageResults.login(user1Vals.get(NewUserPageMSO.$AccountIDTextValue),newPassword1);
		this.selectSiteAndCancelPatientFilter();
		CommonObjectsResults.$SignOutButtonOnKanaToolBar.interact().click();
		General.closeAndDestroyBrowser("spBrowser");		
		
		QAProof.writeInfo(">>Step 23 to 27");
		General.openAndPointToAddtionalBrowser("spBrowser2", super.testInventory.browser);
		General.setBrowserWaitTime(10,TimeUnit.SECONDS);
		LoginPageResults.login(user1Vals.get(NewUserPageMSO.$AccountIDTextValue),newPassword1);
		LoginPageResults.$TwoFactorAnswerWhileLoggingIn.interact().setText(PageObject.td(LoginPageResults.class,LoginPageResults.$TwoFactorChallengeAnswer));
		LoginPageResults.$VerifyButtonInTwoFactorWhileLoggingIn.interact().click();
		this.selectSiteAndCancelPatientFilter();
		CommonObjectsResults.$SignOutButtonOnKanaToolBar.interact().click();
		LoginPageResults.login(user1Vals.get(NewUserPageMSO.$AccountIDTextValue),newPassword1);
		
		String incorrectTwoFactorAnswer = "BlaBlaBla";
		LoginPageResults.$TwoFactorAnswerWhileLoggingIn.interact().setText(incorrectTwoFactorAnswer);
		LoginPageResults.$VerifyButtonInTwoFactorWhileLoggingIn.interact().click();
		String twoFactorErrorExpected = PageObject.td(LoginPageResults.class,"_IncorrectTwoFactorAnswerErrorMessage");
		String twoFactorErrorActual = PageObject.getData(LoginPageResults.$LoginErrorMessageText);
		headingController.verifing("Login with incorrect TwoFactor Password");
		boolean loginResult = verify.withReport(LoginPageResults.$LoginErrorMessageText.get())
									.isSame(twoFactorErrorExpected, twoFactorErrorActual, false);
		if (loginResult){
			verify.reportSuccess("User NOT able to login with Incorrect Two factor answer as expected");
		}else{
			verify.reportIssue("Login with incorrect 2 factor answer did NOT behave as expected");
		}
		
		LoginPageResults.$CancelButtonInTwoFactorWhileLoggingIn.interact().click();
		SeleniumLib.waitForVisiblity(LoginPageResults.$LoginButton);
				  
		QAProof.writeInfo(">>Step 28 to 30");
		General.switchToBrowser("main");
		CommonObjectsMSO.goToSystemSettingsInAdministrationTab();
		CommonObjectsMSO.searchTextBoxFor("com.mirth.kana.securitysetting.passwordResetUrl");
		SystemSettingsPageMSO.$SystemSettingsTable.forceProcessTable();
		
		String settingExpected = "com.mirth.kana.securitysetting.passwordResetUrl";
		String settingActual = SystemSettingsPageMSO.$SystemSettingsTable.getValueFromTable(SystemSettingsPageMSO.settingHeading,1);
		PageObject.verifyStringData("PasswordResetURLSetting",settingExpected,settingActual,false);
		
		SystemSettingsPageMSO.$SystemSettingsTable.clickFirstRowOfUsersTable(SystemSettingsPageMSO.valueHeading);
		String valueExpected = SystemSettingsPageMSO.updateLocalHostInValueWithCurrentServer();
		CommonObjectsMSO.$SaveChangesLinkInLeftPane.interact().click();
		CommonObjectsMSO.$BackToListLinkInLeftPane.interact().click();
		
		String valueActual = SystemSettingsPageMSO.$SystemSettingsTable.getValueFromTable(SystemSettingsPageMSO.valueHeading, 1);
		PageObject.verifyStringData("Value seen",valueExpected,valueActual,false);
		
		QAProof.writeInfo(">>Step 28 to 31");
		
				
	}
	

	

	
	
		
}//class
