package com.mirth.application.version2.mso.pages;

import java.util.Map;

import com.mirth.application.master.PageObject;
import com.mirth.utility.seleniumLib.CheckBoxS;
import com.mirth.utility.seleniumLib.LinkS;
import com.mirth.utility.seleniumLib.PlainTextS;
import com.mirth.utility.seleniumLib.SeleniumElement;
import com.mirth.utility.seleniumLib.TextBoxS;

public final class SSOConfigurationPageMSO extends PageObject{
	
	public static PlainTextS $ActionsBarTaskPaneHeader = new PlainTextS("ActionsBarTaskPaneHeader");
	public static PlainTextS $IdpConfigButton = new PlainTextS("IdpConfigButton");
	public static PlainTextS $IdpConfigHeader = new PlainTextS("IdpConfigHeader");
	public static PlainTextS $IdentifierLabelInIdpConfig = new PlainTextS("IdentifierLabelInIdpConfig");
	public static PlainTextS $IdentifierTextBoxInIdpConfig = new PlainTextS("IdentifierTextBoxInIdpConfig");
	public static PlainTextS $SessionTimeOutPeriodLabelInIdpConfig = new PlainTextS("SessionTimeOutPeriodLabelInIdpConfig");
	public static PlainTextS $SessionTimeOutPeriodTextBoxInIdpConfig = new PlainTextS("SessionTimeOutPeriodTextBoxInIdpConfig");
	public static PlainTextS $ProviderUrlLabelInIdpConfig = new PlainTextS("ProviderUrlLabelInIdpConfig");
	public static PlainTextS $ProviderUrlTextBoxInIdpConfig = new PlainTextS("ProviderUrlTextBoxInIdpConfig");
	public static PlainTextS $NameIdAttributeLabelInIdpConfig = new PlainTextS("NameIdAttributeLabelInIdpConfig");
	public static PlainTextS $NameIdAttributeTextBoxInIdpConfig = new PlainTextS("NameIdAttributeTextBoxInIdpConfig");
	public static PlainTextS $TwoFactorValidationAttemptsLabelInIdpConfig = new PlainTextS("TwoFactorValidationAttemptsLabelInIdpConfig");
	public static PlainTextS $TwoFactorValidationAttemptsTextBoxInIdpConfig = new PlainTextS("TwoFactorValidationAttemptsTextBoxInIdpConfig");
	public static PlainTextS $KeystoreFilePathLabelInIdpConfig = new PlainTextS("KeystoreFilePathLabelInIdpConfig");
	public static PlainTextS $KeystoreFilePathTextBoxInIdpConfig = new PlainTextS("KeystoreFilePathTextBoxInIdpConfig");
	public static PlainTextS $KeystorePasswordLabelInIdpConfig = new PlainTextS("KeystorePasswordLabelInIdpConfig");
	public static PlainTextS $KeystorePasswordTextBoxInIdpConfig = new PlainTextS("KeystorePasswordTextBoxInIdpConfig");
	public static PlainTextS $KeystoreTypeLabelInIdpConfig = new PlainTextS("KeystoreTypeLabelInIdpConfig");
	public static PlainTextS $KeystoreTypeTextBoxInIdpConfig = new PlainTextS("KeystoreTypeTextBoxInIdpConfig");
	public static PlainTextS $PrivateKeyAliasLabelInIdpConfig = new PlainTextS("PrivateKeyAliasLabelInIdpConfig");
	public static PlainTextS $PrivateKeyAliasTextBoxInIdpConfig = new PlainTextS("PrivateKeyAliasTextBoxInIdpConfig");
	public static PlainTextS $PrivateKeyPasswordLabelInIdpConfig = new PlainTextS("PrivateKeyPasswordLabelInIdpConfig");
	public static PlainTextS $PrivateKeyPasswordTextBoxInIdpConfig = new PlainTextS("PrivateKeyPasswordTextBoxInIdpConfig");
	public static PlainTextS $TruststoreFilePathLabelInIdpConfig = new PlainTextS("TruststoreFilePathLabelInIdpConfig");
	public static PlainTextS $TruststoreFilePathTextBoxInIdpConfig = new PlainTextS("TruststoreFilePathTextBoxInIdpConfig");
	public static PlainTextS $TruststorePasswordLabelInIdpConfig = new PlainTextS("TruststorePasswordLabelInIdpConfig");
	public static PlainTextS $TruststorePasswordTextBoxInIdpConfig = new PlainTextS("TruststorePasswordTextBoxInIdpConfig");
	public static PlainTextS $CertificateFingerprintsLabelInIdpConfig = new PlainTextS("CertificateFingerprintsLabelInIdpConfig");
	public static PlainTextS $CertificateFingerprintsTextBoxInIdpConfig = new PlainTextS("CertificateFingerprintsTextBoxInIdpConfig");
	public static PlainTextS $VerifySAMLResponseLabelInIdpConfig = new PlainTextS("VerifySAMLResponseLabelInIdpConfig");
	public static PlainTextS $VerifySAMLResponseCheckBoxInIdpConfig = new PlainTextS("VerifySAMLResponseCheckBoxInIdpConfig");
	public static PlainTextS $AllowOnlyOneSSOSessionperUserLabelInIdpConfig = new PlainTextS("AllowOnlyOneSSOSessionperUserLabelInIdpConfig");
	public static PlainTextS $AllowOnlyOneSSOSessionperUserCheckBoxInIdpConfig = new PlainTextS("AllowOnlyOneSSOSessionperUserCheckBoxInIdpConfig");
	public static PlainTextS $UploadKeystoreLabelInIdpConfig = new PlainTextS("UploadKeystoreLabelInIdpConfig");
	public static PlainTextS $UploadKeystoreBrowseButtonInIdpConfig = new PlainTextS("UploadKeystoreBrowseButtonInIdpConfig");
	public static PlainTextS $ExistingFingerprintsLabelInIdpConfig = new PlainTextS("ExistingFingerprintsLabelInIdpConfig");
	public static PlainTextS $ExistingFingerprintsTextBoxInIdpConfig = new PlainTextS("ExistingFingerprintsTextBoxInIdpConfig");
	public static LinkS $PasswordPolicyLink = new LinkS("PasswordPolicyLink");
	public static TextBoxS  $MaxPasswordAgeTextBox = new TextBoxS("MaxPasswordAgeTextBox");
	public static TextBoxS $MinLengthTextBox = new TextBoxS("MinLengthTextBox");
	public static TextBoxS $MaxLengthTextBox = new TextBoxS("MaxLengthTextBox");
	public static TextBoxS $PasswordExpiryAlertTextBox = new TextBoxS("PasswordExpiryAlertTextBox");
	public static TextBoxS $LockoutDurationTextBox = new TextBoxS("LockoutDurationTextBox");
	public static TextBoxS $PasswordRetriesTextBox = new TextBoxS("PasswordRetriesTextBox");
	public static TextBoxS $PasswordHistorySizeTextBox = new TextBoxS("PasswordHistorySizeTextBox");
	public static CheckBoxS $NotifAdminAccountLockCheckBox = new CheckBoxS("NotifAdminAccountLockCheckBox");
	public static CheckBoxS $WarnUserLastLoginCheckBox = new CheckBoxS("WarnUserLastLoginCheckBox");
	public static CheckBoxS $MixedCasePasswordCheckBox = new CheckBoxS("MixedCasePasswordCheckBox");
	public static CheckBoxS $CaseSensitiveCheckBox = new CheckBoxS("CaseSensitiveCheckBox");
	public static TextBoxS $PasswordNoOfDigitsTextBox = new TextBoxS("PasswordNoOfDigitsTextBox");
	public static CheckBoxS $AllowPasswordReuseCheckBox = new CheckBoxS("AllowPasswordReuseCheckBox");
	public static CheckBoxS $ForgotPasswordCheckBox = new CheckBoxS("ForgotPasswordCheckBox");
	public static CheckBoxS $AllowBlankPasswordCheckBox = new CheckBoxS("AllowBlankPasswordCheckBox");

	
	
	
	//Setting passwordFileds
	public static void setPasswordPolicyFields(Map<SeleniumElement,String> changeableFields,boolean submit) throws Exception{
		for (SeleniumElement selEle: changeableFields.keySet()){
			setData(selEle,changeableFields.get(selEle));
		}
		
		if (changeableFields.size()>=1 && submit) {
			CommonObjectsMSO.$SaveChangesLinkInLeftPane.interact().click();
		}
		
	}
	
	public static void setPasswordPolicyFields(Map<SeleniumElement,String> changeableFields) throws Exception{
		setPasswordPolicyFields(changeableFields,false);
	}
	
	
	
}//class