
package com.mirth.application.version2.mso.testcases.regression;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import com.mirth.application.master.General;
import com.mirth.application.master.PageObject;
import com.mirth.application.master.Testcase;
import com.mirth.application.version2.mso.pages.CommonObjectsMSO;
import com.mirth.application.version2.mso.pages.LoginPageMSO;
import com.mirth.application.version2.mso.pages.NewTemplatePageMSO;
import com.mirth.application.version2.mso.pages.NewUserPageMSO;
import com.mirth.application.version2.mso.pages.TemplatesPageMSO;
import com.mirth.application.version2.mso.pages.UsersPageMSO;
import com.mirth.application.version2.results.pages.CommonObjectsResults;
import com.mirth.application.version2.results.pages.LoginPageResults;
import com.mirth.application.version2.results.pages.PatientsPageResults;
import com.mirth.application.version2.results.pages.SiteSelectorPageResults;
import com.mirth.results.QAProof;
import com.mirth.utility.general.SleepLib;
import com.mirth.utility.seleniumLib.SeleniumElement;
import com.mirth.utility.seleniumLib.SeleniumLib;

public class SignOn_609_AccountLockChangePasswordAndUserNavigation extends Testcase {
	
	public void selectSiteAndCancelPatientFilter() throws Exception {
		QAProof.screenshot(SiteSelectorPageResults.$SiteSelectorTable);
		SiteSelectorPageResults.$SiteSelectorTable.forceProcessTable();
		SiteSelectorPageResults.selectSite();
		PatientsPageResults.$CancelButtonOnAdvancedPatientFilter.interact().click();
	}
	
	@Override
	protected void steps() throws Exception {		
		
		QAProof.writeInfo(">>Step 1 and 2");
		General.openFreshBrowser("main", super.testInventory.browser);
		General.setBrowserWaitTime(10,TimeUnit.SECONDS);
		LoginPageMSO.launch();
		LoginPageMSO.login();
		CommonObjectsMSO.clickUsersTab();
		UsersPageMSO.clickCreateNewUser();		
		Map<SeleniumElement,String> newUserVals = NewUserPageMSO.createNewUserFromTestData();
		NewUserPageMSO.clickOnSaveChangesLink();
		headingController.verifing("NewUserPage of MSO Applicaiton after clicking on Save changes link");
		QAProof.screenshot(NewUserPageMSO.$AccountStatusDropDown);
		
		QAProof.writeInfo(">>Step 3");
		//Open new additional browser to test service provider application
		General.openAndPointToAddtionalBrowser("spBrowser", super.testInventory.browser);
		General.setBrowserWaitTime(10,TimeUnit.SECONDS);
		LoginPageResults.launch();
		LoginPageResults.login(newUserVals.get(NewUserPageMSO.$AccountIDTextValue),newUserVals.get(NewUserPageMSO.$NewPassword));
		this.selectSiteAndCancelPatientFilter();
		SleepLib.sleep2Seconds();
		QAProof.screenshot(CommonObjectsResults.$SignOutButtonOnKanaToolBar);
		headingController.verifing("login of the created user.");
		verify.reportSuccess("Succesfully Logged in to Mirth Results with newly created user: "+newUserVals.get(NewUserPageMSO.$AccountIDTextValue)+" (Account ID).");
		CommonObjectsResults.$SignOutButtonOnKanaToolBar.interact().click();
		
		QAProof.writeInfo(">>Step 4 and 5");
		General.switchToBrowser("main");
		String newPass = "test@1234";
		NewUserPageMSO.changePassword(newPass,false);
		
		QAProof.writeInfo(">>Step 6");
		General.switchToBrowser("spBrowser");
		LoginPageResults.login(newUserVals.get(NewUserPageMSO.$AccountIDTextValue),newPass);
		QAProof.screenshot(LoginPageResults.$LoginErrorMessageText);
		String expectedErrorMessage = PageObject.td(LoginPageResults.class,"_WrongCredentialsErrorMessage");
		expectedErrorMessage = expectedErrorMessage.replace("<ActualAccountID>", newUserVals.get(NewUserPageMSO.$AccountIDTextBox));
		String errorMessageActual = PageObject.getData(LoginPageResults.$LoginErrorMessageText);
		headingController.verifing("Login with wrong password(i.e. password which was entered by cancelled.)");
		boolean loginResult = verify.withReport(LoginPageResults.$LoginErrorMessageText.get()).isSame(expectedErrorMessage, errorMessageActual, false);
		if (loginResult){
			verify.reportSuccess("User NOT able to login with wrong password as expected");
		}else{
			verify.reportIssue("Login with incorrect password user did NOT behave as expected");
		}
		LoginPageResults.login(newUserVals.get(NewUserPageMSO.$AccountIDTextValue),newUserVals.get(NewUserPageMSO.$NewPassword));
		this.selectSiteAndCancelPatientFilter();
		headingController.verifing("login of the created user.");
		verify.reportSuccess("Succesfully Logged in to Mirth Results with old password: "+newUserVals.get(NewUserPageMSO.$AccountIDTextValue)+" (Account ID).");
		CommonObjectsResults.$SignOutButtonOnKanaToolBar.interact().click();
		
		QAProof.writeInfo(">>Step 7 and 8");
		General.switchToBrowser("main");
		NewUserPageMSO.changePassword(newPass,true);
		String alertExpected="Password changed successfully";
		QAProof.screenshot(CommonObjectsMSO.$InformationAlertAtTop);
		String alertSeen = CommonObjectsMSO.$InformationAlertAtTop.interact().getText();
		verify.withReport("Alert After Password Change").isSame(alertExpected, alertSeen, false);
		
		newUserVals.put(NewUserPageMSO.$NewPassword, newPass);
		newUserVals.put(NewUserPageMSO.$ConfirmNewPassword, newPass);
		
		General.switchToBrowser("spBrowser");
		LoginPageResults.login(newUserVals.get(NewUserPageMSO.$AccountIDTextValue),newPass);
		this.selectSiteAndCancelPatientFilter();
		QAProof.screenshot(CommonObjectsResults.$SignOutButtonOnKanaToolBar);
		headingController.verifing("login of the created user.");
		verify.reportSuccess("Succesfully Logged in to Mirth Results with newly created password: " + newUserVals.get(NewUserPageMSO.$AccountIDTextValue)+" (Account ID).");
		CommonObjectsResults.$SignOutButtonOnKanaToolBar.interact().click();
		
		QAProof.writeInfo(">>Step 9 and 10");
		//Account lock and check
		General.switchToBrowser("main");
		NewUserPageMSO.$AccountStatusDropDown.interact().setDropdownByText("Locked");
		CommonObjectsMSO.$SaveChangesLinkInLeftPane.interact().click();
		QAProof.screenshot(NewUserPageMSO.$AccountStatusDropDown);
		
		General.switchToBrowser("spBrowser");
		LoginPageResults.login(newUserVals.get(NewUserPageMSO.$AccountIDTextValue),newUserVals.get(NewUserPageMSO.$NewPassword));
		QAProof.screenshot(LoginPageResults.$LoginErrorMessageText);
		expectedErrorMessage = PageObject.td(LoginPageResults.class,"_AccountLockedErrorMessage");
		expectedErrorMessage = expectedErrorMessage.replace("<ActualAccountID>", newUserVals.get(NewUserPageMSO.$AccountIDTextValue));
		errorMessageActual = PageObject.getData(LoginPageResults.$LoginErrorMessageText);
		headingController.verifing("Login with Locked account");
		
		loginResult = verify.withReport(LoginPageResults.$LoginErrorMessageText.get()).isSame(expectedErrorMessage, errorMessageActual, false);
		if (loginResult){
			verify.reportSuccess("User NOT able to login with Locked account as expected");
		}else{
			verify.reportIssue("Login of locked user did NOT behave as expected");
		}

		QAProof.writeInfo(">>Step 11 and 12");
		//Account Unlock and check
		General.switchToBrowser("main");
		NewUserPageMSO.$AccountStatusDropDown.interact().setDropdownByText("Active");
		CommonObjectsMSO.$SaveChangesLinkInLeftPane.interact().click();
		QAProof.screenshot(NewUserPageMSO.$AccountStatusDropDown);
		
		General.switchToBrowser("spBrowser");
		LoginPageResults.login(newUserVals.get(NewUserPageMSO.$AccountIDTextValue),newUserVals.get(NewUserPageMSO.$NewPassword));
		this.selectSiteAndCancelPatientFilter();
		QAProof.screenshot(CommonObjectsResults.$SignOutButtonOnKanaToolBar);
		headingController.verifing("login of the created user.");
		verify.reportSuccess("Succesfully Logged in to Mirth Results after unlocking user: "+newUserVals.get(NewUserPageMSO.$AccountIDTextValue)+" (Account ID).");
		CommonObjectsResults.$SignOutButtonOnKanaToolBar.interact().click();
		
		QAProof.writeInfo(">>Step 13");
		General.switchToBrowser("main");
		//create template
		
		CommonObjectsMSO.$TemplatesTab.interact().click();
		TemplatesPageMSO.$CreateUserTemplateLinkInActionsBar.interact().click();
		Map<SeleniumElement,String> newTemplateVals = NewTemplatePageMSO.createNewTemplateFromTestData();
		SeleniumLib.waitForVisiblity(CommonObjectsMSO.$SaveChangesLinkInLeftPane);
		CommonObjectsMSO.$SaveChangesLinkInLeftPane.interact().click();
		QAProof.screenshot(NewTemplatePageMSO.$LabelTextBox);
		NewTemplatePageMSO.verifyNewTemplateValues(newTemplateVals);
		QAProof.writeText("Succesfully created a new Template.");	
		
		QAProof.writeInfo(">>Step 14");
		//Creating 2 new users with that template
		
		CommonObjectsMSO.$UsersTab.interact().click();
		UsersPageMSO.$CreateUserLinkInUserActions.interact().click();
		
		List<SeleniumElement> requiredUserFields = new ArrayList<SeleniumElement>();
		requiredUserFields.add(NewUserPageMSO.$AccountStatusDropDown);
		requiredUserFields.add(NewUserPageMSO.$Name);
		requiredUserFields.add(NewUserPageMSO.$Email);
		requiredUserFields.add(NewUserPageMSO.$VoiceContact);
		requiredUserFields.add(NewUserPageMSO.$FaxContact);
		requiredUserFields.add(NewUserPageMSO.$AddressLine1);
		requiredUserFields.add(NewUserPageMSO.$AddressLine2);
		requiredUserFields.add(NewUserPageMSO.$City);
		requiredUserFields.add(NewUserPageMSO.$State);
		requiredUserFields.add(NewUserPageMSO.$Postal);
		requiredUserFields.add(NewUserPageMSO.$Country);
		requiredUserFields.add(NewUserPageMSO.$NewPassword);
		requiredUserFields.add(NewUserPageMSO.$ConfirmNewPassword);
		
		
		Map<Integer,Map<SeleniumElement,String>> usersVal = new HashMap<Integer,Map<SeleniumElement,String>>();
		
		for (int i=1;i<=2;i++){
			
			NewUserPageMSO.uncheckAllUserCheckBoxs();
			NewUserPageMSO.deleteAllSecurityRolesIfPresent();
						
			Map<SeleniumElement,String> tempUserVal = NewUserPageMSO.createNewUserFromTestData();	
			SeleniumLib.waitForVisiblity(NewUserPageMSO.$UserEntityTemplateNameDropDown);
			tempUserVal.put(NewUserPageMSO.$UserEntityTemplateNameDropDown, PageObject.setData(NewUserPageMSO.$UserEntityTemplateNameDropDown,newTemplateVals.get(NewTemplatePageMSO.$LabelTextBox)));
			SeleniumLib.waitForVisiblity(NewUserPageMSO.$AccountIDTextBox);
			tempUserVal.put(NewUserPageMSO.$AccountIDTextBox, PageObject.setData(NewUserPageMSO.$AccountIDTextBox));		
			tempUserVal.putAll(NewUserPageMSO.createNewUserWithSelectedFields(requiredUserFields));
			tempUserVal.put(NewUserPageMSO.$SelectListInAddSecurityRolePopup, newTemplateVals.get(NewTemplatePageMSO.$SelectListInSecurityRolesPopup));
			SeleniumLib.waitForVisiblity(NewUserPageMSO.$SecurityRolesTable);
			CommonObjectsMSO.$SaveChangesLinkInLeftPane.interact().click();
			QAProof.screenshot(CommonObjectsMSO.$BackToListLinkInLeftPane);
			QAProof.writeText("Finished creating user with Template: " + tempUserVal.get(NewTemplatePageMSO.$LabelTextBox));
			CommonObjectsMSO.$BackToListLinkInLeftPane.interact().click();
			UsersPageMSO.$CreateUserLinkInUserActions.interact().click();
			usersVal.put(i, tempUserVal);			
		}
		
		QAProof.writeInfo(">>Step 15");
		//Create 2 new normal users with just Administrator roles.
		
		for (int i=3;i<=4;i++){
			Map<SeleniumElement,String> tempUserVal = NewUserPageMSO.createNewUserFromTestData();
			SeleniumLib.waitForVisiblity(NewUserPageMSO.$SecurityRolesTable);
			CommonObjectsMSO.$SaveChangesLinkInLeftPane.interact().click();
			QAProof.screenshot(CommonObjectsMSO.$BackToListLinkInLeftPane);
			CommonObjectsMSO.$BackToListLinkInLeftPane.interact().click();
			UsersPageMSO.$CreateUserLinkInUserActions.interact().click();
			usersVal.put(i, tempUserVal);			
		}		
		
		QAProof.writeText("Finished creating 2 users with Administrator role");
		
		Map<SeleniumElement,String> securityRoleMap = new HashMap<SeleniumElement,String>();
		securityRoleMap.put(NewUserPageMSO.$SelectListInAddSecurityRolePopup, "System");
		
		Map<SeleniumElement,String> tempUserVal = NewUserPageMSO.createNewUserFromTestDataWithCustomReplace(securityRoleMap);
		SeleniumLib.waitForVisiblity(NewUserPageMSO.$SecurityRolesTable);
		CommonObjectsMSO.$SaveChangesLinkInLeftPane.interact().click();
		QAProof.screenshot(CommonObjectsMSO.$BackToListLinkInLeftPane);
		CommonObjectsMSO.$BackToListLinkInLeftPane.interact().click();
		usersVal.put(5, tempUserVal);
		
		QAProof.writeText("Finished creating 1 user with System role");
		
		QAProof.writeInfo(">>Step 16");
		//lock all users of the template
		UsersPageMSO.$LockORUnlockUserAccountsLink.interact().click();
		UsersPageMSO.$LockRadioButtonInLockUnlockPopup.interact().click();
		UsersPageMSO.$AllUsersOfTemplateRadioButtonInLockUnlockPopup.interact().click();
		PageObject.setData(UsersPageMSO.$SelectTemplateListInLockUnlockPopup, newTemplateVals.get(NewTemplatePageMSO.$IdentifierTextBox));
		QAProof.screenshot(UsersPageMSO.$SubmitButtonInLockUnlockPopup);
		UsersPageMSO.$SubmitButtonInLockUnlockPopup.interact().click();
		SleepLib.sleep2Seconds();
		
		//verifying the users
		usersVal.get(1).put(NewUserPageMSO.$AccountStatusDropDown,"Locked");
		usersVal.get(2).put(NewUserPageMSO.$AccountStatusDropDown,"Locked");
		//SleepLib.sleep2Seconds();
		SeleniumLib.waitForVisiblity(CommonObjectsMSO.$SearchUsersTextBox);
		
		for (int i=1;i<=2;i++){
			CommonObjectsMSO.searchTextBoxFor(usersVal.get(i).get(NewUserPageMSO.$AccountIDTextBox));
			UsersPageMSO.verifyUserDataAsPerCreation(usersVal.get(i));
			UsersPageMSO.$UsersTable.clickFirstRowOfUsersTable(UsersPageMSO.usersTableAccountID);
			PageObject.verifyStringData(NewUserPageMSO.$AccountStatusDropDown,"Locked",false);
			CommonObjectsMSO.$BackToListLinkInLeftPane.interact().click();
		}
		QAProof.writeText("Verified that the correct users have been locked as per tempalate");
		QAProof.writeText("Now, verifying that the other users are not locked.");
		
		for (int i=4;i<=5;i++){
			CommonObjectsMSO.searchTextBoxFor(usersVal.get(i).get(NewUserPageMSO.$AccountIDTextBox));
			UsersPageMSO.verifyUserDataAsPerCreation(usersVal.get(i));
			UsersPageMSO.$UsersTable.clickFirstRowOfUsersTable(UsersPageMSO.usersTableAccountID);
			PageObject.verifyStringData(NewUserPageMSO.$AccountStatusDropDown,"Active",false);
			CommonObjectsMSO.$BackToListLinkInLeftPane.interact().click();
		}
		QAProof.writeText("Verified that the other users apart from the ones associated with locked template is NOT locked.");		

		QAProof.writeInfo(">>Step 17");
		QAProof.writeText("Now, Lock the users based on security role.");
		//lock all users of the Administrator role
		UsersPageMSO.$LockORUnlockUserAccountsLink.interact().click();
		UsersPageMSO.$LockRadioButtonInLockUnlockPopup.interact().click();
		UsersPageMSO.$AllUsersOfRolesRadioButtonInLockUnlockPopup.interact().click();
		PageObject.setData(UsersPageMSO.$SelectRoelsListInLockUnlockPopup, "Administrator");
		QAProof.screenshot(UsersPageMSO.$SubmitButtonInLockUnlockPopup);
		UsersPageMSO.$SubmitButtonInLockUnlockPopup.interact().click();
		SleepLib.sleep2Seconds();
		//Verifying Lock
		usersVal.get(3).put(NewUserPageMSO.$AccountStatusDropDown,"Locked");
		usersVal.get(4).put(NewUserPageMSO.$AccountStatusDropDown,"Locked");
	
		
		for (int i=3;i<=4;i++){
			SeleniumLib.waitForVisiblity(CommonObjectsMSO.$SearchUsersTextBox);
			CommonObjectsMSO.searchTextBoxFor(usersVal.get(i).get(NewUserPageMSO.$AccountIDTextBox));
			UsersPageMSO.verifyUserDataAsPerCreation(usersVal.get(i));
			UsersPageMSO.$UsersTable.clickFirstRowOfUsersTable(UsersPageMSO.usersTableAccountID);
			PageObject.verifyStringData(NewUserPageMSO.$AccountStatusDropDown,"Locked",false);
			CommonObjectsMSO.$BackToListLinkInLeftPane.interact().click();
		}
		QAProof.writeText("Verified that the correct users have been locked as per roles");
		QAProof.writeText("Now, verifying that the other user is not locked.");
		CommonObjectsMSO.searchTextBoxFor(usersVal.get(5).get(NewUserPageMSO.$AccountIDTextBox));
		UsersPageMSO.verifyUserDataAsPerCreation(usersVal.get(5));
		UsersPageMSO.$UsersTable.clickFirstRowOfUsersTable(UsersPageMSO.usersTableAccountID);
		PageObject.verifyStringData(NewUserPageMSO.$AccountStatusDropDown,"Active",false);
		CommonObjectsMSO.$BackToListLinkInLeftPane.interact().click();
		QAProof.writeText("Verified that the other users apart from the ones associated with locked role is NOT locked.");
		
		QAProof.writeInfo(">>Step 18");
		QAProof.writeText("Now, manually unlocking user 3[AccountID: "+ usersVal.get(3).get(NewUserPageMSO.$AccountIDTextBox) +"] having admin role.");
		CommonObjectsMSO.searchTextBoxFor(usersVal.get(3).get(NewUserPageMSO.$AccountIDTextBox));
		UsersPageMSO.$UsersTable.clickFirstRowOfUsersTable(UsersPageMSO.usersTableAccountID);
		PageObject.setData(NewUserPageMSO.$AccountStatusDropDown,"Active");
		CommonObjectsMSO.$SaveChangesLinkInLeftPane.interact().click();
		QAProof.screenshot(CommonObjectsMSO.$BackToListLinkInLeftPane);
		CommonObjectsMSO.$BackToListLinkInLeftPane.interact().click();		
		usersVal.get(3).put(NewUserPageMSO.$AccountStatusDropDown,"Active");
		
		//Lock all users except admin
		UsersPageMSO.$LockORUnlockUserAccountsLink.interact().click();
		UsersPageMSO.$LockRadioButtonInLockUnlockPopup.interact().click();
		UsersPageMSO.$AllUsersExceptAdminRadioButtonInLockUnlockPopup.interact().click();
		QAProof.screenshot(UsersPageMSO.$SubmitButtonInLockUnlockPopup);
		UsersPageMSO.$SubmitButtonInLockUnlockPopup.interact().click();
		SleepLib.sleep3Seconds();		
		usersVal.get(5).put(NewUserPageMSO.$AccountStatusDropDown,"Locked");
		
		CommonObjectsMSO.searchTextBoxFor(usersVal.get(5).get(NewUserPageMSO.$AccountIDTextBox));
		UsersPageMSO.verifyUserDataAsPerCreation(usersVal.get(5));
		UsersPageMSO.$UsersTable.clickFirstRowOfUsersTable(UsersPageMSO.usersTableAccountID);
		PageObject.verifyStringData(NewUserPageMSO.$AccountStatusDropDown,"Locked",false);
		CommonObjectsMSO.$BackToListLinkInLeftPane.interact().click();
		QAProof.writeText("Verified that the Non administrator users are blocked.");
		
		QAProof.writeText("Now, verifying that the other user who is admin user is Not locked.");
		CommonObjectsMSO.searchTextBoxFor(usersVal.get(3).get(NewUserPageMSO.$AccountIDTextBox));
		UsersPageMSO.verifyUserDataAsPerCreation(usersVal.get(3));
		UsersPageMSO.$UsersTable.clickFirstRowOfUsersTable(UsersPageMSO.usersTableAccountID);
		PageObject.verifyStringData(NewUserPageMSO.$AccountStatusDropDown,"Active",false);
		CommonObjectsMSO.$BackToListLinkInLeftPane.interact().click();
		QAProof.writeText("Verified that the other users apart from the ones associated with locked role is NOT locked.");
		
		QAProof.writeInfo(">>Step 19");
		General.switchToBrowser("spBrowser");
		
		for(int i=2;i<=5;i++ ){
			
			if (i==3) {continue;} //As we have unlocked user 3 manually
			
			LoginPageResults.login(usersVal.get(i).get(NewUserPageMSO.$AccountIDTextBox),newUserVals.get(NewUserPageMSO.$NewPassword));
			QAProof.screenshot(LoginPageResults.$LoginErrorMessageText);
			expectedErrorMessage = PageObject.td(LoginPageResults.class,"_AccountLockedErrorMessage");
			expectedErrorMessage = expectedErrorMessage.replace("<ActualAccountID>", usersVal.get(i).get(NewUserPageMSO.$AccountIDTextBox));
			errorMessageActual = PageObject.getData(LoginPageResults.$LoginErrorMessageText);
			headingController.verifing("Login with Locked account for user: AccountID["+usersVal.get(i).get(NewUserPageMSO.$AccountIDTextBox)+"]");
			loginResult = verify.withReport(LoginPageResults.$LoginErrorMessageText.get()).isSame(expectedErrorMessage, errorMessageActual, false);
			if (loginResult){
				verify.reportSuccess("User NOT able to login with Locked account as expected");
			}else{
				verify.reportIssue("Login of locked user did NOT behave as expected");
			}
		}//for(int i=2;i<=5;i++ )
		
		
		QAProof.writeInfo(">>Step 20");		
		QAProof.writeInfo("Test case End");
		
	}
	

	

	
	
		
}//class
