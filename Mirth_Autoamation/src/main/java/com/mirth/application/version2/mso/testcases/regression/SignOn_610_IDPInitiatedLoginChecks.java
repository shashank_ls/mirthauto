package com.mirth.application.version2.mso.testcases.regression;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;

import com.mirth.application.master.General;
import com.mirth.application.master.PageObject;
import com.mirth.application.master.Testcase;
import com.mirth.application.version2.match.pages.CommonObjectsMatch;
import com.mirth.application.version2.match.pages.LoginPageMatch;
import com.mirth.application.version2.mso.pages.ApplicationPageMSO;
import com.mirth.application.version2.mso.pages.CommonObjectsMSO;
import com.mirth.application.version2.mso.pages.LoginPageMSO;
import com.mirth.application.version2.mso.pages.NewEditApplicaionMSO;
import com.mirth.application.version2.results.pages.CommonObjectsResults;
import com.mirth.application.version2.results.pages.LoginPageResults;
import com.mirth.controller.Verification;
import com.mirth.exceptions.AppNotPresentException;
import com.mirth.exceptions.ClassPageMappingNotFoundException;
import com.mirth.properties.ApplicationConfig;
import com.mirth.results.QAProof;


public class SignOn_610_IDPInitiatedLoginChecks extends Testcase{

	@Override
	protected void steps() throws Exception {
		
		General.openFreshBrowser("main", super.testInventory.browser);
		General.setBrowserWaitTime(10, TimeUnit.SECONDS);
		LoginPageMSO.launch();
		LoginPageMSO.login();
		QAProof.writeInfo("Steps 1 to 4");
		CommonObjectsMSO.$ApplicaitonTab.interact().click();
		setIDPInitiatedLogin(PageObject.td(ApplicationPageMSO.class,"_resultsIdentifier"),false);

		QAProof.writeInfo("Step 5");
		setIDPInitiatedLogin(PageObject.td(ApplicationPageMSO.class,"_matchIdentifier"),false);
			

		QAProof.writeInfo("Steps 6 ");
		//Open new additional browser to test service provider application
		General.openAndPointToAddtionalBrowser("spBrowser", super.testInventory.browser);
		General.setBrowserWaitTime(10,TimeUnit.SECONDS);
		LoginPageResults.launch();
		LoginPageResults.login();
		
		verifyNonIDPUrl(verify, ApplicationConfig.getValue("Match"));
		CommonObjectsResults.$MirthMatchInKanaToolBar.interact().click();
				
		QAProof.writeInfo("Steps 7 and 8");
		verifyNonIDPUrl(verify, ApplicationConfig.getValue("Results"));
		CommonObjectsMatch.$SignOutButtonOnKanaToolBar.interact().click();
		 
		QAProof.writeInfo("Steps 9");
		General.switchToBrowser("main");
		setIDPInitiatedLogin(PageObject.td(ApplicationPageMSO.class,"_resultsIdentifier"),true);	
		
		QAProof.writeInfo("Steps 10 to 13");
		General.switchToBrowser("spBrowser");
		LoginPageMatch.login();
		
		verifyIDPUrl(verify, ApplicationConfig.getValue("Results"));
		CommonObjectsMatch.$MirthResultsInKanaToolBar.interact().click();
		
		verifyNonIDPUrl(verify, ApplicationConfig.getValue("Match"));
		CommonObjectsResults.$MirthMatchInKanaToolBar.interact().click();
		CommonObjectsMatch.$SignOutButtonOnKanaToolBar.interact().click();
		
		QAProof.writeInfo("Steps 14");
		General.switchToBrowser("main");
		setIDPInitiatedLogin(PageObject.td(ApplicationPageMSO.class,"_matchIdentifier"),true);
		
		QAProof.writeInfo("Steps 15 to 18");
		General.switchToBrowser("spBrowser");
		LoginPageMatch.login();
		verifyIDPUrl(verify, ApplicationConfig.getValue("Results"));
		CommonObjectsMatch.$MirthResultsInKanaToolBar.interact().click();
		verifyIDPUrl(verify, ApplicationConfig.getValue("Match"));
		CommonObjectsResults.$MirthMatchInKanaToolBar.interact().click();
		CommonObjectsMatch.$SignOutButtonOnKanaToolBar.interact().click();
		
		General.closeAndDestroyBrowser("spBrowser");
		
		General.switchToBrowser("main");
		CommonObjectsMSO.$LogoutLink.interact().click();
		General.closeAndDestroyBrowser("main");
		
		QAProof.writeInfo("Test case END");
		
		
	}
	
	public static void verifyNonIDPUrl(Verification verify,String appName) throws InvalidFormatException, IOException, ClassPageMappingNotFoundException, AppNotPresentException, Exception{
		QAProof.screenshot();	
		if (appName.equals(ApplicationConfig.getValue("Results"))){
			String resultsReferenceActual = CommonObjectsMatch.$MirthResultsInKanaToolBar.interact().webElement().getAttribute("href");
			if (resultsReferenceActual.toLowerCase().contains("idpinitlogin?t=")){
				verify.reportIssue("(Results)IDP initiated URL is seen when it is NOT supposed to be seen\nURL seen: "+resultsReferenceActual);
			}
			else{
				verify.reportSuccess("(Results)IDP initiated URL is NOT seen as expected\nURL seen: "+resultsReferenceActual);
			}
		}
		
		else {//assume match
			String matchReferenceActual = CommonObjectsResults.$MirthMatchInKanaToolBar.interact().webElement().getAttribute("href");
					
			if (matchReferenceActual.toLowerCase().contains("idpinitlogin?t=")){
				verify.reportIssue("(Match)IDP initiated URL is seen when it is NOT supposed to be seen\nURL seen: "+matchReferenceActual);
			}
			else{
				verify.reportSuccess("(Match)IDP initiated URL is NOT seen as expected\nURL seen: "+matchReferenceActual);
			}
		}	

	}//public static void verifyIDPUrl(Verification verify,String appName) 
	
	public static void verifyIDPUrl(Verification verify,String appName) throws InvalidFormatException, IOException, ClassPageMappingNotFoundException, AppNotPresentException, Exception{
		QAProof.screenshot();
		if (appName.equals(ApplicationConfig.getValue("Results"))){
			String resultsReferenceActual = CommonObjectsMatch.$MirthResultsInKanaToolBar.interact().webElement().getAttribute("href");
			if (resultsReferenceActual.toLowerCase().contains("idpinitlogin?t=")){
				verify.reportSuccess("(Results)IDP initiated URL is seen as expected\nURL seen: "+resultsReferenceActual);
			}
			else{
				verify.reportIssue("(Results)IDP initiated URL is NOT seen\nURL seen: "+resultsReferenceActual);
			}
		}
		
		else {//assume match
			String matchReferenceActual = CommonObjectsResults.$MirthMatchInKanaToolBar.interact().webElement().getAttribute("href");
			if (matchReferenceActual.toLowerCase().contains("idpinitlogin?t=")){
				verify.reportSuccess("(Match)IDP initiated URL is seen as expected\nURL seen: "+matchReferenceActual);
			}
			else{
				verify.reportIssue("(Match)IDP initiated URL is NOT seen\nURL seen: "+matchReferenceActual);
			}
		}	

	}//public static void verifyIDPUrl(Verification verify,String appName) 
	
	
	
	public static void setIDPInitiatedLogin(String applicationIdentifier,boolean idpCheckBox) throws Exception{
		ApplicationPageMSO.$ApplicationTable.forceProcessTable();
		int appRow = ApplicationPageMSO.$ApplicationTable
							.findRowHavingText(applicationIdentifier,ApplicationPageMSO.identifierHeading,false,false);
		
		ApplicationPageMSO.$ApplicationTable.clickRow(ApplicationPageMSO.identifierHeading, appRow);
		NewEditApplicaionMSO.$ApplicationInstancesTable.forceProcessTable();
		NewEditApplicaionMSO.$ApplicationInstancesTable.clickFirstRowOfUsersTable(NewEditApplicaionMSO.labelHeading);
		
		if (idpCheckBox){
			NewEditApplicaionMSO.$IDPInitiatedLoginCheckBox.interact().setCheckBox_ON();	
		}
		else{
			NewEditApplicaionMSO.$IDPInitiatedLoginCheckBox.interact().setCheckBox_OFF();
		}		
		
		QAProof.screenshot(CommonObjectsMSO.$SubmitButtonOnPopup);
		CommonObjectsMSO.$SubmitButtonOnPopup.interact().click();
		CommonObjectsMSO.$SaveChangesLinkInLeftPane.interact().click();
		CommonObjectsMSO.$BackToListLinkInLeftPane.interact().click();
		
	}
	

}
