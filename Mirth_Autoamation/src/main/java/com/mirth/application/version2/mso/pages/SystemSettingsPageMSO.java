package com.mirth.application.version2.mso.pages;

import java.io.File;
import java.io.IOException;

import com.mirth.application.master.PageObject;
import com.mirth.controller.Interact;
import com.mirth.exceptions.AppNotPresentException;
import com.mirth.exceptions.ClassPageMappingNotFoundException;
import com.mirth.exceptions.InvalidCheckBoxValueException;
import com.mirth.exceptions.InvalidRadioButtonValueException;
import com.mirth.exceptions.TestDataNotPresentException;
import com.mirth.properties.ApplicationConfig;
import com.mirth.utility.general.MirthLib;
import com.mirth.utility.general.StaticLib;
import com.mirth.utility.seleniumLib.ButtonS;
import com.mirth.utility.seleniumLib.TableS;
import com.mirth.utility.seleniumLib.TextBoxS;

public final class SystemSettingsPageMSO extends PageObject{
	
	public static final TableS $SystemSettingsTable =   new TableS ("SystemSettingsTable"); 
	public static final TextBoxS $ValueTextBox = new TextBoxS("ValueTextBox");
	public static final ButtonS $BrowseButtonForSetting = new ButtonS("BrowseButtonForSetting");
	
	public static final String settingHeading = "Setting";
	public static final String valueHeading = "Value";
	
	public static boolean isDisclaimerLoaded = false;
	public static String customDisclaimerPath = "";
	
	
	public static String updateLocalHostInValueWithCurrentServer() throws Exception{
		return updateLocalHostInValueWith(MirthLib.getServerNameOrIP());
	}
	
	public static String updateLocalHostInValueWith(String server) throws IOException, ClassPageMappingNotFoundException, AppNotPresentException, Exception {
		String currentValue = $ValueTextBox.interact().getText();
		if (currentValue.contains("localhost:8888")) {
			currentValue = currentValue.replace("localhost:8888", server);
		}
		
		else if (currentValue.contains("localhost:3020")) {
			currentValue = currentValue.replace("localhost:3020", server);
		}
		
		else {
			return currentValue;
		}
		
		return currentValue;
		
	}
	
	public static void enterBrowsePath(String filePath) throws Exception{
		PageObject.setData($BrowseButtonForSetting,filePath);
	}
	
	public static synchronized void loadDisclaimers() throws IllegalAccessException, IllegalArgumentException, IOException, InvalidRadioButtonValueException, InvalidCheckBoxValueException, TestDataNotPresentException, ClassPageMappingNotFoundException, AppNotPresentException{
		
		if (!isDisclaimerLoaded){
			
			customDisclaimerPath = StaticLib.appendFolderSeparatorIfNotPresent(PageObject.getTestDataFolderNameForApp(ApplicationConfig.getValue("MSO")));
			customDisclaimerPath+= Interact.getRawSetterDataPair(td("_customDisclaimer1"))[1];
		
			//this is to incorporate windows and mac
			customDisclaimerPath = new File(customDisclaimerPath).getAbsolutePath();
		
			isDisclaimerLoaded = true;
							
		}
		
	}
	
	public static void uploadDisclaimer(String filePath,boolean shouldSubmit) throws Exception{
		enterBrowsePath(filePath);
		
		if (shouldSubmit){
			CommonObjectsMSO.$SaveChangesLinkInLeftPane.interact().click();
		}
	}

	
	
}//class