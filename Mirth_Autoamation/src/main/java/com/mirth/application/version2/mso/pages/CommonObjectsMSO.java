package com.mirth.application.version2.mso.pages;

import com.mirth.application.master.PageObject;
import com.mirth.utility.seleniumLib.ButtonS;
import com.mirth.utility.seleniumLib.CheckBoxS;
import com.mirth.utility.seleniumLib.LinkS;
import com.mirth.utility.seleniumLib.PlainTextS;
import com.mirth.utility.seleniumLib.TextBoxS;

public class CommonObjectsMSO extends PageObject {
	
	public static LinkS $UsersTab = new LinkS("UsersTab");
	public static LinkS $ApplicaitonTab = new LinkS("ApplicaitonTab");
	public static LinkS $ProvisioningErrorsTab = new LinkS("ProvisioningErrorsTab");
		
	public static LinkS $TemplatesTab = new LinkS("TemplatesTab");
	public static LinkS $RolesTab = new LinkS("RolesTab");
	public static LinkS $SSOConfigurationTab = new LinkS("SSOConfigurationTab");
	public static LinkS $LogoutLink = new LinkS("LogoutLink");
	public static TextBoxS $SearchUsersTextBox = new TextBoxS("SearchUsersTextBox");
	public static ButtonS $QuickSearchMagnifyingButton = new ButtonS("QuickSearchMagnifyingButton");
	public static PlainTextS $InformationAlertAtTop = new PlainTextS("InformationAlertAtTop");
	public static LinkS $AdministrationTab = new LinkS("AdministrationTab");
	public static LinkS $SettingsInAdministrationTab = new LinkS("SettingsInAdministrationTab");
	public static LinkS $CertificatesTab = new LinkS("CertificatesTab");
	public static LinkS $TrushAuthoritiesTab = new LinkS("TrushAuthoritiesTab");
	public static LinkS $ActiveSessionsTab = new LinkS("ActiveSessionsTab");
	
	public static LinkS $BackToListLinkInLeftPane = new LinkS("BackToListLinkInLeftPane");
	public static LinkS $SaveChangesLinkInLeftPane = new LinkS("SaveChangesLinkInLeftPane");
	public static LinkS $EditLinkInLeftPane = new LinkS("EditLinkInLeftPane");
	public static LinkS $DeleteLinkInLeftPane = new LinkS("DeleteLinkInLeftPane");
	public static LinkS $ViewLinkInLeftPage = new LinkS("ViewLinkInLeftPage");
	public static LinkS $CreateLinkInLeftPane = new LinkS("CreateLinkInLeftPane");
	public static LinkS $RefreshLinkInLeftPane = new LinkS("RefreshLinkInLeftPane");
	
	//SSOConfigurationTab
	public static TextBoxS $SSOConfigurationSessionTimeoutPeriod = new TextBoxS("SSOConfigurationSessionTimeoutPeriod");
	public static CheckBoxS $SSOConfigurationAllowOnlyOneSSOSessionCheckbox = new CheckBoxS("SSOConfigurationAllowOnlyOneSSOSessionCheckbox");
		
	
	//AboutPopup
	public static LinkS $AboutLinkInRightBottomCorner = new LinkS("AboutLinkInRightBottomCorner");
	public static LinkS $NoticesAndDisclaimersLinkInAboutPopup = new LinkS("NoticesAndDisclaimersLinkInAboutPopup");
	public static LinkS $PrivacyStatementLinkInAboutPopup = new LinkS("PrivacyStatementLinkInAboutPopup");
	public static PlainTextS $DisclaimerTextInDisclaimerPopup = new PlainTextS("DisclaimerTextInDisclaimerPopup");
	public static PlainTextS $PrivacyStatementTextInPrivacyStatementPopup = new PlainTextS("PrivacyStatementTextInPrivacyStatementPopup");
	public static ButtonS $CloseButtonInAboutPopup = new ButtonS("CloseButtonInAboutPopup");
	public static ButtonS $CloseButtonInDisclaimerPopup = new ButtonS("CloseButtonInDisclaimerPopup");
	public static ButtonS $CloseButtonInPrivacyStatementPopup = new ButtonS("CloseButtonInPrivacyStatementPopup");
	
	//Advanced Search
	public static ButtonS $AdvancedFilterLink = new ButtonS("AdvancedFilterLink");
	public static ButtonS $AdvancedSearchSubmit = new ButtonS("AdvancedSearchSubmit");
	public static CheckBoxS $AdvancedSearchCheckboxIncludeAdmins = new CheckBoxS("AdvancedSearchCheckboxIncludeAdmins");
	public static CheckBoxS $AdvancedSearchCheckboxIncludeSystem = new CheckBoxS("AdvancedSearchCheckboxIncludeSystem");
	public static CheckBoxS $AdvancedSearchCheckboxIncludeLocked = new CheckBoxS("AdvancedSearchCheckboxIncludeLocked");
	public static CheckBoxS $AdvancedSearchCheckboxIncludeDeactivated = new CheckBoxS("AdvancedSearchCheckboxIncludeDeactivated");
	public static CheckBoxS $AdvancedSearchCheckboxIncludeExternalUsers = new CheckBoxS("AdvancedSearchCheckboxIncludeExternalUsers");
	public static TextBoxS $AdvancedSearchName = new TextBoxS("AdvancedSearchName");
	public static TextBoxS $AdvancedSearchAccountId = new TextBoxS("AdvancedSearchAccountId");
	public static TextBoxS $AdvancedSearchEmail = new TextBoxS("AdvancedSearchEmail");	
	public static TextBoxS $AdvancedSearchSecurityRole = new TextBoxS("AdvancedSearchSecurityRole");
	
	public static final ButtonS $SubmitButtonOnPopup = new ButtonS("SubmitButtonOnPopup");
	public static final ButtonS $CancelButtonOnPopup = new ButtonS("CancelButtonOnPopup");
	
	
	public static void clickUsersTab() throws Exception {
			interactWith($UsersTab).click();
	}
	
	public static void clickActiveSessionsTab() throws Exception {
		interactWith($ActiveSessionsTab).click();
	}
	
	public static void logout() throws Exception {
		interactWith($LogoutLink).click();
	}
	
	public static void setSearchTextBox(String text) throws Exception {		
		interactWith($SearchUsersTextBox).setText(text);
	}
	
	public static void clickMagnigyingSearchButton() throws Exception {
		interactWith($QuickSearchMagnifyingButton).click();
	}
	
	/*Search and advanced Search*/
	
	//1. Basic searches	
	public static void searchTextBoxFor(String text) throws Exception {
		setSearchTextBox(text);
		clickMagnigyingSearchButton();
	}
	
	public static void goToSystemSettingsInAdministrationTab() throws Exception{
		$AdministrationTab.interact().click();
		$SettingsInAdministrationTab.interact().click();		
	}
	
	
}
