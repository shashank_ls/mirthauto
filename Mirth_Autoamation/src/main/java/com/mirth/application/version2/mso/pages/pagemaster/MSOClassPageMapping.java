package com.mirth.application.version2.mso.pages.pagemaster;

import java.io.IOException;
import java.util.Properties;

import com.mirth.application.master.ClassPageMapping;

public class MSOClassPageMapping implements ClassPageMapping{
	
	private MSOClassPageMapping() {
		//Singleton
	}

	//singleton object
	private static MSOClassPageMapping msoClassPageObject =null; 
	private Properties config;
	private String configFileName= "msoClassPageMapping.properties";
	//Config Variable END----------------------
	
	
	public synchronized static ClassPageMapping getObject() {
		
		if (MSOClassPageMapping.msoClassPageObject == null) {
			MSOClassPageMapping.msoClassPageObject = new MSOClassPageMapping();
		}
		
		return MSOClassPageMapping.msoClassPageObject;
	}
	
	
	private synchronized void initiateControllerConfig() throws IOException {
		config = new Properties();
		config.load(MSOClassPageMapping.class.getResourceAsStream(configFileName));
		
	}//initiateControllerConfig() throws IOException
	
	@Override
	public synchronized String getPageNameForClass(String configKey) throws IOException {
		
		if (isConfigPropertyNull()) {
			initiateControllerConfig();
		}
		
		return config.getProperty(configKey);
	}

	public synchronized boolean isConfigPropertyNull() {
		return (config==null)?true:false;
	}
	
}
