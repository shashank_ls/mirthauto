
package com.mirth.application.version2.mso.testcases.regression;

import java.util.concurrent.TimeUnit;

import com.mirth.application.master.General;
import com.mirth.application.master.PageObject;
import com.mirth.application.master.Testcase;
import com.mirth.application.version2.mso.pages.CommonObjectsMSO;
import com.mirth.application.version2.mso.pages.LoginPageMSO;
import com.mirth.application.version2.mso.pages.SystemSettingsPageMSO;
import com.mirth.controller.Interact;
import com.mirth.controller.objects.SeleniumLocator;
import com.mirth.results.QAProof;
import com.mirth.utility.general.MirthLib;
import com.mirth.utility.general.SshClient;
import com.mirth.utility.general.TextFile;

public class SignOn_612_AdministrationSettingsVerification extends Testcase {
	

	@Override
	protected void steps() throws Exception {		
		
		
		QAProof.writeInfo(">>Step 1 and 2");
		General.openFreshBrowser("main", super.testInventory.browser);
		General.setBrowserWaitTime(10,TimeUnit.SECONDS);
		LoginPageMSO.launch();
		LoginPageMSO.login();
		CommonObjectsMSO.goToSystemSettingsInAdministrationTab();
		
		QAProof.writeInfo(">>Step 3 to 5");		
		String disclaimerLabel = "com.mirth.kana.disclaimer";
		CommonObjectsMSO.searchTextBoxFor(disclaimerLabel);
		SystemSettingsPageMSO.$SystemSettingsTable.forceProcessTable();
		
		if (SystemSettingsPageMSO.$SystemSettingsTable.isTableEmpty()) {
			verify.reportIssue("Setting <"+disclaimerLabel+"> is NOT seen in the table.");		}
		
		SystemSettingsPageMSO.$SystemSettingsTable.clickFirstRowOfUsersTable(SystemSettingsPageMSO.valueHeading);
		
		SystemSettingsPageMSO.loadDisclaimers();
		SystemSettingsPageMSO.uploadDisclaimer(SystemSettingsPageMSO.customDisclaimerPath, true);
		QAProof.screenshot(CommonObjectsMSO.$AboutLinkInRightBottomCorner);
		
		
		QAProof.writeInfo(">>Step 6 to 7");
		CommonObjectsMSO.$AboutLinkInRightBottomCorner.interact().click();
		CommonObjectsMSO.$NoticesAndDisclaimersLinkInAboutPopup.interact().click();

		String discalimerExpectedValue= TextFile.readFileAsString(SystemSettingsPageMSO.customDisclaimerPath);
		discalimerExpectedValue = discalimerExpectedValue.replaceAll("<.*?>", "");

		QAProof.screenshot(CommonObjectsMSO.$DisclaimerTextInDisclaimerPopup);		
		PageObject.verifyStringData(CommonObjectsMSO.$DisclaimerTextInDisclaimerPopup, discalimerExpectedValue, true);
		CommonObjectsMSO.$CloseButtonInDisclaimerPopup.interact().click();		
		
		
		QAProof.writeInfo(">>Step 8 to 9");
		String privacyStatement= "This is a privacy statement you know. Long live Automation..";
		String msoPath = "/opt/mirthsignon/";
		String fileName = "privacyStatement.txt";
		String commandToCreatePrivacyStatement = "echo -n " + privacyStatement + " > " + msoPath+fileName ;
		
		String host = MirthLib.getServerNameOrIP();
		String sshUserName = PageObject.td(CommonObjectsMSO.class,"_sshUserName");
		String sshPassword = PageObject.td(CommonObjectsMSO.class,"_sshPassword");;
				
		SshClient sshclient = new SshClient(host,sshUserName,sshPassword);
		String commandResult = sshclient.runCommand(commandToCreatePrivacyStatement);
		if (commandResult == null) {
			
			String commandToExecute = "ls -la "+msoPath;
			commandResult = null;
			commandResult = sshclient.runCommand(commandToExecute);			
			verify.reportSuccess("Privacy statement file created succesfully in the applicance box in location "+ msoPath +"\n"+commandResult);
		}
		else {
			verify.reportIssue("Some Error while creating privacyStatement.txt file in "+msoPath + "\n"+commandResult);
			System.out.println("Error: \n"+commandResult);
		}
		
		QAProof.writeInfo(">>Step 8 to 9");
		CommonObjectsMSO.$PrivacyStatementLinkInAboutPopup.interact().click();
		QAProof.screenshot(CommonObjectsMSO.$PrivacyStatementTextInPrivacyStatementPopup);	
		PageObject.verifyStringData(CommonObjectsMSO.$PrivacyStatementTextInPrivacyStatementPopup, privacyStatement, true);
		CommonObjectsMSO.$CloseButtonInPrivacyStatementPopup.interact().click();
		CommonObjectsMSO.$CloseButtonInAboutPopup.interact().click();
		
		
		QAProof.writeInfo(">>Step 12 to 14");
		CommonObjectsMSO.$BackToListLinkInLeftPane.interact().click();
		CommonObjectsMSO.goToSystemSettingsInAdministrationTab();
		String warningTextLabel = "com.mirth.kana.loginWarningText";
		CommonObjectsMSO.searchTextBoxFor(warningTextLabel);
		SystemSettingsPageMSO.$SystemSettingsTable.forceProcessTable();
		
		if (SystemSettingsPageMSO.$SystemSettingsTable.isTableEmpty()) {
			verify.reportIssue("Setting <"+warningTextLabel+"> is NOT seen in the table.");		
		}
		
		SystemSettingsPageMSO.$SystemSettingsTable.clickFirstRowOfUsersTable(SystemSettingsPageMSO.valueHeading);
		
		String plainWarningText = "<div id=plainText>This is a sample custom automation Warning Text</div>";
		String onlyItalicText = "<i id=onlyItalics>Italicized disclaimer</i><br/>";
		String onlyBoldText = "<b id=onlyBold>Bold disclaimer</b><br/>";
		String bothBoldAndItalics = "<b><i id=boldAndItalics>Bold and Italicized disclaimer</i></b>";
		
		String warningText = plainWarningText+"\n"+onlyItalicText+"\n"+onlyBoldText+"\n"+bothBoldAndItalics;
		
		PageObject.setData(SystemSettingsPageMSO.$ValueTextBox,warningText);
		CommonObjectsMSO.$SaveChangesLinkInLeftPane.interact().click();
		QAProof.screenshot(CommonObjectsMSO.$LogoutLink);
		CommonObjectsMSO.$LogoutLink.interact().click();
		
		String warningTextExpectedWithoutFormatting = warningText.replaceAll("<.*?>", "");
		QAProof.screenshot(LoginPageMSO.$WarningText);
		PageObject.verifyStringData(LoginPageMSO.$WarningText, warningTextExpectedWithoutFormatting, true);
		
		//Verify italics and bold.
		
		Interact warningTextActualInteract = LoginPageMSO.$WarningText.interact();
		Interact plainTextInteract = warningTextActualInteract.getInteractElement(SeleniumLocator.XPath, "//div[@id='plainText']");
		Interact justItalicsInteract = warningTextActualInteract.getInteractElement(SeleniumLocator.XPath, "//i[@id='onlyItalics']");
		Interact justBoldInteract = warningTextActualInteract.getInteractElement(SeleniumLocator.XPath, "//b[@id='onlyBold']");
		Interact boldAndItalicsInteract = warningTextActualInteract.getInteractElement(SeleniumLocator.XPath, "//b/i[@id='boldAndItalics']");
		
		String plainWarningTextActual = plainTextInteract.getText();
		String onlyItalicTextActual = justItalicsInteract.getText();
		String onlyBoldTextActual = justBoldInteract.getText();
		String bothBoldAndItalicsActual = boldAndItalicsInteract.getText();
		
		PageObject.verifyStringData("Plain String", plainWarningText.replaceAll("<.*?>", ""), plainWarningTextActual, true);
		PageObject.verifyStringData("Ony Italicized Strig", onlyItalicText.replaceAll("<.*?>", ""), onlyItalicTextActual, true);
		PageObject.verifyStringData("Only Bold String", onlyBoldText.replaceAll("<.*?>", ""), onlyBoldTextActual, true);
		PageObject.verifyStringData("BoldAndItalicizedString", bothBoldAndItalics.replaceAll("<.*?>", ""), bothBoldAndItalicsActual, true);
		
		General.closeAndDestroyBrowser("main");		
		System.out.println("\n\n(--------"+plainWarningTextActual+"----------)");
	}
			
}//class
