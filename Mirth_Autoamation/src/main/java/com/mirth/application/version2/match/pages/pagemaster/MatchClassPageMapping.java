package com.mirth.application.version2.match.pages.pagemaster;

import java.io.IOException;
import java.util.Properties;

import com.mirth.application.master.ClassPageMapping;

public class MatchClassPageMapping implements ClassPageMapping{
	
	private MatchClassPageMapping() {
		//Singleton
	}

	//singleton object
	private static MatchClassPageMapping msoClassPageObject =null; 
	private Properties config;
	private String configFileName= "matchClassPageMapping.properties";
	//Config Variable END----------------------
	
	
	public synchronized static ClassPageMapping getObject() {
		
		if (MatchClassPageMapping.msoClassPageObject == null) {
			MatchClassPageMapping.msoClassPageObject = new MatchClassPageMapping();
		}
		
		return MatchClassPageMapping.msoClassPageObject;
	}
	
	
	private synchronized void initiateControllerConfig() throws IOException {
		config = new Properties();
		config.load(MatchClassPageMapping.class.getResourceAsStream(configFileName));
		
	}//initiateControllerConfig() throws IOException
	
	@Override
	public synchronized String getPageNameForClass(String configKey) throws IOException {
		
		if (isConfigPropertyNull()) {
			initiateControllerConfig();
		}
		
		return config.getProperty(configKey);
	}

	public synchronized boolean isConfigPropertyNull() {
		return (config==null)?true:false;
	}
	
}
