package com.mirth.application.version2.match.testcases;

import java.io.IOException;
import java.util.Properties;

import com.mirth.application.master.TestCaseMapping;

public class MatchTestCaseMapping implements TestCaseMapping{
	
	private MatchTestCaseMapping() {
		//Singleton
	}

	//singleton object
	private static MatchTestCaseMapping testCaseObject =null; 
	private Properties config;
	private String configFileName= "matchTestCaseConfig.properties";
	//Config Variable END----------------------
	
	public synchronized static MatchTestCaseMapping getObject() {
		
		if (MatchTestCaseMapping.testCaseObject == null) {
			MatchTestCaseMapping.testCaseObject = new MatchTestCaseMapping();
		}
		
		return MatchTestCaseMapping.testCaseObject;
	}
	
	
	private synchronized void initiateControllerConfig() throws IOException {
		config = new Properties();
		config.load(MatchTestCaseMapping.class.getResourceAsStream(configFileName));
		
	}//initiateControllerConfig() throws IOException
	
	
	public synchronized String getTestClassRelativeName(String configKey) throws IOException {
		
		if (isConfigPropertyNull()) {
			initiateControllerConfig();
		}
		
		return config.getProperty(configKey);
	}

	public synchronized boolean isConfigPropertyNull() {
		return (config==null)?true:false;
	}
	
}
