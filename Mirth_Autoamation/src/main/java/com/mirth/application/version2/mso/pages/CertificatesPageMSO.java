package com.mirth.application.version2.mso.pages;

import java.io.File;
import java.io.IOException;

import com.mirth.application.master.PageObject;
import com.mirth.controller.Interact;
import com.mirth.exceptions.AppNotPresentException;
import com.mirth.exceptions.ClassPageMappingNotFoundException;
import com.mirth.exceptions.InvalidCheckBoxValueException;
import com.mirth.exceptions.InvalidRadioButtonValueException;
import com.mirth.exceptions.TestDataNotPresentException;
import com.mirth.properties.ApplicationConfig;
import com.mirth.utility.general.SleepLib;
import com.mirth.utility.general.StaticLib;
import com.mirth.utility.seleniumLib.ButtonS;
import com.mirth.utility.seleniumLib.LinkS;
import com.mirth.utility.seleniumLib.PlainTextS;
import com.mirth.utility.seleniumLib.TableS;

public final class CertificatesPageMSO extends PageObject{
	
	public static final LinkS $UploadCertificateLink =   new LinkS ("UploadCertificateLink");
	public static final ButtonS $BrowseButtonInUploadCertificatePopup = new ButtonS("BrowseButtonInUploadCertificatePopup");
	public static final ButtonS $SubmitButtonInUploadCertificatePopup = new ButtonS("SubmitButtonInUploadCertificatePopup");
	public static final ButtonS $CancelButtonInUploadCertificatePopup = new ButtonS("CancelButtonInUploadCertificatePopup");
	public static final TableS $CertificatesTable = new TableS("CertificatesTable");
	
	//Edit Certificate Page
	public static final PlainTextS $CertificateFingerPrintInEditCertificate = new PlainTextS("CertificateFingerPrintInEditCertificate");
	
	
	//Certificate Table Headers
	public static final String subjectHeading = "Subject DN";
	public static final String domainHeading = "Domain";
	public static final String issuerDnHeading = "Issuer DN";
	public static final String fingerPrintHeading = "Fingerprint";
	
	public static boolean isCertificatesLoaded = false;
	public static String cert1Path = "";
	public static String cert2Path = "";
	public static String cert3Path = "";
	
	public static void enterCertificatePathToBrowseButton(String filePath) throws Exception{
		PageObject.setData(CertificatesPageMSO.$BrowseButtonInUploadCertificatePopup,filePath);
	}
	
	public static void uploadCertificate(String filePath,boolean shouldSubmit) throws Exception{  
		CertificatesPageMSO.$UploadCertificateLink.interact().click();
		SleepLib.sleep1Second();
		enterCertificatePathToBrowseButton(filePath);
		
		if (shouldSubmit){
			$SubmitButtonInUploadCertificatePopup.interact().click();
		}
		else {
			$CancelButtonInUploadCertificatePopup.interact().click();
		}		
	}	
	
	public static synchronized void loadCertificates() throws IllegalAccessException, IllegalArgumentException, IOException, InvalidRadioButtonValueException, InvalidCheckBoxValueException, TestDataNotPresentException, ClassPageMappingNotFoundException, AppNotPresentException{
		
		if (!isCertificatesLoaded){
			
			cert1Path = StaticLib.appendFolderSeparatorIfNotPresent(PageObject.getTestDataFolderNameForApp(ApplicationConfig.getValue("MSO")));
			cert2Path = cert1Path;
			cert3Path = cert1Path;
			
			cert1Path+= Interact.getRawSetterDataPair(td("_certificate1Path"))[1];
			cert2Path+= Interact.getRawSetterDataPair(td("_certificate2Path"))[1];
			cert3Path+= Interact.getRawSetterDataPair(td("_certificate3Path"))[1];		
			
			//this is to incorporate windows and mac
			cert1Path = new File(cert1Path).getAbsolutePath();
			cert2Path = new File(cert2Path).getAbsolutePath();
			cert3Path = new File(cert3Path).getAbsolutePath();			
			
			isCertificatesLoaded = true;
							
		}
		
	}
	
	
	
		
	
}//class