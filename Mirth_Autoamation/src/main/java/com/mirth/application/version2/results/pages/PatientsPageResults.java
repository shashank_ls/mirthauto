

package com.mirth.application.version2.results.pages;

import com.mirth.application.master.PageObject;
import com.mirth.utility.seleniumLib.ButtonS;

public final class PatientsPageResults extends PageObject{
	
	public static final ButtonS $CancelButtonOnAdvancedPatientFilter =   new ButtonS ("CancelButtonOnAdvancedPatientFilter");
		
}//class