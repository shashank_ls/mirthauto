package com.mirth.application.version2.mso.pages;

import com.mirth.application.master.General;
import com.mirth.application.master.PageObject;
import com.mirth.results.QAProof;
import com.mirth.utility.general.SleepLib;
import com.mirth.utility.seleniumLib.CheckBoxS;
import com.mirth.utility.seleniumLib.LinkS;
import com.mirth.utility.seleniumLib.SimpleDropDownS;
import com.mirth.utility.seleniumLib.TableS;
import com.mirth.utility.seleniumLib.TextBoxS;

public class NewEditApplicaionMSO extends PageObject {
	
	public static final TableS $ApplicationRolesTable = new TableS("ApplicationRolesTable");
	public static final TableS $ApplicationAttributesTable = new TableS("ApplicationAttributesTable");
	public static final TableS $ApplicationInstancesTable = new TableS("ApplicationInstancesTable");
	public static final CheckBoxS $EnableUserPushCheckBox = new CheckBoxS("EnableUserPushCheckBox");
	public static final CheckBoxS $ShowKanaToolBarCheckBox = new CheckBoxS("ShowKanaToolBarCheckBox");
	public static final CheckBoxS $IDPInitiatedLoginCheckBox = new CheckBoxS("IDPInitiatedLoginCheckBox");
	public static LinkS $AddApplicationRoleLink = new LinkS("AddApplicationRoleLink");
	public static LinkS $AddApplicationAttributeLink = new LinkS("AddApplicationAttributeLink");
	public static LinkS $AddApplicationInstanceLink = new LinkS("AddApplicationInstanceLink");
	public static TextBoxS $AppRoleTextBoxInAddAppPopup = new TextBoxS("AppRoleTextBoxInAddAppPopup");
	public static TextBoxS $DescriptionTextBoxInAddAppPopup = new TextBoxS("DescriptionTextBoxInAddAppPopup");
	public static SimpleDropDownS $MSORoleListInAddAppPopup = new SimpleDropDownS("MSORoleListInAddAppPopup");
	
	public static TextBoxS $MSOSAMLMetaDataURL = new TextBoxS("MSOSAMLMetaDataURL");
	
	//Application Role Headings
	public static final String appRoleHeading = "Application Role";
	public static final String descriptionHeading = "Description";
	public static final String msoRoleHeading = "MirthSignOn Role";
	
	//Applicaiton Instances Heading
	public static final String labelHeading = "Label";
	public static final String metaURLHeading = "Meta URL";
	public static final String userPushHeading = "User Push Enabled";
	public static final String showKanaHeading = "Show on Kana Toolbar";	
	public static final String idpHeading = "IdP Initiated Login";
	
	public static void addAppRole(String appRole, String description, String msoRole) throws Exception {
		
		if (!General.checkIfElementVisibleImmediately($AppRoleTextBoxInAddAppPopup)){
			$AddApplicationRoleLink.interact().click();
		}
		
		setData($AppRoleTextBoxInAddAppPopup,appRole);
		setData($DescriptionTextBoxInAddAppPopup,description);
		setData($MSORoleListInAddAppPopup,msoRole);
		
		SleepLib.sleep1Second();	
		CommonObjectsMSO.$SubmitButtonOnPopup.interact().click();
		SleepLib.sleep1Second();
		CommonObjectsMSO.$SaveChangesLinkInLeftPane.interact().click();
		QAProof.screenshot(CommonObjectsMSO.$BackToListLinkInLeftPane);
		
	}
	
}
