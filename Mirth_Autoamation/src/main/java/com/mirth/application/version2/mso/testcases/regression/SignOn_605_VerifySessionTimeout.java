/*
 * Test case author: Jessica Dear
 */

package com.mirth.application.version2.mso.testcases.regression;

import java.util.concurrent.TimeUnit;

import com.mirth.application.master.General;
import com.mirth.application.master.Testcase;
import com.mirth.application.version2.match.pages.CommonObjectsMatch;
import com.mirth.application.version2.match.pages.LoginPageMatch;
import com.mirth.application.version2.mso.pages.ActiveSessionsPageMSO;
import com.mirth.application.version2.mso.pages.CommonObjectsMSO;
import com.mirth.application.version2.mso.pages.LoginPageMSO;
import com.mirth.application.version2.results.pages.CommonObjectsResults;
import com.mirth.application.version2.results.pages.LoginPageResults;
import com.mirth.results.QAProof;

public class SignOn_605_VerifySessionTimeout extends Testcase{

	@Override
	public void steps() throws Exception {
			
		General.openFreshBrowser("main", super.testInventory.browser);
		General.setBrowserWaitTime(10, TimeUnit.SECONDS);
		LoginPageMSO.launch();
		LoginPageMSO.login();
		CommonObjectsMSO.clickActiveSessionsTab();
		
		//2
		General.openFreshBrowser("match", super.testInventory.browser);
		General.switchToBrowser("match");
		General.setBrowserWaitTime(10, TimeUnit.SECONDS);
		LoginPageMatch.launch();
		LoginPageMatch.login();
		
		General.openFreshBrowser("results", super.testInventory.browser);
		General.switchToBrowser("results");
		General.setBrowserWaitTime(10, TimeUnit.SECONDS);
		LoginPageResults.launch();
		LoginPageResults.login();
		
		General.switchToBrowser("main");
		CommonObjectsMSO.$RefreshLinkInLeftPane.interact().click();
		
		if (!ActiveSessionsPageMSO.$ActiveSessionsTable.isTableEmpty())
		{
			verify.reportSuccess("Active session present");
		}
		else {
			verify.reportIssue("There should be active sessions.");
		}
		
		General.switchToBrowser("match");
		CommonObjectsMatch.$SignOutButtonOnKanaToolBar.interact().click();
		
		General.switchToBrowser("results");
		CommonObjectsResults.$SignOutButtonOnKanaToolBar.interact().click();
		
		General.switchToBrowser("main");
		CommonObjectsMSO.$RefreshLinkInLeftPane.interact().click();
		
		if (ActiveSessionsPageMSO.$ActiveSessionsTable.isTableEmpty())
		{
			verify.reportSuccess("No active sessions");
		}
		else {
			verify.reportIssue("There should not be any active sessions.");
		}
		
		CommonObjectsMSO.$SSOConfigurationTab.interact().click();
		CommonObjectsMSO.$SSOConfigurationSessionTimeoutPeriod.interact().setText("1");
		CommonObjectsMSO.$SaveChangesLinkInLeftPane.interact().click();
		
		General.switchToBrowser("match");
		LoginPageMatch.login();
		
		General.switchToBrowser("results");
		LoginPageResults.login();
		General.setBrowserWaitTime(30, TimeUnit.SECONDS);
		General.acceptAlert();
		
		General.switchToBrowser("match");
		General.acceptAlert();
		//check for warning alert
		//click on keep me signed in button
		
		//7
		General.switchToBrowser("main");
		CommonObjectsMSO.clickActiveSessionsTab();
		if (!ActiveSessionsPageMSO.$ActiveSessionsTable.isTableEmpty())
		{
			verify.reportSuccess("Active session present");
		}
		else {
			verify.reportIssue("There should be active sessions.");
		}

		General.switchToBrowser("match");
		General.switchToBrowser("results");
		General.setBrowserWaitTime(60, TimeUnit.SECONDS);
		
		//do not click on keep me signed in button
	
		General.switchToBrowser("main");
		if (ActiveSessionsPageMSO.$ActiveSessionsTable.isTableEmpty())
		{
			verify.reportSuccess("No active sessions");
		}
		else {
			verify.reportIssue("There should not be any active sessions.");
		}
		
		General.switchToBrowser("match");
		CommonObjectsMatch.$SignOutButtonOnKanaToolBar.interact().click();
		
		General.switchToBrowser("results");
		CommonObjectsResults.$SignOutButtonOnKanaToolBar.interact().click();
		
		General.switchToBrowser("main");
		CommonObjectsMSO.$RefreshLinkInLeftPane.interact().click();
		if (ActiveSessionsPageMSO.$ActiveSessionsTable.isTableEmpty())
		{
			verify.reportSuccess("No active sessions");
		}
		else {
			verify.reportIssue("There should not be any active sessions.");
		}
		
		General.switchToBrowser("match");
		LoginPageMatch.login();
		
		General.switchToBrowser("results");
		LoginPageResults.login();
		General.setBrowserWaitTime(30, TimeUnit.SECONDS);
		
		General.switchToBrowser("match");
		CommonObjectsMatch.$Administration.interact().click();
		
		General.switchToBrowser("results");
		CommonObjectsResults.$Administration.interact().click();
		General.setBrowserWaitTime(30, TimeUnit.SECONDS);
		General.switchToBrowser("match");
		CommonObjectsMatch.$Administration.interact().click();
		
		General.switchToBrowser("results");
		CommonObjectsResults.$Administration.interact().click();
		General.setBrowserWaitTime(30, TimeUnit.SECONDS);
		
		General.switchToBrowser("match");
		CommonObjectsMatch.$Administration.interact().click();
		
		General.switchToBrowser("results");
		CommonObjectsResults.$Administration.interact().click();
		General.setBrowserWaitTime(30, TimeUnit.SECONDS);
		General.switchToBrowser("match");
		CommonObjectsMatch.$Administration.interact().click();
		
		General.switchToBrowser("results");
		CommonObjectsResults.$Administration.interact().click();
		General.setBrowserWaitTime(30, TimeUnit.SECONDS);
		
		General.switchToBrowser("match");
		CommonObjectsMatch.$Administration.interact().click();
		
		General.switchToBrowser("results");
		CommonObjectsResults.$Administration.interact().click();
		General.setBrowserWaitTime(30, TimeUnit.SECONDS);
		General.switchToBrowser("match");
		CommonObjectsMatch.$Administration.interact().click();
		
		General.switchToBrowser("results");
		CommonObjectsResults.$Administration.interact().click();
		General.setBrowserWaitTime(30, TimeUnit.SECONDS);
		
		General.switchToBrowser("match");
		CommonObjectsMatch.$Administration.interact().click();
		
		General.switchToBrowser("results");
		CommonObjectsResults.$Administration.interact().click();
		General.setBrowserWaitTime(30, TimeUnit.SECONDS);
		General.switchToBrowser("match");
		CommonObjectsMatch.$Administration.interact().click();
		
		General.switchToBrowser("results");
		CommonObjectsResults.$Administration.interact().click();
		
		
		General.switchToBrowser("main");
		CommonObjectsMSO.$RefreshLinkInLeftPane.interact().click();
		ActiveSessionsPageMSO.$ActiveSessionsTableCheckbox.interact().click();
		ActiveSessionsPageMSO.$ActiveSessionsTableDelete.interact().click();
		General.acceptAlert();
		
		if (ActiveSessionsPageMSO.$ActiveSessionsTable.isTableEmpty())
		{
			verify.reportSuccess("No active sessions");
		}
		else {
			verify.reportIssue("There should not be any active sessions.");
		}
		
		
		//16
		General.switchToBrowser("match");
		LoginPageMatch.login();
		General.closeAndDestroyBrowser("match");
		
		General.switchToBrowser("results");
		LoginPageResults.login();
		General.closeAndDestroyBrowser("results");
		General.setBrowserWaitTime(180, TimeUnit.SECONDS);
		
		General.switchToBrowser("main");
		CommonObjectsMSO.$RefreshLinkInLeftPane.interact().click();
		
		if (ActiveSessionsPageMSO.$ActiveSessionsTable.isTableEmpty())
		{
			verify.reportSuccess("No active sessions");
		}
		else {
			verify.reportIssue("There should not be any active sessions.");
		}
		
		General.switchToBrowser("main");
		CommonObjectsMSO.$SSOConfigurationTab.interact().click();
		CommonObjectsMSO.$SSOConfigurationSessionTimeoutPeriod.interact().setText("3");
		CommonObjectsMSO.$SaveChangesLinkInLeftPane.interact().click();
		
		//20
		General.openFreshBrowser("results", super.testInventory.browser);
		General.switchToBrowser("results");
		General.setBrowserWaitTime(10, TimeUnit.SECONDS);
		LoginPageResults.launch();
		LoginPageResults.login();
		General.setBrowserWaitTime(300, TimeUnit.SECONDS);
		
		General.switchToBrowser("main");
		CommonObjectsMSO.$ActiveSessionsTab.interact().click();
		CommonObjectsMSO.$RefreshLinkInLeftPane.interact().click();
		
		if (ActiveSessionsPageMSO.$ActiveSessionsTable.isTableEmpty())
		{
			verify.reportSuccess("No active sessions");
		}
		else {
			verify.reportIssue("There should not be any active sessions.");
		}
		
		
		LoginPageResults.login();
		
		General.openFreshBrowser("match", super.testInventory.browser);
		General.switchToBrowser("match");
		General.setBrowserWaitTime(10, TimeUnit.SECONDS);
		LoginPageMatch.launch();
		LoginPageMatch.login();
		General.closeAndDestroyBrowser("match");
		
		General.switchToBrowser("results");
		General.closeAndDestroyBrowser("results");
		
		General.switchToBrowser("main");
		General.setBrowserWaitTime(180, TimeUnit.SECONDS);
		CommonObjectsMSO.clickActiveSessionsTab();
		if (ActiveSessionsPageMSO.$ActiveSessionsTable.isTableEmpty())
		{
			verify.reportSuccess("No active sessions");
		}
		else {
			verify.reportIssue("There should not be any active sessions.");
		}
		
		//23
//		General.openFreshBrowser("results", super.testInventory.browser);
//		General.switchToBrowser("results");
//		General.setBrowserWaitTime(10, TimeUnit.SECONDS);
//		LoginPageResults.launch();
//		LoginPageResults.login();
//		General.setBrowserWaitTime(60, TimeUnit.SECONDS);
//		General.setBrowserWaitTime(60, TimeUnit.SECONDS);
//		General.setBrowserWaitTime(60, TimeUnit.SECONDS);
//		//**********click on stuff^
		
		LoginPageMSO.launch();
		LoginPageMSO.login();
		CommonObjectsMSO.$SSOConfigurationTab.interact().click();
	
		CommonObjectsMSO.$SSOConfigurationAllowOnlyOneSSOSessionCheckbox.interact().setCheckBox_ON();
		CommonObjectsMSO.$SaveChangesLinkInLeftPane.interact().click();
		
		
		CommonObjectsMSO.$SSOConfigurationTab.interact().click();
		CommonObjectsMSO.$SSOConfigurationAllowOnlyOneSSOSessionCheckbox.interact().setCheckBox_OFF();
		CommonObjectsMSO.$SaveChangesLinkInLeftPane.interact().click();
		

		QAProof.writeInfo("Test case End");
	}
}
