
package com.mirth.application.version2.mso.pages;
import com.mirth.application.master.General;
import com.mirth.application.master.PageObject;
import com.mirth.controller.ThreadController;
import com.mirth.results.QAProof;
import com.mirth.utility.general.SleepLib;
import com.mirth.utility.seleniumLib.LinkS;
import com.mirth.utility.seleniumLib.PlainTextS;
import com.mirth.utility.seleniumLib.TableS;

public class TemplatesPageMSO extends PageObject {

	public static PlainTextS $TaskPaneHeaderInActionsBar = new PlainTextS("TaskPaneHeaderInActionsBar");
	public static LinkS $RefreshListLinkInActionsBar = new LinkS("RefreshListLinkInActionsBar");
	public static LinkS $CreateUserTemplateLinkInActionsBar = new LinkS("CreateUserTemplateLinkInActionsBar");
	public static TableS $TemplatesTable = new TableS("TemplatesTable");
	
	public static PlainTextS $EditUserTemplateLinkInActionsBar = new PlainTextS("EditUserTemplateLinkInActionsBar");
	public static PlainTextS $DeleteUserTemplateLink = new PlainTextS("DeleteUserTemplateLink");
	public static PlainTextS $UserTemplatesTableHeader = new PlainTextS("UserTemplatesTableHeader");
	public static PlainTextS $UserTemplatesTablePagingStatus = new PlainTextS("UserTemplatesTablePagingStatus");
	public static PlainTextS $UserTemplatesTableGrid = new PlainTextS("UserTemplatesTableGrid");
	public static PlainTextS $ToggleButton = new PlainTextS("ToggleButton");
	public static PlainTextS $SimpleSearchTextBox = new PlainTextS("SimpleSearchTextBox");
	public static PlainTextS $SimpleSearchIcon = new PlainTextS("SimpleSearchIcon");
	
	public static final String identifierHeading = "Identifier";
	public static final String labelHeading = "Label";
	public static final String descriptionHeading = "Description";
	
	
	public static void deleteTemplate(String templateName) throws Exception{
		
		if (!General.checkIfElementVisibleImmediately($TemplatesTable)) {
			CommonObjectsMSO.$TemplatesTab.interact().click();						
		}
		
		$TemplatesTable.forceProcessTable();
		int rowNum = $TemplatesTable.findRowHavingText(templateName, identifierHeading, false,true);
		
		if (rowNum < 1 ) {
			throw new IllegalArgumentException("IllegalArguementException: The templateName sent in arguement is NOT present in tempaltes table");
		}
		
		QAProof.writeInfo("Deleting template: " + templateName);
		$TemplatesTable.getCellTDElement(rowNum, 1).click();
		CommonObjectsMSO.$DeleteLinkInLeftPane.interact().click();
		General.acceptAlert();
		$TemplatesTable.forceProcessTable();
		rowNum = $TemplatesTable.findRowHavingText(templateName, identifierHeading, false,true);
		QAProof.screenshot();
		
		if (rowNum>1){
			ThreadController.getVerifyOfCurrentThread().reportIssue("Template did NOT get deleted succesfully");
		}else{
			ThreadController.getVerifyOfCurrentThread().reportSuccess("Template deleted succesfully");
		}
			
	}


	public static void addRoleToExistingTemplate(String templateName, String rolesToBeAdded) throws Exception {
	
		if (!General.checkIfElementVisibleImmediately($TemplatesTable)) {
			CommonObjectsMSO.$TemplatesTab.interact().click();						
		}
		
		$TemplatesTable.forceProcessTable();
		int rowNum = $TemplatesTable.findRowHavingText(templateName, identifierHeading, false,true);
		
		if (rowNum < 1 ) {
			throw new IllegalArgumentException("IllegalArguementException: The templateName sent in arguement is NOT present in tempaltes table");
		}
		
		$TemplatesTable.clickRow(identifierHeading, rowNum); 
		NewTemplatePageMSO.$AddSecurityRoleLinkInTemplateActions.interact().click();
		SleepLib.sleep1Second();
		setData(NewTemplatePageMSO.$SelectListInSecurityRolesPopup,rolesToBeAdded);
		NewTemplatePageMSO.$SubmitButtonInAddSecurityRolePopup.interact().click();
		CommonObjectsMSO.$SaveChangesLinkInLeftPane.interact().click();
		QAProof.screenshot(CommonObjectsMSO.$BackToListLinkInLeftPane);
	
	}

	public static void deleteRoleOfTemplate(String templateName, String roleName) throws Exception{
		
		if (!General.checkIfElementVisibleImmediately($TemplatesTable)) {
			CommonObjectsMSO.$TemplatesTab.interact().click();						
		}
		
		$TemplatesTable.forceProcessTable();
		int rowNum = $TemplatesTable.findRowHavingText(templateName, identifierHeading, false,true);
		
		if (rowNum < 1 ) {
			throw new IllegalArgumentException("IllegalArguementException: The templateName sent in arguement is NOT present in tempaltes table");
		}
		
		$TemplatesTable.clickRow(identifierHeading, rowNum);
		NewTemplatePageMSO.$SecurityRolesTable.forceProcessTable();
		rowNum=-1;
		rowNum = NewTemplatePageMSO.$SecurityRolesTable.findRowHavingText(roleName, NewTemplatePageMSO.securityRoleNameColumnHeading, false,true);
		if (rowNum < 1 ) {
			throw new IllegalArgumentException("IllegalArguementException: The role sent in arguement is NOT present in roles table");
		}
		
		NewTemplatePageMSO.$SecurityRolesTable.getCellTDElement(rowNum, 1).click();
		NewTemplatePageMSO.$DeleteSecurityRolesLinkInUserActions.interact().click();
		General.acceptAlert();
		CommonObjectsMSO.$SaveChangesLinkInLeftPane.interact().click();
				
		NewTemplatePageMSO.$SecurityRolesTable.forceProcessTable();
		QAProof.screenshot();
		rowNum = NewTemplatePageMSO.$SecurityRolesTable.findRowHavingText(roleName, NewTemplatePageMSO.securityRoleNameColumnHeading, false,true);
		if (rowNum < 1 ) {
			ThreadController.getVerifyOfCurrentThread().reportSuccess( "Succesfully deleted role("+roleName+" from the template("+templateName+")" );
		}
		else{
			ThreadController.getVerifyOfCurrentThread().reportSuccess( "Deletion of role("+roleName+" from the template("+templateName+") NOT succesfull" );
		}
		
	}	
		
	
}//class

