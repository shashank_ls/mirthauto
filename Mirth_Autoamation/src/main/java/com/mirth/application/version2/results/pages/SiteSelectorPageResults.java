
package com.mirth.application.version2.results.pages;

import com.mirth.application.master.PageObject;
import com.mirth.exceptions.NoSuchEntityException;
import com.mirth.utility.seleniumLib.TableS;

public final class SiteSelectorPageResults extends PageObject{
	
	public static final TableS $SiteSelectorTable =   new TableS ("SiteSelectorTable"); 
	
	//Element constants
	public static final String siteNameHeading = "Site Name";

	
	public static void selectSite() throws Exception {
		selectSite(td("SiteToSelect"));
	}
	public static void selectSite(String data) throws Exception {
		$SiteSelectorTable.processTable();
		
		int maxRows = $SiteSelectorTable.getDataRowsCount();
	
		for (int row =1; row<= maxRows; row++) {
			if (data.equalsIgnoreCase($SiteSelectorTable.getValueFromTable(siteNameHeading, row))) {
				$SiteSelectorTable.getCellTDElement(siteNameHeading, row).click();
				return;
			}
		}//for
		
		//if control reaches here, then no such site.
		throw new NoSuchEntityException("No such Site to select: " + data);		
	}
	

	
}//class