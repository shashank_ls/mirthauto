package com.mirth.application.version2.mso.pages;

import com.mirth.application.master.PageObject;
import com.mirth.results.QAProof;
import com.mirth.utility.general.MirthLib;
import com.mirth.utility.seleniumLib.ButtonS;
import com.mirth.utility.seleniumLib.PlainTextS;
import com.mirth.utility.seleniumLib.TextBoxS;

public final class LoginPageMSO extends PageObject{
	
	public static final PlainTextS SSOLogoLoginPage =   new PlainTextS ("SSOLogoLoginPage"); 
	public static final PlainTextS $WarningText =   new PlainTextS ("WarningText"); 
	public static final PlainTextS $WarningLogo =   new PlainTextS ("WarningLogo"); 
	public static final PlainTextS $UsernameLabel =   new PlainTextS ("UsernameLabel"); 	
	public static final TextBoxS $UsernameTextBox =   new TextBoxS ("UsernameTextBox"); 
	public static final PlainTextS $ForgotUsernameLink =   new PlainTextS ("ForgotUsernameLink"); 
	public static final PlainTextS $PasswordLabel =   new PlainTextS ("PasswordLabel"); 
	public static final TextBoxS $PasswordTextBox =   new TextBoxS ("PasswordTextBox"); 
	public static final PlainTextS $ForgotPasswordLink =   new PlainTextS ("ForgotPasswordLink"); 
	public static final ButtonS $LoginButton =   new ButtonS ("LoginButton");
	
	public static final PlainTextS $FooterText =   new PlainTextS ("FooterText"); 
	public static final PlainTextS $CopyrightText =   new PlainTextS ("CopyrightText"); 
	public static final PlainTextS $AboutLink =   new PlainTextS ("AboutLink"); 
	public static final PlainTextS $AboutPopUp =   new PlainTextS ("AboutPopUp"); 
	public static final PlainTextS $AboutPopUpCloseButton =   new PlainTextS ("AboutPopUpCloseButton"); 
	public static final PlainTextS $AboutPopUpLogo =   new PlainTextS ("AboutPopUpLogo"); 
	public static final PlainTextS $AboutPopUpVersionLabel =   new PlainTextS ("AboutPopUpVersionLabel"); 
	public static final PlainTextS $AboutPopUpVersionNumber =   new PlainTextS ("AboutPopUpVersionNumber"); 
	public static final PlainTextS $AboutPopUpRealmLabel =   new PlainTextS ("AboutPopUpRealmLabel"); 
	public static final PlainTextS $AboutPopUpRealmValue =   new PlainTextS ("AboutPopUpRealmValue"); 
	public static final PlainTextS $AboutPopUpNoticeAndDisclaimerLink =   new PlainTextS ("AboutPopUpNoticeAndDisclaimerLink"); 
	public static final PlainTextS $AboutPopUpNoticeAndDisclaimerPopUp =   new PlainTextS ("AboutPopUpNoticeAndDisclaimerPopUp"); 
	public static final PlainTextS $AboutPopUpNoticeAndDisclaimerPopUpCloseButton =   new PlainTextS ("AboutPopUpNoticeAndDisclaimerPopUpCloseButton"); 
	public static final PlainTextS $AboutPopUpNoticeAndDisclaimerPopUpText =   new PlainTextS ("AboutPopUpNoticeAndDisclaimerPopUpText"); 
	public static final PlainTextS $AboutPopUpPrivacyLink =   new PlainTextS ("AboutPopUpPrivacyLink"); 
	public static final PlainTextS $AboutPopUpPrivacyPopUp =   new PlainTextS ("AboutPopUpPrivacyPopUp"); 
	public static final PlainTextS $AboutPopUpPrivacyPopUpCloseButton =   new PlainTextS ("AboutPopUpPrivacyPopUpCloseButton"); 
	public static final PlainTextS $AboutPopUpPrivacyPopUpText =   new PlainTextS ("AboutPopUpPrivacyPopUpText"); 
	public static final PlainTextS $AboutPopUpCopyrightText =   new PlainTextS ("AboutPopUpCopyrightText ");
	
	
	public static void launch() throws Exception {
		
		String url = "http://" + MirthLib.getServerNameOrIP()
					 + td("_RelativeApplicationURL");
		
		getDriver().get(url);		
	}
	
	
	public static void login() throws Exception {
		
		String userName = getProcessedTestData($UsernameTextBox);
		String password = getProcessedTestData($PasswordTextBox);
		login(userName, password);
		
	}
	
	public static void login(String userName,String password) throws Exception {
		
		setData($UsernameTextBox,userName);
		setData($PasswordTextBox,password);
		QAProof.screenshot();
		interactWith($LoginButton).click();
		
	}
	
	
}//class