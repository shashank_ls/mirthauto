
package com.mirth.application.version2.mso.pages;

import java.util.List;
import java.util.Map;

import com.mirth.application.master.General;
import com.mirth.application.master.PageObject;
import com.mirth.properties.InteractConfig;
import com.mirth.results.QAProof;
import com.mirth.utility.general.SleepLib;
import com.mirth.utility.general.StaticLib;
import com.mirth.utility.seleniumLib.ButtonS;
import com.mirth.utility.seleniumLib.CheckBoxS;
import com.mirth.utility.seleniumLib.LinkS;
import com.mirth.utility.seleniumLib.MultiSelectDropDownS;
import com.mirth.utility.seleniumLib.PlainTextS;
import com.mirth.utility.seleniumLib.RadioButtonS;
import com.mirth.utility.seleniumLib.SeleniumElement;
import com.mirth.utility.seleniumLib.SeleniumLib;
import com.mirth.utility.seleniumLib.SimpleDropDownS;
import com.mirth.utility.seleniumLib.TableS;
import com.mirth.utility.seleniumLib.TextBoxS;

public class NewUserPageMSO extends PageObject{

	public static TextBoxS $AccountIDTextBox = new TextBoxS("AccountIDTextBox");
	public static PlainTextS $AccountIDTextValue = new PlainTextS("AccountIDTextValue");
	public static SimpleDropDownS $UserEntityTemplateNameDropDown = new SimpleDropDownS("UserEntityTemplateNameDropDown");
	public static SimpleDropDownS $AccountStatusDropDown = new SimpleDropDownS("AccountStatusDropDown");
	public static CheckBoxS $Chk_ChangePasswordNextLogin = new CheckBoxS("Chk_ChangePasswordNextLogin");
	public static CheckBoxS $Chk_TwoFactorAuthentication = new CheckBoxS("Chk_TwoFactorAuthentication");
	public static RadioButtonS $Rad_OTP = new RadioButtonS("Rad_OTP");
	public static RadioButtonS $Rad_ChallengeResponse = new RadioButtonS("Rad_ChallengeResponse");
	public static CheckBoxS $Chk_SecurityQuestion = new CheckBoxS("Chk_SecurityQuestion");
	public static CheckBoxS $Chk_IgnoreTwoFactorToken = new CheckBoxS("Chk_IgnoreTwoFactorToken");
	public static TextBoxS $Name = new TextBoxS("Name");
	public static TextBoxS $Email = new TextBoxS("Email");
	public static TextBoxS $VoiceContact = new TextBoxS("VoiceContact");
	public static TextBoxS $FaxContact = new TextBoxS("FaxContact");
	public static TextBoxS $AddressLine1 = new TextBoxS("AddressLine1");
	public static TextBoxS $AddressLine2 = new TextBoxS("AddressLine2");
	public static TextBoxS $City = new TextBoxS("City");
	public static TextBoxS $State = new TextBoxS("State");
	public static TextBoxS $Postal = new TextBoxS("Postal");
	public static SimpleDropDownS $Country = new SimpleDropDownS("Country");
	public static TextBoxS $NewPassword = new TextBoxS("NewPassword");
	public static TextBoxS $ConfirmNewPassword = new TextBoxS("ConfirmNewPassword");
	public static LinkS $AddSecurityRoleLinkInUserActions = new LinkS("AddSecurityRoleLinkInUserActions");
	public static MultiSelectDropDownS $SelectListInAddSecurityRolePopup = new MultiSelectDropDownS("SelectListInAddSecurityRolePopup");
	public static TableS $SecurityRolesTable = new TableS("SecurityRolesTable");
	public static ButtonS $SubmitButtonInAddSecurityRolePopup = new ButtonS("SubmitButtonInAddSecurityRolePopup");
	public static ButtonS $CancelButtonInAddSecurityRolePopup = new ButtonS("CancelButtonInAddSecurityRolePopup");
	public static ButtonS $CloxeXMarkButtonInAddSecurityRolePopup = new ButtonS("CloxeXMarkButtonInAddSecurityRolePopup");
	
	public static LinkS $ChangePasswordLinkInNewUserPage = new LinkS("ChangePasswordLinkInNewUserPage");
	public static TextBoxS $NewPasswordTextBoxInChangePasswordPopup = new TextBoxS("NewPasswordTextBoxInChangePasswordPopup");
	public static TextBoxS $ConfirmNewPasswordTextBoxInChangePasswordPopup = new TextBoxS("ConfirmNewPasswordTextBoxInChangePasswordPopup");
	public static PlainTextS $ErrorMessageInChangePasswordPopup = new PlainTextS("ErrorMessageInChangePasswordPopup");
	public static ButtonS $SubmitButtonInChangePasswordPopup = new ButtonS("SubmitButtonInChangePasswor dPopup");
	public static ButtonS $CancelButtonInChangePasswordPopup = new ButtonS("CancelButtonInChangePasswordPopup");
	public static LinkS $DeleteSecurityRolesLinkInUserActions = new LinkS("DeleteSecurityRolesLinkInUserActions");
	
	
	//Element Constants
	public static String securityRoleNameColumnHeading = "Name";
	
	
	
	public static Map<SeleniumElement,String> createNewUserFromTestData() throws Exception {
		return createNewUserFromTestDataWithCustomReplace(null);
	
	}
	
	public static Map<SeleniumElement,String> createNewUserFromTestDataWithCustomReplace(Map<SeleniumElement,String> changableFields) throws Exception {
		return createNewUserFromTestDataWithCustomReplace(changableFields,true);
	}
	 
	public static Map<SeleniumElement,String> createNewUserFromTestDataWithCustomReplace(Map<SeleniumElement,String> changableFields,boolean isShouldSave) throws Exception {
		
		Map<SeleniumElement,String> resultMap = null;
		
		uncheckAllUserCheckBoxs();
		deleteAllSecurityRolesIfPresent();
		
		
		resultMap = getProcessedTestData(
						$UserEntityTemplateNameDropDown,		
						$AccountIDTextBox,
						$AccountStatusDropDown,
						$Chk_ChangePasswordNextLogin,
						$Chk_TwoFactorAuthentication,
						$Rad_OTP,
						$Rad_ChallengeResponse,
						$Chk_SecurityQuestion,
						$Chk_IgnoreTwoFactorToken,
						$Name,
						$Email,
						$VoiceContact,
						$FaxContact,
						$AddressLine1,
						$AddressLine2,
						$City,
						$State,
						$Postal,
						$Country,
						$NewPassword,
						$ConfirmNewPassword																
					);
		
		String securityRolesToBeSelected = getProcessedTestData($SelectListInAddSecurityRolePopup);
		
		
		if (changableFields!=null){
			
			String secRolesInChangeableFields = changableFields.remove($SelectListInAddSecurityRolePopup);
			
			//this "for loop" should be compulsory after above assignment
			for(SeleniumElement selEle: changableFields.keySet()){
				resultMap.put(selEle, changableFields.get(selEle));
			}			
			
			if (secRolesInChangeableFields!=null){
				securityRolesToBeSelected = secRolesInChangeableFields;
			}
		}//if (changableFields!=null)
		
		setData(resultMap);
		
		//Now, Select the security Role and Submit
		$AddSecurityRoleLinkInUserActions.interact().click();
		SleepLib.sleep1Second();
		setData($SelectListInAddSecurityRolePopup,securityRolesToBeSelected);
		resultMap.put($SelectListInAddSecurityRolePopup, securityRolesToBeSelected);
		$SubmitButtonInAddSecurityRolePopup.interact().click();
		
		SeleniumLib.waitForVisiblity(CommonObjectsMSO.$SaveChangesLinkInLeftPane);
		if (isShouldSave){
			CommonObjectsMSO.$SaveChangesLinkInLeftPane.interact().click();
		}
		
		resultMap.put($AccountIDTextValue, resultMap.get($AccountIDTextBox));
		return resultMap;		
	}
	
	public static Map<SeleniumElement,String> createNewUserWithSelectedFields(SeleniumElement... selElements) throws Exception {
		Map<SeleniumElement,String> resultMap = null;
		
		if (selElements==null) {return null;}
		
		resultMap = getProcessedTestData(selElements);
		String securityRolesToBeSelected = resultMap.remove($SelectListInAddSecurityRolePopup);
		setData(resultMap);
		resultMap.put($AccountIDTextValue, resultMap.get($AccountIDTextBox));
			
		if (securityRolesToBeSelected !=null){
			//Now, Select the security Role and Submit
			$AddSecurityRoleLinkInUserActions.interact().click();
			SleepLib.sleep1Second();
			setData($SelectListInAddSecurityRolePopup,securityRolesToBeSelected);
			resultMap.put($SelectListInAddSecurityRolePopup, securityRolesToBeSelected);
			$SubmitButtonInAddSecurityRolePopup.interact().click();			
		}
		
		return resultMap;
		
	}
	
	public static Map<SeleniumElement,String> createNewUserWithSelectedFields(List<SeleniumElement> selElementList) throws Exception {
		Map<SeleniumElement,String> resultMap = null;
		
		resultMap = getProcessedTestData(selElementList);
		String securityRolesToBeSelected = resultMap.remove($SelectListInAddSecurityRolePopup);
		setData(resultMap);
		resultMap.put($AccountIDTextValue, resultMap.get($AccountIDTextBox));
			
		if (securityRolesToBeSelected !=null){
			//Now, Select the security Role and Submit
			$AddSecurityRoleLinkInUserActions.interact().click();
			SleepLib.sleep1Second();
			setData($SelectListInAddSecurityRolePopup,securityRolesToBeSelected);
			resultMap.put($SelectListInAddSecurityRolePopup, securityRolesToBeSelected);
			$SubmitButtonInAddSecurityRolePopup.interact().click();			
		}
		
		return resultMap;
		
	}  
	
	public static Map<SeleniumElement,String> createNewUserWithLiteralSelectedFields(Map<SeleniumElement,String> actualMap, boolean isShouldSave) throws Exception {
		Map<SeleniumElement,String> resultMap = StaticLib.getNewMapCopy(actualMap);
		actualMap=null;
	
		boolean isTemplateAlreadySelected=false;
		String templateTobeSelected = resultMap.get($UserEntityTemplateNameDropDown);
		if (templateTobeSelected!=null){
			String currentTemplateSelected=getData($UserEntityTemplateNameDropDown);
			if (templateTobeSelected.equalsIgnoreCase(currentTemplateSelected)){
				isTemplateAlreadySelected = true;
			}//if (templateTobeSelected.equalsIgnoreCase(currentTemplateSelected)){
		}//if (templateTobeSelected!=null){

		if (!isTemplateAlreadySelected) {
			uncheckAllUserCheckBoxs();
			deleteAllSecurityRolesIfPresent();
		}				
		
		String securityRolesToBeSelected = resultMap.remove($SelectListInAddSecurityRolePopup);
		for (SeleniumElement selEle: resultMap.keySet()){
			setData(selEle,resultMap.get(selEle));
		}	
		
		resultMap.put($AccountIDTextValue, resultMap.get($AccountIDTextBox));
			
		if (securityRolesToBeSelected !=null){
			//Now, Select the security Role and Submit
			$AddSecurityRoleLinkInUserActions.interact().click();
			SleepLib.sleep1Second();
			setData($SelectListInAddSecurityRolePopup,securityRolesToBeSelected);
			resultMap.put($SelectListInAddSecurityRolePopup, securityRolesToBeSelected);
			$SubmitButtonInAddSecurityRolePopup.interact().click();			
		}
		
		if (isShouldSave){
			CommonObjectsMSO.$SaveChangesLinkInLeftPane.interact().click();
		}
		
		resultMap.put($UserEntityTemplateNameDropDown, getData($UserEntityTemplateNameDropDown));
		resultMap.put($AccountIDTextBox, getData($AccountIDTextValue));
		resultMap.put($AccountIDTextValue, getData($AccountIDTextValue));
		resultMap.put($AccountStatusDropDown, getData($AccountStatusDropDown));
		resultMap.put($Chk_ChangePasswordNextLogin, getData($Chk_ChangePasswordNextLogin));
		resultMap.put($Chk_IgnoreTwoFactorToken, getData($Chk_IgnoreTwoFactorToken));
		resultMap.put($Chk_SecurityQuestion, getData($Chk_SecurityQuestion));
		resultMap.put($Chk_TwoFactorAuthentication, getData($Chk_TwoFactorAuthentication));
		resultMap.put($Rad_ChallengeResponse, getData($Rad_ChallengeResponse));
		resultMap.put($Rad_OTP, getData($Rad_OTP));
		resultMap.put($Name, getData($Name));
		resultMap.put($Email, getData($Email));
		resultMap.put($VoiceContact, getData($VoiceContact));
		resultMap.put($FaxContact, getData($FaxContact));
		resultMap.put($AddressLine1, getData($AddressLine1));
		resultMap.put($AddressLine2, getData($AddressLine2));
		resultMap.put($City, getData($City));
		resultMap.put($State, getData($State));
		resultMap.put($Postal, getData($Postal));
		resultMap.put($Country, getData($Country));
		resultMap.put($NewPassword, getData($NewPassword));
		resultMap.put($ConfirmNewPassword, getData($ConfirmNewPassword));
		
		
		$SecurityRolesTable.forceProcessTable();
		String currentSecurityRoles = StaticLib.getStringListValuesAsString($SecurityRolesTable.getAllColumnValuesAsListForHeader(NewUserPageMSO.securityRoleNameColumnHeading),
															InteractConfig.getValue("MultiSelectSeparator"));
		resultMap.put($SelectListInAddSecurityRolePopup, currentSecurityRoles);	
		
		
		return resultMap;
		
	} 
	
	public static Map<SeleniumElement,String> updateNewUserPageFor(Map<SeleniumElement,String> changableFields) throws Exception {
		if (changableFields == null){
			return null;
		}
		
		String securityRolesToBeSelected = changableFields.remove($SelectListInAddSecurityRolePopup);
		
		for(SeleniumElement selEle: changableFields.keySet()){
			setData(selEle,changableFields.get(selEle));
		}
		
		if (securityRolesToBeSelected !=null){
			$AddSecurityRoleLinkInUserActions.interact().click();
			SleepLib.sleep1Second();
			setData($SelectListInAddSecurityRolePopup,securityRolesToBeSelected);
			changableFields.put($SelectListInAddSecurityRolePopup, securityRolesToBeSelected);
			$SubmitButtonInAddSecurityRolePopup.interact().click();	
		}
		
		return changableFields;		
	}
	
	public static Map<SeleniumElement,Boolean> verifyNewUserValues(Map<SeleniumElement,String> actualMap) throws Exception {
		Map<SeleniumElement,String> verificationMap = StaticLib.getNewMapCopy(actualMap);
		actualMap = null;
		
		Map<SeleniumElement,Boolean> resultBooleanMap;
		
		verificationMap.remove($NewPassword);
		verificationMap.remove($ConfirmNewPassword);
		
		String accountID = verificationMap.remove($AccountIDTextBox);
		if (accountID!=null){
			verificationMap.put($AccountIDTextValue, accountID);
		} 
		
		String rolesExpected = verificationMap.remove($SelectListInAddSecurityRolePopup);
		
		resultBooleanMap = verifyStringData(verificationMap,false);
		$SecurityRolesTable.forceProcessTable();
		
		
		String rolesActual = StaticLib.getStringListValuesAsString($SecurityRolesTable.getAllColumnValuesAsListForHeader(securityRoleNameColumnHeading),InteractConfig.getValue("MultiSelectSeparator"));
		
		resultBooleanMap.put($SecurityRolesTable, verifyStringData($SecurityRolesTable.get(), rolesExpected, rolesActual, false) );
		
		return resultBooleanMap;
		
	}	
	
	public static void clickOnSaveChangesLink() throws Exception {
		interactWith(CommonObjectsMSO.$SaveChangesLinkInLeftPane).click();
	}

	public static void clickOnBackToListLink() throws Exception {
		interactWith(CommonObjectsMSO.$BackToListLinkInLeftPane).click();		
	}

	public static void changePassword(String newPassword,boolean isSubmit)  throws Exception{
		
		
		if (!General.checkIfElementVisibleImmediately($NewPasswordTextBoxInChangePasswordPopup)){		
			System.out.println("Inside");
			//System.exit(1);
			$ChangePasswordLinkInNewUserPage.interact().click();
		}
		
		
		setData($NewPasswordTextBoxInChangePasswordPopup,newPassword);
		setData($ConfirmNewPasswordTextBoxInChangePasswordPopup,newPassword);
		QAProof.screenshot();
		
		if (isSubmit){
			$SubmitButtonInChangePasswordPopup.interact().click();
		} 
		else{
			$CancelButtonInChangePasswordPopup.interact().click();
		}
	}
	
	public static void deleteAllSecurityRolesIfPresent() throws Exception{
		$SecurityRolesTable.forceProcessTable();
		if (!$SecurityRolesTable.isTableEmpty()){
			
			for (int i=1; i<= $SecurityRolesTable.getDataRowsCount(); i++){
				$SecurityRolesTable.getCellTDElement(i, 1).click();				
			}
			
			$DeleteSecurityRolesLinkInUserActions.interact().click();
			SleepLib.sleep2Seconds();
			General.acceptAlert();
	
		}//if (!$SecurityRolesTable.isTableEmpty())
	}//public static void deleteAllSecurityRolesIfPresent() t

	
	public static void uncheckAllUserCheckBoxs() throws Exception {
		$Chk_ChangePasswordNextLogin.interact().setCheckBox_OFF();
		$Chk_TwoFactorAuthentication.interact().setCheckBox_OFF();
		$Chk_SecurityQuestion.interact().setCheckBox_OFF();
		$Chk_IgnoreTwoFactorToken.interact().setCheckBox_OFF();	
	}
			
}
