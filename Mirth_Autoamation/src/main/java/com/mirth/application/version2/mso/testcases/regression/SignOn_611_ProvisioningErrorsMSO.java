
package com.mirth.application.version2.mso.testcases.regression;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import com.mirth.application.master.General;
import com.mirth.application.master.Testcase;
import com.mirth.application.version2.mso.pages.ApplicationPageMSO;
import com.mirth.application.version2.mso.pages.CommonObjectsMSO;
import com.mirth.application.version2.mso.pages.LoginPageMSO;
import com.mirth.application.version2.mso.pages.NewEditApplicaionMSO;
import com.mirth.application.version2.mso.pages.NewUserPageMSO;
import com.mirth.application.version2.mso.pages.ProvisioningErrorsMSO;
import com.mirth.application.version2.mso.pages.UsersPageMSO;
import com.mirth.application.version2.results.pages.LoginPageResults;
import com.mirth.application.version2.results.pages.PatientsPageResults;
import com.mirth.application.version2.results.pages.SiteSelectorPageResults;
import com.mirth.application.version2.results.pages.UserAdminPageResults;
import com.mirth.controller.Verification;
import com.mirth.exceptions.AppNotPresentException;
import com.mirth.exceptions.ClassPageMappingNotFoundException;
import com.mirth.exceptions.TestDataNotPresentException;
import com.mirth.results.QAProof;
import com.mirth.utility.general.SleepLib;
import com.mirth.utility.seleniumLib.SeleniumElement;

public class SignOn_611_ProvisioningErrorsMSO extends Testcase {
	
	private static Map<SeleniumElement,String>  newUserVals;
	
	@Override
	protected void steps() throws Exception {		
		
	
		
		QAProof.writeInfo(">>Step 1");
		General.openFreshBrowser("main", super.testInventory.browser);
		General.setBrowserWaitTime(10,TimeUnit.SECONDS);
		LoginPageMSO.launch();
		LoginPageMSO.login();
		
		General.openAndPointToAddtionalBrowser("spBrowser", super.testInventory.browser);
		General.setBrowserWaitTime(10,TimeUnit.SECONDS);
		LoginPageResults.launch();
		LoginPageResults.login();
		SiteSelectorPageResults.selectSite();
		PatientsPageResults.$CancelButtonOnAdvancedPatientFilter.interact().click();
		SleepLib.sleep2Seconds();

		General.switchToBrowser("main");
		QAProof.writeInfo(">>Step 2 to 6");

		//Clear all entries before proceeding (Not in manual test case. But doing this for convenience)
		
		Map<SeleniumElement,String> instanceData=new HashMap<SeleniumElement,String>();
		instanceData.put(NewEditApplicaionMSO.$MSOSAMLMetaDataURL, "");
		instanceData = RegressionUtilities.getApplicaitonInstanceVals(true,ApplicationPageMSO.td(ApplicationPageMSO.class, "_resultsIdentifier"),instanceData);
		String originalSamlMetaValue = instanceData.get(NewEditApplicaionMSO.$MSOSAMLMetaDataURL);
		
		
		QAProof.writeInfo(">>Step 7 to 9");
		createUserAndVerifySetting(verify,instanceData,true,originalSamlMetaValue+"EXTRA",true);
		
		QAProof.writeInfo(">>Step 10 and 11");
		createUserAndVerifySetting(verify,instanceData,true,"https://giberish.com/",true);
		
		QAProof.writeInfo(">>Step 12 and 13");
		createUserAndVerifySetting(verify,instanceData,true,"abc",true);

		QAProof.writeInfo(">>Step 14 and 15");
		CommonObjectsMSO.$ProvisioningErrorsTab.interact().click();	
		ProvisioningErrorsMSO.clearTable();		
		//Disable UserPush
		setAppInstance(instanceData, false, "abc");
		createUser();
		
		QAProof.writeInfo(">>Step 16 to 18");
		setAppInstance(instanceData,true,"abc");
		//Enable User push and sync accounts
		CommonObjectsMSO.$UsersTab.interact().click();
		UsersPageMSO.syncAllUsers();	
		checkProvError(verify, true, instanceData);
		
		QAProof.writeInfo(">>Step 19 to 22");
		createUserAndVerifySetting(verify,instanceData,true,originalSamlMetaValue,false);
		
		General.closeAndDestroyBrowser("spBrowser");
		General.closeAndDestroyBrowser("main");
		QAProof.writeInfo(">>Test Case End<<");
	}
	
	public static void createUserAndVerifySetting(Verification verify,Map<SeleniumElement,String> instanceData,boolean userPush,String newSamlValue, boolean shouldItError) throws Exception{
		General.switchToBrowser("main");
		setAppInstance(instanceData, userPush, newSamlValue);
		createUser();		
		checkProvError(verify, shouldItError, instanceData);
		
		General.switchToBrowser("spBrowser");
		verifyUserPresenceInResults(verify, !shouldItError, newUserVals);
		General.switchToBrowser("main");
						
	}//func		
		
	
	public static void setAppInstance(Map<SeleniumElement,String> instanceData,boolean userPush,String newSamlValue) throws IllegalAccessException, IllegalArgumentException, IOException, TestDataNotPresentException, ClassPageMappingNotFoundException, AppNotPresentException, Exception{
		CommonObjectsMSO.$ProvisioningErrorsTab.interact().click();		
		ProvisioningErrorsMSO.clearTable();		
		instanceData.put(NewEditApplicaionMSO.$MSOSAMLMetaDataURL,newSamlValue);
		RegressionUtilities.setApplicaitonInstanceVals(false,ApplicationPageMSO.td(ApplicationPageMSO.class, "_resultsIdentifier"),userPush,true,instanceData);
	}
	
	public static void createUser() throws Exception{
		CommonObjectsMSO.clickUsersTab();
		UsersPageMSO.clickCreateNewUser();		
		Map<SeleniumElement,String> newUserVals1 = NewUserPageMSO.createNewUserFromTestData();
		QAProof.screenshot(NewUserPageMSO.$SecurityRolesTable);
		newUserVals =  newUserVals1;
		SleepLib.sleep2Seconds();
	}
	
	
	public static void checkProvError(Verification verify,boolean shouldItError,Map<SeleniumElement,String> instanceData) throws Exception{

		
		CommonObjectsMSO.$ProvisioningErrorsTab.interact().click();
		ProvisioningErrorsMSO.$ProvisioningErrorsTable.forceProcessTable();
		QAProof.screenshot(ProvisioningErrorsMSO.$ProvisioningErrorsTable);
		
		if (shouldItError){
			if (ProvisioningErrorsMSO.$ProvisioningErrorsTable.isTableEmpty()){
				verify.reportIssue("Provisioning error table is empty when we are supposed to see the error.");
			}
			else {
				boolean isErrorSeen = false;
				provisionErrorTest1:
				for (int i=1;i<=ProvisioningErrorsMSO.$ProvisioningErrorsTable.getDataRowsCount();i++){
					String errorMessage = ProvisioningErrorsMSO.$ProvisioningErrorsTable.getValueFromTable(ProvisioningErrorsMSO.errorMessageHeading, i);
					if (errorMessage.contains(instanceData.get(NewEditApplicaionMSO.$MSOSAMLMetaDataURL))){
						verify.reportSuccess("Provisioning error seen correctly.");
						isErrorSeen=true;
						break provisionErrorTest1;
					}//if
				}			
				if(!isErrorSeen){
					verify.reportIssue("Provisioning error NOT seen as expected.");
				}			
			}//else -> if(!isErrorSeen)
		}//if (shouldItError)
		
		else {
			if (ProvisioningErrorsMSO.$ProvisioningErrorsTable.isTableEmpty()){
				verify.reportSuccess("Provisioning error table is empty (this is expected)");
			}//if (ProvisioningErrorsMSO.$ProvisioningErrorsTable.isTableEmpty())
			else {
				verify.reportIssue("Some provisioning errors seen when it is NOT expected to be seen.");
			}//else - if (ProvisioningErrorsMSO.$ProvisioningErrorsTable.isTableEmpty())
		}//else -> if (shouldItError)

	}

	
	public static void verifyUserPresenceInResults(Verification verify,boolean shouldUserBeSeen,Map<SeleniumElement,String>newUserVals) throws Exception{
		//Open new additional browser to test service provider application
		boolean isUserSeen = UserAdminPageResults.verifyIfUserExists(newUserVals.get(NewUserPageMSO.$AccountIDTextBox));
		
		if (isUserSeen && shouldUserBeSeen){
			verify.reportSuccess("User is seen in MirthResults (as per expectation)");
			
		}
		else if (isUserSeen && !shouldUserBeSeen){
			verify.reportIssue("User seen in the table when it is NOT supposed to be seen");
			
		}
		else if (!isUserSeen && shouldUserBeSeen){
			verify.reportIssue("User Not seen in the table)");
		}
		else if (!isUserSeen && !shouldUserBeSeen){
			verify.reportSuccess("User Not seen in the table (this is expected).");
		}//if elseif loop
		
	}

	
	
}//class
