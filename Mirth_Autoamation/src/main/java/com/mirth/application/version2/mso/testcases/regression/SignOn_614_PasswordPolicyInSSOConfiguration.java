package com.mirth.application.version2.mso.testcases.regression;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import com.mirth.application.master.General;
import com.mirth.application.master.PageObject;
import com.mirth.application.master.Testcase;
import com.mirth.application.version2.mso.pages.CommonObjectsMSO;
import com.mirth.application.version2.mso.pages.LoginPageMSO;
import com.mirth.application.version2.mso.pages.NewUserPageMSO;
import com.mirth.application.version2.mso.pages.SSOConfigurationPageMSO;
import com.mirth.application.version2.mso.pages.UsersPageMSO;
import com.mirth.application.version2.results.pages.CommonObjectsResults;
import com.mirth.application.version2.results.pages.LoginPageResults;
import com.mirth.exceptions.AppNotPresentException;
import com.mirth.exceptions.ClassPageMappingNotFoundException;
import com.mirth.exceptions.TestDataNotPresentException;
import com.mirth.results.QAProof;
import com.mirth.utility.general.MirthLib;
import com.mirth.utility.general.SleepLib;
import com.mirth.utility.general.SshClient;
import com.mirth.utility.seleniumLib.SeleniumElement;

public class SignOn_614_PasswordPolicyInSSOConfiguration extends Testcase{
	
	SshClient sshclient = null;
	
	
	@Override
	protected void steps() throws Exception {
	
		
		QAProof.writeInfo("Steps 1 to 3");
		General.openFreshBrowser("main", super.testInventory.browser);
		General.setBrowserWaitTime(10, TimeUnit.SECONDS);
		LoginPageMSO.launch();
		LoginPageMSO.login();
		CommonObjectsMSO.$SSOConfigurationTab.interact().click();

		SSOConfigurationPageMSO.$PasswordPolicyLink.interact().click();
		String minLength="5";
		String maxLength="10";		
		PageObject.setData(SSOConfigurationPageMSO.$MinLengthTextBox,minLength);
		PageObject.setData(SSOConfigurationPageMSO.$MaxLengthTextBox,maxLength);
		CommonObjectsMSO.$SaveChangesLinkInLeftPane.interact().click();
			
		QAProof.writeInfo("Step 4 to 9");
		CommonObjectsMSO.$UsersTab.interact().click();
		UsersPageMSO.$CreateUserLinkInUserActions.interact().click();
		
		String defaultPassword = PageObject.getData(NewUserPageMSO.$NewPassword);
		Map<SeleniumElement,String> changableFieldsUser1 = new LinkedHashMap<SeleniumElement,String>();
		changableFieldsUser1.put(NewUserPageMSO.$Chk_ChangePasswordNextLogin, "ON");
		changableFieldsUser1.put(NewUserPageMSO.$NewPassword, null);
		changableFieldsUser1.put(NewUserPageMSO.$ConfirmNewPassword, null);
		
		

		Map<SeleniumElement,String> user1Vals = NewUserPageMSO.createNewUserFromTestDataWithCustomReplace(changableFieldsUser1,true);
		
		user1Vals.put(NewUserPageMSO.$NewPassword, defaultPassword);
		user1Vals.put(NewUserPageMSO.$ConfirmNewPassword, defaultPassword);
		
		QAProof.writeInfo("Step 10");
		General.openAndPointToAddtionalBrowser("spBrowser", super.testInventory.browser);
		General.setBrowserWaitTime(10,TimeUnit.SECONDS);
		LoginPageResults.launch();
		
		LoginPageResults.login(user1Vals.get(NewUserPageMSO.$AccountIDTextBox),defaultPassword);
				
		QAProof.writeInfo("Step 11 to 13");
		String passwordErrorMessage = "Your new password does not conform to the stated guidelines.";
		LoginPageResults.setNewPassword("abc1");
		PageObject.verifyStringData(LoginPageResults.$PasswordErrorMessage, passwordErrorMessage,false);
		
		LoginPageResults.setNewPassword("abc12345678");
		PageObject.verifyStringData( LoginPageResults.$PasswordErrorMessage, passwordErrorMessage,false);

		String newCorrectPassword = "abc12345";
		LoginPageResults.setNewPassword(newCorrectPassword);
		CommonObjectsResults.$SignOutButtonOnKanaToolBar.interact().click();
		user1Vals.put(NewUserPageMSO.$NewPassword, newCorrectPassword);
		user1Vals.put(NewUserPageMSO.$ConfirmNewPassword, newCorrectPassword);
		
		QAProof.writeInfo("Step 14 to 16");
		General.switchToBrowser("main");
		CommonObjectsMSO.$SSOConfigurationTab.interact().click();
		SSOConfigurationPageMSO.$PasswordPolicyLink.interact().click();
		
		Map<SeleniumElement,String> passwordChangeableFields1 = new LinkedHashMap<SeleniumElement,String>();
		passwordChangeableFields1.put(SSOConfigurationPageMSO.$PasswordRetriesTextBox,"2");
		passwordChangeableFields1.put(SSOConfigurationPageMSO.$LockoutDurationTextBox,"1");
		passwordChangeableFields1.put(SSOConfigurationPageMSO.$NotifAdminAccountLockCheckBox,"ON");
		passwordChangeableFields1.put(SSOConfigurationPageMSO.$WarnUserLastLoginCheckBox,"ON");
		SSOConfigurationPageMSO.setPasswordPolicyFields(passwordChangeableFields1,true);
		
		QAProof.writeInfo("Step 17 and 18");		
		General.switchToBrowser("spBrowser");
		LoginPageResults.login(user1Vals.get(NewUserPageMSO.$AccountIDTextBox),"incorrectPassword");
		QAProof.screenshot(LoginPageResults.$LoginErrorMessageText);
		String loginErrorExpected = PageObject.td(LoginPageResults.class,"_WrongCredentialsWithLockWarningErrorMessage");
		loginErrorExpected = loginErrorExpected.replace("<ActualAccountID>", user1Vals.get(NewUserPageMSO.$AccountIDTextBox));
		String loginErrorActual = PageObject.getData(LoginPageResults.$LoginErrorMessageText);
		headingController.verifing("Login with wrong password for the 1st time)");
		boolean loginResult = verify.withReport(LoginPageResults.$LoginErrorMessageText.get()).isSame(loginErrorExpected, loginErrorActual, false);
		if (loginResult){
			verify.reportSuccess("User NOT able to login with wrong password as expected");
		}else{
			verify.reportIssue("Login with incorrect password did NOT behave as expected");
		}
		
		LoginPageResults.login(user1Vals.get(NewUserPageMSO.$AccountIDTextBox),"incorrectPassword");
		QAProof.screenshot(LoginPageResults.$LoginErrorMessageText);
		loginErrorExpected = PageObject.td(LoginPageResults.class,"_AccountLockedInvalidAttemptErrorMessage");
		loginErrorExpected = loginErrorExpected.replace("<ActualAccountID>", user1Vals.get(NewUserPageMSO.$AccountIDTextBox));
		loginErrorActual = PageObject.getData(LoginPageResults.$LoginErrorMessageText);
		headingController.verifing("Login with wrong password for the 2nd time)");
		loginResult = verify.withReport(LoginPageResults.$LoginErrorMessageText.get()).isSame(loginErrorExpected, loginErrorActual, false);
		if (loginResult){
			verify.reportSuccess("User NOT able to login with wrong password as expected. Waiting for 1 minute now.");
		}else{
			verify.reportIssue("Login with incorrect password did NOT behave as expected");
		}
		

		QAProof.writeInfo("Step 19");	
		System.out.println("Sleeping for 1 minute 2 seconds");
		SleepLib.sleep1Minute();
		SleepLib.sleep2Seconds();
		System.out.println("Sleeping finished");
		
		QAProof.writeText("After 1 minute");
		LoginPageResults.login(user1Vals.get(NewUserPageMSO.$AccountIDTextBox),user1Vals.get(NewUserPageMSO.$NewPassword));
		QAProof.screenshot();
		CommonObjectsResults.$SignOutButtonOnKanaToolBar.interact().click();
		General.closeAndDestroyBrowser("spBrowser");
		 
		
		QAProof.writeInfo("Step 20 to 24");	
		General.switchToBrowser("main");

		PageObject.setData(SSOConfigurationPageMSO.$PasswordNoOfDigitsTextBox,"2");
		CommonObjectsMSO.$SaveChangesLinkInLeftPane.interact().click();
		QAProof.screenshot(CommonObjectsMSO.$UsersTab);
		
		CommonObjectsMSO.$UsersTab.interact().click();
		CommonObjectsMSO.searchTextBoxFor(user1Vals.get(NewUserPageMSO.$AccountIDTextBox));
		
		UsersPageMSO.$UsersTable.forceProcessTable();
		UsersPageMSO.$UsersTable.clickFirstRowOfUsersTable(UsersPageMSO.usersTableAccountID);
		QAProof.screenshot(CommonObjectsMSO.$UsersTab);
		
		NewUserPageMSO.changePassword("abcde", true);
		PageObject.verifyStringData(NewUserPageMSO.$ErrorMessageInChangePasswordPopup, passwordErrorMessage,false);
		QAProof.screenshot();
		
		NewUserPageMSO.changePassword("abc1de", true);
		PageObject.verifyStringData(NewUserPageMSO.$ErrorMessageInChangePasswordPopup, passwordErrorMessage,false);
		QAProof.screenshot();
		
		newCorrectPassword = "abc1de2";
		NewUserPageMSO.changePassword(newCorrectPassword, true);
		String passwordChangeSuccessMessage = "Password changed successfully";
		PageObject.verifyStringData(CommonObjectsMSO.$InformationAlertAtTop, passwordChangeSuccessMessage,false);
		QAProof.screenshot();
		user1Vals.put(NewUserPageMSO.$NewPassword, newCorrectPassword);
		user1Vals.put(NewUserPageMSO.$ConfirmNewPassword, newCorrectPassword);
		
		QAProof.writeInfo("Step 25 to 27");
		CommonObjectsMSO.$SSOConfigurationTab.interact().click();
		SSOConfigurationPageMSO.$PasswordPolicyLink.interact().click();
		
		PageObject.setData(SSOConfigurationPageMSO.$PasswordNoOfDigitsTextBox,"0");
		CommonObjectsMSO.$SaveChangesLinkInLeftPane.interact().click();
		QAProof.screenshot(CommonObjectsMSO.$UsersTab);
		
		CommonObjectsMSO.$UsersTab.interact().click();
		CommonObjectsMSO.searchTextBoxFor(user1Vals.get(NewUserPageMSO.$AccountIDTextBox));
		UsersPageMSO.$UsersTable.clickFirstRowOfUsersTable(UsersPageMSO.usersTableAccountID);
		QAProof.screenshot(CommonObjectsMSO.$UsersTab);
		
		newCorrectPassword = "abcde";
		NewUserPageMSO.changePassword(newCorrectPassword, true);
		passwordChangeSuccessMessage = "Password changed successfully";
		PageObject.verifyStringData(CommonObjectsMSO.$InformationAlertAtTop, passwordChangeSuccessMessage,false);
		QAProof.screenshot();
		user1Vals.put(NewUserPageMSO.$NewPassword, newCorrectPassword);
		user1Vals.put(NewUserPageMSO.$ConfirmNewPassword, newCorrectPassword);
		
		QAProof.writeInfo("Step 28 to 30");
		CommonObjectsMSO.$SSOConfigurationTab.interact().click();
		SSOConfigurationPageMSO.$PasswordPolicyLink.interact().click();
		
		PageObject.setData(SSOConfigurationPageMSO.$PasswordHistorySizeTextBox,"3");
		PageObject.setData(SSOConfigurationPageMSO.$AllowPasswordReuseCheckBox,"ON");
		CommonObjectsMSO.$SaveChangesLinkInLeftPane.interact().click();
		QAProof.screenshot(CommonObjectsMSO.$UsersTab);
		
		CommonObjectsMSO.$UsersTab.interact().click();
		CommonObjectsMSO.searchTextBoxFor(user1Vals.get(NewUserPageMSO.$AccountIDTextBox));
		UsersPageMSO.$UsersTable.clickFirstRowOfUsersTable(UsersPageMSO.usersTableAccountID);
		QAProof.screenshot(CommonObjectsMSO.$UsersTab);
		
		newCorrectPassword = "abcde";
		NewUserPageMSO.changePassword(newCorrectPassword, true);
		passwordChangeSuccessMessage = "Password changed successfully";
		PageObject.verifyStringData(CommonObjectsMSO.$InformationAlertAtTop, passwordChangeSuccessMessage,false);
		QAProof.screenshot();
		
		NewUserPageMSO.changePassword(newCorrectPassword, true);
		passwordChangeSuccessMessage = "Password changed successfully";
		PageObject.verifyStringData(CommonObjectsMSO.$InformationAlertAtTop, passwordChangeSuccessMessage,false);
		QAProof.screenshot();
		
		CommonObjectsMSO.$LogoutLink.interact().click();
		General.closeAndDestroyBrowser("main");	
		
		
	}
	
	public void restartLDAPServer() throws IllegalAccessException, IllegalArgumentException, IOException, TestDataNotPresentException, ClassPageMappingNotFoundException, AppNotPresentException, InterruptedException{
		
		if (this.sshclient == null){
			
			String host = MirthLib.getServerNameOrIP();
			String sshUserName = PageObject.td(CommonObjectsMSO.class,"_sshUserName");
			String sshPassword = PageObject.td(CommonObjectsMSO.class,"_sshPassword");
			
			sshclient = new SshClient(host, sshUserName, sshPassword);			
		}
		
		String ldapRestartCommand = "service mirth-openldap restart";
		sshclient.runCommand(ldapRestartCommand);
		
	}

}
