package com.mirth.application.version2.mso.testcases;

import java.io.IOException;
import java.util.Properties;

import com.mirth.application.master.TestCaseMapping;

public class MSOTestCaseMapping implements TestCaseMapping{
	
	private MSOTestCaseMapping() {
		//Singleton
	}

	//singleton object
	private static MSOTestCaseMapping testCaseObject =null; 
	private Properties config;
	private String configFileName= "msoTestCaseConfig.properties";
	//Config Variable END----------------------
	
	public synchronized static MSOTestCaseMapping getObject() {
		
		if (MSOTestCaseMapping.testCaseObject == null) {
			MSOTestCaseMapping.testCaseObject = new MSOTestCaseMapping();
		}
		
		return MSOTestCaseMapping.testCaseObject;
	}
	
	
	private synchronized void initiateControllerConfig() throws IOException {
		config = new Properties();
		config.load(MSOTestCaseMapping.class.getResourceAsStream(configFileName));
		
	}//initiateControllerConfig() throws IOException
	
	
	public synchronized String getTestClassRelativeName(String configKey) throws IOException {
		
		if (isConfigPropertyNull()) {
			initiateControllerConfig();
		}
		
		return config.getProperty(configKey);
	}

	public synchronized boolean isConfigPropertyNull() {
		return (config==null)?true:false;
	}
	
}
