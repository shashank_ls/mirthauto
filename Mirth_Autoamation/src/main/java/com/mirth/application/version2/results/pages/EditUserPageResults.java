
package com.mirth.application.version2.results.pages;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import com.mirth.application.master.PageObject;
import com.mirth.application.version2.mso.pages.NewUserPageMSO;
import com.mirth.properties.InteractConfig;
import com.mirth.utility.general.StaticLib;
import com.mirth.utility.seleniumLib.CheckBoxS;
import com.mirth.utility.seleniumLib.PlainTextS;
import com.mirth.utility.seleniumLib.SeleniumElement;
import com.mirth.utility.seleniumLib.SimpleDropDownS;
import com.mirth.utility.seleniumLib.TableS;
import com.mirth.utility.seleniumLib.TextBoxS;

public final class EditUserPageResults extends PageObject{
	
	public static PlainTextS $AccountIDTextValue = new PlainTextS("AccountIDTextValue");
	public static TextBoxS $Name = new TextBoxS("Name");
	public static TextBoxS $Email = new TextBoxS("Email");

	public static TableS $SecurityRolesTable = new TableS("SecurityRolesTable");
	
	public static SimpleDropDownS $AccountStatusDropDown = new SimpleDropDownS("AccountStatusDropDown");
	public static CheckBoxS $Chk_ChangePasswordNextLogin = new CheckBoxS("Chk_ChangePasswordNextLogin");
	public static CheckBoxS $Chk_SecurityQuestion = new CheckBoxS("Chk_SecurityQuestion");

	
	public static TextBoxS $VoiceContact = new TextBoxS("VoiceContact");
	public static TextBoxS $FaxContact = new TextBoxS("FaxContact");
	public static TextBoxS $AddressLine1 = new TextBoxS("AddressLine1");
	public static TextBoxS $AddressLine2 = new TextBoxS("AddressLine2");
	public static TextBoxS $City = new TextBoxS("City");
	public static TextBoxS $State = new TextBoxS("State");
	public static TextBoxS $Postal = new TextBoxS("Postal");
	public static SimpleDropDownS $Country = new SimpleDropDownS("Country");

	
	//Element constants
	public static String nameInSecurityRoleTable = "Name";
	public static String descriptionInSecurityRoleTable = "Description";
	
	
	public static Map<SeleniumElement,Boolean> verifyUserDetailsAsPerMso(Map<SeleniumElement,String> actualMap) throws Exception{
		Map<SeleniumElement,String> verificationMap = StaticLib.getNewMapCopy(actualMap);
		actualMap = null;
		
		Map<SeleniumElement,Boolean> resultBooleanMap  = new LinkedHashMap<SeleniumElement,Boolean>();
		
		
		resultBooleanMap.put(NewUserPageMSO.$AccountIDTextBox, verifyStringData("AccountID",verificationMap.get(NewUserPageMSO.$AccountIDTextBox),getData($AccountIDTextValue),false));		
		resultBooleanMap.put(NewUserPageMSO.$Name, verifyStringData("Name",verificationMap.get(NewUserPageMSO.$Name),getData($Name),false));
		resultBooleanMap.put(NewUserPageMSO.$Email, verifyStringData("Email",verificationMap.get(NewUserPageMSO.$Email),getData($Email),false));
		
		//Check Table Data
		String[] securityRolesExpectedArray = verificationMap.get(NewUserPageMSO.$SelectListInAddSecurityRolePopup).split(Pattern.quote(InteractConfig.getValue("MultiSelectSeparator")));
		List<String> securityRolesExpectedList = Arrays.asList(securityRolesExpectedArray);
		$SecurityRolesTable.processTable();
		List<String> securityRolesActualList = $SecurityRolesTable.getAllColumnValuesAsListForHeader(nameInSecurityRoleTable);
		Boolean securityRoleTest = StaticLib.isContainsSameValues(securityRolesExpectedList, securityRolesActualList);
		if (securityRoleTest){
			printSuccessReport("Security Roles", securityRolesExpectedList.toString(), securityRolesActualList.toString());
			resultBooleanMap.put(NewUserPageMSO.$SelectListInAddSecurityRolePopup, securityRoleTest);
		}
		else {
			printFailureReport("Security Roles", securityRolesExpectedList.toString(), securityRolesActualList.toString());
			resultBooleanMap.put(NewUserPageMSO.$SelectListInAddSecurityRolePopup, securityRoleTest);
		}
		
		
		resultBooleanMap.put(NewUserPageMSO.$AccountStatusDropDown, verifyStringData("AccountStatusDropDown",verificationMap.get(NewUserPageMSO.$AccountStatusDropDown),getData($AccountStatusDropDown),false));
		resultBooleanMap.put(NewUserPageMSO.$Chk_ChangePasswordNextLogin, verifyStringData("Chk_ChangePasswordNextLogin",verificationMap.get(NewUserPageMSO.$Chk_ChangePasswordNextLogin),getData($Chk_ChangePasswordNextLogin),false));
		resultBooleanMap.put(NewUserPageMSO.$Chk_SecurityQuestion, verifyStringData("Chk_SecurityQuestion",verificationMap.get(NewUserPageMSO.$Chk_SecurityQuestion),getData($Chk_SecurityQuestion),false));
		
		resultBooleanMap.put(NewUserPageMSO.$VoiceContact, verifyStringData("VoiceContact",StaticLib.getUSPhoneFormat(verificationMap.get(NewUserPageMSO.$VoiceContact)),getData($VoiceContact),false));
		resultBooleanMap.put(NewUserPageMSO.$FaxContact, verifyStringData("FaxContact",StaticLib.getUSPhoneFormat(verificationMap.get(NewUserPageMSO.$FaxContact)),getData($FaxContact),false));
		resultBooleanMap.put(NewUserPageMSO.$AddressLine1, verifyStringData("AddressLine1",verificationMap.get(NewUserPageMSO.$AddressLine1),getData($AddressLine1),false));
		resultBooleanMap.put(NewUserPageMSO.$AddressLine2, verifyStringData("AddressLine2",verificationMap.get(NewUserPageMSO.$AddressLine2),getData($AddressLine2),false));
		resultBooleanMap.put(NewUserPageMSO.$City, verifyStringData("City",verificationMap.get(NewUserPageMSO.$City),getData($City),false));
		resultBooleanMap.put(NewUserPageMSO.$State, verifyStringData("State",verificationMap.get(NewUserPageMSO.$State),getData($State),false));
		resultBooleanMap.put(NewUserPageMSO.$Postal, verifyStringData("Postal",StaticLib.getUSPostalFormat(verificationMap.get(NewUserPageMSO.$Postal)),getData($Postal),false));
		resultBooleanMap.put(NewUserPageMSO.$Country, verifyStringData("Country",verificationMap.get(NewUserPageMSO.$Country),getData($Country),false));
		
		return resultBooleanMap;
				
	}
	

	
}//class
