package com.mirth.application.version2.mso.pages;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;

import com.mirth.application.master.General;
import com.mirth.application.master.PageObject;
import com.mirth.controller.ThreadController;
import com.mirth.exceptions.AppNotPresentException;
import com.mirth.exceptions.ClassPageMappingNotFoundException;
import com.mirth.exceptions.NotATableElementException;
import com.mirth.results.QAProof;
import com.mirth.utility.seleniumLib.SeleniumElement;
import com.mirth.utility.seleniumLib.TableS;
import com.mirth.utility.seleniumLib.TextBoxS;

public class RolesPageMSO extends PageObject {
	
	public static TableS $SecurityRolesTable = new TableS("SecurityRolesTable");
	public static TextBoxS $NameTextBox = new TextBoxS("NameTextBox");
	public static TextBoxS $DefaultURLTextBox = new TextBoxS("DefaultURLTextBox");
	public static TextBoxS $RoleDNTextBox = new TextBoxS("RoleDNTextBox");
	public static TextBoxS $RoleDescriptionTextBox = new TextBoxS("RoleDescriptionTextBox");
		
	public static final String nameHeading = "Name";
	public static final String userHeading = "Users";
	public static final String descriptionHeading = "Description";
	
	public static Map<SeleniumElement,String> createDefaultRole() throws IOException, ClassPageMappingNotFoundException, AppNotPresentException, NotATableElementException, Exception{
		
		return createRole(
					getProcessedTestData(RolesPageMSO.$NameTextBox),
					getProcessedTestData(RolesPageMSO.$DefaultURLTextBox),
					getProcessedTestData(RolesPageMSO.$RoleDNTextBox),
					getProcessedTestData(RolesPageMSO.$RoleDescriptionTextBox)	
				);
		
	}
	
	public static Map<SeleniumElement,String> createRole(String name, String defaultURL,String roleDN,String description) throws IOException, ClassPageMappingNotFoundException, AppNotPresentException, Exception{
	
		if (!General.checkIfElementExistsImmediately(RolesPageMSO.$NameTextBox)) {
			CommonObjectsMSO.$CreateLinkInLeftPane.interact().click();
		}
		
		Map<SeleniumElement,String> resultMap = new LinkedHashMap<SeleniumElement,String>();
		resultMap.put($NameTextBox, name);
		resultMap.put($DefaultURLTextBox, defaultURL);
		resultMap.put($RoleDNTextBox, roleDN);
		resultMap.put($RoleDescriptionTextBox, description);
		
		setData(resultMap);
		
		CommonObjectsMSO.$SaveChangesLinkInLeftPane.interact().click();
		QAProof.screenshot(CommonObjectsMSO.$BackToListLinkInLeftPane);
		CommonObjectsMSO.$BackToListLinkInLeftPane.interact().click();		
		
		return resultMap;
		
	}
	
	
	public static void deleteRole(String roleName) throws Exception{
		
		if (!General.checkIfElementVisibleImmediately($SecurityRolesTable)) {
			CommonObjectsMSO.$RolesTab.interact().click();						
		}
		
		$SecurityRolesTable.forceProcessTable();
		int rowNum = $SecurityRolesTable.findRowHavingText(roleName, nameHeading, false,true);
		
		if (rowNum < 1 ) {
			throw new IllegalArgumentException("IllegalArguementException: The roleName sent in arguement is NOT present in tempaltes table");
		}
		
		QAProof.writeInfo("Deleting template: " + roleName);
		$SecurityRolesTable.getCellTDElement(rowNum, 1).click();
		CommonObjectsMSO.$DeleteLinkInLeftPane.interact().click();
		General.acceptAlert();
		$SecurityRolesTable.forceProcessTable();
		rowNum = $SecurityRolesTable.findRowHavingText(roleName, nameHeading, false,true);
		QAProof.screenshot();
		
		if (rowNum>1){
			ThreadController.getVerifyOfCurrentThread().reportIssue("Role did NOT get deleted succesfully");
		}else{
			ThreadController.getVerifyOfCurrentThread().reportSuccess("Role deleted succesfully");
		}
			
	}	
	
	
}
