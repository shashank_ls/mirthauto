package com.mirth.results;

import java.io.File;
import java.io.IOException;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.openqa.selenium.WebDriver;

import com.mirth.controller.MirthLogger;
import com.mirth.controller.ThreadController;
import com.mirth.dataObjects.TestInventoryData;
import com.mirth.exceptions.AppNotPresentException;
import com.mirth.exceptions.ClassPageMappingNotFoundException;
import com.mirth.properties.ResultConfig;
import com.mirth.utility.general.Screenshot;
import com.mirth.utility.general.StaticLib;
import com.mirth.utility.microsoftDocX.TextImageDoc;
import com.mirth.utility.seleniumLib.SeleniumElement;

public class QAProof {
	
	private static String infoColor = "FF00FF";//Cyan

	public static TextImageDoc getCorrespondingImageDoc(TestInventoryData testInventory) {
		
		String screenshotFolderPath = StaticLib.appendFolderSeparatorIfNotPresent(
				ResultWriter.getRunResultFolderPath()+ResultConfig.getConfigVal("ScreenShotFolderName"));
		File screenShotFolder = new File(screenshotFolderPath);
		if (!screenShotFolder.exists()){
			synchronized(QAProof.class){
				if(!screenShotFolder.exists()) screenShotFolder.mkdir();
			}			
		}
		
		String 	screenShotAppFolderPath = 	StaticLib.appendFolderSeparatorIfNotPresent(screenshotFolderPath + testInventory.appName);
		File screenShotAppFolder = new File(screenShotAppFolderPath);
		if (!screenShotAppFolder.exists()) {
			synchronized(QAProof.class){
				if(!screenShotAppFolder.exists()) screenShotAppFolder.mkdir();
			}	
		}
		
		String textImageDocFileName = screenShotAppFolderPath + testInventory.automationScriptID + "." + ResultConfig.getConfigVal("QAProofDocFileExtension");
		return new TextImageDoc(textImageDocFileName);
		
	}//public static TextImageDoc getCorrespondingImageDoc(TestInventoryData testInventory) 
	
	public static void screenshot(SeleniumElement selElement) throws ClassPageMappingNotFoundException, AppNotPresentException, Exception{
		selElement.interact().isDisplayed();
		QAProof.screenshot();
	}
	
	public static void screenshot() throws IOException, InvalidFormatException {
		
		if (!ThreadController.getInventoryOfThread(Thread.currentThread().getId()).isQAProofNeed) {
			return;
		}
		
		WebDriver driver = ThreadController.getDriverControllerForThread(
				Thread.currentThread().getId() )
				.getCurrentMirthDriver().getDriver();
				
				
		if (driver == null) {
			return;
		}
		
		int width = Integer.parseInt(ResultConfig.getConfigVal("ScreenShotWidth"));
		int height = Integer.parseInt(ResultConfig.getConfigVal("ScreenShotHeight"));
		
		/*
		byte[] screenshotBytes = Screenshot.getScreenshotInBytes(driver);
			
		ThreadController.getTextImageDocOfThread(
				Thread.currentThread().getId() )
				.insertImage(screenshotBytes, width, height);				
		*/
		
		File screenshotFile = Screenshot.getScreenshotInFile(driver);
		ThreadController.getTextImageDocOfThread(
				Thread.currentThread().getId() )
				.insertImage(screenshotFile, width, height);
		
		MirthLogger.log("Screenshot taken.");
	}
	
	public static void writeText(String data, String colorInHex) throws IOException {
		
		if (!ThreadController.getInventoryOfThread(Thread.currentThread().getId()).isQAProofNeed) {
			return;
		}
		
		ThreadController.getTextImageDocOfThread(
				Thread.currentThread().getId() )
				.writeText(data, colorInHex);
		
		MirthLogger.log(data);
		
	}
	
	public static void writeText(String data) throws IOException {		
		writeText(data,"000000");
	}
	
	public static void writeInfo(String data) throws IOException{
		writeText(data,infoColor);
	}
	
}//class
