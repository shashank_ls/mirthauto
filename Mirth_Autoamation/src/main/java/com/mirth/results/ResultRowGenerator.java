package com.mirth.results;

import java.util.HashMap;
import java.util.Map;

import com.mirth.dataObjects.TestCaseResultData;

public class ResultRowGenerator {

	public static final String noRunRow = "NoRun_";
	public static final String runResultRow = "ResultRow_";
	public static final String noRunSerialSymbol = "#";
	
	private static Map<String,Map<Integer,String>> noRunEntryMap = new HashMap<String,Map<Integer,String>>();
	private static Map<String,Map<Integer,String>> actualResultsEntryMap = new HashMap<String,Map<Integer,String>>();
	
	public static String getNoRunTrEntry(String appName,int entryNumber){		
		
		return noRunEntryMap.get(appName).get(entryNumber);
		
	}
	
	public static int getCurentNoRunEntries(String appName){
		
		Map<Integer,String> entryMap = noRunEntryMap.get(appName);
		if (entryMap == null) {
			return 0;
		}
		
		return entryMap.size();
		
	}
	
	public static int getNextNoRunEntry(String appName){
			
		Map<Integer,String> entryMap = noRunEntryMap.get(appName);
		
		if (entryMap == null){
			return 1;
		}
		
		return (entryMap.size()+1);
		
	}

	public static int getNextActualResultsEntry(String appName){
		
		Map<Integer,String> entryMap = actualResultsEntryMap.get(appName);
		
		if (entryMap == null){
			return 1;
		}
		
		return (entryMap.size()+1);
		
	}

	
	public static String generateRow(TestCaseResultData resultData,boolean isNoRunRow){
		
		String trEntry = "";
		int entryNum = 0;
		
		Map<Integer,String> trEntryString =null;
		
		if (isNoRunRow){
			
			trEntryString =  noRunEntryMap.get(resultData.appName);
			if (trEntryString==null){				
				trEntryString = new HashMap<Integer,String>();
				noRunEntryMap.put(resultData.appName, trEntryString);											
			}
			
			entryNum = getNextNoRunEntry(resultData.appName);
			trEntryString.put(entryNum, trEntry);
			trEntry = "<tr id='"+noRunRow+entryNum+"'>\n";
			trEntry += "<td>"+noRunSerialSymbol+"</td>\n";
		}
		
		//if NOT a noRunRow
		else {
			trEntryString =  actualResultsEntryMap.get(resultData.appName);
			if (trEntryString==null){				
				trEntryString = new HashMap<Integer,String>();
				actualResultsEntryMap.put(resultData.appName, trEntryString);											
			}
			
			entryNum = getNextActualResultsEntry(resultData.appName);
			trEntryString.put(entryNum, trEntry);
			trEntry = "<tr id='"+runResultRow+entryNum+"'>\n";
			trEntry += "<td>"+entryNum+"</td>\n";	
		}
		
		
		trEntry += "<td>" + resultData.jiraID + "</td>\n";
		trEntry += "<td>" + resultData.scriptID + "</td>\n";
		trEntry += "<td>" + resultData.belongsToModules + "</td>\n";
		trEntry += "<td>" + resultData.scriptName + "</td>\n";
		trEntry += "<td>" + resultData.browserName + "</td>\n";
		
		if (isNoRunRow){
			trEntry += "<td style=\"color: blue\">No Run</td>\n";
		}//if (isNoRunRow){
		
		else {
			
			if (resultData.getResult()==true){
				trEntry += "<td style=\"color: green\">Pass</td>\n";
			}//if (resultData.getResult()==true)
			
			else {
				trEntry += "<td style=\"color: red\">Fail</td>\n";
			}//else - //if (resultData.getResult()==true)
			
		}//else - if (isNoRunRow)	
				
		trEntry += "<td>" + resultData.getIssues() + "</td>\n";
		trEntry += "<td>" + resultData.getMiscData() + "</td>\n";
		trEntry += "<td>" + resultData.startDateTime.replace(" ", "<br/>") + "</td>\n";
		trEntry += "<td>" + resultData.endDateTime.replace(" ", "<br/>") + "</td>\n";		
				
		trEntry += "</tr>";	
		
		//fillData to Maps
		trEntryString.put(entryNum, trEntry);		
		
		return trEntry;			
		
	}
	
}
