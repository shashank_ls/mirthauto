package com.mirth.results;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;

import com.mirth.controller.Assertions;
import com.mirth.controller.DriverScript;
import com.mirth.controller.Executor;
import com.mirth.controller.MirthLogger;
import com.mirth.controller.TestOrganizer;
import com.mirth.controller.ThreadController;
import com.mirth.dataObjects.ApplicationResultData;
import com.mirth.dataObjects.ControllerData;
import com.mirth.dataObjects.SynchronizedDataQueue;
import com.mirth.dataObjects.TestCaseResultData;
import com.mirth.dataObjects.TestInventoryData;
import com.mirth.properties.ControllerConfig;
import com.mirth.properties.ResultConfig;
import com.mirth.properties.TestDataConfig;
import com.mirth.utility.general.SleepLib;
import com.mirth.utility.general.StaticLib;
import com.mirth.utility.general.StringLib;
import com.mirth.utility.general.TextFileEditor;
import com.mirth.utility.spreadsheet.CellFormatting;
import com.mirth.utility.spreadsheet.MicrosoftSpreadsheet;


//Need to implement multiple reports for each project
//Need to implement Attribute value properties.

public class ResultWriter implements Runnable{
	
	private static boolean isExcel=true;
	private static boolean isHTML=true;	
	
	private static boolean isResultWriterInitiated = false;
	private static ResultWriter thisWriter;
	private static String resultFolderPath;
	private static Map<String,ApplicationResultData> appResultContainer = new HashMap<String,ApplicationResultData>();
	private static Map<String,Integer> testCaseHeaderRow = new HashMap<String,Integer>();
	private static SynchronizedDataQueue<TestCaseResultData> testResultContainer = new SynchronizedDataQueue<TestCaseResultData>();
	private static boolean isResultWriterFinished=false;
	private static int resultStallTimeInSeconds = 1;
	private static String noAppDataString = "<Unknown>";
	private static String runName="";
	private static String runNameSeparator="";	
	private static String runResultFolderPath = null;
	
	//New data fields - 23/May/2016
	private static boolean resultThreadReady = false;
	private static LinkedList<TestInventoryData> allTestCases;
	
	//this map is to keep track and removie no run entries.
	private static Map<String,Integer> resultEntryMap = new HashMap<String,Integer>();
	
	
	//private static String scriptBrowserSeparator = "#:#";
	private static Map<String,TextFileEditor> htmlReportPath = new HashMap<String,TextFileEditor>();
	private static Map<String,Integer> numberOfResults= new HashMap<String,Integer>();
	
	private static Map<String,Integer> passed= new HashMap<String,Integer>();
	private static Map<String,Integer> failed= new HashMap<String,Integer>();
	
	private static String runID = "";
	
	
	
	public static boolean isResultThreadReady(){
		return resultThreadReady;
	}
	
	public static String getRunResultFolderPath() {
		return ResultWriter.runResultFolderPath;
	}

//
	private static Map<String, Integer> nextTestCaseNumMap = new HashMap<String,Integer>();
	
	private static String getNextTestCaseNumber(String appName) {
		Integer num = nextTestCaseNumMap.get(appName);
		
		if(Assertions.isNull(num)) {
			num = 1;
		}
		
		nextTestCaseNumMap.put(appName, num+1);	
		
		return num.toString();
	}
	
	private static int getTestCaseHeaderRow(String appName) {
		
		Integer num = testCaseHeaderRow.get(appName);
		
		if(Assertions.isNull(num)) {
			num = 1;
		}
		
		return num;
		
	}
	
	private static void putTestCaseHeaderRow(String appName,int rowNum) {
		
		testCaseHeaderRow.put(appName, rowNum);
		
	}
	
	public static boolean getIsResultWriterFinished() {
		return isResultWriterFinished;
	}
	
	private ResultWriter() {
		//Singleton Object
	}
	
	public static ResultWriter getWriter() {
		
		if (thisWriter == null) {
			thisWriter = new ResultWriter();
		}
		
		return thisWriter;
	}
	
	public static synchronized void addTestResult(TestCaseResultData testResult){
		//putData is synchronized internally.
		testResultContainer.putData(testResult);
	}
	
	
	public static synchronized void addAppResult(ApplicationResultData appResult){
		appResultContainer.put(appResult.appName, appResult);		
	}	
	
	private static synchronized TestCaseResultData getNextTestResult(){
		//putData is synchronized internally.
		return testResultContainer.getNextData();
	}
	
	private static synchronized boolean isResultContainerEmpty(){
		//putData is synchronized internally.
		return testResultContainer.isContainerEmpty();
	}
	
	private static synchronized ApplicationResultData getAppResult(String appName){
		return appResultContainer.get(appName);	
	}
	
	
	@Override
	public void run() {
		
		if (ResultWriter.isResultWriterInitiated) {
			//Result writer already started. It has been called incorrectly. So quit thread!!
			return;
		}
		ResultWriter.isResultWriterInitiated = true;
		
		
			
				try {
					ThreadController.updateThreadLogMapping(
																Thread.currentThread().getId(),
																MirthLogger.getNewResultsThreadLogFile()
														   );
					if (TestOrganizer.resultReportType.equals(ControllerConfig.Excel)){
						isHTML=false;
						isExcel=true;
					}else if (TestOrganizer.resultReportType.equals(ControllerConfig.HTML)){
						isExcel=false;
						isHTML=true;					
					} 
					
					processResults();

				} catch (IllegalAccessException | IllegalArgumentException | InterruptedException | 
						IOException | InstantiationException | ClassNotFoundException e) {
					
					e.printStackTrace();
					
					//issue with result reporting.. Exit execution as there is no point in executing without reporting results
					System.out.println("Issue with result Writing!!!");
					e.printStackTrace();
					System.out.println("Issue with result Writing!!!");
					System.exit(1);
				}
				
	}//public void run()
	
	
	private void processResults() throws InterruptedException, IOException, IllegalAccessException, IllegalArgumentException, InstantiationException, ClassNotFoundException {

		MirthLogger.log("Starting Results Thread!!");
				
		String resultTemplatePath = StaticLib.appendFolderSeparatorIfNotPresent(StaticLib.getFrameWorkPackageWithSeparator()+TestDataConfig.getValue("TestDataFolderName")) + "resultReportTemplate";
		TextFileEditor repFile = new TextFileEditor(new File(resultTemplatePath));
		
		runID = StaticLib.getCustomDateTimeNameInFormat(DriverScript.executionStartTime,runName+runNameSeparator, ResultConfig.RunDateTimeFormat);

		
		//Create a spreadsheet
		String runSpreadSheetPath = ResultWriter.runResultFolderPath + runID;
		runSpreadSheetPath+= "."+ResultConfig.ResultSpreadSheetFormat;
		MicrosoftSpreadsheet resultXL = new MicrosoftSpreadsheet(runSpreadSheetPath);        
        
        /*        
        repFile.copyFile(htmlResultFile,true);
        repFile=null;
        
        TextFileEditor resultHTML = new TextFileEditor(htmlResultFile);
        */
        
        
		MirthLogger.log("Pointed to Results file path");	
		
		
		//Copy all test inventory to results sheet with no run status and set Execution start flag
		allTestCases = Executor.testContainer.getCopyOfAllData(); 
		for (TestInventoryData tInv: allTestCases){
			
			//If new application, do initializations
			if (appResultContainer.get(tInv.appName)==null){			

				
		        //Need to update application Results
				ApplicationResultData appResultsData = new ApplicationResultData();				
				appResultsData.appName = tInv.appName;
				appResultsData.environment = ControllerData.getAppDataMap().get(tInv.appName).getLocalApplicationAttributesMap().get(ControllerConfig.TestServerSH);
				appResultsData.buildNumber = "Unknown";
				appResultsData.runStart = "";
				appResultsData.runEnd = "";
				appResultsData.runID = runID;
				appResultContainer.put(tInv.appName, appResultsData);
				
				
				numberOfResults.put(tInv.appName, 0);
				passed.put(tInv.appName, 0);
				failed.put(tInv.appName, 0);
				
				
				String runHTMLpath = ResultWriter.runResultFolderPath + tInv.appName+runNameSeparator+runID;
		        runHTMLpath += "."+"html";
		        File htmlResultFile = new File(runHTMLpath);
		        repFile.copyFile(htmlResultFile,true);
		        TextFileEditor resultHTML = new TextFileEditor(htmlResultFile);
		        htmlReportPath.put(tInv.appName, resultHTML);	

		        
				setHTMLTagField(resultHTML,"ApplicationName",appResultsData.appName);
				setHTMLTagField(resultHTML,"ApplicationEnv",appResultsData.environment);
				setHTMLTagField(resultHTML,"ApplicationBuild",appResultsData.buildNumber);
				setHTMLTagField(resultHTML,"ApplicaitonRunStart",appResultsData.runStart);
				setHTMLTagField(resultHTML,"ApplicationRunEnd",appResultsData.runEnd);
				setHTMLTagField(resultHTML,"ApplicationRunID",appResultsData.runID);
		        
				setHTMLTagField(resultHTML,"TestCaseTotal",0);
				setHTMLTagField(resultHTML,"TestCaseNoRun",0);
				setHTMLTagField(resultHTML,"TestCasePass",0);
				setHTMLTagField(resultHTML,"TestCaseFail",0);

				
				setHTMLTagField(resultHTML,"PercentagePass",0);
				setHTMLTagField(resultHTML,"PercentageFail",0);
				setHTMLTagField(resultHTML,"PercentageNoRun",100);
		        
				
	
				
			}		
			
			TestCaseResultData resultData = new TestCaseResultData();
			resultData.appName = tInv.appName;
			resultData.jiraID = tInv.manualTestID;
			resultData.scriptID = tInv.automationScriptID;
			resultData.scriptName = tInv.automationScriptName;
			resultData.belongsToModules = StaticLib.getStringListValuesAsString(tInv.belongsToModulesList, ",");
			resultData.browserName = tInv.browserString;

			htmlWrite(resultData,true);
			//the below line has to be present only after calling htmlWrite function.
			resultEntryMap.put(tInv.appName+tInv.automationScriptID+tInv.browserString, ResultRowGenerator.getCurentNoRunEntries(tInv.appName));
			//------------------
			
			int numOfResForApp = numberOfResults.get(tInv.appName);
			numberOfResults.put(tInv.appName,++numOfResForApp);
			
			
			setHTMLTagField(htmlReportPath.get(tInv.appName),"TestCaseTotal",numberOfResults.get(tInv.appName));
			setHTMLTagField(htmlReportPath.get(tInv.appName),"TestCaseNoRun",numberOfResults.get(tInv.appName));			
			
		}
		
		resultThreadReady = true;
        	
		//Run forever until all the execution is completed.
		while(true) {
			
			//if Result container is NOT empty keep on writing results!!
			while (!isResultContainerEmpty()){
				TestCaseResultData resultData = getNextTestResult();
				
				if(resultData.getResult()==true){					
					int passedAppTC = passed.get(resultData.appName);
					passed.put(resultData.appName,++passedAppTC);
				}
				else if (resultData.getResult()==false){
					int failedAppTC = failed.get(resultData.appName);
					failed.put(resultData.appName,++failedAppTC);
				}
				
				if (isHTML){					
					htmlWrite(resultData);		

				}//if (isHTML)
				
				//Check1
				if (isExcel) {						
					excelWrite(resultData,resultXL);					
				}//if (isExcel)
				

				
				
			}//while (!isResultContainerEmpty()){
			
			
			//Check2
			if (isExcel){
				resultXL.closeXl();
			}
			if (isHTML){
				//not required to do anything
			}
			
			//this block should be in the bottom of while loop
			if (Executor.isExecutionCompleted) {
				
				if (ResultWriter.isResultContainerEmpty()) {
					break;
				}//if (ResultWriter.getIsFinishedWithResults())
				
			}//if (Executor.isExecutionCompleted)			
			
			SleepLib.sleepSeconds(resultStallTimeInSeconds);
			
		}//while(true) 
		
		//Check3
		if (isExcel){
			if (!resultXL.isWorkBookPresent()) {
				resultXL.createWorkbook("No Execution", false);
				resultXL.swithchToSheet("No Execution");	
				
				CellFormatting cellFormat= new CellFormatting();
				cellFormat.boldFont = true;
				cellFormat.fontColor = IndexedColors.RED.getIndex();
				cellFormat.fontHeight = 20;
				
				resultXL.writeCell("A1", "None of the test cases were executed!!", cellFormat, true);
			}//if (!resultXL.isWorkBookPresent()) 
			resultXL.closeXl();
		}//(isExcel3)
		
		if (isHTML){
			
			//housekeeping at last for HTML
			
		}//(isHTML3)
		
		
		ResultWriter.isResultWriterFinished = true;
		System.out.println("ResultWriterDone!!!");
		ThreadController.getLogFileOfThread(Thread.currentThread().getId()).close();
	}//private void processResults() throws InterruptedException
	
	
	private static void excelWrite(TestCaseResultData resultData,MicrosoftSpreadsheet resultXL) throws IOException, IllegalAccessException, IllegalArgumentException{
		
		File resultXLFile = new File(resultXL.getFilePath());
		MirthLogger.log("Writing excel result for test case: " + resultData.scriptID +" for app: "+resultData.appName);
		if (!resultXLFile.exists()) {
			resultXL.createWorkbook(resultData.appName,true);
			
		}				
						
		if (!resultXL.isSheetPresent(resultData.appName)){
			resultXL.createSheet(resultData.appName);
		}
		
		//Switch to appropriateSheet
		resultXL.closeXl();
		MirthLogger.log("Switching to sheet: "+resultData.appName);
		resultXL.swithchToSheet(resultData.appName);
		int maxRow = resultXL.getMaxRows();
		MirthLogger.log("Max rows in "+resultData.appName + " sheet as of now is " + maxRow);
										
		//If Appresult is NOT written, then write it or leave space for it
		ApplicationResultData appResult = getAppResult(resultData.appName);
		
		//in case app result is not passed, put blank data
		if (appResult==null) {
			appResult = new ApplicationResultData();
			appResult.appName = resultData.appName;
			addAppResult(appResult);					
		}
		
		if (!appResult.isWritten) {
			MirthLogger.log("Writing applicaion related result");
			//Logic for writing appResult
			maxRow = 1;
			
			CellFormatting cellFormat = new CellFormatting();
			cellFormat.foregroundColor = IndexedColors.BLACK.getIndex();
			cellFormat.fontColor=IndexedColors.GOLD.getIndex();
			cellFormat.boldFont = true;
			cellFormat.borderType = CellStyle.BORDER_THICK;
			cellFormat.fullBorders = true;
			cellFormat.fontHeight = 14;
			cellFormat.alignMent = CellStyle.ALIGN_CENTER;
			
			resultXL.writeCell(maxRow,1,ResultConfig.RunInformationH,cellFormat,false);
			//below is a dummy write for Border to appear around Run Information
			resultXL.writeCell(maxRow++,2,"",cellFormat,false);
			
			resultXL.mergeColumnsInRow(1,1,2);
			
			cellFormat.fontHeight = Font.SS_NONE;
			cellFormat.fontColor=IndexedColors.BLACK.getIndex();
			cellFormat.foregroundColor = IndexedColors.GOLD.getIndex();
			resultXL.writeCell(maxRow,1,ResultConfig.AppHeadingConfigH,cellFormat,false);
			resultXL.writeCell(maxRow++,2,ResultConfig.AppHeadingValueH,cellFormat,false);
			
			cellFormat.alignMent = CellStyle.ALIGN_GENERAL;
			cellFormat.foregroundColor = CellStyle.NO_FILL;
			cellFormat.fullBorders = false;
			cellFormat.bucketBorder = true;
			cellFormat.borderType = CellStyle.BORDER_THIN;
			cellFormat.boldFont = false;
			resultXL.writeCell(maxRow,1,ResultConfig.ApplicationNameH,cellFormat,false);
			resultXL.writeCell(maxRow++,2,StringLib.getNotNullString(appResult.appName, noAppDataString),cellFormat,false);
			
			resultXL.writeCell(maxRow,1,ResultConfig.EnvironmentH,cellFormat,false);
			resultXL.writeCell(maxRow++,2,StringLib.getNotNullString(appResult.environment,noAppDataString),cellFormat,false);
			
			resultXL.writeCell(maxRow,1,ResultConfig.BuildNumberH,cellFormat,false);
			resultXL.writeCell(maxRow++,2,StringLib.getNotNullString(appResult.buildNumber,noAppDataString),cellFormat,false);
			
			resultXL.writeCell(maxRow,1,ResultConfig.RunStartH,cellFormat,false);
			resultXL.writeCell(maxRow++,2,StringLib.getNotNullString(appResult.runStart,noAppDataString),cellFormat,false);
			
			resultXL.writeCell(maxRow,1,ResultConfig.RunEndH,cellFormat,false);
			resultXL.writeCell(maxRow++,2,StringLib.getNotNullString(appResult.runEnd,noAppDataString),cellFormat,false);
			
			resultXL.writeCell(maxRow,1,ResultConfig.RunIDH,cellFormat,false);		
			resultXL.writeCell(maxRow++,2,StringLib.getNotNullString(appResult.runID,noAppDataString),cellFormat,false);
								
			appResult.isWritten = true;
			
			//Mandatorily print this Heading
			maxRow+=2;
			
			cellFormat =null;
			cellFormat = new CellFormatting();
			cellFormat.foregroundColor = IndexedColors.SKY_BLUE.getIndex();
			cellFormat.boldFont = true;
			cellFormat.borderType = CellStyle.BORDER_THICK;
			cellFormat.fullBorders = true;
			cellFormat.alignMent = CellStyle.ALIGN_CENTER;
			
			
			resultXL.writeCell(maxRow,1,ResultConfig.TestCaseNumber,cellFormat,false);
			resultXL.writeCell(maxRow,2,ResultConfig.StartDateTime,cellFormat,false);
			resultXL.writeCell(maxRow,3,ResultConfig.EndDateTime,cellFormat,false);
			resultXL.writeCell(maxRow,4,ResultConfig.JiraID,cellFormat,false);
			resultXL.writeCell(maxRow,5,ResultConfig.ScriptID,cellFormat,false);
			resultXL.writeCell(maxRow,6,ResultConfig.TestName,cellFormat,false);
			resultXL.writeCell(maxRow,7,ResultConfig.BelongsToModule,cellFormat,false);
			resultXL.writeCell(maxRow,8,ResultConfig.Browser,cellFormat,false);
			resultXL.writeCell(maxRow,9,ResultConfig.Result,cellFormat,false);
			resultXL.writeCell(maxRow,10,ResultConfig.Issues,cellFormat,false);
			resultXL.writeCell(maxRow,11,ResultConfig.MiscData,cellFormat,false);
			
			putTestCaseHeaderRow(resultData.appName, maxRow);
			resultXL.save();
			resultXL.closeXl();
			resultXL.swithchToSheet(resultData.appName);
			MirthLogger.log("Finished writing applicaion related result");
		}//if (!appResult.isWritten)
		
		CellFormatting cellFormat = new CellFormatting();
		cellFormat.boldFont = false;
		cellFormat.borderType = CellStyle.BORDER_THIN;
		cellFormat.bucketBorder = true;
		
		//if control reaches here, then AppResult and test heading is already Written. So, just write the result row.
		MirthLogger.log("Writing actual test result to row now.");
		maxRow++;
		int headingRowOfExcel=getTestCaseHeaderRow(resultData.appName);
		resultXL.writeCell(headingRowOfExcel,ResultConfig.TestCaseNumber,maxRow,"["+getNextTestCaseNumber(resultData.appName)+"]",cellFormat,false);
		resultXL.writeCell(headingRowOfExcel,ResultConfig.StartDateTime,maxRow,resultData.startDateTime,cellFormat,false);
		resultXL.writeCell(headingRowOfExcel,ResultConfig.EndDateTime,maxRow,resultData.endDateTime,cellFormat,false);
		resultXL.writeCell(headingRowOfExcel,ResultConfig.JiraID,maxRow,resultData.jiraID,cellFormat,false);
		resultXL.writeCell(headingRowOfExcel,ResultConfig.ScriptID,maxRow,resultData.scriptID,cellFormat,false);
		resultXL.writeCell(headingRowOfExcel,ResultConfig.TestName,maxRow,resultData.scriptName,cellFormat,false);
		resultXL.writeCell(headingRowOfExcel,ResultConfig.BelongsToModule,maxRow,resultData.belongsToModules,cellFormat,false);
		resultXL.writeCell(headingRowOfExcel,ResultConfig.Browser,maxRow,resultData.browserName,cellFormat,false);
		resultXL.writeCell(headingRowOfExcel,ResultConfig.Issues,maxRow,resultData.getIssues(),cellFormat,false);
		resultXL.writeCell(headingRowOfExcel,ResultConfig.MiscData,maxRow,resultData.getMiscData(),cellFormat,false);	

		//Write result data in color
		cellFormat.boldFont = true;			
		String resultString = resultData.getResultString();
		if (resultString.equalsIgnoreCase(ResultConfig.Pass)) {
			cellFormat.fontColor = IndexedColors.GREEN.getIndex();
		}				
		else {
			cellFormat.fontColor = IndexedColors.RED.getIndex();
		}
		resultXL.writeCell(headingRowOfExcel,ResultConfig.Result,maxRow,resultString,cellFormat,false);
		
		MirthLogger.log("Auto sizing columns in sheet: "+resultData.appName);
		resultXL.autoSizeSheet(resultData.appName,testCaseHeaderRow.get(resultData.appName));
		resultXL.save();
	}
	
	private static void htmlWrite(TestCaseResultData resultData) throws IOException{
		htmlWrite(resultData,false);
	}
	
	private static void htmlWrite(TestCaseResultData resultData,boolean isNoRunEntry) throws IOException{
		TextFileEditor resultHTML = htmlReportPath.get(resultData.appName);
		//System.out.println(resultData.appName+": "+resultHTML.getFilePathAsString());
		
		String trEntry = ResultRowGenerator.generateRow(resultData,isNoRunEntry);
		//System.out.println(trEntry);
		
		if (isNoRunEntry){		
			resultHTML.replaceString(ResultConfig.getConfigVal("TextCaseRowEntryEnd"), 
					trEntry+"\n\n"+ResultConfig.getConfigVal("TextCaseRowEntryEnd"));			
		}
		
		else {
			
			int passedAppTC = passed.get(resultData.appName);
			if (passedAppTC==1){
				appResultContainer.get(resultData.appName).runStart = resultData.startDateTime;
				setHTMLTagField(resultHTML,"ApplicaitonRunStart", resultData.startDateTime);
			}
			
			
			int oldEntryNum = resultEntryMap.get(resultData.appName+resultData.scriptID+resultData.browserName);
			resultHTML.replaceString(ResultRowGenerator.getNoRunTrEntry(resultData.appName,oldEntryNum), "");
			
			resultHTML.replaceString(ResultConfig.getConfigVal("TextCaseRowEntryNoRun"),
					trEntry+"\n\n"+ResultConfig.getConfigVal("TextCaseRowEntryNoRun"));	
			
			appResultContainer.get(resultData.appName).runStart = resultData.endDateTime;
			setHTMLTagField(resultHTML,"ApplicationRunEnd",resultData.endDateTime);

		}

		int noRun=numberOfResults.get(resultData.appName)-(passed.get(resultData.appName)+failed.get(resultData.appName));
		
		
		float passRep = passed.get(resultData.appName);
		float failRep = failed.get(resultData.appName);
		float noRunRep = noRun;
		
		float passPercentage = 100*(passRep/numberOfResults.get(resultData.appName));
		float failPercentage = 100*(failRep/numberOfResults.get(resultData.appName));		
		float noRunPercentage =100*(noRunRep/numberOfResults.get(resultData.appName));
		
		setHTMLTagField(resultHTML,"TestCasePass",passed.get(resultData.appName));
		setHTMLTagField(resultHTML,"TestCaseFail",failed.get(resultData.appName));
		setHTMLTagField(resultHTML,"TestCaseNoRun",noRun);
		
		setHTMLTagField(resultHTML,"PercentagePass",Math.round(passPercentage));
		setHTMLTagField(resultHTML,"PercentageFail",Math.round(failPercentage));
		setHTMLTagField(resultHTML,"PercentageNoRun",Math.round(noRunPercentage));	
		
	}
	
	
	public static void createRequiredResultFolders() throws IOException {
		MirthLogger.methodEntry();
		//Create a resultFolder if not exists.
		ResultWriter.resultFolderPath = StaticLib.appendFolderSeparatorIfNotPresent(StaticLib.getFrameWrorkFolderWithSeparator() + ResultConfig.ResultFolderName);
		File resultFolder = new File(ResultWriter.resultFolderPath);
		if (!resultFolder.exists()) {
			resultFolder.mkdir();
		}//if (!resultFolder.exists()) 
				
		//Create a run folder
		runName = TestOrganizer.runResultPrefix;
		if (!Assertions.isNotEmpty(runName)) {
			runName = ResultConfig.RunPrefix;
		}//if (runFolderName==null || runFolderName.equalsIgnoreCase(""))
		
		runNameSeparator = "_";
		//Below code can be used if we need a folder for each run
		
		String runFolderName=StaticLib.getCustomDateTimeNameInFormat(DriverScript.executionStartTime,runName+runNameSeparator, ResultConfig.RunDateTimeFormat);
		String runFolderPath = StaticLib.appendFolderSeparatorIfNotPresent(ResultWriter.resultFolderPath + runFolderName);
		
		File runFolder = new File(runFolderPath);
		if (!runFolder.exists()) {
			runFolder.mkdir();
		}//if (!runFolder.exists())
		
		//Now, ready for ThreadExecution.
		ResultWriter.runResultFolderPath = runFolderPath;
		MirthLogger.methodExit();
		
	}
	
	private static void setHTMLTagField(TextFileEditor resultHTML,String id,int val) throws IOException{
		
		setHTMLTagField(resultHTML, id, val+"");
	}	
	
	private static void setHTMLTagField(TextFileEditor resultHTML,String id,String val) throws IOException{
		
		String stringToBeReplaced = "id='"+ResultConfig.getConfigVal(id)+"'>"+".*</";
		
		String replacementString = "id='"+ResultConfig.getConfigVal(id)+"'>"+val+"</";
		
		resultHTML.replaceString(stringToBeReplaced,replacementString);
		
	}
	
}//class
