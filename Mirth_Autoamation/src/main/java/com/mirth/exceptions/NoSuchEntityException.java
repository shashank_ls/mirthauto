package com.mirth.exceptions;

import java.io.IOException;

import com.mirth.controller.MirthLogger;
import com.mirth.utility.general.StaticLib;

public class NoSuchEntityException extends Exception {
	
	String entity;
	
	public NoSuchEntityException(String eName) throws IOException{
		entity = eName;
		System.out.println(eName);
		MirthLogger.log("Exception while using the value: "+eName + "\n" + StaticLib.getExceptionAsString(this));
	}
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
