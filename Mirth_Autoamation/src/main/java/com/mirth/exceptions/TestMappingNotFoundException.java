package com.mirth.exceptions;

import java.io.IOException;

import com.mirth.controller.MirthLogger;
import com.mirth.utility.general.StaticLib;

public class TestMappingNotFoundException extends Exception {
	
	String testAppName;
	
	public TestMappingNotFoundException(String appName) throws IOException{
		testAppName = appName;
		System.out.println(appName);
		MirthLogger.log("Exception while using the value: "+appName + "\n" + StaticLib.getExceptionAsString(this));
	}
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
