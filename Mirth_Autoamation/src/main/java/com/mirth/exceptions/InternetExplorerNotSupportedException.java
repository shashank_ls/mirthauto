package com.mirth.exceptions;

import java.io.IOException;

import com.mirth.controller.MirthLogger;
import com.mirth.utility.general.StaticLib;

public class InternetExplorerNotSupportedException extends Exception {
	public InternetExplorerNotSupportedException() throws IOException{
		MirthLogger.log("InternetExplorer is NOT supported in this Machine\n" + StaticLib.getExceptionAsString(this));
	}
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
