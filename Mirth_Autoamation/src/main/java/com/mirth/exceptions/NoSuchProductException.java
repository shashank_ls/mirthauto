package com.mirth.exceptions;

import java.io.IOException;

import com.mirth.controller.MirthLogger;
import com.mirth.utility.general.StaticLib;

public class NoSuchProductException extends Exception {

	public String productName;
	
	public NoSuchProductException(String product) throws IOException {
		productName = product;
		System.out.println(productName);
		MirthLogger.log("Exception while using the value: "+product + "\n" + StaticLib.getExceptionAsString(this));
		//Log
	}
	
	private static final long serialVersionUID = 1L;

}
