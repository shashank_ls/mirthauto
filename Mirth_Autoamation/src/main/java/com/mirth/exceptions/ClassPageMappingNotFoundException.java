package com.mirth.exceptions;

import java.io.IOException;

import com.mirth.controller.MirthLogger;
import com.mirth.utility.general.StaticLib;

public class ClassPageMappingNotFoundException extends Exception {
	
	String testAppName;
	
	public ClassPageMappingNotFoundException(String appName) throws IOException{
		testAppName = appName;
		MirthLogger.log("Exception while using the value: "+appName + "\n" + StaticLib.getExceptionAsString(this));
		System.out.println(appName);
	}
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
