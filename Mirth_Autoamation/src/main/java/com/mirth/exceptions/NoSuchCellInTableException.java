package com.mirth.exceptions;

import java.io.IOException;

import com.mirth.controller.MirthLogger;
import com.mirth.utility.general.StaticLib;

public class NoSuchCellInTableException extends Exception {
	
	int row;
	int col;
	
	public NoSuchCellInTableException(int r, int c) throws IOException{
		this.row = r;
		this.col = c;
		System.out.println(row+","+col);
		MirthLogger.log("Exception while using the values: {"+row + ","+col+"}\n" + StaticLib.getExceptionAsString(this));
	}
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
