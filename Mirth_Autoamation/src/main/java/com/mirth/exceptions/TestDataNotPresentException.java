package com.mirth.exceptions;

import java.io.IOException;

import com.mirth.controller.MirthLogger;
import com.mirth.utility.general.StaticLib;

public class TestDataNotPresentException extends Exception {

	public String testKey;
	
	public TestDataNotPresentException(String key) throws IOException {

		testKey = key;
		System.out.println(key);
		MirthLogger.log("Exception while using the value: "+key + "\n" + StaticLib.getExceptionAsString(this));
		//Log
	}
	
	private static final long serialVersionUID = 1L;

}
