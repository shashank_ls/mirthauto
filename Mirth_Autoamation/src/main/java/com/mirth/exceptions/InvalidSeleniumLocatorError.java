package com.mirth.exceptions;

import java.io.IOException;

import com.mirth.controller.MirthLogger;
import com.mirth.utility.general.StaticLib;

public class InvalidSeleniumLocatorError extends Exception {

	public String locatorName;
	
	public InvalidSeleniumLocatorError(String locator) throws IOException {
		locatorName = locator;
		System.out.println(locatorName);
		MirthLogger.log("Exception while using the value: "+locator + "\n" + StaticLib.getExceptionAsString(this));
		//Log
	}
	
	private static final long serialVersionUID = 1L;

}
