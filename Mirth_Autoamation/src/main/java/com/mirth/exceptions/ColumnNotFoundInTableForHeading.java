package com.mirth.exceptions;

import java.io.IOException;

import com.mirth.controller.MirthLogger;
import com.mirth.utility.general.StaticLib;


public class ColumnNotFoundInTableForHeading extends Exception {

	String element;
	private static final long serialVersionUID = 1L;
	
	public ColumnNotFoundInTableForHeading(String supposedHeading) throws IOException {
		this.element = supposedHeading;
		MirthLogger.log("Exception while using the value: "+supposedHeading + "\n" + StaticLib.getExceptionAsString(this));
	}
	
	public String toString() {
		if (element == null) {
			return null;
		}
		
		return element.toString();
	}

}
