package com.mirth.exceptions;

import java.io.IOException;

import com.mirth.controller.MirthLogger;
import com.mirth.utility.general.StaticLib;

public class ProductSheetNotPresentException extends Exception {

	public String sheetName;
	
	public ProductSheetNotPresentException(String sheetName) throws IOException {
		this.sheetName = sheetName;
		System.out.println(sheetName);
		MirthLogger.log("Exception while using the value: "+sheetName + "\n" + StaticLib.getExceptionAsString(this));
		//Log
	}
	
	private static final long serialVersionUID = 1L;

}
