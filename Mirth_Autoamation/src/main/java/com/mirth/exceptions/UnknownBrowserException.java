package com.mirth.exceptions;

import java.io.IOException;

import com.mirth.controller.MirthLogger;
import com.mirth.utility.general.StaticLib;

public class UnknownBrowserException extends Exception {

	public String browserName;
	
	public UnknownBrowserException(String browser) throws IOException {

		browserName = browser;
		System.out.println(browserName);
		MirthLogger.log("Exception while trying to use the browser: "+browser + "\n" + StaticLib.getExceptionAsString(this));
		//Log
	}
	
	private static final long serialVersionUID = 1L;

}
