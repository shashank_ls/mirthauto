package com.mirth.exceptions;

import java.io.IOException;

import com.mirth.controller.MirthLogger;
import com.mirth.utility.general.StaticLib;

public class AppNotPresentException extends Exception {
	
	String canonicalName;
	
	public AppNotPresentException(String appName) throws IOException{
		super(appName);
		canonicalName = appName;
		MirthLogger.log("Exception while using the value: "+appName + "\n" + StaticLib.getExceptionAsString(this));
	}
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
