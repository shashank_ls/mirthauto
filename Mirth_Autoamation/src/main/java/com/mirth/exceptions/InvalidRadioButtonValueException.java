package com.mirth.exceptions;

import java.io.IOException;

import com.mirth.controller.MirthLogger;
import com.mirth.utility.general.StaticLib;

public class InvalidRadioButtonValueException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	String value = null;
	
	public InvalidRadioButtonValueException(String value) throws IOException {
		this.value = value;
		MirthLogger.log("Exception while using the value: "+value + "\n" + StaticLib.getExceptionAsString(this));
	}
	
}
