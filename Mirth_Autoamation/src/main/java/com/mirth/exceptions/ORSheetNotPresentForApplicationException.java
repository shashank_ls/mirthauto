package com.mirth.exceptions;

import java.io.IOException;

import com.mirth.controller.MirthLogger;
import com.mirth.utility.general.StaticLib;

public class ORSheetNotPresentForApplicationException extends Exception {

	public String appName;
	
	public ORSheetNotPresentForApplicationException(String applicationName) throws IOException {
		appName = applicationName;
		System.out.println(appName);
		MirthLogger.log("Exception while using the value: "+appName + "\n" + StaticLib.getExceptionAsString(this));
		//Log
	}
	
	private static final long serialVersionUID = 1L;

}
