package com.mirth.exceptions;

import java.io.IOException;

import org.openqa.selenium.WebElement;

import com.mirth.controller.MirthLogger;
import com.mirth.utility.general.StaticLib;

public class NotATableElementException extends Exception {

	WebElement element;
	private static final long serialVersionUID = 1L;
	
	public NotATableElementException(WebElement supposedTableElement) throws IOException {
		this.element = supposedTableElement;
		MirthLogger.log("Treating an element as a table but it is NOT.\n" + StaticLib.getExceptionAsString(this));
	}
	
	public String toString() {
		if (element == null) {
			return null;
		}
		
		return element.toString();
	}

}
